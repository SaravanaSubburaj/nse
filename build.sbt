name        := "cadup"
description := "Cadence User Profiler"
version     := "1.0"

lazy val cadup = (project in file("."))
  .dependsOn(
    utils       % Common.compileToCompileAndTestToTest,
    bifrost     % Common.compileToCompileAndTestToTest,
    transformer % Common.compileToCompileAndTestToTest,
    fury        % Common.compileToCompileAndTestToTest
  )
  .settings(Common.settings: _*)
  .settings(Common.packSettings: _*)
  .settings(Common.assemblySettings: _*)
  .aggregate(fury, bifrost, utils, transformer, harness)

lazy val fury = (project in file("fury"))
  .dependsOn(
    harness % Common.testToTest,
    utils   % Common.compileToCompileAndTestToTest
  )
  .settings(Common.settings: _*)

lazy val bifrost = (project in file("bifrost"))
  .dependsOn(
    harness % Common.testToTest,
    utils   % Common.compileToCompileAndTestToTest,
    fury    % Common.compileToCompileAndTestToTest
  )
  .settings(Common.settings: _*)

lazy val transformer = (project in file("transformer"))
  .dependsOn(
    harness % Common.testToTest,
    utils   % Common.compileToCompileAndTestToTest,
    fury    % Common.compileToCompileAndTestToTest
  )
  .settings(Common.settings: _*)

lazy val utils = (project in file("utils"))
  .settings(Common.settings: _*)
  .dependsOn(harness % Common.testToTest)
  .settings(libraryDependencies ++= Dependencies.jsonDependencies)

lazy val harness = (project in file("harness"))
  .settings(Common.settings: _*)

enablePlugins(AssemblyPlugin)
enablePlugins(PackPlugin)
