package in.datateam.utils.configparser

import org.scalatest.FlatSpec

import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.PartitionHelper

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
class PartitionHelperTest extends FlatSpec {
  "PartitionHelperTest" should "return last 10 days" in {
    val partitionDetails: PartitionDetails = PartitionDetails(
      Array("date"),
      CommonEnums.DAILY,
      Array("yyyy-MM-dd"),
      10
    )
    val actualResult =
      PartitionHelper.getAllPartitions(partitionDetails, "2019-01-07")
    val expectedResult = Array(
      "date LIKE '2018-12-29%'",
      "date LIKE '2018-12-30%'",
      "date LIKE '2018-12-31%'",
      "date LIKE '2019-01-01%'",
      "date LIKE '2019-01-02%'",
      "date LIKE '2019-01-03%'",
      "date LIKE '2019-01-04%'",
      "date LIKE '2019-01-05%'",
      "date LIKE '2019-01-06%'",
      "date LIKE '2019-01-07%'"
    )
    assertResult(expectedResult)(actualResult)
  }

  "PartitionHelperTest" should "return last 10 days multi-level partitions" in {
    val partitionDetails: PartitionDetails = PartitionDetails(
      Array("year", "month", "day"),
      CommonEnums.DAILY,
      Array("yyyy", "MM", "dd"),
      10
    )
    val actualResult =
      PartitionHelper.getAllPartitions(partitionDetails, "2019^01^07")
    val expectedResult = Array(
      "year='2018' AND month='12' AND day='29'",
      "year='2018' AND month='12' AND day='30'",
      "year='2018' AND month='12' AND day='31'",
      "year='2019' AND month='01' AND day='01'",
      "year='2019' AND month='01' AND day='02'",
      "year='2019' AND month='01' AND day='03'",
      "year='2019' AND month='01' AND day='04'",
      "year='2019' AND month='01' AND day='05'",
      "year='2019' AND month='01' AND day='06'",
      "year='2019' AND month='01' AND day='07'"
    )
    assertResult(expectedResult)(actualResult)
  }

  "PartitionHelperTest" should "return last 10  hourly partitions" in {
    val partitionDetails: PartitionDetails = PartitionDetails(
      Array("date"),
      CommonEnums.HOURLY,
      Array("yyyy-MM-dd-HH"),
      10
    )
    val actualResult =
      PartitionHelper.getAllPartitions(partitionDetails, "2019-01-07-23")
    val expectedResult = Array(
      "date LIKE '2019-01-07-14%'",
      "date LIKE '2019-01-07-15%'",
      "date LIKE '2019-01-07-16%'",
      "date LIKE '2019-01-07-17%'",
      "date LIKE '2019-01-07-18%'",
      "date LIKE '2019-01-07-19%'",
      "date LIKE '2019-01-07-20%'",
      "date LIKE '2019-01-07-21%'",
      "date LIKE '2019-01-07-22%'",
      "date LIKE '2019-01-07-23%'"
    )
    assertResult(expectedResult)(actualResult)
  }

  "PartitionHelperTest" should "return last 24 hourly partitions for daily profile" in {
    val partitionDetails: PartitionDetails = PartitionDetails(
      Array("date"),
      CommonEnums.HOURLY,
      Array("yyyy-MM-dd-HH"),
      24
    )
    val actualResult =
      PartitionHelper.getAllPartitions(partitionDetails, "2019-01-07-23")
    val expectedResult = Array(
      "date LIKE '2019-01-07-00%'",
      "date LIKE '2019-01-07-01%'",
      "date LIKE '2019-01-07-02%'",
      "date LIKE '2019-01-07-03%'",
      "date LIKE '2019-01-07-04%'",
      "date LIKE '2019-01-07-05%'",
      "date LIKE '2019-01-07-06%'",
      "date LIKE '2019-01-07-07%'",
      "date LIKE '2019-01-07-08%'",
      "date LIKE '2019-01-07-09%'",
      "date LIKE '2019-01-07-10%'",
      "date LIKE '2019-01-07-11%'",
      "date LIKE '2019-01-07-12%'",
      "date LIKE '2019-01-07-13%'",
      "date LIKE '2019-01-07-14%'",
      "date LIKE '2019-01-07-15%'",
      "date LIKE '2019-01-07-16%'",
      "date LIKE '2019-01-07-17%'",
      "date LIKE '2019-01-07-18%'",
      "date LIKE '2019-01-07-19%'",
      "date LIKE '2019-01-07-20%'",
      "date LIKE '2019-01-07-21%'",
      "date LIKE '2019-01-07-22%'",
      "date LIKE '2019-01-07-23%'"
    )
    assertResult(expectedResult)(actualResult)
  }
}
