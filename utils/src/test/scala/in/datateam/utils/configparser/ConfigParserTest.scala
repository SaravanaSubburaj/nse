package in.datateam.utils.configparser

import org.apache.spark.sql.SparkSession

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
class ConfigParserTest extends FlatSpec with SparkTest {

  "ConfigParserTest" should "parse and return ConfigParser case class for entity config" in withSparkSession {
    _: SparkSession =>
      val entityConfig: EntityConfig = ConfigParser.getEntityConfig(
        "utils/src/test/resources/entity_config.json"
      )
      entityConfig.toStandardConfig
      val expectedEntityName = "xyz_entity"
      assertResult(expectedEntityName)(entityConfig.entityDetails.entityName)
  }

  "ConfigParserTest" should "parse and return ConfigParser case class for profile config" in withSparkSession {
    _: SparkSession =>
      val profileConfig: ProfileConfig = ConfigParser.getProfileConfig(
        "utils/src/test/resources/profile_config.json"
      )
      val expectedEntityName = "xyz_entity"
      assertResult(expectedEntityName)(
        profileConfig.entityDetails(0).entityName
      )
  }

}
