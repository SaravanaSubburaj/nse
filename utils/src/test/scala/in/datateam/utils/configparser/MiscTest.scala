package in.datateam.utils.configparser

import org.scalatest.FlatSpec

import in.datateam.utils.helper.Misc

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
class MiscTest extends FlatSpec {
  "MiscTest" should "transform date" in {
    val sampleDateList = Array(
      ("2019-01-11", "yyyy-MM-dd"),
      ("20190111", "yyyyMMdd"),
      ("2019/01/11", "yyyy/MM/dd"),
      ("11-01-2019", "dd-MM-yyyy"),
      ("11012019", "ddMMyyyy"),
      ("11/01/2019", "dd/MM/yyyy"),
      ("01-11-2019", "MM-dd-yyyy"),
      ("01112019", "MMddyyyy"),
      ("01/11/2019", "MM/dd/yyyy")
    )
    val actualResult = sampleDateList.map(
      d => Misc.getFormattedPartitionDate(d._2, "daily", d._1)
    )
    val expectedResult = Array(
      "2019-01-11",
      "2019-01-11",
      "2019-01-11",
      "2019-01-11",
      "2019-01-11",
      "2019-01-11",
      "2019-01-11",
      "2019-01-11",
      "2019-01-11"
    )
    assertResult(expectedResult)(actualResult)
  }

  "MiscTest" should "getDate" in {
    val sampleDateList = Array(
      ("2019-01-11", -1),
      ("2019-01-11", -2),
      ("2019-01-11", -3),
      ("2019-01-11", -4),
      ("2019-01-11", -11)
    )
    val actualResult = sampleDateList.map(d => Misc.getNthDate(d._1, d._2))
    val expectedResult = Array(
      "2019-01-10",
      "2019-01-09",
      "2019-01-08",
      "2019-01-07",
      "2018-12-31"
    )
    assertResult(expectedResult)(actualResult)
  }
}
