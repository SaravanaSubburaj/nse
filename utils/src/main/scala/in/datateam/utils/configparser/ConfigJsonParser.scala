package in.datateam.utils.configparser

import spray.json.DefaultJsonProtocol

import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
case class PartitionDetails(
    partitionColumn: Array[String],
    partitionType: String,
    partitionFormat: Array[String],
    numberOfPartitions: Int)

case class ExclusionJoinColumn(factJoinColumn: String, dimensionJoinColumn: String)
case class ExclusionDetails(
    exclusionFlag: String,
    joinDetails: Array[ExclusionJoinColumn],
    exclusionStorageDetails: StorageDetailOption)

case class ExtraOption(key: String, value: String)

case class SourceDetails(
    entityName: String,
    entityType: String,
    extractionType: Option[String],
    sourceType: String,
    dataFormat: Option[String],
    dataFormatOption: Option[Array[ExtraOption]],
    fileSystem: Option[String],
    pathUrl: Option[String],
    databaseName: Option[String],
    tableName: Option[String],
    partitionDetails: Option[PartitionDetails],
    joinColumn: Option[String],
    primaryColumns: Option[Array[String]],
    isFact: Option[String],
    exclusionDetails: Option[ExclusionDetails],
    loadLatest: Option[String]) {

  def getDataFormatOptions: Map[String, String] = {
    dataFormatOption match {
      case Some(dfOptions) =>
        dfOptions.map(op => (op.key, op.value)).toMap
      case None => Map[String, String]()
    }
  }

  def toEntitySourceDetails: SourceDetails = {
    val factFlag = if (isFact.isDefined) isFact else Some("true")
    val loadLatestFlag = Some("false")
    SourceDetails(
      entityName,
      entityType,
      extractionType,
      sourceType,
      dataFormat,
      dataFormatOption,
      fileSystem,
      pathUrl,
      databaseName,
      tableName,
      partitionDetails,
      joinColumn,
      primaryColumns,
      factFlag,
      exclusionDetails,
      loadLatestFlag
    )
  }

  def toProfileSourceDetails: SourceDetails = {
    val factFlag = if (isFact.isDefined) isFact else Some("false")
    val loadLatestFlag = if (loadLatest.isDefined) loadLatest else Some("false")
    SourceDetails(
      entityName,
      entityType,
      extractionType,
      sourceType,
      dataFormat,
      dataFormatOption,
      fileSystem,
      pathUrl,
      databaseName,
      tableName,
      partitionDetails,
      joinColumn,
      primaryColumns,
      factFlag,
      exclusionDetails,
      loadLatestFlag
    )
  }
}

case class EntityColumnMappingUnit(sourceColumn: String, targetColumn: String) {

  def toColumnMappingUnit(entityAlias: String): ColumnMappingUnit = {
    val columnMappingUnit =
      ColumnMappingUnit(entityAlias, sourceColumn, targetColumn)
    columnMappingUnit
  }
}

case class ColumnMappingUnit(sourceEntity: String, sourceColumn: String, targetColumn: String)

case class ValidationUnit(columnName: String, validationFunction: String, validationExpression: Option[String])

case class TransformationUnit(
    sourceColumns: Array[String],
    targetColumn: String,
    transformationFunction: String,
    transformationOption: Option[Array[ExtraOption]],
    transformationExpression: Option[String]) {

  def toAttributeTransformationUnit(entityAlias: String): AttributeTransformationUnit = {
    //FIXME GTP-223 :- Remove/refactor aggregationDetails block.
    val attributeTransformationArray =
      AttributeTransformationUnit(
        targetColumn,
        sourceColumns.map(columnName => SourceColumn(entityAlias, columnName)),
        None,
        transformationFunction,
        transformationOption,
        None,
        None
      )
    attributeTransformationArray
  }
}

case class StorageDetailOption(
    recordType: String,
    dataFormat: Option[String],
    fileSystem: Option[String],
    pathUrl: Option[String],
    persistFlag: Option[String],
    dataFormatOption: Option[Array[ExtraOption]],
    storageHandler: String) {

  def toEntityStorageDetailFormat: StorageDetail = {
    val persistFlagValue = persistFlag match {
      case Some(x) => x
      case None => "true"
    }
    StorageDetail(
      recordType,
      dataFormat,
      fileSystem,
      pathUrl,
      persistFlagValue,
      dataFormatOption,
      storageHandler
    )
  }

  def toProfileStorageDetailFormat: StorageDetail = {
    val persistFlagValue = persistFlag match {
      case Some(x) => x
      case None =>
        if (recordType == CommonEnums.INVALID_RECORDS) "false" else "true"
    }
    StorageDetail(
      recordType,
      dataFormat,
      fileSystem,
      pathUrl,
      persistFlagValue,
      dataFormatOption,
      storageHandler
    )
  }
}

case class StorageDetail(
    recordType: String,
    dataFormat: Option[String],
    fileSystem: Option[String],
    pathUrl: Option[String],
    persistFlag: String,
    dataFormatOption: Option[Array[ExtraOption]],
    storageHandler: String) {

  def getDataFormatOptions: Map[String, String] = {
    dataFormatOption match {
      case Some(dfOptions) =>
        dfOptions.map(op => (op.key, op.value)).toMap
      case None => Map[String, String]()
    }
  }
}

case class SourceSchemaUnit(columnName: String, dataType: String)

case class TargetSchemaUnit(
    columnName: String,
    dataType: String,
    carryForwardFlag: String,
    carryForwardJoinFlag: String)

case class CommonTargetSchemaUnit(
    columnName: String,
    dataType: String,
    carryForwardFlag: Option[String],
    carryForwardJoinFlag: Option[String]) {

  def toTargetSchemaUnit: TargetSchemaUnit = {
    val cFlag = carryForwardFlag match {
      case Some(x) => x
      case None => "false"
    }
    val cJoinFlag = carryForwardJoinFlag match {
      case Some(x) => x
      case None => "false"
    }
    TargetSchemaUnit(columnName, dataType, cFlag, cJoinFlag)
  }
}

case class LookupDetail(lookupName: String, dataFormat: String, fileSystem: String, pathUrl: String)

//FIXME GTP-223 :- Remove/refactor aggregationDetails block.
case class EntityConfig(
    entityDetails: SourceDetails,
    sourceSchema: Option[Array[SourceSchemaUnit]],
    targetSchema: Array[CommonTargetSchemaUnit],
    columnMapping: Array[EntityColumnMappingUnit],
    validationsDetails: Array[ValidationUnit],
    transformationsDetails: Array[TransformationUnit],
    aggregationDetails: Option[Array[AttributeTransformationUnit]],
    partitionColumns: Option[Array[String]],
    storageDetails: Array[StorageDetailOption],
    lookupDetails: Option[Array[LookupDetail]]) {

  def toStandardConfig(): Config = {
    val config: Config = Config(
      CommonEnums.ENTITY,
      Array(entityDetails.toEntitySourceDetails),
      sourceSchema,
      targetSchema.map { targetSchemaUnit =>
        targetSchemaUnit.toTargetSchemaUnit
      },
      columnMapping.map { columnMappingUnit =>
        columnMappingUnit.toColumnMappingUnit(entityDetails.entityName)
      },
      Some(validationsDetails),
      transformationsDetails.map { transformation =>
        transformation.toAttributeTransformationUnit(entityDetails.entityName)
      } ++ aggregationDetails.toList.flatten,
      partitionColumns,
      storageDetails.map { storageDetail =>
        storageDetail.toEntityStorageDetailFormat
      },
      lookupDetails
    )
    config
  }
}

case class SourceColumn(entityName: String, columnName: String)

case class JoinColumn(entityJoinColumn: String, profileJoinColumn: String)

case class AttributeTransformationUnit(
    attributeName: String,
    sourceColumns: Array[SourceColumn],
    joinColumns: Option[Array[JoinColumn]],
    transformationFunction: String,
    transformationOption: Option[Array[ExtraOption]],
    groupByColumns: Option[Array[SourceColumn]],
    attributeColumns: Option[Array[String]]) {

  def getTransformationOptionsMap(): Map[String, String] = {
    transformationOption match {
      case Some(tOption) => tOption.map(op => (op.key, op.value)).toMap[String, String]
      case None => Map[String, String]()
    }
  }
}

case class ProfileConfig(
    entityDetails: Array[SourceDetails],
    profileSchema: Array[CommonTargetSchemaUnit],
    columnMapping: Array[ColumnMappingUnit],
    attributeMapping: Array[AttributeTransformationUnit],
    partitionColumns: Option[Array[String]],
    storageDetails: Array[StorageDetailOption]) {

  def toStandardConfig: Config = {
    val config: Config = Config(
      CommonEnums.PROFILE,
      entityDetails.map { sourceDetail =>
        sourceDetail.toProfileSourceDetails
      },
      None,
      profileSchema.map { targetSchemaUnit =>
        targetSchemaUnit.toTargetSchemaUnit
      },
      columnMapping,
      None,
      attributeMapping,
      partitionColumns,
      storageDetails.map { storageDetail =>
        storageDetail.toProfileStorageDetailFormat
      },
      None
    )
    config
  }
}

object UtilsJsonProtocol extends DefaultJsonProtocol {
  implicit val partitionDetailsFormat = jsonFormat4(PartitionDetails)
  implicit val dataFormatOptionFormat = jsonFormat2(ExtraOption)
  implicit val exclusionJoinColumnFormat = jsonFormat2(ExclusionJoinColumn)
  implicit val targetDetailOptionFormat = jsonFormat7(StorageDetailOption)
  implicit val exclusionDetailsFormat = jsonFormat3(ExclusionDetails)
  implicit val sourceDetailsFormat = jsonFormat16(SourceDetails)
  implicit val entityColumnMappingUnitFormat = jsonFormat2(EntityColumnMappingUnit)
  implicit val columnMappingUnitFormat = jsonFormat3(ColumnMappingUnit)
  implicit val validationUnitFormat = jsonFormat3(ValidationUnit)
  implicit val transformationsFormat = jsonFormat5(TransformationUnit)
  implicit val targetDetailFormat = jsonFormat7(StorageDetail)
  implicit val sourceSchemaFormat = jsonFormat2(SourceSchemaUnit)
  implicit val targetSchemaFormat = jsonFormat4(TargetSchemaUnit)
  implicit val commonTargetSchemaFormat = jsonFormat4(CommonTargetSchemaUnit)
  implicit val broadcastsDetailFormat = jsonFormat4(LookupDetail)

  implicit val joinColumnFormat = jsonFormat2(JoinColumn)
  implicit val sourceColumnFormat = jsonFormat2(SourceColumn)
  implicit val attributeMappingFormat = jsonFormat7(AttributeTransformationUnit)

  //FIXME GTP-223 :- Remove/refactor aggregationDetails block.
  implicit val entityConfigFormat = jsonFormat10(EntityConfig)
  implicit val profileConfigFormat = jsonFormat6(ProfileConfig)

}
