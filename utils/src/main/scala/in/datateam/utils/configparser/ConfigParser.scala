package in.datateam.utils.configparser

import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ConfigParser {

  import spray.json._

  import in.datateam.utils.configparser.UtilsJsonProtocol._

  def getEntityConfig(jsonConfigPath: String): EntityConfig = {
    val configJson: String = FileSystemUtils.readFile(jsonConfigPath)
    configJson.parseJson.convertTo[EntityConfig]
  }

  def getProfileConfig(jsonConfigPath: String): ProfileConfig = {
    val configJson: String = FileSystemUtils.readFile(jsonConfigPath)
    configJson.parseJson.convertTo[ProfileConfig]
  }

  def getConfig(eventType: String, jsonConfigPath: String): Config = {
    val config: Config = eventType match {
      case CommonEnums.ENTITY => populateConfigFromEntityConfig(jsonConfigPath)
      case CommonEnums.PROFILE =>
        populateConfigFromProfileConfig(jsonConfigPath)
    }
    config.validate()
    config
  }

  def populateConfigFromEntityConfig(jsonConfigPath: String): Config = {
    val entityConfig: EntityConfig =
      ConfigParser.getEntityConfig(jsonConfigPath)
    entityConfig.toStandardConfig
  }

  def populateConfigFromProfileConfig(jsonConfigPath: String): Config = {
    val profileConfig: ProfileConfig =
      ConfigParser.getProfileConfig(jsonConfigPath)
    profileConfig.toStandardConfig
  }

}
