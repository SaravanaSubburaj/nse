package in.datateam.utils.configparser

import in.datateam.utils.InvalidConfigException
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
case class Config(
    configType: String,
    sourceDetails: Array[SourceDetails],
    sourceSchema: Option[Array[SourceSchemaUnit]],
    targetSchema: Array[TargetSchemaUnit],
    columnMapping: Array[ColumnMappingUnit],
    validationsDetails: Option[Array[ValidationUnit]],
    transformationsDetails: Array[AttributeTransformationUnit],
    partitionColumns: Option[Array[String]],
    storageDetails: Array[StorageDetail],
    lookupDetails: Option[Array[LookupDetail]]) {

  def getAllColumns: List[String] = {
    val allColumns = targetSchema
        .map(targetCol => targetCol.columnName) ++ List(
        CommonEnums.DATE_PARTITION_COLUMN
      )
    allColumns.toList
  }

  def getFactEntityName(): String = {
    sourceDetails.filter(sourceDetail => sourceDetail.isFact.get == "true").head.entityName
  }

  def getDimensionEntityDetails(): Array[SourceDetails] = {
    sourceDetails
      .filter(sourceDetail => sourceDetail.isFact.get == "false")
  }

  def isEntityFact(entityName: String): Boolean = {
    sourceDetails.filter(sourceDetail => sourceDetail.entityName == entityName).head.isFact.get == "true"
  }

  def getStorageDetails(recordType: String): Array[StorageDetail] = {
    recordType match {
      case CommonEnums.VALID_RECORDS | CommonEnums.INVALID_RECORDS | CommonEnums.AUDIT_RECORDS |
          CommonEnums.LINEAGE_RECORDS | CommonEnums.DELTA_RECORDS | CommonEnums.LAST_MODIFIED_RECORDS =>
        Unit
      case x: Any =>
        throw InvalidConfigException(
          s"Expected one of ${CommonEnums.VALID_RECORDS}, " +
          s"${CommonEnums.INVALID_RECORDS}, ${CommonEnums.AUDIT_RECORDS}, ${CommonEnums.LINEAGE_RECORDS}, " +
          s"${CommonEnums.LINEAGE_RECORDS}, ${CommonEnums.LAST_MODIFIED_RECORDS} but got ${x}."
        )
    }

    val storageDetailList = storageDetails.filter { t: StorageDetail =>
      t.recordType == recordType
    }
    storageDetailList
  }

  //FIXME: Find better way to pass partition column
  def getExistingPartitionColumn(): String = {
    if (configType == CommonEnums.ENTITY && sourceDetails.head.partitionDetails.isDefined) {
      s"${sourceDetails.head.entityName}${CommonEnums.DATA_DELIMITER}${sourceDetails.head.partitionDetails.get.partitionColumn
        .mkString(CommonEnums.PARTITION_DELIMITER.toString)}"
    } else {
      ""
    }
  }

  private[Config] def validateFactDetails(): Unit = {
    val factEntityCount = sourceDetails.filter(sourceDetail => sourceDetail.isFact.get == "true").length
    if (factEntityCount != 1) {
      throw InvalidConfigException(s"Expected only one fact entity but got ${factEntityCount}.")
    }
  }

  private[Config] def checkEntityExist(entityName: String): Boolean = {
    entityName match {
      case CommonEnums.PROFILE_MASTER => true
      case _ =>
        sourceDetails.exists(
          sourceDetail => sourceDetail.entityName == entityName
        )
    }
  }

  def checkColumnExistTargetSchema(columnName: String): Boolean = {
    targetSchema.exists(columnDetail => columnDetail.columnName == columnName)
  }

  private[Config] def checkColumnExistColumnMapping(columnName: String): Boolean = {
    columnMapping.exists(
      columnMappingUnit => columnMappingUnit.targetColumn == columnName
    )
  }

  def checkColumnExistAsAttributeArgument(columnName: String): Boolean = {
    transformationsDetails.exists { transformationsDetail =>
      transformationsDetail.sourceColumns.exists { sourceColumn =>
        sourceColumn.columnName == columnName && sourceColumn.entityName == CommonEnums.PROFILE_MASTER
      }
    }
  }

  private[Config] def validateConfigType(): Unit = {
    configType match {
      case CommonEnums.ENTITY | CommonEnums.PROFILE => Unit
      case x: Any =>
        throw InvalidConfigException(
          s"Expected one of ${CommonEnums.ENTITY} or ${CommonEnums.PROFILE} but got ${x}."
        )
    }
  }

  private[Config] def validatePartitionDetails(partitionDetails: PartitionDetails): Unit = {
    if (partitionDetails.partitionColumn.length == 0) {
      throw InvalidConfigException(
        s"Partition column is expected for Profile calculation."
      )
    }
    partitionDetails.partitionType match {
      case CommonEnums.HOURLY | CommonEnums.DAILY | CommonEnums.MONTHLY => Unit
      case x: Any =>
        throw InvalidConfigException(
          s"Expected one of ${CommonEnums.DAILY}, ${CommonEnums.MONTHLY} but got ${x}."
        )
    }
    if (partitionDetails.partitionFormat.length == 0) {
      throw InvalidConfigException(
        s"Partition column is expected for Profile calculation."
      )
    }
    if (partitionDetails.numberOfPartitions <= 0) {
      throw InvalidConfigException(
        s"Number of partition should not be negative."
      )
    }
  }

  private[Config] def validateSourceDetail(entityType: String, sourceDetails: SourceDetails): Unit = {
    if (sourceDetails.entityName == "") {
      throw InvalidConfigException(s"Expected source entity name.")
    }
    sourceDetails.entityType match {
      case CommonEnums.SNAPSHOT | CommonEnums.MUTABLE | CommonEnums.EVENT =>
        Unit
      case x: Any =>
        throw InvalidConfigException(
          s"Expected one of ${CommonEnums.SNAPSHOT} but got ${x}."
        )
    }
    if (sourceDetails.sourceType == CommonEnums.FILESYSTEM) {
      sourceDetails.dataFormat.getOrElse("") match {
        case CommonEnums.CSV | CommonEnums.PARQUET => Unit
        case x: Any =>
          throw InvalidConfigException(
            s"Expected one of ${CommonEnums.CSV} or ${CommonEnums.PARQUET} but got ${x}."
          )
      }
      sourceDetails.fileSystem.getOrElse("") match {
        case CommonEnums.HDFS | CommonEnums.S3 | "" => Unit
        case x: Any =>
          throw InvalidConfigException(
            s"Expected one of ${CommonEnums.HDFS} or ${CommonEnums.S3} but got ${x}."
          )
      }
      if (sourceDetails.pathUrl.getOrElse("") == "") {
        throw InvalidConfigException(s"Expected path url for source detail.")
      }
    } else if (sourceDetails.sourceType == CommonEnums.DATABASE) {
      if (sourceDetails.databaseName.getOrElse("") == "") {
        throw InvalidConfigException(s"Expected databaseName for source detail.")
      }
      if (sourceDetails.tableName.getOrElse("") == "") {
        throw InvalidConfigException(s"Expected tableName for source detail.")
      }
    }
    if (sourceDetails.extractionType.isDefined) {
      sourceDetails.extractionType.get match {
        case CommonEnums.BATCH => Unit
        case x: Any =>
          throw InvalidConfigException(
            s"Expected one of ${CommonEnums.BATCH} but got ${x}."
          )
      }
    }
    if (sourceDetails.partitionDetails.isDefined) {
      validatePartitionDetails(sourceDetails.partitionDetails.get)
    }
    entityType match {
      case CommonEnums.ENTITY =>
        if (sourceDetails.primaryColumns.isEmpty) {
          throw InvalidConfigException(
            s"Primary column is expected for Profile calculation."
          )
        } else if (sourceDetails.primaryColumns.get.length == 0) {
          throw InvalidConfigException(
            s"Primary column is expected for Profile calculation."
          )
        }
      case CommonEnums.PROFILE =>
        if (sourceDetails.primaryColumns.isDefined) {
          throw InvalidConfigException(
            s"Partition column is not expected for Profile calculation."
          )
        }
    }
    if (sourceDetails.isFact == "true") {
      if (sourceDetails.exclusionDetails.isDefined) {
        throw InvalidConfigException(s"Exclusion details is not expected for fact type entity.")
      }
    } else {
      validateExclusionDetails(sourceDetails.exclusionDetails)
    }
    sourceDetails.loadLatest match {
      case Some(loadLatest) =>
        if (loadLatest != "true" && loadLatest != "false") {
          throw InvalidConfigException(s"loadLatest is expected to be true/false but got ${loadLatest}.")
        }
      case None => throw InvalidConfigException(s"loadLatest is expected to be defined.")
    }
  }

  private[Config] def validateExclusionDetails(exclusionDetailsOption: Option[ExclusionDetails]): Unit = {
    exclusionDetailsOption match {
      case Some(exclusionDetails) =>
        if (exclusionDetails.exclusionFlag != "true" && exclusionDetails.exclusionFlag != "false") {
          throw InvalidConfigException(
            s"exclusionFlag is expected to be true/false but got ${exclusionDetails.exclusionFlag}"
          )
        }
        exclusionDetails.joinDetails.foreach { joinColumn: ExclusionJoinColumn =>
          if (joinColumn.factJoinColumn == "") {
            throw InvalidConfigException(s"Fact join column is expected.")
          }
          if (joinColumn.dimensionJoinColumn == "") {
            throw InvalidConfigException(s"Dimension join column is expected.")
          }
        }
        if (exclusionDetails.exclusionStorageDetails.recordType != CommonEnums.EXCLUSION_RECORDS) {
          throw InvalidConfigException(
            s"Expected ${CommonEnums.EXCLUSION_RECORDS} but got ${exclusionDetails.exclusionStorageDetails.recordType}."
          )
        }
        if (exclusionDetails.exclusionStorageDetails.storageHandler == "") {
          throw InvalidConfigException(s"Storage Handler name is expected.")
        }

      case None => Unit
    }

  }

  private[Config] def validateSourceSchema(schema: Array[SourceSchemaUnit]): Unit = {
    schema.foreach { schemaUnit =>
      if (schemaUnit.columnName == "") {
        throw InvalidConfigException(
          s"Column Name is expected in target schema."
        )
      }
      //      FIXME: Add all data type check.
      if (schemaUnit.dataType == "") {
        throw InvalidConfigException(
          s"Column type is expected in target schema."
        )
      }
    }
  }

  private[Config] def validateTargetSchema(schema: Array[TargetSchemaUnit]): Unit = {
    schema.foreach { schemaUnit: TargetSchemaUnit =>
      if (schemaUnit.columnName == "") {
        throw InvalidConfigException(
          s"Column Name is expected in target schema."
        )
      }
      //      FIXME: Add all data type check.
      if (schemaUnit.dataType == "") {
        throw InvalidConfigException(
          s"Column type is expected in target schema."
        )
      }
      schemaUnit.carryForwardFlag match {
        case "true" | "false" => Unit
        case _ =>
          throw InvalidConfigException(
            s"Carry forward flag is expected in target schema."
          )
      }
      schemaUnit.carryForwardJoinFlag match {
        case "true" | "false" => Unit
        case _ =>
          throw InvalidConfigException(
            s"Carry forward join flag is expected in target schema."
          )
      }
    }

    if (configType == CommonEnums.PROFILE) {
      val targetColumnNames = targetSchema.map(x => x.columnName)
      val targetColumnNamesCount = targetColumnNames.groupBy(identity).mapValues(_.size)
      val duplicateColumns = targetColumnNamesCount.filter(x => x._2 > 1)
      if (duplicateColumns.size > 0) {
        throw InvalidConfigException(s"duplicate columns in profile schema : ${duplicateColumns.mkString("\n")}")
      }
    }

  }

  private[Config] def validateColumnMapping(columnMapping: Array[ColumnMappingUnit]): Unit = {
    columnMapping.foreach { columnMappingUnit: ColumnMappingUnit =>
      if (columnMappingUnit.sourceColumn == "") {
        throw InvalidConfigException(
          s"Source Column Name is expected in Column Mapping."
        )
      }
      if (columnMappingUnit.sourceEntity == "") {
        throw InvalidConfigException(
          s"Source Entity Name is expected in Column Mapping."
        )
      }
      if (columnMappingUnit.targetColumn == "") {
        throw InvalidConfigException(
          s"Target Column Name is expected in Column Mapping."
        )
      } else {
        if (!checkEntityExist(columnMappingUnit.sourceEntity)) {
          throw InvalidConfigException(
            s"Entity: ${columnMappingUnit.sourceEntity} does not exist in entity details."
          )
        }
      }
      if (configType == CommonEnums.PROFILE) {
        val factEntityName = getFactEntityName()
        if (columnMappingUnit.sourceEntity != factEntityName) {
          throw InvalidConfigException(
            s"Source Entity Name is expected to be ${factEntityName} Column Mapping but got ${columnMappingUnit.sourceEntity}."
          )
        }
      }
    }
  }

  private[Config] def validateValidationFunctions(validationFunction: Array[ValidationUnit]): Unit = {
    validationFunction.foreach { validationUnit: ValidationUnit =>
      if (validationUnit.columnName == "") {
        throw InvalidConfigException(
          s"Source Column Name is expected in validation function."
        )
      } else {
        if (checkColumnExistTargetSchema(validationUnit.columnName) ||
            checkColumnExistColumnMapping(validationUnit.columnName)) {
          Unit
        } else {
          throw InvalidConfigException(
            s"Column: ${validationUnit.columnName} does not exist in target schema."
          )
        }
      }
      if (validationUnit.validationFunction == "") {
        throw InvalidConfigException(
          s"Validation Function Name is expected in validation function."
        )
      }
    }
  }

  private[Config] def validateTransformationFunctions(
      transformationFunction: Array[AttributeTransformationUnit]
    ): Unit = {
    transformationFunction.foreach { transformationUnit: AttributeTransformationUnit =>
      if (transformationUnit.attributeName == "") {
        throw InvalidConfigException(s"Target Column Name is expected.")
      } else {
        if (checkColumnExistTargetSchema(transformationUnit.attributeName) ||
            checkColumnExistAsAttributeArgument(
              transformationUnit.attributeName
            )) {
          Unit
        } else {
          throw InvalidConfigException(
            s"Attribute/Target Column: ${transformationUnit.attributeName} does " +
            s"not exist in target schema."
          )
        }
      }
      if (transformationUnit.transformationFunction == "") {
        throw InvalidConfigException(s"Function Name is expected.")
      }
      if (transformationUnit.sourceColumns.length == 0) {
        throw InvalidConfigException(s"Source Column Name is expected.")
      } else {
        transformationUnit.sourceColumns.foreach { sourceColumn: SourceColumn =>
          if (sourceColumn.columnName == "") {
            throw InvalidConfigException(s"Source Column Name is expected.")
          }
          if (sourceColumn.entityName == "") {
            throw InvalidConfigException(s"Source Entity Name is expected.")
          } else {
            if (checkEntityExist(sourceColumn.entityName)) {
              Unit
            } else {
              throw InvalidConfigException(
                s"Source Entity Name: ${sourceColumn.entityName} not found for attribute ${transformationUnit.attributeName}."
              )
            }
          }
        }
      }
      if (transformationUnit.joinColumns.isDefined) {
        transformationUnit.joinColumns.get.foreach { joinColumn =>
          if (joinColumn.entityJoinColumn == "") {
            throw InvalidConfigException(s"Entity join column is expected.")
          }
          if (joinColumn.profileJoinColumn == "") {
            throw InvalidConfigException(s"Profile join column is expected.")
          }
        }
      }
    }
    val transformationsCount = transformationFunction.map(x => x.attributeName).groupBy(identity).mapValues(_.size)
    val filteredTransformationsCount = transformationsCount.filter(x => x._2 > 1)
    if (configType == CommonEnums.PROFILE && filteredTransformationsCount.size > 0) {
      throw InvalidConfigException(
        s"Possible duplicates in attributeMapping of config, check : \n ${filteredTransformationsCount.mkString("\n")}"
      )
    }
  }

  private[Config] def validateStorageDetails(storageDetails: Array[StorageDetail]): Unit = {
    storageDetails.foreach { storageDetail: StorageDetail =>
      storageDetail.recordType match {
        case CommonEnums.VALID_RECORDS | CommonEnums.INVALID_RECORDS | CommonEnums.AUDIT_RECORDS |
            CommonEnums.LINEAGE_RECORDS | CommonEnums.DELTA_RECORDS | CommonEnums.LAST_MODIFIED_RECORDS =>
          Unit
        case x: Any =>
          throw InvalidConfigException(
            s"Expected one of ${CommonEnums.VALID_RECORDS}, " +
            s"${CommonEnums.INVALID_RECORDS}, ${CommonEnums.AUDIT_RECORDS}, ${CommonEnums.LINEAGE_RECORDS}, " +
            s"${CommonEnums.LINEAGE_RECORDS}, ${CommonEnums.LAST_MODIFIED_RECORDS} but got ${x}."
          )
      }
      if (storageDetail.storageHandler == "") {
        throw InvalidConfigException(s"Storage Handler name is expected.")
      }
    }
  }

  private[Config] def validateLookupDetails(lookupDetails: Array[LookupDetail]): Unit = {
    lookupDetails.foreach { lookupDetail: LookupDetail =>
      if (lookupDetail.lookupName == "") {
        throw InvalidConfigException(s"Lookup Name is expected.")
      }
      lookupDetail.fileSystem match {
        case CommonEnums.HDFS | CommonEnums.S3 | "local" => Unit
        case x: Any =>
          throw InvalidConfigException(
            s"Expected one of ${CommonEnums.HDFS} or ${CommonEnums.S3} but got ${x}."
          )
      }
      lookupDetail.dataFormat match {
        case CommonEnums.CSV | CommonEnums.PARQUET | CommonEnums.JSON => Unit
        case x: Any =>
          throw InvalidConfigException(
            s"Expected one of ${CommonEnums.CSV} or ${CommonEnums.PARQUET} but got ${x}."
          )
      }
      if (lookupDetail.pathUrl == "") {
        throw InvalidConfigException(s"Source path url is expected.")
      }
    }
  }

  def validate(): Unit = {
    validateConfigType()
    if (configType == CommonEnums.PROFILE) {
      validateFactDetails()
    }
    sourceDetails.foreach { sourceDetail: SourceDetails =>
      validateSourceDetail(configType, sourceDetail)
    }
    if (sourceSchema.isDefined) {
      validateSourceSchema(sourceSchema.get)
    }

    validateTargetSchema(targetSchema)

    validateColumnMapping(columnMapping)
    if (validationsDetails.isDefined) {
      validateValidationFunctions(validationsDetails.get)
    }
    validateTransformationFunctions(transformationsDetails)
    validateStorageDetails(storageDetails)
    if (lookupDetails.isDefined) {
      validateLookupDetails(lookupDetails.get)
    }
  }
}
