package in.datateam.utils

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
sealed trait MyException {
  self: Throwable =>
  val message: String
  val exceptionCode: Int
}

case class InvalidArgumentException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10001
}

case class InvalidSourceTypeException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10002
}

case class InvalidRecordTypeException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10003
}

case class InvalidCurrencyFormatException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10004
}

case class InvalidFileSystemException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10005
}

case class DateFormatNotSupportedException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10006
}

case class UnsupportedPartitionTypeException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10007
}

case class InvalidConfigException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10008
}

case class RerunCleanUpException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10009
}

case class TargetExistException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10010
}

case class DuplicateRecordsException(message: String) extends Exception(message) with MyException {
  val exceptionCode = 10011
}
