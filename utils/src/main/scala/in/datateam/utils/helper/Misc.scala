package in.datateam.utils.helper

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar

import scala.util.Try

import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object Misc {
  val dateAndTimeFormatter = new SimpleDateFormat("yyyyMMdd hh:mm aa")
  val hourlyDateFormat = "yyyy-MM-dd-HH"
  val dateFormat = "yyyy-MM-dd"
  val monthOnlyDateFormat = "yyyy-MM"
  val hourlyDateFormatter = new SimpleDateFormat(hourlyDateFormat)
  val dateFormatter = new SimpleDateFormat(dateFormat)
  val monthOnlyDateFormatter = new SimpleDateFormat(monthOnlyDateFormat)

  // FIXME : Only allows the following formats.
  val possibleDateFormats =
    Array("yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy", "yyyy/MM/dd")

  val possibleFormatters: Array[DateTimeFormatter] = possibleDateFormats.map { x =>
    new DateTimeFormatterBuilder()
      .appendPattern(x + "[ HH:mm:ss.SSS]")
      .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
      .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
      .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
      .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
      .toFormatter()
  }

  def parseDateString(stringDate: String): Option[String] = {
    val parsedDTs = possibleFormatters.map { x =>
      val dtVal = Try(LocalDateTime.parse(stringDate.trim, x))
      Option[LocalDateTime](dtVal.getOrElse(null))
    }

    val matchedFormatVal = parsedDTs.find(x => x.isDefined).flatten

    matchedFormatVal match {
      case Some(x) =>
        Some(x.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")))
      case _ => None
    }
  }

  def getDate(date: Date = new Date()): String = {
    dateFormatter.format(date)
  }

  def getDateAndTime(date: Date = new Date()): String = {
    dateAndTimeFormatter.format(date)
  }

  def getFormattedPartitionDate(partitionFormat: String, partitionType: String, partitionValue: String): String = {
    val partitionDateFormatter = new SimpleDateFormat(partitionFormat)
    val parsedPartitionDate = partitionDateFormatter.parse(partitionValue)
    partitionType match {
      case CommonEnums.HOURLY => hourlyDateFormatter.format(parsedPartitionDate)
      case CommonEnums.DAILY => dateFormatter.format(parsedPartitionDate)
      case CommonEnums.MONTHLY =>
        monthOnlyDateFormatter.format(parsedPartitionDate)
    }
  }

  def getFormattedPartitionDate(partitionValue: String): String = {
    val partitionDateFormatter = new SimpleDateFormat(dateFormat)
    val parsedPartitionDate = partitionDateFormatter.parse(partitionValue)
    dateFormatter.format(parsedPartitionDate)
  }

  def getNthDate(dateStr: String, n: Int): String = {
    val date = dateFormatter.parse(dateStr)
    val endCalendar: Calendar = new GregorianCalendar()
    val endDate: Date = date
    endCalendar.setTime(endDate)
    val startCalendar: Calendar = endCalendar.clone().asInstanceOf[Calendar]
    startCalendar.add(Calendar.DATE, n)
    dateFormatter.format(startCalendar.getTime)
  }
}
