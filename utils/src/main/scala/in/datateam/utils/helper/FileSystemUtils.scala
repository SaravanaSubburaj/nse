package in.datateam.utils.helper

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.io.PrintWriter

import scala.annotation.tailrec

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

import org.apache.hadoop.fs.FileStatus
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.FileUtil
import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger

import in.datateam.utils.InvalidFileSystemException
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object FileSystemUtils {
  val logger: Logger = Logger.getLogger(getClass.getName)
  val spark: SparkSession = SparkSession.getDefaultSession.get
  val conf = spark.sparkContext.hadoopConfiguration
  val fetchFileSystem: FileSystem = FileSystem.get(conf)

  val fetchS3FileSystem: String => FileSystem = path => {
    new Path(path).getFileSystem(conf)
  }

  def checkPathExist(path: Path)(fs: FileSystem): Boolean = {
    fs.exists(path)
  }

  def checkPathExist(pathUri: String): Boolean = {
    val fs = new Path(pathUri).getFileSystem(conf)
    val path = new Path(pathUri)
    fs.exists(path)
  }

  def readFile(pathUri: String): String = {
    val fs = new Path(pathUri).getFileSystem(conf)
    val path = new Path(pathUri)
    val reader = new BufferedReader(new InputStreamReader(fs.open(path)))

    @tailrec
    def readContent(builder: StringBuilder, reader: BufferedReader): String = {
      val line = reader.readLine()
      if (line != null) {
        builder.append(line)
        readContent(builder, reader)
      } else {
        builder.mkString
      }
    }
    try {
      val builder = new StringBuilder
      readContent(builder, reader)
    } finally {
      reader.close()
    }
  }

  def writeIntoFile(path: Path)(fs: FileSystem)(str: String): Unit = {
    val bis = new PrintWriter(fs.create(path))
    bis.write(str)
    bis.close()

  }

  def getFileSystem(fsProtocol: String, pathUri: String): FileSystem = {
    val fs: FileSystem = fsProtocol match {
      case CommonEnums.HDFS => FileSystemUtils.fetchFileSystem
      case CommonEnums.S3 => FileSystemUtils.fetchS3FileSystem(pathUri)
      case _ => throw InvalidFileSystemException("Invalid filesystem.")
    }
    fs
  }

  def getFileSystem(pathUri: String): FileSystem = {
    val fsProtocol = if (pathUri.startsWith("s3")) {
      CommonEnums.S3
    } else {
      CommonEnums.HDFS
    }
    val fs: FileSystem = fsProtocol match {
      case CommonEnums.HDFS => FileSystemUtils.fetchFileSystem
      case CommonEnums.S3 => FileSystemUtils.fetchS3FileSystem(pathUri)
      case _ => throw InvalidFileSystemException("Invalid filesystem.")
    }
    fs
  }

  def getTempDir(fsProtocol: String, directoryInfo: String)(implicit spark: SparkSession): String = {
    val tempDir = fsProtocol match {
      case CommonEnums.HDFS | CommonEnums.S3 =>
        if (System.getenv("SPARK_YARN_STAGING_DIR") != null) {
          s"${System.getenv("SPARK_YARN_STAGING_DIR")}/temp_storage/${directoryInfo}"
        } else {
          val checkpointDir = spark.sparkContext.getCheckpointDir.get
          s"${checkpointDir}/temp_storage/${directoryInfo}"
        }
      case _ =>
        s"/tmp/${spark.sparkContext.applicationId}/temp_storage/${directoryInfo}"
    }
    tempDir
  }

  def getCheckpointDir()(implicit spark: SparkSession): String = {
    getTempDir(CommonEnums.HDFS, CommonEnums.CHECKPOINT_DIR)
  }

  def deletePath(targetPathStr: String, deleteOnExit: Boolean = true)(implicit spark: SparkSession): Unit = {
    val targetPath = new Path(targetPathStr)
    val fs: FileSystem = getFileSystem(targetPathStr)
    if (deleteOnExit) {
      fs.deleteOnExit(targetPath)
    } else {
      fs.delete(targetPath, true)
    }

  }

  def deleteCheckpointDir()(implicit spark: SparkSession): Unit = {
    val checkpointPathTempStr = getCheckpointDir() + "_temp"
    val checkpointPathTemp = new Path(checkpointPathTempStr)
    val checkpointPathStr = getCheckpointDir()
    val checkpointPath = new Path(checkpointPathStr)
    val fs: FileSystem = getFileSystem(CommonEnums.HDFS, checkpointPathStr)
    logger.info("Deleting temporary directories.")
    fs.delete(checkpointPathTemp, true)
    fs.delete(checkpointPath, true)
  }

  def cleanUp()(implicit spark: SparkSession): Unit = {
    deleteCheckpointDir()
  }

  def readTempData(schema: StructType)(implicit spark: SparkSession): DataFrame = {
    val checkpointPathStr = getCheckpointDir()
    val tempDF =
      spark.read.schema(schema).format("parquet").load(checkpointPathStr)
    tempDF.createOrReplaceTempView("tempDF")
    logger.info("Reading and refreshing temporary data.")
    spark.sql("REFRESH TABLE tempDF")
    tempDF
  }

  def writeTempData(
      tempDF: DataFrame,
      config: Config,
      partitionColumns: List[String]
    )(
      implicit spark: SparkSession
    ): Unit = {
    val checkpointPathTempStr = getCheckpointDir() + "_temp"
    val checkpointPathStr = getCheckpointDir()
    tempDF.write
      .partitionBy(partitionColumns: _*)
      .mode(SaveMode.Overwrite)
      .format("parquet")
      .save(checkpointPathTempStr)
    logger.info("Writing temporary data.")
    moveHdfsFiles(checkpointPathTempStr, checkpointPathStr)
  }

  private[FileSystemUtils] def writeHeader(header: Array[String], targetPathStr: String, fs: FileSystem): Unit = {
    val targetPath = new Path(targetPathStr)
    val outputStream = fs.create(targetPath)
    val writer = new PrintWriter(outputStream)
    val line = s"${header.mkString(",")}\n"
    writer.write(line)
    writer.flush()
    outputStream.close()
  }

  def mergeCsv(
      sourcePathStr: String,
      header: Array[String],
      partitionValue: String,
      csvFileName: String,
      addHeader: Boolean = false,
      isPartitioned: Boolean = false
    )(
      implicit spark: SparkSession
    ): Unit = {
    val sourcePathWithPartitionStr = if (isPartitioned) {
      sourcePathStr
    } else {
      s"${sourcePathStr}/${CommonEnums.DATE_PARTITION_COLUMN}=${partitionValue}"
    }
    val headerFilePath = s"${sourcePathWithPartitionStr}/_header.csv"
    val sourcePathWithPartition = new Path(sourcePathWithPartitionStr)
    val checkPointDir = getCheckpointDir()
    val combinedCSVFilePathStr = s"${checkPointDir}/${csvFileName}"
    val combinedCSVFilePath = new Path(combinedCSVFilePathStr)
    val finalOutputPathStr = s"${sourcePathWithPartitionStr}/${csvFileName}"
    val finalOutputPath = new Path(finalOutputPathStr)
    logger.info(s"Merging csv from ${sourcePathWithPartitionStr}")
    val fs: FileSystem = getFileSystem(CommonEnums.HDFS, combinedCSVFilePathStr)
    if (addHeader) {
      writeHeader(header, headerFilePath, fs)
    }
    FileUtil.copyMerge(
      fs,
      sourcePathWithPartition,
      fs,
      combinedCSVFilePath,
      true,
      conf,
      null
    )

    //    FIXME: Check why rename is not working in glue
    //    fs.rename(combinedCSVFilePath, finalOutputPath)
    FileUtil.copy(fs, combinedCSVFilePath, fs, finalOutputPath, true, conf)
  }

  def moveHdfsFiles(
      sourcePathString: String,
      destPathString: String,
      fsProtocol: String = CommonEnums.HDFS,
      fileNamePrefix: String = "",
      deletePreviousFiles: Boolean = true
    )(
      implicit spark: SparkSession
    ): Unit = {
    logger.info(
      "Moving Hdfs files from " + sourcePathString + " to " + destPathString
    )
    val sourcePath = new Path(sourcePathString)
    val destinationPath = new Path(destPathString)
    val srcFS: FileSystem = getFileSystem("")
    val destFS: FileSystem = getFileSystem(fsProtocol, destPathString)
    srcFS.deleteOnExit(sourcePath)
    if (!destFS.exists(destinationPath)) destFS.mkdirs(destinationPath)
    if (srcFS.exists(sourcePath)) {
      srcFS.listStatus(sourcePath).filter(_.isFile).filter(_.getPath.getName != "_SUCCESS").foreach { level0 =>
        val srcPathStr: String = sourcePath.toString + File.separator + level0.getPath.getName
        val destPathStr: String = destinationPath.toString + File.separator + level0.getPath.getName
        logger.info("Moving path : " + srcPathStr + " to " + destPathStr)
        val srcPath: Path = new Path(srcPathStr)
        val destPath: Path = new Path(destPathStr)
        destFS.mkdirs(destPath.getParent)
        if (srcFS.getScheme == destFS.getScheme && destFS.getScheme != CommonEnums.S3) {
          if (!srcFS.rename(srcPath, destPath)) {
            throw new IllegalStateException(
              s"Application failed renaming from $sourcePath to $destinationPath"
            )
          }
        } else {
          FileUtil.copy(srcFS, srcPath, destFS, destPath, true, conf)
        }
      }

      srcFS.listStatus(sourcePath).filter(_.isDirectory).foreach { level1 =>
        val partitionDir: String = level1.getPath.getName
        val dstPartition: Path = new Path(
          destinationPath.toString + File.separator + partitionDir + File.separator
        )
        if (destFS.exists(dstPartition) && deletePreviousFiles) {
          destFS.delete(dstPartition, true)
        }
        val fileList: Array[FileStatus] = srcFS
          .listStatus(level1.getPath)
          .filterNot(_.getPath.getName.startsWith("_"))
          .filterNot(_.getPath.getName.startsWith("."))

        fileList.filter(_.isFile).foreach { name =>
          val srcPathStr: String = sourcePath.toString + File.separator +
            level1.getPath.getName + File.separator + name.getPath.getName
          val destPathStr: String = destinationPath.toString + File.separator +
            level1.getPath.getName + File.separator + fileNamePrefix + name.getPath.getName
          logger.info("Moving path : " + srcPathStr + " to " + destPathStr)
          val srcPath: Path = new Path(srcPathStr)
          val destPath: Path = new Path(destPathStr)
          destFS.mkdirs(destPath.getParent)
          if (srcFS.getScheme == destFS.getScheme && destFS.getScheme != CommonEnums.S3) {
            if (!srcFS.rename(srcPath, destPath)) {
              throw new IllegalStateException(
                s"Application failed renaming from $sourcePath to $destinationPath"
              )
            }
          } else {
            FileUtil.copy(srcFS, srcPath, destFS, destPath, true, conf)
          }
        }
        fileList.filter(_.isDirectory).foreach { _ =>
          val path1: String = sourcePath.toString + File.separator +
            level1.getPath.getName
          val path2: String = destinationPath.toString + File.separator +
            level1.getPath.getName
          logger.info("Moving dir : " + path1 + " to " + path2)
          moveHdfsFiles(
            path1,
            path2,
            fsProtocol,
            fileNamePrefix,
            deletePreviousFiles
          )
        }
      }
    }
  }
}
