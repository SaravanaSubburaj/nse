package in.datateam.utils.helper

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar

import scala.util.Try

import in.datateam.utils.UnsupportedPartitionTypeException
import in.datateam.utils.configparser.PartitionDetails
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object PartitionHelper {

  def getPreviousPartition(partitionDetails: PartitionDetails, partitionValue: String): String = {
    val prevPartitionDetails = PartitionDetails(
      partitionDetails.partitionColumn,
      partitionDetails.partitionType,
      partitionDetails.partitionFormat,
      2
    )
    getAllPartitionsRaw(prevPartitionDetails, partitionValue).head
  }

  def getAllPartitions(
      partitionDetails: PartitionDetails,
      partitionValue: String,
      isHDFS: Boolean = false
    ): List[String] = {
    val allPartitions = getAllPartitionsRaw(partitionDetails, partitionValue)
    if (isHDFS) {
      allPartitions.map { d: String =>
        val partition = partitionDetails.partitionColumn zip d.split(
            CommonEnums.PARTITION_DELIMITER
          )
        if (partitionDetails.partitionColumn.size == 1) {
          partition.map(p => s"${p._1}='${p._2}*'").mkString("/")
        } else {
          partition.map(p => s"${p._1}='${p._2}'").mkString("/")
        }
      }
    } else {
      allPartitions.map { d: String =>
        val partition = partitionDetails.partitionColumn zip d.split(
            CommonEnums.PARTITION_DELIMITER
          )
        if (partitionDetails.partitionColumn.size == 1) {
          partition.map(p => s"${p._1} LIKE '${p._2}%'").mkString(" AND ")
        } else {
          partition.map(p => s"${p._1}='${p._2}'").mkString(" AND ")
        }
      }
    }
  }

  def getAllPartitionsRaw(partitionDetails: PartitionDetails, partitionValue: String): List[String] = {
    val partitionFormatter: SimpleDateFormat = new SimpleDateFormat(
      partitionDetails.partitionFormat
        .mkString(CommonEnums.PARTITION_DELIMITER.toString)
    )
    val partitionVal = Try(partitionFormatter.parse(partitionValue)).toOption
    if (partitionVal.isEmpty) {
      throw UnsupportedPartitionTypeException(
        s"Unable to parse ${partitionValue} using format ${partitionDetails.partitionFormat} ."
      )
    }
    val iterationType = partitionDetails.partitionType match {
      case CommonEnums.HOURLY => Calendar.HOUR
      case CommonEnums.DAILY => Calendar.DATE
      case CommonEnums.MONTHLY => Calendar.MONTH
      case _ =>
        throw UnsupportedPartitionTypeException(
          s"Expected partition type as ${CommonEnums.HOURLY}, ${CommonEnums.DAILY}, ${CommonEnums.MONTHLY} but got " +
          s"${partitionDetails.partitionType}"
        )
    }
    val allPartitions =
      getLastNPartitions(
        partitionFormatter,
        partitionVal.get,
        partitionDetails.numberOfPartitions,
        iterationType
      )
    allPartitions
  }

  private[PartitionHelper] def getLastNPartitions(
      partitionFormatter: SimpleDateFormat,
      partitionValue: Date,
      n: Int,
      iterationType: Int
    ): List[String] = {
    val endCalendar: Calendar = new GregorianCalendar()
    val endDate: Date = partitionValue
    endCalendar.setTime(endDate)
    val startCalendar: Calendar = endCalendar.clone().asInstanceOf[Calendar]
    startCalendar.add(iterationType, -n + 1)
    getPartitions(partitionFormatter, startCalendar, endCalendar, iterationType)
  }

  private[PartitionHelper] def getPartitions(
      partitionFormatter: SimpleDateFormat,
      startDate: Calendar,
      endDate: Calendar,
      iterationType: Int
    ): List[String] = {
    if (startDate == endDate) {
      val st: Calendar = startDate.clone().asInstanceOf[Calendar]
      startDate.add(iterationType, 1)
      List(partitionFormatter.format(st.getTime))
    } else {
      val st: Calendar = startDate.clone().asInstanceOf[Calendar]
      startDate.add(iterationType, 1)
      partitionFormatter.format(st.getTime) :: getPartitions(
        partitionFormatter,
        startDate,
        endDate,
        iterationType
      )
    }
  }
}
