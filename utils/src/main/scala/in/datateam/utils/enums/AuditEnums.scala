package in.datateam.utils.enums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object AuditEnums {
  val JOB_ID = "jobId"
  val ROW_ID = "rowId"
  val PROCESS_NAME = "processName"
  val RECORDS_CONTENT = "recordsContent"
  val REASON = "reason"
  val DATE_PARTITION = "date"
  val EXIT_ON_DUPLICATE_DETECTION = true
  val COLUMN_MAPPING_FUNCTION_NAME = "ColumnMapping"
  val RECORDS = "records"
  val ENTITY_NAME = "entityName"
  val ENTITY = "entity"
  val COLUMN_NAME = "columnName"
}
