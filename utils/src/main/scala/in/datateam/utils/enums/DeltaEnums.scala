package in.datateam.utils.enums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object DeltaEnums {
  val RECORD_MODIFIER_COLUMN = "operationType"
  val INSERT = "I"
  val BEFORE = "B"
  val AFTER = "A"
  val NO_CHANGE = "N"
  val DELETE = "D"
}
