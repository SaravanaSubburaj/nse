package in.datateam.utils.enums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object CommonEnums {
  val ENTITY_TYPE = "entityType"
  val ENTITY = "entity"
  val PROFILE = "profile"
  val JOB_TYPES = Array(ENTITY, PROFILE)

  val CSV = "csv"
  val PARQUET = "parquet"
  val AUDIT_RECORDS = "audit-records"
  val LINEAGE_RECORDS = "lineage-records"
  val INVALID_RECORDS = "invalid-records"
  val VALID_RECORDS = "valid-records"
  val DELTA_RECORDS = "delta-records"
  val EXCLUSION_RECORDS = "exclusion-records"
  val LAST_MODIFIED_RECORDS = "last-modified-records"
  val LAST_MODIFIED_COLUMN_DATA_TYPE = "string"
  val DATA_DELIMITER = "\u0001"
  val ROW_DELIMITER = "\u0002"

  val PARTITION_DELIMITER = '^'
  val DATE_PARTITION_COLUMN = "date"

  //<editor-fold desc="CONFIG ENUMS">
  val FILESYSTEM = "filesystem"
  val DATABASE = "database"
  val REDSHIFT = "redshift"
  val SNAPSHOT = "snapshot"
  val MUTABLE = "mutable"
  val EVENT = "event"
  val JSON = "json"
  val BATCH = "batch"
  val HOURLY = "hourly"
  val DAILY = "daily"
  val MONTHLY = "monthly"
  val HDFS = "hdfs"
  val S3 = "s3"
  val PROFILE_MASTER = "profile_master"
  //</editor-fold>

  //<editor-fold desc="FILESYSTEM_UTILS ENUMS">
  val CHECKPOINT_DIR = "checkpoint"
  //</editor-fold>

  val ACTIVE_SUBS_IND = "activeSubsInd"
  val ACTIVE_SUBS_INSERT = "I"
  val ACTIVE_SUBS_UPDATE = "A"
  val ACTIVE_SUBS_DELETE = "D"
}
