package in.datateam.bifrost

import org.apache.spark.sql.AnalysisException
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit

import org.apache.log4j.Logger

import in.datateam.bifrost.processor.ColumnMappingProcessor
import in.datateam.bifrost.processor.ExclusionProcessor
import in.datateam.bifrost.processor.ValidationProcessor
import in.datateam.bifrost.processor.source.ReadHadoopFS
import in.datateam.bifrost.processor.source.ReadHive
import in.datateam.bifrost.processor.source.ReadRedshift
import in.datateam.bifrost.utils.InvalidDFCaseClass
import in.datateam.bifrost.utils.ValidAndInvalidDFCaseClass
import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser._
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.enums.DeltaEnums
import in.datateam.utils.helper.Misc

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object Bifrost {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def readDataFrame(
      config: Config,
      sourceDetail: SourceDetails,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    try {
      sourceDetail.sourceType match {
        case CommonEnums.FILESYSTEM => ReadHadoopFS.process(sourceDetail, partitionValue)
        case CommonEnums.DATABASE => ReadHive.process(sourceDetail, partitionValue)
        case CommonEnums.REDSHIFT => ReadRedshift.process(config, sourceDetail, partitionValue)
      }
    } catch {
      case analysisException: AnalysisException =>
        logger.info(s"Spark AnalysisException occurred: Reason ${analysisException.message}.")
        if (config.configType == CommonEnums.PROFILE) {
          spark.sqlContext.emptyDataFrame
        } else {
          throw analysisException
        }
    }
  }

  def applySourceSchema(
      sourceDF: DataFrame,
      sourceSchema: Array[SourceSchemaUnit]
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val zippedColumns = sourceDF.columns zip sourceSchema
    val columns = zippedColumns.map {
      case (sourceColumn, sourceSchemaUnit) =>
        col(sourceColumn)
          .as(sourceSchemaUnit.columnName)
          .cast(sourceSchemaUnit.dataType)
    }
    sourceDF.select(columns: _*)
  }

  def transformPartitionColumn(
      config: Config,
      sourceDF: DataFrame,
      partition: String,
      sourceDetails: SourceDetails
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val addedPartitionColumn = sourceDetails.partitionDetails match {
      case Some(partitionDetails) =>
        //FIXME: Find better way to pass partiton column
        val tempPartitionColumnName = config.getExistingPartitionColumn()
//          s"${sourceDetails.entityName}${CommonEnums.DATA_DELIMITER}${partitionDetails.partitionColumn
//            .mkString(CommonEnums.PARTITION_DELIMITER.toString)}"
        val renamedPartitionedColDF = sourceDF
          .withColumnRenamed(
            partitionDetails.partitionColumn
              .mkString(CommonEnums.PARTITION_DELIMITER.toString),
            tempPartitionColumnName
          )
        renamedPartitionedColDF.withColumn(
          CommonEnums.DATE_PARTITION_COLUMN,
          lit(
            Misc.getFormattedPartitionDate(
              partitionDetails.partitionFormat
                .mkString(CommonEnums.PARTITION_DELIMITER.toString),
              partitionDetails.partitionType,
              partition
            )
          )
        )
      case None =>
        sourceDF.withColumn(
          CommonEnums.DATE_PARTITION_COLUMN,
          lit(Misc.getFormattedPartitionDate(partition))
        )
    }
    addedPartitionColumn
  }

  def readSourceDataFrame(
      config: Config,
      partition: String,
      sourceDetails: SourceDetails
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val sourceDF = readDataFrame(config, sourceDetails, partition)
    val sourceWithSchemaDF = if (config.sourceSchema.isDefined) {
      applySourceSchema(sourceDF, config.sourceSchema.get)
    } else {
      sourceDF
    }
    val addedDateColumn = if (config.configType == CommonEnums.ENTITY) {
      transformPartitionColumn(config, sourceWithSchemaDF, partition, sourceDetails)
    } else {
      sourceWithSchemaDF
    }
    addedDateColumn
  }

  def readDataFrames(config: Config, partition: String)(implicit spark: SparkSession): Map[String, DataFrame] = {
    config.sourceDetails
      .map(
        sourceDetail =>
          (
            sourceDetail.entityName,
            readSourceDataFrame(config, partition, sourceDetail)
          )
      )
      .toMap
  }

  def populateAllColumns(config: Config, columnMappedDF: DataFrame)(implicit spark: SparkSession): DataFrame = {
    val columns = columnMappedDF.columns.toSeq
    val addedEmptyColumnsDF =
      config.targetSchema.foldLeft[DataFrame](columnMappedDF) { (df: DataFrame, columnDetail: TargetSchemaUnit) =>
        val cellValue =
          if (columns.contains(columnDetail.columnName)) {
            col(columnDetail.columnName)
          } else {
            lit("")
          }
        df.withColumn(
          columnDetail.columnName,
          cellValue.cast(columnDetail.dataType)
        )
      }
    addedEmptyColumnsDF
  }

  def applyTargetSchema(
      config: Config,
      dataFrame: DataFrame,
      isDelta: Boolean = false
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val targetColumnsArray: Array[TargetSchemaUnit] = config.targetSchema
    val extraColumns = if (isDelta) {
      List(
        TargetSchemaUnit(CommonEnums.DATE_PARTITION_COLUMN, "string", "false", "false"),
        TargetSchemaUnit(DeltaEnums.RECORD_MODIFIER_COLUMN, "string", "false", "false")
      )
    } else {
      List(
        TargetSchemaUnit(CommonEnums.DATE_PARTITION_COLUMN, "string", "false", "false")
      )
    }

    val addedColumnsArray = (targetColumnsArray ++ extraColumns)
    //FIXME GTP-223 :- Populating only certain columns in case of aggregated entity
    val columnCastArray =
      if (config.configType == CommonEnums.ENTITY
          && config.sourceDetails.head.entityType == CommonEnums.EVENT) {
        addedColumnsArray
          .filter(column => dataFrame.columns.contains(column.columnName))
          .map(
            schemaUnit => col(schemaUnit.columnName).cast(schemaUnit.dataType)
          )
      } else {
        addedColumnsArray.map(
          schemaUnit => col(schemaUnit.columnName).cast(schemaUnit.dataType)
        )
      }

    val typeCastedDF = dataFrame.select(columnCastArray: _*)
    typeCastedDF
  }

  def processEntity(
      config: Config,
      partition: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): (ValidAndInvalidDFCaseClass, Map[String, DataFrame]) = {
    val sourceDFMap = readDataFrames(config, partition)
    val updatedPartitionValue: String =
      config.sourceDetails.head.partitionDetails match {
        case Some(partitionDetails) =>
          Misc.getFormattedPartitionDate(
            partitionDetails.partitionFormat
              .mkString(CommonEnums.PARTITION_DELIMITER.toString),
            partitionDetails.partitionType,
            partition
          )
        case None => Misc.getFormattedPartitionDate(partition)
      }
    val processedDF = ColumnMappingProcessor
      .columnMappingDataFrame(
        config,
        sourceDFMap(config.sourceDetails.head.entityName)
      )

    val addedEmptyColumnDF = populateAllColumns(config, processedDF)
    val lineageCols: List[String] = List(CommonEnums.DATE_PARTITION_COLUMN) ++
      config.sourceDetails.head.primaryColumns.get.toList
    val validAndInvalidDF: ValidAndInvalidDFCaseClass =
      ValidationProcessor.process(
        config,
        addedEmptyColumnDF,
        sourceDFMap,
        updatedPartitionValue,
        lineageCols: _*
      )
    logger.info("Validation Completed.")
    (validAndInvalidDF, sourceDFMap)
  }

  def processProfile(
      config: Config,
      partition: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): (ValidAndInvalidDFCaseClass, Map[String, DataFrame]) = {
    val sourceDFMap = readDataFrames(config, partition)
    val postExclusionSourceDFMap: Map[String, DataFrame] = ExclusionProcessor.process(config, sourceDFMap, partition)
    val lineageCols: List[String] = List(config.targetSchema.head.columnName)
    val columnMappedDF = ColumnMappingProcessor
      .columnMappingDataFrames(config, postExclusionSourceDFMap, partition, lineageCols: _*)
    val addedEmptyColumnDF = populateAllColumns(config, columnMappedDF)
    val addedDatePartitionDF = addedEmptyColumnDF.withColumn(
      CommonEnums.DATE_PARTITION_COLUMN,
      lit(partition)
    )

    val invalidDFSchema = Encoders.product[InvalidDFCaseClass].schema
    val emptyDF =
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], invalidDFSchema)
    val validAndInvalidDFCaseClass =
      ValidAndInvalidDFCaseClass(addedDatePartitionDF, emptyDF)
    (validAndInvalidDFCaseClass, sourceDFMap)
  }

  def process(
      config: Config,
      partition: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): (ValidAndInvalidDFCaseClass, Map[String, DataFrame]) = {
    config.configType match {
      case CommonEnums.ENTITY => processEntity(config, partition)
      case CommonEnums.PROFILE => processProfile(config, partition)
    }
  }

}
