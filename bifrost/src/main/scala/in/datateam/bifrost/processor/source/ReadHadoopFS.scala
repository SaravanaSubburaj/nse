package in.datateam.bifrost.processor.source

import org.apache.spark.sql.Column
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.apache.log4j.Logger

import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.utils.configparser.SourceDetails
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ReadHadoopFS {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(sourceDetail: SourceDetails, partitionValue: String)(implicit spark: SparkSession): DataFrame = {
    logger.info(
      s"Reading ${sourceDetail.entityName} path ${sourceDetail.pathUrl.get} for ${partitionValue}"
    )

    val allOptionsMap: Map[String, String] = sourceDetail.getDataFormatOptions

    val inputDF = spark.read
      .format(sourceDetail.dataFormat.get)
      .options(allOptionsMap)
      .load(sourceDetail.pathUrl.get)
    val srcDF = sourceDetail.partitionDetails match {
      case Some(partitionDetails) =>
        val latestPartitionValue = calculateLatestPartition(inputDF, sourceDetail, partitionValue)
        val partitionCondition: String =
          MiscFunctions.createPartitionCondition(partitionDetails, latestPartitionValue)
        val partitionColumns = partitionDetails.partitionColumn
        val customSchema = StructType(inputDF.schema.fields.map { field =>
          if (partitionColumns.contains(field.name)) {
            StructField(field.name, StringType, nullable = true)
          } else {
            field
          }
        })
        spark.read
          .format(sourceDetail.dataFormat.get)
          .options(allOptionsMap)
          .schema(customSchema)
          .load(sourceDetail.pathUrl.get)
          .where(partitionCondition)
      case None =>
        inputDF
    }
    srcDF
  }

  def calculateLatestPartition(dataFrame: DataFrame, sourceDetail: SourceDetails, partitionValue: String): String = {
    logger.info(s"Entity: ${sourceDetail.entityName}")
    if (sourceDetail.loadLatest.getOrElse("false") == "true") {
      val latestPartitionColumn = "latestPartition"
      val concatenatedPartitionColumn = "concatPartition"
      val partitionCols = sourceDetail.partitionDetails.get.partitionColumn.map(c => col(c))
      val updatedColumnDF = dataFrame.withColumn(
        concatenatedPartitionColumn,
        concat_ws(CommonEnums.PARTITION_DELIMITER.toString, partitionCols: _*)
      )
      val filterCondition: Column = col(concatenatedPartitionColumn) <= partitionValue
      val calculatedLatestPartition = updatedColumnDF
        .where(filterCondition)
        .select(max(col(concatenatedPartitionColumn)).as(latestPartitionColumn))
        .select(latestPartitionColumn)
        .take(1)
      if (!calculatedLatestPartition.isEmpty) {
        calculatedLatestPartition.head.getString(0)
      } else {
        partitionValue
      }
    } else {
      partitionValue
    }
  }
}
