package in.datateam.bifrost.processor.sink

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.utils.InvalidConfigException
import in.datateam.utils.InvalidRecordTypeException
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.StorageDetail
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object PersistHadoopFS extends PersistTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      dataFrame: DataFrame,
      config: Config,
      targetDetail: StorageDetail,
      partitionColumns: List[String]
    )(
      implicit spark: SparkSession
    ): Unit = {
    logger.info(
      s"Started storing ${targetDetail.recordType} for ${config.configType}."
    )
    val allOptionsMap: Map[String, String] = targetDetail.getDataFormatOptions
    val tempDir = FileSystemUtils.getTempDir(
      targetDetail.fileSystem.get,
      targetDetail.recordType
    )
    targetDetail.recordType match {
      case CommonEnums.VALID_RECORDS | CommonEnums.DELTA_RECORDS | CommonEnums.EXCLUSION_RECORDS =>
        if (targetDetail.dataFormat.get == CommonEnums.CSV) {
          val allOptionsExceptHeaderMap = allOptionsMap.filter {
            case (key, _) => key != "header" && key != "csv.filename"
          }
          val allFields = if (targetDetail.recordType == CommonEnums.EXCLUSION_RECORDS) {
            dataFrame.columns
          } else {
            config.targetSchema.map(schemaUnit => schemaUnit.columnName)
          }
          val partitionValue: String = dataFrame
            .select(CommonEnums.DATE_PARTITION_COLUMN)
            .head
            .getAs[String](0)
          val addHeader = allOptionsMap.getOrElse("header", "false") == "true"
          val csvFileName =
            allOptionsMap.getOrElse("csv.filename", "part-00000.csv")
          if (allOptionsMap.getOrElse("csv.mergefiles", "false") == "true") {
            val isPartitioned = targetDetail.recordType == CommonEnums.EXCLUSION_RECORDS
            saveDataFrame(dataFrame, config, allOptionsExceptHeaderMap, partitionColumns, targetDetail, tempDir)
            FileSystemUtils.mergeCsv(tempDir, allFields, partitionValue, csvFileName, addHeader, isPartitioned)
          } else {
            saveDataFrame(dataFrame, config, allOptionsMap, partitionColumns, targetDetail, tempDir)
          }
        } else {
          saveDataFrame(dataFrame, config, allOptionsMap, partitionColumns, targetDetail, tempDir)
        }
      case CommonEnums.INVALID_RECORDS | CommonEnums.AUDIT_RECORDS | CommonEnums.LINEAGE_RECORDS |
          CommonEnums.LAST_MODIFIED_RECORDS =>
        val partitionColumns =
          List(CommonEnums.DATE_PARTITION_COLUMN, CommonEnums.ENTITY)
        saveDataFrame(dataFrame, config, allOptionsMap, partitionColumns, targetDetail, tempDir)
      case _ =>
        throw InvalidRecordTypeException(
          s"Expected one of '${CommonEnums.VALID_RECORDS}', " +
          s"'${CommonEnums.INVALID_RECORDS}', '${CommonEnums.AUDIT_RECORDS}', '${CommonEnums.LINEAGE_RECORDS}', " +
          s"'${CommonEnums.LAST_MODIFIED_RECORDS}' but got" +
          s" ${targetDetail.recordType}."
        )
    }
    logger.info("Started moving processed data into target directory.")
    FileSystemUtils.moveHdfsFiles(
      tempDir,
      targetDetail.pathUrl.get,
      targetDetail.fileSystem.get,
      deletePreviousFiles = false
    )
    logger.info("Moving completed successfully.")
  }

  private[PersistHadoopFS] def saveDataFrame(
      dataFrame: DataFrame,
      config: Config,
      optionsMap: Map[String, String],
      partitionColumns: List[String],
      targetDetail: StorageDetail,
      finalPath: String
    )(
      implicit spark: SparkSession
    ): Unit = {

    //FIXME :- Remove/Refactor targetColumns part
    val targetCols =
      if (config.configType == CommonEnums.ENTITY
          && config.sourceDetails.head.entityType == CommonEnums.EVENT) {
        config.getAllColumns
          .filter(col => dataFrame.columns.contains(col))
          .map(columnName => col(columnName))
      } else {
        config.getAllColumns.map(columnName => col(columnName))
      }

    if (targetDetail.recordType == CommonEnums.VALID_RECORDS) {
      dataFrame
        .select(targetCols: _*)
        .write
        .partitionBy(partitionColumns: _*)
        .options(optionsMap)
        .mode(SaveMode.Append)
        .format(targetDetail.dataFormat.get)
        .save(finalPath)
    } else {
      dataFrame.write
        .partitionBy(partitionColumns: _*)
        .options(optionsMap)
        .mode(SaveMode.Append)
        .format(targetDetail.dataFormat.get)
        .save(finalPath)
    }
  }

  override def validateConfig(storageDetail: StorageDetail)(implicit spark: SparkSession): Unit = {
    storageDetail.pathUrl match {
      case Some(pathUrl) =>
        if (pathUrl == "") {
          throw InvalidConfigException(s"Source path url is expected.")
        }
      case None => throw InvalidConfigException(s"Source path url is expected.")
    }

    storageDetail.fileSystem match {
      case Some(fileSystem) =>
        fileSystem match {
          case CommonEnums.HDFS | CommonEnums.S3 | "" => Unit
          case x: Any =>
            throw InvalidConfigException(
              s"Expected one of ${CommonEnums.HDFS} or" +
              s" ${CommonEnums.S3} but got ${x}."
            )
        }
      case None => throw InvalidConfigException(s"fileSystem is expected.")
    }
    storageDetail.dataFormat match {
      case Some(dataFormat) =>
        dataFormat match {
          case CommonEnums.CSV | CommonEnums.PARQUET => Unit
          case x: Any =>
            throw InvalidConfigException(
              s"Expected one of ${CommonEnums.CSV} or" +
              s" ${CommonEnums.PARQUET} but got ${x}."
            )
        }
      case None => throw InvalidConfigException(s"dataFormat is expected.")
    }
  }
}
