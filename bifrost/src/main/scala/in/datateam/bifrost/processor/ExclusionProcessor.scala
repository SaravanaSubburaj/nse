package in.datateam.bifrost.processor

import org.apache.spark.sql.Column
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StructType

import org.apache.log4j.Logger

import in.datateam.bifrost.Bifrost
import in.datateam.bifrost.utils.IncludedAndExcludedDF
import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.ExclusionDetails
import in.datateam.utils.configparser.ExclusionJoinColumn
import in.datateam.utils.configparser.SourceDetails
import in.datateam.utils.configparser.StorageDetail
import in.datateam.utils.enums.CommonEnums

object ExclusionProcessor {

  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(
      config: Config,
      sourceDFMap: Map[String, DataFrame],
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): Map[String, DataFrame] = {
    val factEntityName: String = config.getFactEntityName
    val factEntityDF: DataFrame = sourceDFMap(factEntityName)
    val dimensionEntityDetails: Array[SourceDetails] = config.getDimensionEntityDetails()
    dimensionEntityDetails.map { dimensionEntityDetail =>
      val dimensionEntityName = dimensionEntityDetail.entityName
      val dimensionEntityDF = sourceDFMap(dimensionEntityName)
      dimensionEntityDetail.exclusionDetails match {
        case Some(exclusionDetails) =>
          if (exclusionDetails.exclusionFlag == "true") {
            val exclusionSchema = dimensionEntityDF.schema
            val previousExcludedDF = readExclusionDate(config, dimensionEntityDetail, exclusionSchema, partitionValue)
            val unionDF = dimensionEntityDF.union(previousExcludedDF)
            val includedAndExcludedDFCaseClass = performExclusion(factEntityDF, unionDF, exclusionDetails.joinDetails)
            val includedDimensionEntityDF = includedAndExcludedDFCaseClass.includedDF
            writeExclusionDate(config, includedAndExcludedDFCaseClass.excludedDF, exclusionDetails)
            (dimensionEntityName, includedDimensionEntityDF)
          } else {
            (dimensionEntityName, dimensionEntityDF)
          }
        case None => (dimensionEntityName, dimensionEntityDF)
      }
    }
    sourceDFMap
  }

  private[ExclusionProcessor] def readExclusionDate(
      config: Config,
      sourceDetail: SourceDetails,
      exclusionSchema: StructType,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val exclusionDetails = sourceDetail.exclusionDetails.get
    val srcDetail: SourceDetails = SourceDetails(
      sourceDetail.entityName,
      sourceDetail.entityType,
      sourceDetail.extractionType,
      CommonEnums.FILESYSTEM,
      exclusionDetails.exclusionStorageDetails.dataFormat,
      exclusionDetails.exclusionStorageDetails.dataFormatOption,
      exclusionDetails.exclusionStorageDetails.fileSystem,
      exclusionDetails.exclusionStorageDetails.pathUrl,
      None,
      None,
      None,
      sourceDetail.joinColumn,
      sourceDetail.primaryColumns,
      Some("false"),
      None,
      None
    )
    val exclusionDF = Bifrost.readDataFrame(config, srcDetail, partitionValue)
    if (exclusionDF.take(1).isEmpty) {
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], exclusionSchema)
    } else {
      exclusionDF
    }
  }
  private[ExclusionProcessor] def writeExclusionDate(
      config: Config,
      dataFrame: DataFrame,
      exclusionDetail: ExclusionDetails
    )(
      implicit spark: SparkSession
    ): Unit = {
    val targetDetail: StorageDetail = exclusionDetail.exclusionStorageDetails.toProfileStorageDetailFormat
    PersistProcessor.process(config, dataFrame, targetDetail)
  }

  private[ExclusionProcessor] def performExclusion(
      factEntityDF: DataFrame,
      dimensionEntityDF: DataFrame,
      joinDetails: Array[ExclusionJoinColumn]
    )(
      implicit spark: SparkSession
    ): IncludedAndExcludedDF = {
    val selectedColumnFact = joinDetails.map { joinDetail =>
      col(joinDetail.factJoinColumn)
    }
    val selectedColumnFactDF = factEntityDF.select(selectedColumnFact: _*)
    val joinCondition: Column = joinDetails.map { joinDetail =>
      dimensionEntityDF(joinDetail.dimensionJoinColumn) === factEntityDF(joinDetail.factJoinColumn)
    }.reduce { (left, right) =>
      left && right
    }
    val joinedDF = dimensionEntityDF.join(selectedColumnFactDF, joinCondition, "leftouter")
    val excludedCondition = joinDetails.map { joinDetail =>
      selectedColumnFactDF(joinDetail.factJoinColumn).isNull
    }.reduce { (left, right) =>
      left && right
    }
    val columnsToDrop = joinDetails.map { joinDetail =>
      factEntityDF(joinDetail.factJoinColumn)
    }
    val excludedDF = joinedDF.filter(excludedCondition)
    val droppedExcludedDF = MiscFunctions.dropMultipleColumns(excludedDF, columnsToDrop)
    val includedDF = joinedDF.filter(!excludedCondition)
    val droppedIncludedDF = MiscFunctions.dropMultipleColumns(includedDF, columnsToDrop)
    IncludedAndExcludedDF(droppedIncludedDF, droppedExcludedDF)
  }
}
