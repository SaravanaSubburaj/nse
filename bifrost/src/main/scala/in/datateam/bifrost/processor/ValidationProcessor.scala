package in.datateam.bifrost.processor

import scala.reflect.runtime.{universe => ru}

import org.apache.spark.sql.Column
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.concat_ws
import org.apache.spark.sql.functions.lit

import org.apache.log4j.Logger

import in.datateam.bifrost.enums.ValidationEnums
import in.datateam.bifrost.utils.ValidAndInvalidDFCaseClass
import in.datateam.bifrost.validation.ValidatorColTrait
import in.datateam.bifrost.validation.ValidatorRowTrait
import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.ValidationUnit
import in.datateam.utils.enums.AuditEnums
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ValidationProcessor {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(
      config: Config,
      addedEmptyColumnsDF: DataFrame,
      sourceDFs: Map[String, DataFrame],
      partition: String,
      lineageCols: String*
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): ValidAndInvalidDFCaseClass = {
    validateDataFrame(
      config,
      addedEmptyColumnsDF,
      sourceDFs,
      partition,
      lineageCols: _*
    )
  }

  def validateDataFrame(
      config: Config,
      addedEmptyColumnsDF: DataFrame,
      sourceDFs: Map[String, DataFrame],
      partition: String,
      lineageCols: String*
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): ValidAndInvalidDFCaseClass = {
    val validDFCols: Seq[Column] = addedEmptyColumnsDF.schema.fields
      .map(structField => col(structField.name).as(structField.name))
      .toSeq
    val rawValidDF = addedEmptyColumnsDF
      .withColumn(ValidationEnums.VALID_FLAG, lit(true))
      .withColumn(ValidationEnums.VALIDATION_PROCESS_NAME, lit(""))
      .withColumn(ValidationEnums.INVALID_MESSAGE, lit(""))
    implicit val rawValidDFEncoder = RowEncoder(rawValidDF.schema)
    val emptyRawInvalidatedDF: DataFrame = spark
      .createDataFrame(spark.sparkContext.emptyRDD[Row], rawValidDF.schema)

    case class ValidAndInvalidDF(preDF: DataFrame, invalidDF: DataFrame)

    val rawPreAndInvalidDF = config.validationsDetails.get
      .foldLeft[ValidAndInvalidDF](
        ValidAndInvalidDF(rawValidDF, emptyRawInvalidatedDF)
      ) { (validAndInvalidDF: ValidAndInvalidDF, validationDetail: ValidationUnit) =>
        val handlerValue =
          s"in.datateam.bifrost.validation.${validationDetail.validationFunction}"
        val runtime = ru.runtimeMirror(this.getClass.getClassLoader)
        val module = runtime.staticModule(handlerValue)
        val objectInstance = runtime.reflectModule(module).instance
        val nonFilteredPostDF = objectInstance match {
          case validationFunction: ValidatorRowTrait =>
            validAndInvalidDF.preDF
              .map(
                row => validationFunction.process(row, validationDetail.columnName)
              )(rawValidDFEncoder)
          case validationFunction: ValidatorColTrait =>
            validationFunction.process(
              validAndInvalidDF.preDF,
              validationDetail.columnName
            )
          case _ =>
            throw new ClassNotFoundException(s"${handlerValue} is not an instance of ValidatorTrait")
        }

        val postDF = nonFilteredPostDF.filter(
          row => row.getAs[Boolean](ValidationEnums.VALID_FLAG)
        )
        val invDF = validAndInvalidDF.invalidDF
          .union(
            nonFilteredPostDF
              .filter(row => !row.getAs[Boolean](ValidationEnums.VALID_FLAG))
          )
        auditAndLineageManager.process(
          validAndInvalidDF.preDF,
          postDF,
          validationDetail.validationFunction,
          validationDetail.columnName,
          config,
          partition,
          lineageCols: _*
        )
        ValidAndInvalidDF(postDF, invDF)
      }

    val (rawValidatedDF, rawInvalidatedDF) =
      (rawPreAndInvalidDF.preDF, rawPreAndInvalidDF.invalidDF)

    val validDF = rawValidatedDF.select(validDFCols: _*)
    val invalidDF = rawInvalidatedDF
    val primaryCols = Array(
        lit(partition),
        lit(config.sourceDetails.head.entityName)
      ) ++ config.sourceDetails.head.primaryColumns.get
        .map(columnName => col(columnName))
    val invalidWithAllColumnsDF = invalidDF
      .withColumn(CommonEnums.ENTITY, lit(config.sourceDetails.head.entityName))
      .withColumn(AuditEnums.JOB_ID, lit(spark.sparkContext.applicationId))
      .withColumn(
        AuditEnums.ROW_ID,
        concat_ws(CommonEnums.DATA_DELIMITER, primaryCols.toSeq: _*)
      )
      .withColumn(
        AuditEnums.RECORDS_CONTENT,
        concat_ws(CommonEnums.DATA_DELIMITER, validDFCols: _*)
      )
      .withColumnRenamed(
        ValidationEnums.VALIDATION_PROCESS_NAME,
        AuditEnums.PROCESS_NAME
      )
      .withColumnRenamed(ValidationEnums.INVALID_MESSAGE, AuditEnums.REASON)
      .withColumnRenamed(
        CommonEnums.DATE_PARTITION_COLUMN,
        AuditEnums.DATE_PARTITION
      )
      .select(
        AuditEnums.JOB_ID,
        AuditEnums.ROW_ID,
        AuditEnums.PROCESS_NAME,
        AuditEnums.RECORDS_CONTENT,
        AuditEnums.REASON,
        CommonEnums.ENTITY,
        CommonEnums.DATE_PARTITION_COLUMN
      )
    ValidAndInvalidDFCaseClass(validDF, invalidWithAllColumnsDF)
  }

}
