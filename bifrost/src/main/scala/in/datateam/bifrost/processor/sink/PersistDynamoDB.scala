package in.datateam.bifrost.processor.sink

import scala.collection.JavaConverters._

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.row_number

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.PrimaryKey
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec
import com.amazonaws.services.dynamodbv2.model
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import org.apache.hadoop.dynamodb.DynamoDBItemWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.mapred.JobConf
import org.apache.log4j.Logger

import in.datateam.utils.InvalidConfigException
import in.datateam.utils.InvalidRecordTypeException
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.StorageDetail
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.enums.DeltaEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object PersistDynamoDB extends PersistTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      dataFrame: DataFrame,
      config: Config,
      targetDetail: StorageDetail,
      partitionColumns: List[String]
    )(
      implicit spark: SparkSession
    ): Unit = {
    logger.info(
      s"Started storing ${targetDetail.recordType} for ${config.configType}."
    )
    val allOptionsMap: Map[String, String] = targetDetail.getDataFormatOptions
    val recordsCount = dataFrame.count()
    val dynamoDBConf = new JobConf(spark.sparkContext.hadoopConfiguration)
    setConfig(dynamoDBConf, allOptionsMap, recordsCount)
    // FIXME: Remove once duplicate issue to resolve.
    val partitionCol = config.targetSchema.head.columnName
    val orderCol = config.targetSchema.tail.head.columnName
    val windowSpec = Window.partitionBy(col(partitionCol)).orderBy(col(orderCol))
    val nonDuplicateDF = dataFrame
      .withColumn("rank", row_number().over(windowSpec))
      .where(col("rank") === 1)
      .drop(col("rank"))
      .drop(CommonEnums.DATE_PARTITION_COLUMN)

    targetDetail.recordType match {
      case CommonEnums.VALID_RECORDS =>
        val dynamoDBRDD: RDD[(Text, DynamoDBItemWritable)] =
          getDynamoDBRDD(config, nonDuplicateDF)(spark)
        dynamoDBRDD.saveAsHadoopDataset(dynamoDBConf)

        deleteRecords(nonDuplicateDF, allOptionsMap)

      case _ =>
        throw InvalidRecordTypeException(
          s"Expected one of '${CommonEnums.VALID_RECORDS}'  but got" +
          s" ${targetDetail.recordType}."
        )
    }
  }

  private[PersistDynamoDB] def setConfig(
      dynamoDBConf: JobConf,
      optionsMap: Map[String, String],
      recordsCount: Long
    )(
      implicit Spark: SparkSession
    ): Unit = {
    // A single BatchWriteItem operation can contain up to 25 PutItem
    val numberOfSplits = recordsCount / 25
    logger.info(s"Number of splits for dynamo ${numberOfSplits}")
    dynamoDBConf.set("dynamodb.throughput.write.percent", "1.0")
    dynamoDBConf.set("dynamodb.numberOfSplits", numberOfSplits.toString)
    dynamoDBConf.set(
      "mapred.output.committer.class",
      "org.apache.hadoop.mapred.FileOutputCommitter"
    )
    dynamoDBConf.set(
      "mapred.input.format.class",
      "org.apache.hadoop.dynamodb.read.DynamoDBInputFormat"
    )
    dynamoDBConf.set(
      "mapred.output.format.class",
      "org.apache.hadoop.dynamodb.write.DynamoDBOutputFormat"
    )
    optionsMap.foreach {
      case (dataFormatOptionKey, dataFormatOptionValue) =>
        dynamoDBConf.set(dataFormatOptionKey, dataFormatOptionValue)
    }
  }

  override def validateConfig(storageDetail: StorageDetail)(implicit spark: SparkSession): Unit = {
    storageDetail.dataFormatOption match {
      case Some(dataFormatOption) =>
        val mandatoryKeys = Array(
          "dynamodb.output.tableName",
          "dynamodb.endpoint",
          "dynamodb.regionid"
        )
        val availableKeys = dataFormatOption.map(op => op.key)
        mandatoryKeys.foreach({ key =>
          if (!availableKeys.contains(key)) {
            throw InvalidConfigException(
              s"${key} is expected in dataFormatOption."
            )
          }
        })
      case None =>
        throw InvalidConfigException(s"dataFormatOption is expected.")
    }
  }

  private[PersistDynamoDB] def getDynamoDBRDD(
      config: Config,
      dataFrame: DataFrame
    )(
      Spark: SparkSession
    ): RDD[(Text, DynamoDBItemWritable)] = {
    val nonDeletedRecordsDF =
      if (dataFrame.columns.contains(CommonEnums.ACTIVE_SUBS_IND)) {
        val requiredColumns = dataFrame.columns
          .filter(
            columnName => !Array(DeltaEnums.RECORD_MODIFIER_COLUMN, CommonEnums.ACTIVE_SUBS_IND).contains(columnName)
          )
          .map(columnName => col(columnName))
        dataFrame
          .filter(col(CommonEnums.ACTIVE_SUBS_IND) =!= CommonEnums.ACTIVE_SUBS_DELETE)
          .select(requiredColumns: _*)
      } else {
        dataFrame
      }

    val columnName = config.getAllColumns(1)
    val repartitionedDF = nonDeletedRecordsDF.repartition(col(columnName))

    val schema = repartitionedDF.dtypes
    val rdd: RDD[(Text, DynamoDBItemWritable)] =
      repartitionedDF.rdd.mapPartitions { iter =>
        iter.map { row =>
          val rowMap: Map[String, AttributeValue] = schema.filter { x =>
            if (row.isNullAt(row.fieldIndex(x._1))) {
              false
            } else {
              val value = row.get(row.fieldIndex(x._1)).toString
              value.trim.nonEmpty
            }
          }.map { x =>
            val value = row.get(row.fieldIndex(x._1)).toString
            val attribute = new model.AttributeValue()
            if (x._2 == "double" || x._2 == "long" || x._2 == "int" || x._2
                  .startsWith("decimal")) {
              attribute.setN(value)
            } else {
              attribute.setS(value)
            }
            (x._1, attribute)
          }.toMap
          val item = new DynamoDBItemWritable()
          item.setItem(rowMap.asJava)

          (new Text(""), item)
        }
      }
    rdd
  }
  private[PersistDynamoDB] def getDynamoDBTable(
      dynamoDBEndpoint: String,
      dynamoDBRegion: String,
      dynamoDBTable: String
    ): Table = {
    val config = new AwsClientBuilder.EndpointConfiguration(
      dynamoDBEndpoint,
      dynamoDBRegion
    )
    val client = AmazonDynamoDBClientBuilder.standard
      .withEndpointConfiguration(config)
      .build
    val dynamoDB = new DynamoDB(client)
    val table = dynamoDB.getTable(dynamoDBTable)
    table
  }

  private[PersistDynamoDB] def deleteRecords(
      dataFrame: DataFrame,
      allOptionsMap: Map[String, String]
    )(
      implicit spark: SparkSession
    ): Unit = {
    val deletedRecordsDF = dataFrame
      .filter(col(CommonEnums.ACTIVE_SUBS_IND) === CommonEnums.ACTIVE_SUBS_DELETE)

    val dynamoDBEndpoint = allOptionsMap("dynamodb.endpoint")
    val dynamoDBRegion = allOptionsMap("dynamodb.regionid")
    val dynamoDBTable = allOptionsMap("dynamodb.output.tableName")
    implicit val deletedRecordsDFEncoder = RowEncoder(deletedRecordsDF.schema)
    deletedRecordsDF.foreachPartition { iterator =>
      iterator.foreach({ row =>
        val table = getDynamoDBTable(dynamoDBEndpoint, dynamoDBRegion, dynamoDBTable)
        val tableDescription = table.describe()
        val primaryColumns =
          tableDescription.getKeySchema.asScala.map(x => x.getAttributeName)
        val primaryKey = new PrimaryKey(
          primaryColumns.head,
          row.get(row.fieldIndex(primaryColumns.head))
        )
        val deleteItemSpec = new DeleteItemSpec()
          .withPrimaryKey(primaryKey)
        table.deleteItem(deleteItemSpec)
      })
    }
  }
}
