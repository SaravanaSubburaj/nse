package in.datateam.bifrost.processor.sink

import java.nio.ByteBuffer
import java.nio.charset.Charset

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession

import com.amazonaws.services.kms.AWSKMSClientBuilder
import com.amazonaws.services.kms.model.DecryptRequest
import com.amazonaws.util.Base64
import org.apache.log4j.Logger

import in.datateam.bifrost.enums.SourceEnums
import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.utils.InvalidConfigException
import in.datateam.utils.InvalidRecordTypeException
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.StorageDetail
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object PersistRedshift extends PersistTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      dataFrame: DataFrame,
      config: Config,
      targetDetail: StorageDetail,
      partitionColumns: List[String]
    )(
      implicit spark: SparkSession
    ): Unit = {

    logger.info(
      s"Started storing ${targetDetail.recordType} for ${config.configType}."
    )
    val allOptionsMap: Map[String, String] = targetDetail.getDataFormatOptions
    val tempDir = MiscFunctions.getTemporaryDirectory(allOptionsMap(SourceEnums.TEMPDIR))
    val allOptionsWithTempDirMap: Map[String, String] = allOptionsMap.filter {
        case (_key, _) => _key != SourceEnums.TEMPDIR
      } ++ Array((SourceEnums.TEMPDIR, tempDir))
    val sparkWriteOptions = getSparkWriteOptions(allOptionsWithTempDirMap)
    targetDetail.recordType match {
      case CommonEnums.VALID_RECORDS =>
        dataFrame
          .coalesce(150)
          .write
          .format("com.databricks.spark.redshift")
          .options(sparkWriteOptions)
          .mode(SaveMode.Append)
          .save()
      case _ =>
        throw InvalidRecordTypeException(
          s"Expected '${CommonEnums.VALID_RECORDS}' but got ${targetDetail.recordType}."
        )
    }
    // Manual Clean up required.
    // For more details got to
    // https://docs.databricks.com/spark/latest/data-sources/aws/amazon-redshift.html#authenticating-to-s3-and-redshift
    FileSystemUtils.deletePath(tempDir, false)
  }

  private[PersistRedshift] def getSparkWriteOptions(allOptionsMap: Map[String, String]): Map[String, String] = {
    val client = AWSKMSClientBuilder.standard().build()
    val base64EncodedEncryptedPassword = allOptionsMap("encrypted_password")
    val base64DecodedEncryptedPassword =
      Base64.decode(base64EncodedEncryptedPassword)
    val decryptRequest = new DecryptRequest()
    decryptRequest.setCiphertextBlob(
      ByteBuffer.wrap(base64DecodedEncryptedPassword)
    )
    val decryptResult = client.decrypt(decryptRequest)
    val base64DecodedDecryptedPassword = decryptResult.getPlaintext
    val password = new String(
      base64DecodedDecryptedPassword.array(),
      Charset.forName("UTF-8")
    )
    val updatedOptions: Map[String, String] = allOptionsMap
        .filter(op => !Array("encrypted_password").contains(op._1)) + ("password" -> password)
    updatedOptions
  }

  override def validateConfig(storageDetail: StorageDetail)(implicit spark: SparkSession): Unit = {
    storageDetail.dataFormatOption match {
      case Some(dataFormatOption) =>
        val mandatoryKeys =
          Array(
            "url",
            "dbtable",
            "tempdir",
            "aws_iam_role",
            "user",
            "encrypted_password"
          )
        val availableKeys = dataFormatOption.map(op => op.key)
        mandatoryKeys.foreach({ key =>
          if (!availableKeys.contains(key)) {
            throw InvalidConfigException(
              s"${key} is expected in dataFormatOption."
            )
          }
        })
      case None =>
        throw InvalidConfigException(s"dataFormatOption is expected.")
    }
  }
}
