package in.datateam.bifrost.processor

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.apache.log4j.Logger

import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.DeltaEnums

object DeltaProcessor {

  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(
      config: Config,
      profileDF: DataFrame,
      partitionValue: String,
      primaryKey: String,
      deltaDate: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {

    val previousProfileDF = MiscFunctions.readProfile(config, deltaDate)

    if (!previousProfileDF.take(1).isEmpty) {
      calculateDelta(profileDF, previousProfileDF, primaryKey, partitionValue)
    } else {
      logger.info("Previous or specified profile partition is found empty.")
      profileDF.withColumn(
        DeltaEnums.RECORD_MODIFIER_COLUMN,
        lit(DeltaEnums.INSERT)
      )
    }
  }

  def calculateDelta(
      currentDF: DataFrame,
      previousDF: DataFrame,
      primaryKey: String,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val rawJoinedDF = currentDF
      .join(
        previousDF,
        currentDF(primaryKey) === previousDF(primaryKey),
        "full_outer"
      )
    val totalColumns = currentDF.columns.length
    val primaryKeyIndexLeft = currentDF.columns.indexOf(primaryKey)
    val primaryKeyIndexRight = totalColumns + currentDF.columns.indexOf(
        primaryKey
      )
    val currentDFIndex = (0 until (totalColumns - 1)).toArray
    val previousDFIndex = (totalColumns until (2 * totalColumns - 1)).toArray
    val deltaDFSchema = StructType(
      currentDF.schema.fields
      ++ Array(StructField(DeltaEnums.RECORD_MODIFIER_COLUMN, StringType))
    )
    implicit val deletedRecordsDFEncoder = RowEncoder(deltaDFSchema)

    val joinedDF = rawJoinedDF.flatMap { row =>
      if (row.isNullAt(primaryKeyIndexLeft)) {
        //          Deleted
        val rowContent: Array[Any] = previousDFIndex
            .map(index => row.get(index)) ++ Array(
            partitionValue,
            DeltaEnums.DELETE
          )
        val selectedRow = Row(rowContent: _*)
        List(selectedRow)
      } else if (row.isNullAt(primaryKeyIndexRight)) {
        //          Inserted
        val rowContent: Array[Any] = currentDFIndex
            .map(index => row.get(index)) ++ Array(
            partitionValue,
            DeltaEnums.INSERT
          )
        val selectedRow = Row(rowContent: _*)
        List(selectedRow)
      } else {
        //          Check
        val isUpdatedFlag = !currentDFIndex
          .map(index => row.get(index) == row.get(totalColumns + index))
          .reduce(_ && _)
        if (isUpdatedFlag) {
          val rowContentBefore: Array[Any] = previousDFIndex
              .map(index => row.get(index)) ++ Array(
              partitionValue,
              DeltaEnums.BEFORE
            )
          val selectedRowBefore = Row(rowContentBefore: _*)
          val rowContentAfter: Array[Any] = currentDFIndex
              .map(index => row.get(index)) ++ Array(
              partitionValue,
              DeltaEnums.AFTER
            )
          val selectedRowAfter = Row(rowContentAfter: _*)
          List(selectedRowBefore, selectedRowAfter)
        } else {
          val rowContent: Array[Any] = previousDFIndex
              .map(index => row.get(index)) ++ Array(
              partitionValue,
              DeltaEnums.NO_CHANGE
            )
          val selectedRow = Row(rowContent: _*)
          List(selectedRow)
        }
      }
    }

    val validColumnModifiersList = Array(
      DeltaEnums.INSERT,
      DeltaEnums.DELETE,
      DeltaEnums.BEFORE,
      DeltaEnums.AFTER
    )
    val deltaDF = joinedDF
      .filter(
        col(DeltaEnums.RECORD_MODIFIER_COLUMN)
          .isin(validColumnModifiersList: _*)
      )
    deltaDF
  }

}
