package in.datateam.bifrost.processor.sink

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.StorageDetail

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
trait PersistTrait {

  def process(
      dataFrame: DataFrame,
      config: Config,
      targetDetail: StorageDetail,
      partitionColumns: List[String]
    )(
      implicit spark: SparkSession
    ): Unit

  def validateConfig(storageDetail: StorageDetail)(implicit spark: SparkSession): Unit
}
