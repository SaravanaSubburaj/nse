package in.datateam.bifrost.processor.source

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.bifrost.enums.SourceEnums
import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.SourceDetails
import in.datateam.utils.helper.FileSystemUtils

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ReadRedshift {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(
      config: Config,
      sourceDetail: SourceDetails,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {

    logger.info(
      s"Reading ${sourceDetail.entityName} from redshift table for ${partitionValue}"
    )
    val allColumns = config.columnMapping.map(columnMappingUnit => columnMappingUnit.sourceColumn)
    val allOptions: Map[String, String] = sourceDetail.getDataFormatOptions
    val encryptedPassword = allOptions(SourceEnums.ENCRYPTED_PASSWORD)
    val decryptedPassword = MiscFunctions.decryptPasswordFromKMS(encryptedPassword)
    val tempDir = MiscFunctions.getTemporaryDirectory(allOptions(SourceEnums.TEMPDIR))
    val filteredCondition = sourceDetail.partitionDetails match {
      case Some(partitionDetails) =>
        MiscFunctions.createPartitionCondition(partitionDetails, partitionValue)
      case None => ""
    }
    val tableName = allOptions(SourceEnums.DBTABLE)
    val query = if (filteredCondition.size > 0) {
      s"SELECT ${allColumns.mkString(", ")} FROM ${tableName} WHERE ${filteredCondition}"
    } else {
      s"SELECT ${allColumns.mkString(", ")} FROM ${tableName}"
    }

    val allOptionsWithExtraOptions: Map[String, String] = allOptions.filter {
        case (_key, _) =>
          !Array(SourceEnums.ENCRYPTED_PASSWORD, SourceEnums.DBTABLE, SourceEnums.TEMPDIR).contains(_key)
      } ++ Array((SourceEnums.PASSWORD, decryptedPassword), (SourceEnums.QUERY, query), (SourceEnums.TEMPDIR, tempDir))
    val inputDF = spark.read
      .format(sourceDetail.dataFormat.get)
      .options(allOptionsWithExtraOptions)
      .load()
    FileSystemUtils.deletePath(tempDir)
    inputDF
  }
}
