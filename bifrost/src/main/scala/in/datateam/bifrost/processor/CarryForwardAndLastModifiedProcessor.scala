package in.datateam.bifrost.processor

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.parser.CatalystSqlParser
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.apache.log4j.Logger

import in.datateam.bifrost.utils.CarryForwardDFAndColumnsCaseClass
import in.datateam.bifrost.utils.CarryForwardDFAndLastModifiedDFCaseClass
import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object CarryForwardAndLastModifiedProcessor {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(
      config: Config,
      profileDF: DataFrame,
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): CarryForwardDFAndLastModifiedDFCaseClass = {

    val previousProfileDF =
      MiscFunctions.readPreviousProfile(config, partitionValue)
    val carryForwardDFAndColumns = if (!previousProfileDF.take(1).isEmpty) {
      populateProfile(config, profileDF, previousProfileDF, partitionValue)
    } else {
      logger.info("Previous profile partition is found empty.")
      CarryForwardDFAndColumnsCaseClass(profileDF, Nil)
    }
    val previousPartitionValue = previousProfileDF
      .select(CommonEnums.DATE_PARTITION_COLUMN)
      .take(1)
      .map(x => x.getString(0))
      .headOption
      .getOrElse(partitionValue)

    val previousLastModifiedDF = MiscFunctions.readPreviousLastModifiedDF(config, partitionValue)
    val lastModifiedDF = populateLastModifiedDF(
      config,
      previousLastModifiedDF,
      carryForwardDFAndColumns.carryForwardedColumns,
      partitionValue,
      previousPartitionValue
    )
    CarryForwardDFAndLastModifiedDFCaseClass(carryForwardDFAndColumns.carryForwardedDF, lastModifiedDF)
  }

  def populateProfile(
      config: Config,
      profileDF: DataFrame,
      previousProfileAllColumnDF: DataFrame,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): CarryForwardDFAndColumnsCaseClass = {
    val carryForwardColumns = config.targetSchema
      .filter(targetSchema => targetSchema.carryForwardFlag == "true")
      .map(targetSchema => targetSchema.columnName)

    val allNullColumns = carryForwardColumns.filter { columnName =>
      val notNullCount: Boolean = profileDF
        .select(col(columnName))
        .filter(col(columnName).isNotNull)
        .take(1)
        .isEmpty
      notNullCount
    }
    if (allNullColumns.length > 0) {
      logger.info(
        s"Carry forwarding for these columns ${allNullColumns.mkString(", ")}."
      )
      val carryForwardJoinColumns = config.targetSchema
        .filter(targetSchema => targetSchema.carryForwardJoinFlag == "true")
        .map(targetSchema => targetSchema.columnName)
      val allColumnsToSelect = allNullColumns ++ carryForwardJoinColumns
      val previousProfileDF = previousProfileAllColumnDF
        .select(allColumnsToSelect.map(column => col(column)): _*)
      val allColumnsProfileDF = profileDF
        .join(previousProfileDF, carryForwardJoinColumns, "left_outer")
      val baseCarryForwardDFAndColumns = CarryForwardDFAndColumnsCaseClass(allColumnsProfileDF, Nil)
      val carryForwardDFAndColumns =
        allNullColumns.foldLeft(baseCarryForwardDFAndColumns) { (tempCarryForwardDFAndColumns, col) =>
          val addedNewColumn = tempCarryForwardDFAndColumns.carryForwardedColumns ++ List(col)
          CarryForwardDFAndColumnsCaseClass(
            tempCarryForwardDFAndColumns.carryForwardedDF.drop(profileDF(col)),
            addedNewColumn
          )
        }
      val carryForwardedDF = carryForwardDFAndColumns.carryForwardedDF
      val selectedColumnsDF = carryForwardedDF
        .select(
          config.targetSchema
            .map(targetSchemaUnit => col(targetSchemaUnit.columnName)): _*
        )
        .withColumn(CommonEnums.DATE_PARTITION_COLUMN, lit(partitionValue))
      CarryForwardDFAndColumnsCaseClass(selectedColumnsDF, carryForwardDFAndColumns.carryForwardedColumns)
    } else {
      logger.info(
        "No columns found null in current profile.\nSkipping carry forward."
      )
      CarryForwardDFAndColumnsCaseClass(profileDF, Nil)
    }
  }

  def populateLastModifiedDF(
      config: Config,
      previousLastModifiedDF: DataFrame,
      carryForwardedColumns: List[String],
      partitionValue: String,
      previousPartitionValue: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val allColumns = config.targetSchema.map(targetSchemaUnit => targetSchemaUnit.columnName)
    val schema = StructType(allColumns.map { column =>
      StructField(column, CatalystSqlParser.parseDataType(CommonEnums.LAST_MODIFIED_COLUMN_DATA_TYPE))
    })
    val previousLastModifiedMap = previousLastModifiedDF
      .take(1)
      .map(row => row.getValuesMap[String](allColumns))
      .headOption
      .getOrElse(Map[String, String]())

    val rawLastModifiedList = allColumns.map { columnName =>
      if (carryForwardedColumns.contains(columnName)) {
        previousLastModifiedMap.getOrElse(columnName, previousPartitionValue)
      } else {
        partitionValue
      }
    }
    val row = List(Row(rawLastModifiedList: _*))
    val rawLastModifiedDF = spark.createDataFrame(spark.sparkContext.parallelize(row), schema)
    val lastModifiedDF = rawLastModifiedDF
      .withColumn(CommonEnums.DATE_PARTITION_COLUMN, lit(partitionValue))
      .withColumn(CommonEnums.ENTITY, lit(CommonEnums.PROFILE_MASTER))
    lastModifiedDF
  }

}
