package in.datateam.bifrost.processor

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser.ColumnMappingUnit
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.AuditEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ColumnMappingProcessor {

  def columnMappingDataFrames(
      config: Config,
      sourceDFs: Map[String, DataFrame],
      partition: String,
      lineageCols: String*
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {
    val entityColumnsMap = config.columnMapping
      .map(
        columnMapping => (columnMapping.sourceEntity, columnMapping.sourceColumn)
      )
      .groupBy(_._1)
      .mapValues(_.map(_._2).toList)

    val entityJoinKeyMap = config.sourceDetails
      .map(
        sourceDetail => (sourceDetail.entityName, sourceDetail.joinColumn.get)
      )
      .filter(p => entityColumnsMap.get(p._1).isDefined)

    val sourceDFCols =
      if (entityColumnsMap(entityJoinKeyMap.head._1)
            .contains(entityJoinKeyMap.head._2)) {
        entityColumnsMap(entityJoinKeyMap.head._1)
      } else {
        entityJoinKeyMap.head._2 :: entityColumnsMap(entityJoinKeyMap.head._1)
      }
    val sourceDF = sourceDFs(entityJoinKeyMap.head._1)
      .select(sourceDFCols.map(columnName => col(columnName.toString)): _*)
    val sourceJoinKey = entityJoinKeyMap.head._2
    val joinedDF = entityJoinKeyMap.tail.foldLeft(sourceDF) { (leftDF: DataFrame, rightDetail: (String, String)) =>
      val rightDFCols =
        if (entityColumnsMap(rightDetail._1).contains(rightDetail._2)) {
          entityColumnsMap(rightDetail._1)
        } else {
          rightDetail._2 :: entityColumnsMap(rightDetail._1)
        }
      val rightDF = sourceDFs(rightDetail._1)
        .select(rightDFCols.map(columnName => col(columnName.toString)): _*)
      leftDF
        .join(rightDF, leftDF(sourceJoinKey) === rightDF(rightDetail._2))
        .drop(rightDF(rightDetail._2))
    }
    val renamedColumnDF = config.columnMapping.foldLeft(joinedDF) {
      (df: DataFrame, columnMappingUnit: ColumnMappingUnit) =>
        df.withColumnRenamed(
          columnMappingUnit.sourceColumn,
          columnMappingUnit.targetColumn
        )
    }
    val emptyDF = spark.createDataFrame(
      spark.sparkContext.emptyRDD[Row],
      renamedColumnDF.schema
    )
    auditAndLineageManager.process(
      emptyDF,
      renamedColumnDF,
      AuditEnums.COLUMN_MAPPING_FUNCTION_NAME,
      "",
      config,
      partition,
      lineageCols: _*
    )
    renamedColumnDF
  }

  def columnMappingDataFrame(config: Config, sourceDF: DataFrame)(implicit spark: SparkSession): DataFrame = {
    val columnMappedDF = config.columnMapping.foldLeft[DataFrame](sourceDF) {
      (df: DataFrame, mappedCol: ColumnMappingUnit) =>
        df.withColumnRenamed(mappedCol.sourceColumn, mappedCol.targetColumn)
    }
    columnMappedDF
  }
}
