package in.datateam.bifrost.processor.source

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.bifrost.utils.MiscFunctions
import in.datateam.utils.configparser.SourceDetails

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ReadHive {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(sourceDetail: SourceDetails, partitionValue: String)(implicit spark: SparkSession): DataFrame = {

    logger.info(
      s"Reading ${sourceDetail.entityName} from tables ${sourceDetail.databaseName.get}.${sourceDetail.tableName.get} for ${partitionValue}"
    )
    val tableName = s"${sourceDetail.databaseName.get}.${sourceDetail.tableName.get}"
    val inputDF = spark.table(tableName)
    val filteredDF = sourceDetail.partitionDetails match {
      case Some(partitionDetails) =>
        val partitionCondition = MiscFunctions.createPartitionCondition(partitionDetails, partitionValue)
        inputDF.where(partitionCondition)
      case None => inputDF
    }
    filteredDF
  }
}
