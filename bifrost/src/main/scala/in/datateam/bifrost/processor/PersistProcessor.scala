package in.datateam.bifrost.processor

import scala.reflect.runtime.{universe => ru}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.bifrost.processor.sink.PersistTrait
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.StorageDetail
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object PersistProcessor {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def process(config: Config, dataFrame: DataFrame, targetDetail: StorageDetail)(implicit spark: SparkSession): Unit = {
    if (targetDetail.persistFlag == "false" && targetDetail.recordType != CommonEnums.VALID_RECORDS) {
      logger.info(s"Skipping storage for ${targetDetail.recordType}.")
    } else {
      val partitionColumns =
        getPartitionColumns(config, targetDetail.recordType)
      val handlerValue =
        s"in.datateam.bifrost.processor.sink.${targetDetail.storageHandler}"
      val runtime = ru.runtimeMirror(this.getClass.getClassLoader)
      val module = runtime.staticModule(handlerValue)
      val objectInstance = runtime.reflectModule(module).instance
      objectInstance match {
        case persistFunction: PersistTrait =>
          persistFunction.process(
            dataFrame,
            config,
            targetDetail,
            partitionColumns
          )
        case _ =>
          throw new ClassNotFoundException(
            handlerValue + " is not an instance of " + classOf[PersistTrait].getName
          )
      }
    }
  }

  def getPartitionColumns(config: Config, recordType: String)(implicit spark: SparkSession): List[String] = {
    val partitionColumns: List[String] = recordType match {
      case CommonEnums.VALID_RECORDS | CommonEnums.DELTA_RECORDS =>
        config.partitionColumns match {
          case Some(partitionColumns) =>
            CommonEnums.DATE_PARTITION_COLUMN :: partitionColumns.toList
          case None =>
            List(CommonEnums.DATE_PARTITION_COLUMN)
        }
      case CommonEnums.INVALID_RECORDS | CommonEnums.AUDIT_RECORDS | CommonEnums.LINEAGE_RECORDS |
          CommonEnums.LAST_MODIFIED_RECORDS =>
        List(CommonEnums.DATE_PARTITION_COLUMN, CommonEnums.ENTITY)
      case CommonEnums.EXCLUSION_RECORDS => List()
    }
    partitionColumns
  }

  def validateConfig(config: Config)(implicit spark: SparkSession): Unit = {
    config.storageDetails.foreach { storageDetail: StorageDetail =>
      val handlerValue =
        s"in.datateam.bifrost.processor.sink.${storageDetail.storageHandler}"
      val runtime = ru.runtimeMirror(this.getClass.getClassLoader)
      val module = runtime.staticModule(handlerValue)
      val objectInstance = runtime.reflectModule(module).instance
      objectInstance match {
        case persistFunction: PersistTrait =>
          persistFunction.validateConfig(storageDetail)
        case _ =>
          throw new ClassNotFoundException(
            handlerValue + " is not an instance of " + classOf[PersistTrait].getName
          )
      }
    }
  }

}
