package in.datateam.bifrost.validation

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

object DateFormatValidator extends ValidatorColTrait with Serializable {

  val invalidMsgString = "Incorrect format, change to yyyy-MM-dd"

  def processRow(row: Row, column: String): Row = {
    val schema = row.schema
    val flag: Boolean = Try(
      LocalDate.parse(
        row.getAs[String](column),
        DateTimeFormatter.ofPattern("yyyy-MM-dd")
      )
    ).isSuccess

    val validatedArray = schema.fields.map { field: StructField =>
      field.name match {
        case `validFlag` => flag
        case `validationProcessName` => "DateFormatValidator"
        case `invalidMessage` => if (flag) "" else invalidMsgString
        case x: Any => row.getAs[Any](x)
      }
    }

    val validatedRow = new GenericRowWithSchema(validatedArray, schema)
    validatedRow
  }

  override def process(sourceDF: DataFrame, column: String): DataFrame = {
    implicit val sourceDFencoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => processRow(row, column))
  }

}
