package in.datateam.bifrost.validation

import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object NotNullValidator extends ValidatorRowTrait with Serializable {

  override def process(row: Row, column: String): Row = {
    val rowSchema = row.schema
    val validatedArray: Array[Any] = rowSchema.fields.map { structField: StructField =>
      structField.name match {
        case `validFlag` => !row.isNullAt(row.fieldIndex(column))
        case `validationProcessName` => "NotNullValidator"
        case `invalidMessage` =>
          if (row.isNullAt(row.fieldIndex(column))) "Field is found null."
          else ""
        case x: Any => row.getAs[Any](x)
      }
    }
    val validatedRow = new GenericRowWithSchema(validatedArray, rowSchema)
    validatedRow
  }

}
