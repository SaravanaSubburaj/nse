package in.datateam.bifrost.validation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

object ActiveSubscriberValidator extends ValidatorColTrait with Serializable {

  val invalidMsgString = "Inactive subscriber"

  def processRow(row: Row, column: String): Row = {
    val schema = row.schema
    val rowValue = row.getAs[String](column)
    val inActiveFlag = Array("c", "l", "t")
    val flag: Boolean = !inActiveFlag.contains(rowValue.trim.toLowerCase)

    val validatedArray = schema.fields.map { field: StructField =>
      field.name match {
        case `validFlag` => flag
        case `validationProcessName` => "ActiveSubscriberValidator"
        case `invalidMessage` => if (flag) "" else invalidMsgString
        case x: Any => row.getAs[Any](x)
      }
    }

    val validatedRow = new GenericRowWithSchema(validatedArray, schema)
    validatedRow
  }

  override def process(sourceDF: DataFrame, column: String): DataFrame = {
    implicit val sourceDFencoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => processRow(row, column))
  }

}
