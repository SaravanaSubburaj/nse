package in.datateam.bifrost.validation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
trait ValidatorTrait

trait ValidatorRowTrait extends ValidatorTrait {
  val validFlag: String = "validFlag"
  val validationProcessName: String = "validationProcessName"
  val invalidMessage: String = "invalidMessage"

  def process(row: Row, column: String): Row
}

trait ValidatorColTrait extends ValidatorTrait {
  val validFlag: String = "validFlag"
  val validationProcessName: String = "validationProcessName"
  val invalidMessage: String = "invalidMessage"

  def process(sourceDF: DataFrame, column: String): DataFrame
}
