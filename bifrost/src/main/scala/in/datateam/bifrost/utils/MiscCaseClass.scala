package in.datateam.bifrost.utils

import org.apache.spark.sql.DataFrame

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
case class InvalidDFCaseClass(
    jobId: String,
    rowId: String,
    processName: String,
    recordsContent: String,
    reason: String,
    entity: String,
    partition: String)

case class ValidAndInvalidDFCaseClass(validDF: DataFrame, InvalidDF: DataFrame)
case class ValidAndInvalidDF(preValidDF: DataFrame, preInvalidDF: DataFrame)
case class CarryForwardDFAndColumnsCaseClass(carryForwardedDF: DataFrame, carryForwardedColumns: List[String])
case class CarryForwardDFAndLastModifiedDFCaseClass(carryForwardedDF: DataFrame, lastModifiedDF: DataFrame)

case class IncludedAndExcludedDF(includedDF: DataFrame, excludedDF: DataFrame)
