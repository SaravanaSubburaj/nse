package in.datateam.bifrost.utils

import java.nio.ByteBuffer
import java.nio.charset.Charset

import org.apache.spark.sql.Column
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.parser.CatalystSqlParser
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import com.amazonaws.services.kms.AWSKMSClientBuilder
import com.amazonaws.services.kms.model.DecryptRequest
import com.amazonaws.util.Base64
import org.apache.log4j.Logger

import in.datateam.bifrost.Bifrost
import in.datateam.fury.AuditAndLineageManager
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.ExtraOption
import in.datateam.utils.configparser.PartitionDetails
import in.datateam.utils.configparser.SourceDetails
import in.datateam.utils.configparser.TargetSchemaUnit
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils
import in.datateam.utils.helper.PartitionHelper

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object MiscFunctions {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def readPreviousProfile(
      config: Config,
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {

    val profileStorageDetail =
      config.getStorageDetails(CommonEnums.VALID_RECORDS).head
    val partitionFormat = Array("yyyy-MM-dd")
    val partitionColumn = Array(CommonEnums.DATE_PARTITION_COLUMN)
    val partitionDetails =
      PartitionDetails(partitionColumn, CommonEnums.DAILY, partitionFormat, 1)
    val sourceDetail = SourceDetails(
      CommonEnums.PROFILE_MASTER,
      CommonEnums.PROFILE,
      None,
      CommonEnums.FILESYSTEM,
      profileStorageDetail.dataFormat,
      profileStorageDetail.dataFormatOption,
      profileStorageDetail.fileSystem,
      profileStorageDetail.pathUrl,
      None,
      None,
      Some(partitionDetails),
      None,
      None,
      None,
      None,
      Some("true")
    )
    val prevPartitionValue =
      PartitionHelper.getPreviousPartition(partitionDetails, partitionValue)
    logger.info("Reading previous profile partition.")
    if (checkSourceExist(sourceDetail, prevPartitionValue)) {
      val previousDF = Bifrost.readDataFrame(config, sourceDetail, prevPartitionValue)
      val populatedColumns = previousDF.columns
      val emptyColumns = config.targetSchema.filterNot { targetSchemaUnit =>
        populatedColumns.contains(targetSchemaUnit.columnName)
      }
      val addedEmptyColumns: DataFrame = emptyColumns.foldLeft(previousDF) { (tempDF, schemaUnit) =>
        tempDF.withColumn(schemaUnit.columnName, lit("").cast(schemaUnit.dataType))
      }
      Bifrost.applyTargetSchema(config, addedEmptyColumns)
    } else {
      val allColumns = config.targetSchema ++ Array(
          TargetSchemaUnit(CommonEnums.DATE_PARTITION_COLUMN, "string", "false", "false")
        )
      val schemaEmptyDF = StructType(allColumns.map { targetSchemaUnit =>
        StructField(
          targetSchemaUnit.columnName,
          CatalystSqlParser.parseDataType(targetSchemaUnit.dataType)
        )
      })
      logger.info(
        "Previous profile partition not found. Returning empty dataframe."
      )
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
  }

  def readPreviousLastModifiedDF(
      config: Config,
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {

    if (config.getStorageDetails(CommonEnums.LAST_MODIFIED_RECORDS).size > 0) {
      val lastModifiedStorageDetails =
        config.getStorageDetails(CommonEnums.LAST_MODIFIED_RECORDS).head
      val partitionFormat = Array("yyyy-MM-dd")
      val partitionColumn = Array(CommonEnums.DATE_PARTITION_COLUMN)
      val partitionDetails =
        PartitionDetails(partitionColumn, CommonEnums.DAILY, partitionFormat, 1)
      val dataFormatOption = lastModifiedStorageDetails.dataFormat.get match {
        case CommonEnums.PARQUET =>
          if (lastModifiedStorageDetails.dataFormatOption.isDefined) {
            val addedMergeSchema = lastModifiedStorageDetails.dataFormatOption.get ++ Array(
                ExtraOption("mergeSchema", "true")
              )
            Some(addedMergeSchema)
          } else {
            Some(Array(ExtraOption("mergeSchema", "true")))
          }
        case _ => lastModifiedStorageDetails.dataFormatOption
      }
      val sourceDetail = SourceDetails(
        CommonEnums.LAST_MODIFIED_RECORDS,
        CommonEnums.ENTITY,
        None,
        CommonEnums.FILESYSTEM,
        lastModifiedStorageDetails.dataFormat,
        dataFormatOption,
        lastModifiedStorageDetails.fileSystem,
        lastModifiedStorageDetails.pathUrl,
        None,
        None,
        Some(partitionDetails),
        None,
        None,
        None,
        None,
        Some("true")
      )
      val prevPartitionValue =
        PartitionHelper.getPreviousPartition(partitionDetails, partitionValue)
      logger.info("Reading previous partition data for last modified time.")
      if (checkSourceExist(sourceDetail, prevPartitionValue)) {
        val lastModifiedTimeDF = Bifrost.readDataFrame(config, sourceDetail, prevPartitionValue)
        lastModifiedTimeDF
      } else {
        val schemaEmptyDF = StructType(config.targetSchema.map { targetSchemaUnit =>
          StructField(
            targetSchemaUnit.columnName,
            CatalystSqlParser.parseDataType(CommonEnums.LAST_MODIFIED_COLUMN_DATA_TYPE)
          )
        })
        logger.info(
          "Previous partition not found. Returning empty dataframe."
        )
        spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
      }
    } else {
      val schemaEmptyDF = StructType(config.targetSchema.map { targetSchemaUnit =>
        StructField(
          targetSchemaUnit.columnName,
          CatalystSqlParser.parseDataType(CommonEnums.LAST_MODIFIED_COLUMN_DATA_TYPE)
        )
      })
      logger.info(
        "Previous partition not found. Returning empty dataframe."
      )
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
  }

  def decryptPasswordFromKMS(encryptedPassword: String): String = {
    val client = AWSKMSClientBuilder.standard().build()
    val base64EncodedEncryptedPassword = encryptedPassword
    val base64DecodedEncryptedPassword =
      Base64.decode(base64EncodedEncryptedPassword)
    val decryptRequest = new DecryptRequest()
    decryptRequest.setCiphertextBlob(
      ByteBuffer.wrap(base64DecodedEncryptedPassword)
    )
    val decryptResult = client.decrypt(decryptRequest)
    val base64DecodedDecryptedPassword = decryptResult.getPlaintext
    val password = new String(
      base64DecodedDecryptedPassword.array(),
      Charset.forName("UTF-8")
    )
    password
  }

  def getTemporaryDirectory(rootTempPath: String): String = {
    val tempPath = s"${rootTempPath}${java.util.UUID.randomUUID.toString}"
    tempPath
  }

  def dropMultipleColumns(dataFrame: DataFrame, columns: Array[Column]): DataFrame = {
    val processedDF = columns.foldLeft(dataFrame) { (preDF: DataFrame, column: Column) =>
      preDF.drop(column)
    }
    processedDF
  }

  def createPartitionCondition(partitionDetails: PartitionDetails, partitionValue: String): String = {
    val allPartitions: List[String] =
      PartitionHelper.getAllPartitions(partitionDetails, partitionValue)
    val partitionConditionString = allPartitions.mkString(" OR ")

    partitionConditionString
  }

  def checkSourceExist(sourceDetail: SourceDetails, partitionValue: String)(implicit spark: SparkSession): Boolean = {
    sourceDetail.sourceType match {
      case CommonEnums.FILESYSTEM =>
        val sourcePath = sourceDetail.pathUrl.get
        val partitionPath = sourceDetail.partitionDetails match {
          case Some(_) =>
            PartitionHelper.getAllPartitions(
              sourceDetail.partitionDetails.get,
              partitionValue,
              isHDFS = true
            )
          case None => List("")
        }
        partitionPath.map { pPath =>
          val completeSourcePath = s"${sourcePath}/${pPath}"
          FileSystemUtils.checkPathExist(completeSourcePath)
        }.reduce((left, right) => left || right)
      case CommonEnums.DATABASE => true
    }
  }

  def readProfile(
      config: Config,
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {

    val profileStorageDetail =
      config.getStorageDetails(CommonEnums.VALID_RECORDS).head
    val partitionFormat = Array("yyyy-MM-dd")
    val partitionColumn = Array(CommonEnums.DATE_PARTITION_COLUMN)
    val partitionDetails =
      PartitionDetails(partitionColumn, CommonEnums.DAILY, partitionFormat, 1)
    val sourceDetail = SourceDetails(
      CommonEnums.PROFILE_MASTER,
      CommonEnums.PROFILE,
      None,
      CommonEnums.FILESYSTEM,
      profileStorageDetail.dataFormat,
      profileStorageDetail.dataFormatOption,
      profileStorageDetail.fileSystem,
      profileStorageDetail.pathUrl,
      None,
      None,
      Some(partitionDetails),
      None,
      None,
      None,
      None,
      Some("true")
    )
    logger.info("Reading specified profile partition.")
    if (checkSourceExist(sourceDetail, partitionValue)) {
      val specifiedDF = Bifrost.readDataFrame(config, sourceDetail, partitionValue)
      val populatedColumns = specifiedDF.columns
      val emptyColumns = config.targetSchema.filterNot { targetSchemaUnit =>
        populatedColumns.contains(targetSchemaUnit.columnName)
      }
      val addedEmptyColumns: DataFrame = emptyColumns.foldLeft(specifiedDF) { (tempDF, schemaUnit) =>
        tempDF.withColumn(schemaUnit.columnName, lit("").cast(schemaUnit.dataType))
      }
      Bifrost.applyTargetSchema(config, addedEmptyColumns)
    } else {
      val allColumns = config.targetSchema ++ Array(
          TargetSchemaUnit(CommonEnums.DATE_PARTITION_COLUMN, "string", "false", "false")
        )
      val schemaEmptyDF = StructType(allColumns.map { targetSchemaUnit =>
        StructField(
          targetSchemaUnit.columnName,
          CatalystSqlParser.parseDataType(targetSchemaUnit.dataType)
        )
      })
      logger.info(
        "Specified profile partition not found. Returning empty dataframe."
      )
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
  }
}
