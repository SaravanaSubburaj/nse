package in.datateam.bifrost.enums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object SourceEnums {
  val PASSWORD = "password"
  val QUERY = "query"
  val DBTABLE = "dbtable"
  val ENCRYPTED_PASSWORD = "encrypted_" + PASSWORD
  val TEMPDIR = "tempdir"

}
