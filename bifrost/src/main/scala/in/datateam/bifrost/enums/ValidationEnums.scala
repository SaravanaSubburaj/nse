package in.datateam.bifrost.enums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object ValidationEnums {
  val VALID_FLAG = "validFlag"
  val VALIDATION_PROCESS_NAME = "validationProcessName"
  val INVALID_MESSAGE = "invalidMessage"
  val PUBLISHED_FILTER = "published"
}
