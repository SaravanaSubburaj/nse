package in.datateam.bifrost

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types._

import org.scalatest.FlatSpec

import in.datateam.bifrost.processor.DeltaProcessor
import in.datateam.harness.SparkTest
import in.datateam.utils.enums.DeltaEnums

class DeltaTest extends FlatSpec with SparkTest {

  "DeltaTest" should "compute delta of previous and current partition" in withSparkSession { spark: SparkSession =>
    val previousPartitionValue = "2019-01-15"
    val previousProfileList = List(
      Row("1", "Virgie,Dean", "06/01/2019", 1, true, previousPartitionValue),
      Row(
        "2",
        "Mable,Underwood",
        "07/01/2019",
        2,
        false,
        previousPartitionValue
      ),
      Row(
        "3",
        "Lillie,Hernandez",
        "08/01/2019",
        5,
        true,
        previousPartitionValue
      ),
      Row(
        "4",
        "Violet,Gibson",
        "09/01/2019",
        6,
        false,
        previousPartitionValue
      )
    )

    val currentPartitionValue = "2019-01-16"
    val currentProfileList = List(
      Row("1", "Virgie,Dean", "06/01/2019", 3, true, currentPartitionValue),
      Row(
        "2",
        "Mable,Underwood",
        "07/01/2019",
        2,
        false,
        currentPartitionValue
      ),
      Row(
        "6",
        "Lillie,Hernandez",
        "08/01/2019",
        4,
        true,
        currentPartitionValue
      ),
      Row(
        "4",
        "Flora,Franklin",
        "09/01/2019",
        9,
        false,
        currentPartitionValue
      )
    )
    val expectedProfileSet = List(
      Row(
        "1",
        "Virgie,Dean",
        "06/01/2019",
        1,
        true,
        currentPartitionValue,
        DeltaEnums.BEFORE
      ),
      Row(
        "1",
        "Virgie,Dean",
        "06/01/2019",
        3,
        true,
        currentPartitionValue,
        DeltaEnums.AFTER
      ),
      Row(
        "3",
        "Lillie,Hernandez",
        "08/01/2019",
        5,
        true,
        currentPartitionValue,
        DeltaEnums.DELETE
      ),
      Row(
        "4",
        "Violet,Gibson",
        "09/01/2019",
        6,
        false,
        currentPartitionValue,
        DeltaEnums.BEFORE
      ),
      Row(
        "4",
        "Flora,Franklin",
        "09/01/2019",
        9,
        false,
        currentPartitionValue,
        DeltaEnums.AFTER
      ),
      Row(
        "6",
        "Lillie,Hernandez",
        "08/01/2019",
        4,
        true,
        currentPartitionValue,
        DeltaEnums.INSERT
      )
    )

    val schema = StructType(
      List(
        StructField("subscriberID", StringType, nullable = true),
        StructField("FullName", StringType, nullable = true),
        StructField("activationDate", StringType, nullable = true),
        StructField("yearsSinceActive", IntegerType, nullable = true),
        StructField("subscriberActiveStatus", BooleanType, nullable = true),
        StructField("date", StringType, nullable = true)
      )
    )

    val previousProfileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(previousProfileList),
      schema
    )
    val currentProfileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(currentProfileList),
      schema
    )
    val actualProfileSet = DeltaProcessor
      .calculateDelta(
        currentProfileDF,
        previousProfileDF,
        "subscriberID",
        currentPartitionValue
      )(spark)
      .sort(col("subscriberID"))
      .collect()
    assertResult(expectedProfileSet)(actualProfileSet)
  }

}
