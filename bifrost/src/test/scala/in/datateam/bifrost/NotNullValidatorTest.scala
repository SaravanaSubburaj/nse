package in.datateam.bifrost

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.types.BooleanType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.bifrost.enums.ValidationEnums
import in.datateam.bifrost.validation.NotNullValidator
import in.datateam.harness.SparkTest

/* *
 * Created by Niraj(niraj.kumar.das@thedatateam.in).
 * */
class NotNullValidatorTest extends FlatSpec with SparkTest {

  "NotNullValidatorTest" should "test NotNullValidator implementation" in withSparkSession { spark: SparkSession =>
    val sampleList = List(
      Row("251", "xxxx9698", true, ""),
      Row("252", null, true, ""),
      Row("253", "xxxx9698", true, "")
    )
    val schema = StructType(
      List(
        StructField("ID", StringType, nullable = true),
        StructField("Number", StringType, nullable = true),
        StructField(ValidationEnums.VALID_FLAG, BooleanType, nullable = true),
        StructField(
          ValidationEnums.INVALID_MESSAGE,
          StringType,
          nullable = true
        )
      )
    )
    val encoder = RowEncoder(schema)
    val sampleDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleList),
        schema
      )
    val validatedDF =
      sampleDF.map(row => NotNullValidator.process(row, "Number"))(encoder)
    val collectedValidatedDF = validatedDF.collect()

    val expectedCollectedDF = List(
      Row("251", "xxxx9698", true, ""),
      Row("252", null, false, "Field is found null."),
      Row("253", "xxxx9698", true, "")
    )
    assertResult(expectedCollectedDF)(collectedValidatedDF)
  }
}
