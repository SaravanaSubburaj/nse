package in.datateam.bifrost

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.BooleanType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.bifrost.enums.ValidationEnums
import in.datateam.bifrost.validation.FilterPublishedPartitionValidator
import in.datateam.harness.SparkTest

class FilterPublishedPartitionValidatorTest extends FlatSpec with SparkTest {

  "DateFormatValidatorTest" should "test FilterPublishedPartitionValidator implementation" in withSparkSession {
    spark: SparkSession =>
      val sampleList = List(
        Row("1", "Person_P", "1995-06-07", "published", true, ""),
        Row("2", "Person_C", "1995/02/14", "pending", true, ""),
        Row("3", "Person_M", "11/02/1994", "published", true, ""),
        Row("4", "Person_Z", "16-06-1994", "pending", true, "")
      )

      val schema = StructType(
        List(
          StructField("row_id", StringType, nullable = true),
          StructField("Name", StringType, nullable = true),
          StructField("DOB", StringType, nullable = true),
          StructField("p_state", StringType, nullable = true),
          StructField(
            ValidationEnums.VALID_FLAG,
            BooleanType,
            nullable = false
          ),
          StructField(
            ValidationEnums.INVALID_MESSAGE,
            StringType,
            nullable = false
          )
        )
      )

      val inputDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleList),
          schema
        )
      val validatedDF =
        FilterPublishedPartitionValidator.process(inputDF, "p_state")
      val collectedValidatedDF = validatedDF.collect()

      val resultDF = List(
        Row("1", "Person_P", "1995-06-07", "published", true, ""),
        Row(
          "2",
          "Person_C",
          "1995/02/14",
          "pending",
          false,
          "Incorrect partition column value"
        ),
        Row("3", "Person_M", "11/02/1994", "published", true, ""),
        Row(
          "4",
          "Person_Z",
          "16-06-1994",
          "pending",
          false,
          "Incorrect partition column value"
        )
      )

      assertResult(resultDF)(collectedValidatedDF)

  }
}
