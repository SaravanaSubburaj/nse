package in.datateam.bifrost

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.parser.CatalystSqlParser
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.bifrost.processor.CarryForwardAndLastModifiedProcessor
import in.datateam.harness.SparkTest
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums

/* *
 * Created by Niraj(niraj.kumar.das@thedatateam.in).
 * */
class CarryForwardAndLastModifiedTest extends FlatSpec with SparkTest {

  "CarryForwardAndLastModifiedTest" should "calculate carry forward previous day's data 1" in withSparkSession {
    spark: SparkSession =>
      val previousPartitionValue = "2018-12-06"
      val partitionValue = "2018-12-07"
      val profileList = List(
        Row("1", "sunil", null, null, null, partitionValue),
        Row("2", "xxxxxx", null, 2, null, partitionValue),
        Row("3", "yyyyyy", null, null, null, partitionValue),
        Row("4", "zzzzzz", null, null, null, partitionValue)
      )
      val previousProfileList = List(
        Row("1", "sunil", "12/01/2018", 1, true, previousPartitionValue),
        Row("2", "xxxxxx", "11/01/2018", 2, false, previousPartitionValue),
        Row("3", "yyyyyy", "11/12/2018", 9, false, previousPartitionValue),
        Row("4", "zzzzzz", "11/12/2017", 1, true, previousPartitionValue)
      )

      val previousLastModifiedList = List(
        Row(
          previousPartitionValue,
          previousPartitionValue,
          previousPartitionValue,
          previousPartitionValue,
          previousPartitionValue
        )
      )
      val expectedLastModifiedList = List(
        Row(partitionValue, partitionValue, previousPartitionValue, partitionValue, previousPartitionValue)
      )

      val expectedList = List(
        Row("1", "sunil", "12/01/2018", null, true, partitionValue),
        Row("2", "xxxxxx", "11/01/2018", 2, false, partitionValue),
        Row("3", "yyyyyy", "11/12/2018", null, false, partitionValue),
        Row("4", "zzzzzz", "11/12/2017", null, true, partitionValue)
      )

      val expectedCarryForwardedColumnList = List("activationDate", "subscriberActiveStatus")

      val configJson = "bifrost/src/test/resources/profile_config.json"
      val config = ConfigParser.getConfig(CommonEnums.PROFILE, configJson)
      val lastModifiedSchema = StructType(config.targetSchema.map { targetSchemaUnit =>
        StructField(
          targetSchemaUnit.columnName,
          CatalystSqlParser.parseDataType(CommonEnums.LAST_MODIFIED_COLUMN_DATA_TYPE)
        )
      })
      val previousLastModifiedDF = spark.createDataFrame(
        spark.sparkContext.parallelize(previousLastModifiedList),
        lastModifiedSchema
      )

      val profileSchema = StructType(config.targetSchema.map { targetSchemaUnit =>
        StructField(
          targetSchemaUnit.columnName,
          CatalystSqlParser.parseDataType(targetSchemaUnit.dataType)
        )
      })
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileSchema
      )
      val previousProfileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(previousProfileList),
        profileSchema
      )
      val carryForwardedDFAndColumns = CarryForwardAndLastModifiedProcessor
        .populateProfile(config, profileDF, previousProfileDF, partitionValue)(
          spark
        )
      val finalProfileDF = carryForwardedDFAndColumns.carryForwardedDF
      val actualCarryForwardedColumnList = carryForwardedDFAndColumns.carryForwardedColumns
      val lastModifiedDF = CarryForwardAndLastModifiedProcessor.populateLastModifiedDF(
        config,
        previousLastModifiedDF,
        actualCarryForwardedColumnList,
        partitionValue,
        previousPartitionValue
      )(spark)
      val actualLastModifiedList =
        lastModifiedDF.select(config.targetSchema.map(schemaUnit => col(schemaUnit.columnName)): _*).collect()

      val actualList = finalProfileDF.sort(config.targetSchema.head.columnName).collect()
      assertResult(expectedList)(actualList)
      assertResult(expectedCarryForwardedColumnList)(actualCarryForwardedColumnList)
      assertResult(expectedLastModifiedList)(actualLastModifiedList)
  }
  "CarryForwardAndLastModifiedTest" should "calculate carry forward previous day's data 2" in withSparkSession {
    spark: SparkSession =>
      val profileList = List(
        Row("1", "sunil", null, null, null),
        Row("2", "xxxxxx", null, 2, null),
        Row("3", "yyyyyy", null, null, null),
        Row("4", "zzzzzz", null, null, null)
      )
      val previousProfileList = List(
        Row("1", "sunil", "12/01/2018", 1, true),
        Row("2", "xxxxxx", "11/01/2018", 2, false),
        Row("3", "yyyyyy", "11/12/2018", 9, false),
        Row("4", "zzzzzz", "11/12/2017", 1, true)
      )

      val expectedList = List(
        Row("1", "sunil", null, null, null),
        Row("2", "xxxxxx", null, 2, null),
        Row("3", "yyyyyy", null, null, null),
        Row("4", "zzzzzz", null, null, null)
      )

      val configJson =
        "bifrost/src/test/resources/profile_config_no_carry_forward.json"
      val config = ConfigParser.getConfig(CommonEnums.PROFILE, configJson)
      val profileSchema = StructType(config.targetSchema.map { targetSchemaUnit =>
        StructField(
          targetSchemaUnit.columnName,
          CatalystSqlParser.parseDataType(targetSchemaUnit.dataType)
        )
      })
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileSchema
      )
      val previousProfileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(previousProfileList),
        profileSchema
      )
      val partitionValue = "2018-12-07"
      val finalProfileDF = CarryForwardAndLastModifiedProcessor
        .populateProfile(config, profileDF, previousProfileDF, partitionValue)(
          spark
        )
        .carryForwardedDF
        .sort(config.targetSchema.head.columnName)

      val actualList = finalProfileDF.collect()
      assertResult(expectedList)(actualList)
  }
}
