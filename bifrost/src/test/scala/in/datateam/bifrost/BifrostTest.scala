package in.datateam.bifrost

import scala.reflect.io.Path

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

import org.scalatest.FlatSpec

import in.datateam.bifrost.utils.ValidAndInvalidDFCaseClass
import in.datateam.fury.AuditAndLineageManager
import in.datateam.fury.AuditColumnsCaseClass
import in.datateam.fury.LineageColumnsCaseClass
import in.datateam.harness.SparkTest
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums

/* *
 * Created by Niraj(niraj.kumar.das@thedatateam.in).
 * */
class BifrostTest extends FlatSpec with SparkTest {

  "BifrostTest" should "calculate entity" in withSparkSession { spark: SparkSession =>
    val cleanUpDir = Path("bifrost/target/InvalidRecords")
    cleanUpDir.deleteRecursively()

    val lineageSchema = Encoders.product[LineageColumnsCaseClass].schema
    val auditsSchema = Encoders.product[AuditColumnsCaseClass].schema
    val lineage =
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], lineageSchema)
    val audits =
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], auditsSchema)
    val auditAndLineageManager = new AuditAndLineageManager(lineage, audits)
    auditAndLineageManager.initialize()(spark)

    val configJson =
      "bifrost/src/test/resources/subscriber_entity_config.json"
    val config = ConfigParser.getConfig(CommonEnums.ENTITY, configJson)
    val (_: ValidAndInvalidDFCaseClass, _: Map[String, DataFrame]) = Bifrost
      .process(config, "2019-01-16")(spark, auditAndLineageManager)
  }
}
