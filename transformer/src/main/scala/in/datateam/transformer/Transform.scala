package in.datateam.transformer

import scala.reflect.runtime.{universe => ru}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder

import org.apache.log4j.Logger

import in.datateam.fury.AuditAndLineageManager
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.transformation.TransformerAggColTrait
import in.datateam.transformer.transformation.TransformerAggTrait
import in.datateam.transformer.transformation.TransformerColTrait
import in.datateam.transformer.transformation.TransformerRowTrait
import in.datateam.utils.configparser.AttributeTransformationUnit
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object Transform {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def transformDataFrame(
      config: Config,
      targetDF: DataFrame,
      sourceDFs: Map[String, DataFrame],
      partition: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {
    val runtime = ru.runtimeMirror(this.getClass.getClassLoader)
    implicit val validDFEncoder = RowEncoder(targetDF.schema)
    val extraParameters = Map(
      TransformerEnums.PARTITION_VALUE -> partition
    )
    val transformedDF =
      config.transformationsDetails.zipWithIndex.foldLeft[DataFrame](targetDF) {
        (preDF: DataFrame, transformationDetailsWithIndex: (AttributeTransformationUnit, Int)) =>
          val (transformationDetails: AttributeTransformationUnit, index: Int) = transformationDetailsWithIndex
          val transformationOptionMap = transformationDetails.getTransformationOptionsMap()
          implicit val extraParametersWithTransformationOptions = extraParameters ++ transformationOptionMap

          val handlerValue =
            s"in.datateam.transformer.transformation.${transformationDetails.transformationFunction}"
          val module = runtime.staticModule(handlerValue)

          val lineageCols: List[String] = config.configType match {
            case CommonEnums.ENTITY =>
              List(CommonEnums.DATE_PARTITION_COLUMN) ++
              config.sourceDetails.head.primaryColumns.get.toList
            case CommonEnums.PROFILE =>
              List(
                CommonEnums.DATE_PARTITION_COLUMN,
                config.targetSchema.head.columnName
              )
          }
          val interval = 5
          val ipreDF =
            if (((index + 1) % interval == 0) && config.configType == CommonEnums.PROFILE) {
              // Writing data after calculation of 5 attributes to avoid memory issue in smaller executors.
              val att = config.transformationsDetails.toList
                .slice(index - interval, index)
              logger.info(
                s"Writing attributes ${att.map(x => x.attributeName).mkString(", ")}"
              )
              dagShortener(preDF, config)
            } else {
              preDF
            }

          val objectInstance = runtime.reflectModule(module).instance

          val postDF = objectInstance match {
            case transformationFunction: TransformerRowTrait =>
              ipreDF.map(
                row =>
                  transformationFunction
                    .process(
                      row,
                      transformationDetails.sourceColumns,
                      transformationDetails.attributeName
                    )
              )
            case transformationFunction: TransformerColTrait =>
              transformationFunction.process(
                ipreDF,
                transformationDetails.sourceColumns,
                transformationDetails.attributeName
              )(
                spark,
                extraParametersWithTransformationOptions,
                config.lookupDetails.getOrElse(Array()).toSeq: _*
              )
            case transformationFunction: TransformerAggColTrait =>
              transformationFunction.process(
                sourceDFs,
                ipreDF,
                transformationDetails.sourceColumns,
                transformationDetails.joinColumns,
                transformationDetails.attributeName
              )(spark, extraParametersWithTransformationOptions)
            case transformationFunction: TransformerAggTrait =>
              transformationFunction.process(
                ipreDF,
                ipreDF,
                transformationDetails.groupByColumns.get,
                transformationDetails.sourceColumns,
                transformationDetails.attributeColumns.get
              )(spark, extraParametersWithTransformationOptions)
            case _ =>
              throw new ClassNotFoundException(s"${handlerValue} is not an instance of TransformerTrait")

          }
          auditAndLineageManager.process(
            ipreDF,
            postDF,
            transformationDetails.transformationFunction,
            transformationDetails.attributeName,
            config,
            partition,
            lineageCols: _*
          )
          postDF
      }
    transformedDF
  }

  def process(
      config: Config,
      targetDF: DataFrame,
      sourceDFs: Map[String, DataFrame],
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {
    val updatedPartitionValue =
      config.sourceDetails.head.partitionDetails match {
        case Some(_) =>
          Misc.getFormattedPartitionDate(
            config.sourceDetails.head.partitionDetails.get.partitionFormat
              .mkString(CommonEnums.PARTITION_DELIMITER.toString),
            config.sourceDetails.head.partitionDetails.get.partitionType,
            partitionValue
          )
        case None => Misc.getFormattedPartitionDate(partitionValue)
      }
    logger.info(s"Writing direct columns")
    val intermediateDF = targetDF //dagShortener(targetDF, config)

    val transformedDF = transformDataFrame(
      config,
      intermediateDF,
      sourceDFs,
      updatedPartitionValue
    )
    if (config.configType == CommonEnums.ENTITY) {
      logger.info("Transformation Completed.")
    } else if (config.configType == CommonEnums.PROFILE) {
      logger.info("Attribute mapping Completed.")
    }
    transformedDF
  }

  def dagShortener(dataFrame: DataFrame, config: Config)(implicit spark: SparkSession): DataFrame = {
    //    val partitionColumns = List(CommonEnums.DATE_PARTITION_COLUMN)
    //    FileSystemUtils.writeTempData(dataFrame, config, partitionColumns)
    //    FileSystemUtils.readTempData(dataFrame.schema)
    dataFrame.persist()
    dataFrame.take(1)
    dataFrame
  }
}
