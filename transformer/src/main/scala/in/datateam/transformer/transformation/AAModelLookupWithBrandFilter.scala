package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object AAModelLookupWithBrandFilter extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 4 =>
        val partitionValue =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val entityName = sourceColumns.head.entityName
        val subsId = sourceColumns.head.columnName
        val model = sourceColumns(1).columnName
        val score = sourceColumns(2).columnName
        val brand = sourceColumns(3).columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val filterCondition = targetColumn match {
          case "home_barangay_name" =>
            s"(upper(${model}) == '${TransformerEnums.HOME_LOCATION}')"
          case "home_city_name" =>
            s"(upper(${model}) == '${TransformerEnums.HOME_LOCATION}')"
          case "home_province_name" =>
            s"(upper(${model}) == '${TransformerEnums.HOME_LOCATION}')"
          case "home_region_name" =>
            s"(upper(${model}) == '${TransformerEnums.HOME_LOCATION}')"
          case "work_barangay_name" =>
            s"(upper(${model}) == '${TransformerEnums.WORK_LOCATION}')"
          case "work_city_name" =>
            s"(upper(${model}) == '${TransformerEnums.WORK_LOCATION}')"
          case "work_province_name" =>
            s"(upper(${model}) == '${TransformerEnums.WORK_LOCATION}')"
          case "work_region_name" =>
            s"(upper(${model}) == '${TransformerEnums.WORK_LOCATION}')"
        }
        val scoreIndexValue = targetColumn match {
          case "home_barangay_name" => 3
          case "home_city_name" => 2
          case "home_province_name" => 1
          case "home_region_name" => 0
          case "work_barangay_name" => 3
          case "work_city_name" => 2
          case "work_province_name" => 1
          case "work_region_name" => 0

        }

        val aaModelDF =
          TransformerHelpers
            .lookbackMonthly(sourceDFs, entityName, partitionValue)
            .filter(filterCondition)
            .select(subsId, model, score, brand)

        val aaModelScoreDF = aaModelDF

        val brandFilterDF = aaModelScoreDF
          .filter(
            upper(col(brand))
              .isin(TransformerEnums.TM_PREPAID, TransformerEnums.GHP_PREPAID, TransformerEnums.GHP_POSTPAID)
          )
          .drop(brand)

        val attributeValueDF =
          brandFilterDF.withColumn(targetColumn + "_Temp", split(col(score), "\\|")(scoreIndexValue)).drop(score)

        val resultDF = targetDF
          .join(
            attributeValueDF,
            targetDF(profileJoinColumn) === attributeValueDF(entityJoinColumn),
            "left_outer"
          )
          .drop(attributeValueDF(entityJoinColumn))
          .withColumn(targetColumn, attributeValueDF(targetColumn + "_Temp"))
        resultDF
      case _ =>
        logger.warn(
          "AAModelLookupWithBrandFilter can not be applied as sourceColumns size is not equals to 4."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
