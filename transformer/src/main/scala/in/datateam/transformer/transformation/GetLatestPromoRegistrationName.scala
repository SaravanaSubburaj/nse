package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetLatestPromoRegistrationName extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  /** Calculating last promo registraiton date and last promo registration name
    *
    * @return DF with calculated attribute
    */
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val returnDF = sourceColumns.size match {
      case 9 =>
        val subscriberAvailmentStagingEntity = sourceColumns(0).entityName
        val PrepaidPromoEntity = sourceColumns(1).entityName
        val subscriberIdAvailmentStaging = sourceColumns(2).columnName
        val promoAvailmentDateColumn = sourceColumns(0).columnName
        val prepaidPromoDescriptionColumn = sourceColumns(1).columnName
        val prePaidPromoAvailmentStagingIDColumn = sourceColumns(3).columnName
        val prePaidPromoIDColumn = sourceColumns(4).columnName
        val promoOperationId = sourceColumns(5).columnName
        val availmentChannelCode = sourceColumns(6).columnName
        val serviceId = sourceColumns(7).columnName
        val promoDenominationValue = sourceColumns(8).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val subscriberIdColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val sourceAvailmentStagingDF = sourceDFs
          .get(subscriberAvailmentStagingEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            subscriberIdAvailmentStaging,
            prePaidPromoAvailmentStagingIDColumn,
            promoAvailmentDateColumn,
            promoOperationId,
            availmentChannelCode,
            serviceId,
            promoDenominationValue
          )

        val sourceDFPrepaidPromo = sourceDFs
          .get(PrepaidPromoEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(prepaidPromoDescriptionColumn, prePaidPromoIDColumn)

        val filterDF = sourceAvailmentStagingDF.filter(
          col(promoAvailmentDateColumn) >
          date_add(to_date(lit(partitionDate), Misc.dateFormat), -180)
        )

        val sourceAvailmentFilterDF = filterDF
          .filter(
            col(promoOperationId).isin("1", "6", "39") &&
            (col(availmentChannelCode) =!= "1001" || col(serviceId) === "2374")
          )

        val sourceAvailDenomFilterDF = sourceAvailmentFilterDF
          .withColumn(
            "rank",
            row_number()
              .over(
                Window
                  .partitionBy(col(subscriberIdAvailmentStaging), col(promoAvailmentDateColumn))
                  .orderBy(col(promoDenominationValue).desc)
              )
          )
          .filter(col("rank") === 1)

        val window = Window.partitionBy(subscriberIdAvailmentStaging).orderBy(col(promoAvailmentDateColumn).desc)
        val groupedDF = sourceAvailDenomFilterDF
          .withColumn("rank", row_number().over(window))
          .select(prePaidPromoAvailmentStagingIDColumn, promoAvailmentDateColumn, subscriberIdAvailmentStaging)
          .where("rank == 1")

        val joinWithPromo = groupedDF
          .join(
            sourceDFPrepaidPromo,
            upper(groupedDF(prePaidPromoAvailmentStagingIDColumn)) === upper(
                sourceDFPrepaidPromo(prePaidPromoIDColumn)
              ),
            "inner"
          )
          .drop(sourceDFPrepaidPromo(prePaidPromoIDColumn))

        val tempTargetColumn = targetColumn + "_temp"

        val changeColumnName = joinWithPromo
          .withColumnRenamed(prepaidPromoDescriptionColumn, tempTargetColumn)

        val selectedColDF = changeColumnName.select(tempTargetColumn, subscriberIdAvailmentStaging)
        val joinDF = targetDF
          .join(selectedColDF, targetDF(profileJoinColumn) === selectedColDF(subscriberIdColumn), "left_outer")
          .drop(selectedColDF(subscriberIdColumn))

        val resutldf = joinDF.withColumn(targetColumn, joinDF(tempTargetColumn))
        resutldf

      case _ =>
        logger.warn("GetLatestPromoRegistration can not be applied as sourceColumns size is not equals to 5.")
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
