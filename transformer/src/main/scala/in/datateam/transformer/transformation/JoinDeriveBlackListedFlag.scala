package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.when

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDeriveBlackListedFlag extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit
      spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val blacklistedEntityName = sourceColumns(0).entityName
    val blacklistedAttributeName = sourceColumns(0).columnName

    val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
    val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

    val blacklistedDF = sourceDFs(blacklistedEntityName)

    val resultDF =
      targetDF
        .join(blacklistedDF, targetDF(profileJoinColumn) === blacklistedDF(entityJoinColumn), "left_outer")
        .withColumn(targetColumn, when(col(entityJoinColumn).isNotNull, "Y").otherwise("N"))
        .drop(blacklistedDF(entityJoinColumn))

    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }

}
