package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

//TODO Code is used only for one attribute, if we can supply filter conditions externally, we can make it more generic.

/**
  * Transformation arguments, sourceColumns need to be ordered:
  * index 0: groupBy_1 (relSubscriberId)
  * index 1: filter_1 and return max of (reloadDate)
  * index 2: filter_2  (totalReloadAmount)
  */
object GetLatestReloadDate extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def getLatestReloadDate(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val reloadAmtCol = sourceColumns(2).columnName

    val resultDF = joinColumns match {
      case Some(_) =>
        val repartitionedDF = sourceDF
          .repartitionAndSortWithInPartition(Array(col(subscriberIdCol)), Array(col(reloadDateCol)))

        val groupedDF = repartitionedDF
          .select(col(subscriberIdCol), col(reloadDateCol), col(reloadAmtCol))
          .filter(
            col(reloadDateCol) >= Misc.getNthDate(partitionDate, -91)
            &&
            col(reloadDateCol) <= partitionDateMinusTwo
            &&
            col(reloadAmtCol) > 0
          )
          .groupBy(col(subscriberIdCol))
          .agg(max(col(reloadDateCol)).as(targetColumn + "_temp"))

        val joinedDF = targetDF
          .join(
            groupedDF,
            targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(
                joinColumns.get.head.entityJoinColumn
              ),
            "left_outer"
          )
          .drop(groupedDF(joinColumns.get.head.entityJoinColumn))

        joinedDF.withColumn(targetColumn, joinedDF(targetColumn + "_temp"))

      case None =>
        val groupedDF = targetDF
          .filter(
            col(reloadDateCol) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -91)
            &&
            col(reloadDateCol) <= partitionDateMinusTwo
            &&
            col(reloadAmtCol) > 0
          )
          .groupBy(col(subscriberIdCol))
          .agg(
            max(col(reloadDateCol)).as(targetColumn),
            first(CommonEnums.DATE_PARTITION_COLUMN)
              .as(CommonEnums.DATE_PARTITION_COLUMN)
          )
        groupedDF
          .select(
            col(sourceColumns.head.columnName),
            col(targetColumn),
            col(CommonEnums.DATE_PARTITION_COLUMN)
          )
    }
    resultDF
  }

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.length match {
      case 3 =>
        val sourceDF = sourceDFs(sourceColumns.head.entityName)

        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val filteredReloadsDF = sourceDF.filter(
          to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
            to_date(lit(partitionDate), Misc.dateFormat),
            -91
          )
        )

        getLatestReloadDate(
          filteredReloadsDF,
          targetDF,
          sourceColumns,
          joinColumns,
          targetColumn
        )
      case _ =>
        logger.warn(
          "GetLatestReloadDate can not be applied as sourceColumns size is not equals to 3."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
