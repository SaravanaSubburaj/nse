package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.temporal.TemporalAdjusters

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetEnhancedPast4ClmWeeksIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 8 =>
        val prePaidPromoAvailmentStagingEntity = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val availmentStagingPromoId = sourceColumns(1).columnName
        val promoOperationCode = sourceColumns(2).columnName
        val availmentChannelCode = sourceColumns(3).columnName
        val promoExpirationDate = sourceColumns(4).columnName
        val promoRegistraionDate = sourceColumns(5).columnName
        val prepaidPromoEntity = sourceColumns(6).entityName
        val prepaidPromoId = sourceColumns(6).columnName
        val serviceId = sourceColumns(7).columnName

        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val date = LocalDate.parse(partitionDate)
        val tuesday = java.time.DayOfWeek.TUESDAY
        val lastTuesday = date.`with`(TemporalAdjusters.previous(tuesday))
        val formatedLastTuedayDate = lastTuesday.toString

        val prepaidPromoDF = sourceDFs(prepaidPromoEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(prepaidPromoId)
        val prepaidPromoAvailmentStagingDF = sourceDFs(prePaidPromoAvailmentStagingEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(formatedLastTuedayDate), Misc.dateFormat), -90)
            && to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) <= to_date(lit(formatedLastTuedayDate))
          )
          .select(
            subscriberId,
            promoOperationCode,
            availmentChannelCode,
            promoExpirationDate,
            promoRegistraionDate,
            availmentStagingPromoId,
            serviceId
          )
          .where(
            (col(availmentChannelCode) =!= "1001"
            && col(promoOperationCode).isin("1", "6", "39")) ||
            (col(availmentChannelCode) === "1001"
            && col(promoOperationCode).isin("1", "6", "39")
            && col(serviceId) === "2374")
          )
          .filter(
            to_date(col(promoRegistraionDate)) <= to_date(lit(formatedLastTuedayDate))
            && to_date(col(promoRegistraionDate)) >= date_add(to_date(lit(formatedLastTuedayDate)), -90)
            || to_date(col(promoExpirationDate)) <= to_date(lit(formatedLastTuedayDate))
            && to_date(col(promoExpirationDate)) >= date_add(to_date(lit(formatedLastTuedayDate)), -90)
            || to_date(col(promoRegistraionDate)) <= to_date(lit(formatedLastTuedayDate)) && col(promoExpirationDate).isNull
          )

        val joinWithPromo = prepaidPromoAvailmentStagingDF
          .join(
            prepaidPromoDF,
            prepaidPromoAvailmentStagingDF(availmentStagingPromoId)
              === prepaidPromoDF(prepaidPromoId),
            "inner"
          )
          .drop(prepaidPromoDF(prepaidPromoId))

        val calculatedDF = joinWithPromo.withColumn(
          "expiryDate",
          when(col(promoExpirationDate).isNull, formatedLastTuedayDate).otherwise(col(promoExpirationDate))
        )

        val window = Window.partitionBy(subscriberId).orderBy(col(promoRegistraionDate).desc)
        val calculateStartDateLagDF =
          calculatedDF.withColumn("olLagDay", lag(col(promoRegistraionDate), -1).over(window))

        val calculateExpiryDateLagDF =
          calculateStartDateLagDF.withColumn("expLagDay", lag(col(promoExpirationDate), -1).over(window))

        val finalCalculatedDF = calculateExpiryDateLagDF.withColumn(
          "subsEnahancer",
          when(
            to_date(col("olLagDay")) >= to_date(col(promoRegistraionDate)) &&
            to_date(col("olLagDay")) <= to_date(col(promoExpirationDate)),
            1
          ).when(
              to_date(col("expLagDay")) >= to_date(col(promoRegistraionDate))
              && to_date(col("expLagDay")) <= to_date(col(promoExpirationDate)),
              1
            )
            .otherwise(0)
        )

        val groupDF = finalCalculatedDF.groupBy(subscriberId).agg(sum("subsEnahancer").as("subsEnahancer"))

        val calculateResultDF =
          groupDF.withColumn(targetColumn + "_Temp", when(col("subsEnahancer") >= 1, "1").otherwise("0"))

        val resultDF = targetDF
          .join(
            calculateResultDF,
            targetDF(profileJoinColumn) === calculateResultDF(entityJoinColumn),
            "left_outer"
          )
          .drop(calculateResultDF(entityJoinColumn))
          .withColumn(targetColumn, calculateResultDF(targetColumn + "_Temp"))

        resultDF
      case _ =>
        logger.warn(
          "GetEnhancedPast4ClmWeeksIndicator can not be applied as sourceColumns size is not equals to 8."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
