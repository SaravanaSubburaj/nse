package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Transformation arguments, sourceColumns need to be ordered:
  * index 0: groupBy_1 (relSubscriberId)
  * index 1: filter_1 (reloadDate)
  * index 2: filter_2, sum  (totalReloadAmount)
  */
object TotalDailyTopupRolling30DaysAmountDpaChannelFilter extends TransformerAggColTrait with Serializable {
  val logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.size match {
      case 4 =>
        val reloadsDF = sourceDFs.get(sourceColumns.head.entityName).get

        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val reloadChannelTypeCode = sourceColumns(3).columnName

        val filteredReloadsDF = reloadsDF
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(
                to_date(lit(partitionDate), Misc.dateFormat),
                -31
              )
          )
          .filter(lower(col(reloadChannelTypeCode)).isin("ax", "iv", "ud", "es", "pn", "eg", "rp"))
          .drop(reloadChannelTypeCode)

        calculateTotalDailyTopupRolling(filteredReloadsDF, targetDF, sourceColumns, joinColumns, targetColumn)
      case _ =>
        logger.warn("You need to supply 3 columns to call TotalDailyTopupRolling30Days")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def calculateTotalDailyTopupRolling(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val totalReloadAmountCol = sourceColumns(2).columnName

    val typechangeDF = sourceDF.withColumn(reloadDateCol, col(reloadDateCol).cast(DateType))

    val groupedDF = typechangeDF
      .select(subscriberIdCol, reloadDateCol, totalReloadAmountCol)
      .filter(
        col(reloadDateCol) >= Misc.getNthDate(partitionDate, -31)
        &&
        col(reloadDateCol) <= partitionDateMinusTwo
        &&
        col(totalReloadAmountCol) > 0
      )
      .groupBy(col(subscriberIdCol))
      .agg(sum(col(totalReloadAmountCol)).as(targetColumn + "_temp"))

    val joinedDF = targetDF
      .join(
        groupedDF,
        targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(joinColumns.get.head.entityJoinColumn),
        "left_outer"
      )
      .drop(groupedDF(joinColumns.get.head.entityJoinColumn))

    val retDF = joinedDF.withColumn(targetColumn, joinedDF(targetColumn + "_temp"))

    retDF

  }

}
