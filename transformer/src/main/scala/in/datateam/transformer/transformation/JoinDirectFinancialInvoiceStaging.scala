package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDirectFinancialInvoiceStaging extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 10 =>
        val financialInvoiceStagingEntity = sourceColumns.head.entityName
        val financialaccountEntity = sourceColumns(1).entityName
        val financialSubscriberReferenceEntity = sourceColumns(9).entityName
        val financialAccountIdInvoiceStaging = sourceColumns.head.columnName
        val financialAccountId = sourceColumns(1).columnName
        val attributeColumn = sourceColumns(2).columnName
        val billNumber = sourceColumns(4).columnName
        val financialAccountStatusCode = sourceColumns(5).columnName
        val financialActivationDate = sourceColumns(6).columnName
        val subscriberReferenceSubscriberId = sourceColumns(7).columnName
        val expiryDate = sourceColumns(8).columnName
        val subscriberReferenceFinancialId = sourceColumns(9).columnName
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val subscriberReferenceDf = sourceDFs(financialSubscriberReferenceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberReferenceSubscriberId, subscriberReferenceFinancialId, expiryDate)
          .filter(col(expiryDate).isNull)
          .drop(expiryDate)

        val financialInvoiceStagingDF =
          TransformerHelpers
            .lookbackMonthly(sourceDFs, financialInvoiceStagingEntity, partitionDate)
            .select(financialAccountIdInvoiceStaging, attributeColumn, billNumber)
            .repartitionAndSortWithInPartition(
              Array(col(financialAccountIdInvoiceStaging)),
              Array(col(billNumber).desc)
            )

        val financialAccountDF = sourceDFs(financialaccountEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(
            financialAccountId,
            financialAccountStatusCode,
            financialActivationDate
          )
          .filter(
            trim(upper(col(financialAccountStatusCode))) === "O" || trim(
              upper(col(financialAccountStatusCode))
            ) === "SUS"
          )
          .drop(financialAccountStatusCode)
          .repartitionAndSortWithInPartition(Array(col(financialAccountId)), Array(col(financialActivationDate)))

        val accountGroupDf = financialAccountDF
          .groupBy(financialAccountId)
          .agg(max(financialActivationDate).as(financialActivationDate))
          .drop(financialActivationDate)

        val joinWithSubscriberRefernce = subscriberReferenceDf
          .join(
            accountGroupDf,
            subscriberReferenceDf(subscriberReferenceFinancialId) === accountGroupDf(financialAccountId),
            "left_outer"
          )
          .drop(subscriberReferenceDf(subscriberReferenceFinancialId))

        val window = Window
          .partitionBy(financialAccountIdInvoiceStaging)
          .orderBy(col(billNumber).desc)
        val groupedDF = financialInvoiceStagingDF
          .withColumn("rank", row_number().over(window))
          .select(financialAccountIdInvoiceStaging, attributeColumn)
          .where("rank == 1")
          .repartitionAndSortWithInPartition(
            Array(col(financialAccountIdInvoiceStaging)),
            Array(col(financialAccountIdInvoiceStaging))
          )

        val joinDF = joinWithSubscriberRefernce
          .join(
            groupedDF,
            joinWithSubscriberRefernce(financialAccountId) === groupedDF(
                financialAccountIdInvoiceStaging
              ),
            "inner"
          )
          .drop(groupedDF(financialAccountIdInvoiceStaging))
          .drop(financialAccountId)
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val repartitionedTargetDF = targetDF
          .repartitionAndSortWithInPartition(Array(col(profileJoinColumn)), Array(col(profileJoinColumn)))

        val resultJoinDF = repartitionedTargetDF.join(
          joinDF,
          repartitionedTargetDF(profileJoinColumn) === joinDF(entityJoinColumn),
          "left_outer"
        )

        val finalResultDF = resultJoinDF.withColumn(targetColumn, resultJoinDF(attributeColumn))

        finalResultDF
      case _ =>
        logger.warn(
          "JoinDirectFinancialInvoiceStaging can not be applied as " +
          "sourceColumns size is not equals to 7."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
