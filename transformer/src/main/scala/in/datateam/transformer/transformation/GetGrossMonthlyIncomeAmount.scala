package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetGrossMonthlyIncomeAmount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 2 =>
        val subscriberEntity = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val grossIncomeAmount = sourceColumns(1).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val sourceDf = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, grossIncomeAmount)

        val derivedDf = sourceDf
          .withColumn(
            targetColumn + "_temp",
            when(col(grossIncomeAmount).isNull, "0.00")
              .otherwise(col(grossIncomeAmount))
          )
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val repartitionTargetDF = targetDF
          .repartitionAndSortWithInPartition(Array(col(profileJoinColumn)), Array(col(profileJoinColumn)))

        val resultDF = repartitionTargetDF
          .join(derivedDf, repartitionTargetDF(profileJoinColumn) === derivedDf(entityJoinColumn), "left_outer")
          .drop(derivedDf(subscriberId))
          .withColumn(targetColumn, derivedDf(targetColumn + "_temp"))
        resultDF
      case _ =>
        logger.warn(
          "GetGrossMonthlyIncomeAmount can not be applied as sourceColumns size is not equals to 2."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
