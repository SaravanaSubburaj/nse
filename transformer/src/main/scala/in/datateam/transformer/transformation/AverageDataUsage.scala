package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Created by Prabhu GS(prabhu.gs@thedatateam.in).
  */
object AverageDataUsage extends TransformerAggColTrait with Serializable {

  val logger: Logger = Logger.getLogger(getClass.getName)

  //TODO: make this generic by grouping by one of the sourceColumns
  //TODO: move this function to utils

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.length match {
      case 5 =>
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val sourceDF: DataFrame = sourceDFs(sourceColumns.head.entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -90
            )
          )
        averageDataUsage(
          sourceDF,
          targetDF,
          sourceColumns,
          joinColumns: Option[Array[JoinColumn]],
          targetColumn
        )(spark)
      case _ =>
        logger.warn(
          "AveragePrepaidDataUsage couldn't be applied since sourceColumns size != 5."
        )
        targetDF
    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }

  private def averageDataUsage(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {

    val subscriberIDColumn = sourceColumns.head.columnName
    val usageTypeCodeCol = sourceColumns(1).columnName
    val totalDataVolumeCountCol = sourceColumns(2).columnName
    val totalChargeableRoundedUnitCountCol = sourceColumns(3).columnName
    val usageTransactionDateCol = sourceColumns(4).columnName
    val entityJoinCol = joinColumns.get.head.entityJoinColumn
    val profileJoinCol = joinColumns.get.head.profileJoinColumn

    val requiredColumns: List[String] = List(
        subscriberIDColumn,
        usageTypeCodeCol,
        totalDataVolumeCountCol,
        totalChargeableRoundedUnitCountCol,
        usageTransactionDateCol
      ) ++ List(CommonEnums.DATE_PARTITION_COLUMN)

    val filtered_tempDF: DataFrame = sourceDF
      .filter(
        upper(col(usageTypeCodeCol)) === TransformerEnums.GLOBE_DATA_TRAFFIC_CHARGE
      )
      .select(requiredColumns.map(c => col(c)): _*)

    val totalDataVolumeCountMBCol = totalDataVolumeCountCol + "_MB"
    val filteredDF = filtered_tempDF
      .withColumn(
        totalDataVolumeCountMBCol,
        filtered_tempDF(totalDataVolumeCountCol) / 1024 / 1024
      )

    //TODO: Process Date format instead of string.
    // Currently, since "date" in targetDF is string datediff is processing strings.
    val firstMonthDF: DataFrame = filteredDF.filter(
      datediff(
        col(CommonEnums.DATE_PARTITION_COLUMN),
        col(usageTransactionDateCol)
      ) >= 60
    )
    val secondMonthDF: DataFrame = filteredDF
      .filter(
        datediff(
          col(CommonEnums.DATE_PARTITION_COLUMN),
          col(usageTransactionDateCol)
        ).gt(30)
      )
      .filter(
        datediff(
          col(CommonEnums.DATE_PARTITION_COLUMN),
          col(usageTransactionDateCol)
        ).lt(60)
      )
    val thirdMonthDF: DataFrame = filteredDF.filter(
      datediff(
        col(CommonEnums.DATE_PARTITION_COLUMN),
        col(usageTransactionDateCol)
      ) <= 30
    )

    val firstMonthActiveDays: DataFrame =
      numberOfActiveDays(
        firstMonthDF,
        usageTransactionDateCol,
        subscriberIDColumn
      )
    val secondMonthActiveDays: DataFrame =
      numberOfActiveDays(
        secondMonthDF,
        usageTransactionDateCol,
        subscriberIDColumn
      )
    val thirdMonthActiveDays: DataFrame =
      numberOfActiveDays(
        thirdMonthDF,
        usageTransactionDateCol,
        subscriberIDColumn
      )

    val firstMonthDataVolume: DataFrame = computeDataVolume(
      firstMonthDF,
      totalDataVolumeCountMBCol,
      subscriberIDColumn
    )
    val secondMonthDataVolume: DataFrame = computeDataVolume(
      secondMonthDF,
      totalDataVolumeCountMBCol,
      subscriberIDColumn
    )
    val thirdMonthDataVolume: DataFrame = computeDataVolume(
      thirdMonthDF,
      totalDataVolumeCountMBCol,
      subscriberIDColumn
    )

    //TODO: Optimize by using analytical functions
    val firstMonthVolume: DataFrame = firstMonthDataVolume
      .join(
        firstMonthActiveDays,
        firstMonthActiveDays(subscriberIDColumn) === firstMonthDataVolume(
            subscriberIDColumn
          )
      )
      .drop(firstMonthActiveDays(subscriberIDColumn))
      .withColumn("monthlyVolume", col("dataVolume") / col("activeDays"))
    val secondMonthVolume: DataFrame = secondMonthDataVolume
      .join(
        secondMonthActiveDays,
        secondMonthActiveDays(subscriberIDColumn) === secondMonthDataVolume(
            subscriberIDColumn
          )
      )
      .drop(secondMonthActiveDays(subscriberIDColumn))
      .withColumn("monthlyVolume", col("dataVolume") / col("activeDays"))
    val thirdMonthVolume: DataFrame = thirdMonthDataVolume
      .join(
        thirdMonthActiveDays,
        thirdMonthActiveDays(subscriberIDColumn) === thirdMonthDataVolume(
            subscriberIDColumn
          )
      )
      .drop(thirdMonthActiveDays(subscriberIDColumn))
      .withColumn("monthlyVolume", col("dataVolume") / col("activeDays"))

    val unionDF =
      firstMonthVolume.union(secondMonthVolume).union(thirdMonthVolume)
    val outputDF = unionDF
      .groupBy(subscriberIDColumn)
      .agg((sum(col("monthlyVolume")) / 3).as(targetColumn))

    targetDF
      .join(
        outputDF,
        targetDF(profileJoinCol) === outputDF(entityJoinCol),
        "left_outer"
      )
      .drop(outputDF(subscriberIDColumn))
    //TODO: make current targetDF as sourceDF and join outputDF with targetDF
    //TODO: remove prepaid usage columns from column mapping (profile config)
  }

  /**
    *
    * @param sourceDF
    * @param spark
    * @return Dataframe [subscriberID, activeDays]
    */
  def numberOfActiveDays(
      sourceDF: DataFrame,
      usageTransactionDateCol: String,
      subscriberIDColumn: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    sourceDF
      .groupBy(subscriberIDColumn)
      .agg(countDistinct(col(usageTransactionDateCol)).as("activeDays"))
  }

  def computeDataVolume(
      sourceDF: DataFrame,
      totalDataVolumeCountCol: String,
      subscriberIDColumn: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    sourceDF
      .groupBy(subscriberIDColumn)
      .agg(sum(totalDataVolumeCountCol).as("dataVolume"))
  }
}
