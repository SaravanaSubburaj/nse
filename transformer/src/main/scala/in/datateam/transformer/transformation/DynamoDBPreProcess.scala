package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.enums.DeltaEnums

object DynamoDBPreProcess extends TransformerColTrait with Serializable {
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val subscriberStatusCodeCol = sourceColumns(0).columnName

    val deletedRecordsDF = sourceDF.filter(col(DeltaEnums.RECORD_MODIFIER_COLUMN) === DeltaEnums.DELETE)
    val newRecordsDF = sourceDF.filter(col(DeltaEnums.RECORD_MODIFIER_COLUMN) === DeltaEnums.INSERT)
    val beforeRecordsDF = sourceDF.filter(col(DeltaEnums.RECORD_MODIFIER_COLUMN) === DeltaEnums.BEFORE)
    val afterRecordsDF = sourceDF.filter(col(DeltaEnums.RECORD_MODIFIER_COLUMN) === DeltaEnums.AFTER)

    val deletedInLastRunCondition = lower(trim(beforeRecordsDF(subscriberStatusCodeCol))).isin(
        TransformerEnums.INACTIVE_SUBSCRIBER_STATUS_CODE: _*
      ) === false && lower(trim(afterRecordsDF(subscriberStatusCodeCol)))
        .isin(TransformerEnums.INACTIVE_SUBSCRIBER_STATUS_CODE: _*)

    val activeSubscriberCondition = lower(trim(afterRecordsDF(subscriberStatusCodeCol))).isin(
        TransformerEnums.INACTIVE_SUBSCRIBER_STATUS_CODE: _*
      ) === false

    val subscriberIdCol = beforeRecordsDF.columns.head
    val joinCondition = beforeRecordsDF(subscriberIdCol) === afterRecordsDF(subscriberIdCol)

    val beforeAndAfterJoinedDF = beforeRecordsDF
      .join(afterRecordsDF, joinCondition, "inner")
    val allColumns = sourceDF.columns.map(columnName => afterRecordsDF(columnName))

    val deactivatedSubsToBeDeletedDF = beforeAndAfterJoinedDF
      .filter(deletedInLastRunCondition)
      .select(allColumns: _*)
      .withColumn(CommonEnums.ACTIVE_SUBS_IND, lit(CommonEnums.ACTIVE_SUBS_DELETE))

    val activeSubsToBeUpdatedDF = afterRecordsDF
      .filter(activeSubscriberCondition)
      .withColumn(CommonEnums.ACTIVE_SUBS_IND, lit(CommonEnums.ACTIVE_SUBS_UPDATE))

    val activeSubsToBeInsertedDF = newRecordsDF
      .filter(activeSubscriberCondition)
      .withColumn(CommonEnums.ACTIVE_SUBS_IND, lit(CommonEnums.ACTIVE_SUBS_INSERT))

    val unionDF = deactivatedSubsToBeDeletedDF.union(activeSubsToBeInsertedDF).union(activeSubsToBeUpdatedDF)

    unionDF
  }

}
