package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date
import org.apache.spark.sql.functions.trim

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDirectPullWithEntityAndProfile extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  /**
    * This transformer can be used to extract attribute with direct join between two entity and final join with target.
    * Provide broadcastFlag as "true" in transformationOption in case involved entity is less 10 MB.
    * It expects source columns in following order first source column should be attribute which
    * needed to be extracted and then next it should be join columns with entities in alternate position.
    *
    * sample config
    * {
    *   "attributeName": "attribute_name",
    *   "sourceColumns": [
    *     {"entityName": "entity_1", "columnName": "entity_attribute_name"},
    *     {"entityName": "entity_2", "columnName": "entity_2_column_1"},
    *     {"entityName": "entity_1", "columnName": "entity_1_column_1"},
    *     {"entityName": "entity_2", "columnName": "entity_2_column_2"},
    *     {"entityName": "entity_1", "columnName": "entity_1_column_2"},
    *   ],
    *   "joinColumns": [
    *     {"entityJoinColumn": "joined_entity_col_1", "profileJoinColumn": "profile_col_1"},
    *     {"entityJoinColumn": "joined_entity_col_2", "profileJoinColumn": "profile_col_2"}
    *   ],
    *   "transformationOption": [
    *     {"key": "broadcastEntityFlag", "value": "true"},
    *     {"key": "broadcastJoinedEntityFlag", "value": "true"},
    *   ]
    *   "transformationFunction": "JoinDirectPullWithEntityAndProfile"
    * }
    *
    */
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = validate(sourceDFs, targetDF, sourceColumns, targetJoinColumn) match {
      case true =>
        val entity1Name = sourceColumns(0) entityName
        val entity2Name = sourceColumns(1) entityName
        val entity1attributeColumns =
          (sourceColumns zip (0 to sourceColumns.size)).filter(x => x._2 % 2 == 0).map(c => c._1.columnName)
        val entity2attributeColumns =
          (sourceColumns zip (0 to sourceColumns.size)).filter(x => x._2 % 2 == 1).map(c => c._1.columnName)
        val attributeColumn = entity1attributeColumns.head

        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val broadcastEntityFlag = extraParameter.getOrElse("broadcastEntityFlag", "false").toLowerCase == "true"
        val broadcastFlag = extraParameter.getOrElse("broadcastJoinedEntityFlag", "false").toLowerCase == "true"
        val joinColumns = targetJoinColumn.get

        val entity1DF = sourceDFs(entity1Name)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .drop(col(CommonEnums.DATE_PARTITION_COLUMN))
        val entity2DF = sourceDFs(entity2Name)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .drop(col(CommonEnums.DATE_PARTITION_COLUMN))
        val entityJoinCondition = (entity1attributeColumns.tail zip entity2attributeColumns).map {
          case (entity1JoinColumn: String, entity2JoinColumn: String) =>
            entity1DF(entity1JoinColumn) === entity2DF(entity2JoinColumn)
        }.reduce((a, b) => a && b)

        val entityJoinedAllColumnDF = if (broadcastEntityFlag) {
          entity1DF.join(broadcast(entity2DF), entityJoinCondition, "inner")
        } else {
          entity1DF.join(entity2DF, entityJoinCondition, "inner")
        }
        val entityJoinedDF = entity2attributeColumns.foldLeft(entityJoinedAllColumnDF) { (iDF: DataFrame, joinColumn) =>
          iDF.drop(entity2DF(joinColumn))
        }
        val joinCondition = joinColumns
          .map(
            joinColumn => targetDF(joinColumn.profileJoinColumn) === entityJoinedDF(joinColumn.entityJoinColumn)
          )
          .reduce((a, b) => a && b)

        val joinedAllColumnDF = if (broadcastFlag) {
          targetDF.drop(attributeColumn).join(broadcast(entityJoinedDF), joinCondition, "left_outer")
        } else {
          targetDF.drop(attributeColumn).join(entityJoinedDF, joinCondition, "left_outer")
        }
        val joinedDF = joinColumns.foldLeft(joinedAllColumnDF) { (iDF: DataFrame, joinColumn) =>
          iDF.drop(entityJoinedDF(joinColumn.entityJoinColumn))
        }
        val finalResultDF =
          joinedDF.withColumn(targetColumn, trim(entityJoinedDF(attributeColumn)))
        finalResultDF
      case false =>
        logger.warn(
          "JoinDirectPullWithEntityAndProfile can not be applied as sourceColumns size is not equals to 1."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def validate(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]]
    )(
      implicit spark: SparkSession
    ): Boolean = {
    val result = if (sourceColumns.size >= 3 && sourceColumns.size % 2 == 1) {
      val entity1Name = sourceColumns(0) entityName
      val entity2Name = sourceColumns(1) entityName
      val entity1attributeColumns =
        (sourceColumns zip (0 to sourceColumns.size)).filter(x => x._2 % 2 == 0).map(c => c._1.columnName)
      val entity2attributeColumns =
        (sourceColumns zip (0 to sourceColumns.size)).filter(x => x._2 % 2 == 1).map(c => c._1.columnName)
      val entity1DFColumns = sourceDFs(entity1Name).columns
      val entity2DFColumns = sourceDFs(entity2Name).columns
      val entity1Flag =
        entity1attributeColumns.map(column => entity1DFColumns.contains(column)).reduce((a, b) => a && b)
      val entity2Flag =
        entity2attributeColumns.map(column => entity2DFColumns.contains(column)).reduce((a, b) => a && b)
      if (entity1Flag && entity2Flag) {
        true
      } else {
        logger.warn(
          "JoinDirectPullWithEntityAndProfile can not be applied as columns not found in source entity."
        )
        false
      }
    } else {
      logger.warn(
        "JoinDirectPullWithEntityAndProfile can not be applied as sourceColumns should contain odd number of columns and more than 2 columns."
      )
      false
    }
    result
  }
}
