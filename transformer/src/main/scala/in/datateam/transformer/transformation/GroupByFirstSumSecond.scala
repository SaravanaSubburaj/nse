package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.first
import org.apache.spark.sql.functions.sum

import org.apache.log4j.Logger

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object GroupByFirstSumSecond extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def groupByFirstSumSecond(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val resultDF = joinColumns match {
      case Some(_) =>
        val groupedDF = sourceDF
          .groupBy(col(sourceColumns.head.columnName))
          .agg(
            sum(col(sourceColumns.tail(0).columnName))
              .as(targetColumn + "_temp")
          )
        val joinedDF = targetDF.join(
          groupedDF,
          targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(
              joinColumns.get.head.entityJoinColumn
            )
        )
        val targetColumns: List[String] =
          targetDF.columns.toList.::(targetColumn)
        joinedDF
          .withColumn(targetColumn, joinedDF(targetColumn + "_temp"))
          .select(targetColumns.map(column => col(column)): _*)
      case None =>
        val groupedDF = targetDF
          .groupBy(col(sourceColumns.head.columnName))
          .agg(
            sum(col(sourceColumns.tail(0).columnName)).as(targetColumn),
            first(CommonEnums.DATE_PARTITION_COLUMN)
              .as(CommonEnums.DATE_PARTITION_COLUMN)
          )
        groupedDF
          .select(
            col(sourceColumns.head.columnName),
            col(targetColumn),
            col(CommonEnums.DATE_PARTITION_COLUMN)
          )
    }
    resultDF
  }

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 2 =>
        val sourceDF = sourceDFs(sourceColumns.head.entityName)
        groupByFirstSumSecond(
          sourceDF,
          targetDF,
          sourceColumns,
          joinColumns,
          targetColumn
        )(spark)
      case _ =>
        logger.warn(
          "GroupByFirstSumSecond can not be applied as sourceColumns size is not equals to 2."
        )
        targetDF
    }
    resultDF
  }

}
