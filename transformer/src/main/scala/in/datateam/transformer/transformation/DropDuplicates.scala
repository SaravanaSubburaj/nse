package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

/**
  *Drop duplicates based on columns supplied as source column.
  *  if 1st element of sourceColumns = "*", then use all columns.
  *  TargetColumn is unused, just specify any targetSchema column
  */
object DropDuplicates extends TransformerColTrait with Serializable {
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val returnDF = sourceColumns.head.columnName match {
      case "*" => {
        sourceDF.distinct()
      }
      case _ => {
        val dropDuplicatesColumns = sourceColumns.map(x => x.columnName)
        sourceDF.dropDuplicates(dropDuplicatesColumns)
      }
    }
    returnDF
  }

}
