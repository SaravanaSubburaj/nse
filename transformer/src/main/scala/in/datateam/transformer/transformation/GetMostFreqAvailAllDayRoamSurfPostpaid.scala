package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetMostFreqAvailAllDayRoamSurfPostpaid extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.length match {
      case 3 =>
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val subsAssingedSubsId = sourceColumns(0).columnName
        val billingOfferDesc = sourceColumns(1).columnName
        val subsBillingOfferStDate = sourceColumns(2).columnName

        val subsAssingedBillOfferDF = sourceDFs(sourceColumns(1).entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -90
            )
          )
          .select(subsAssingedSubsId, billingOfferDesc, subsBillingOfferStDate)

        val filterRecs = subsAssingedBillOfferDF
          .filter(
            col(subsBillingOfferStDate) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
            &&
            col(subsBillingOfferStDate) < to_date(lit(partitionDate), Misc.dateFormat)
          )
          .filter(lower(col(billingOfferDesc)) rlike TransformerEnums.ROAM_SURF_POSTPAID_FILTER)
          .repartitionAndSortWithInPartition(Array(col(subsAssingedSubsId), col(billingOfferDesc)), Array())

        val groupedDF = filterRecs
          .groupBy(subsAssingedSubsId, billingOfferDesc)
          .agg(count(billingOfferDesc).alias("cnt"))
          .repartitionAndSortWithInPartition(Array(col(subsAssingedSubsId)), Array(col("cnt").desc))

        val rankedDF = groupedDF
          .withColumn(
            "rank",
            row_number()
              .over(
                Window
                  .partitionBy(col(subsAssingedSubsId))
                  .orderBy(col("cnt").desc)
              )
          )
          .filter(col("rank") === 1)
          .repartitionAndSortWithInPartition(
            Array(col(joinColumns.get.head.entityJoinColumn)),
            Array(col(joinColumns.get.head.entityJoinColumn))
          )

        val joinedDF = targetDF
          .join(
            rankedDF,
            targetDF(joinColumns.get.head.profileJoinColumn) === rankedDF(
                joinColumns.get.head.entityJoinColumn
              ),
            "left_outer"
          )
          .drop(rankedDF(joinColumns.get.head.entityJoinColumn))

        val resultantDF = joinedDF
          .drop(targetColumn)
          .withColumn(targetColumn, joinedDF(billingOfferDesc))

        resultantDF
      case _ =>
        logger.warn(
          "GetMostFreqAvailAllDayRoamSurfPostpaid can not be applied as " +
          "sourceColumns size is not equals to 3."
        )
        targetDF

    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }
}
