package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.when

import org.apache.log4j.Logger

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

/**
  * Created by Prabhu GS (prabhu.gs@thedatateam.in).
  */
object CombinePrepaidAndPostpaidOperator extends TransformerColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.length match {
      case 2 =>
        sourceDF
          .withColumn(
            targetColumn,
            sourceColumns
              .map(sourceColumn => col(sourceColumn.columnName))
              .toSeq
              .reduce { (a, b) =>
                when(a.isNotNull && b.isNull, a)
                  .when(a.isNull && b.isNotNull, b)
                  .otherwise(a)
              }
          )
          .drop(sourceColumns.map(c => c.columnName): _*)
      case _ =>
        logger.warn(
          "CombinePrepaidAndPostpaidOperator requires 2 source columns"
        )
        sourceDF
    }
    resultDF
  }
}
