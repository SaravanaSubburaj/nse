package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.helper.Misc

object StandardizeDatesCol extends TransformerColTrait with Serializable {

  def processRow(row: Row, sourceColumns: Array[SourceColumn], targetColumn: String): Row = {

    val schema = row.schema
    val dateString = row.getAs[String](sourceColumns.head.columnName)

    val processedArray: Array[Any] = schema.fields.map(
      (field: StructField) =>
        field.name match {
          case `targetColumn` =>
            Misc.parseDateString(dateString).getOrElse(dateString)
          case x: Any => row.getAs[Any](x)
        }
    )

    val processedRow = new GenericRowWithSchema(processedArray, schema)
    processedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    implicit val sourceDFEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => processRow(row, sourceColumns, targetColumn))
  }

}
