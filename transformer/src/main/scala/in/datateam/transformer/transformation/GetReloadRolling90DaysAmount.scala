package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetReloadRolling90DaysAmount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.length match {
      case 6 =>
        val entityName = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val totalReloadAmount = sourceColumns(1).columnName
        val reloadChannelTypeCode = sourceColumns(2).columnName
        val reloadDenominationAmount = sourceColumns(3).columnName
        val totalReloadCount = sourceColumns(4).columnName
        val reloadDate = sourceColumns(5).columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val sourceDf = sourceDFs(entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(
                to_date(lit(partitionDate), Misc.dateFormat),
                -90
              )
          )
          .select(subscriberId, totalReloadAmount, reloadChannelTypeCode, reloadDenominationAmount, totalReloadCount)
          .filter(col(totalReloadAmount) > 0)
          .drop(totalReloadAmount)
          .filter(lower(col(reloadChannelTypeCode)).isin("ax", "iv", "ud", "es", "pn", "eg", "rp"))
          .drop(reloadChannelTypeCode)
          .filter(col(totalReloadCount).isNotNull)
          .filter(
            to_date(col(reloadDate)) >= date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
            &&
            to_date(col(reloadDate)) <= to_date(lit(partitionDate))
          )

        val groupedDf = sourceDf
          .groupBy(subscriberId, reloadDenominationAmount)
          .agg(sum(col(totalReloadCount)).as("tCount"))
          .select(subscriberId, reloadDenominationAmount, "tCount")

        val window = Window.partitionBy(subscriberId).orderBy(col("tCount").desc)
        val maxCountTotalReloadCountDF = groupedDf
          .withColumn("rank", rank().over(window))
          .select(subscriberId, reloadDenominationAmount)
          .where("rank == 1")

        val maxDenomValueDf = maxCountTotalReloadCountDF
          .groupBy(subscriberId)
          .agg(max(reloadDenominationAmount).as(reloadDenominationAmount))

        val resultJoinDF = targetDF
          .join(
            maxDenomValueDf,
            targetDF(profileJoinColumn) === maxDenomValueDf(
                entityJoinColumn
              ),
            "left_outer"
          )
          .drop(maxDenomValueDf(entityJoinColumn))

        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, maxDenomValueDf(reloadDenominationAmount))

        finalResultDF
      case _ =>
        logger.warn(
          "GetReloadRolling90DaysAmount can not be applied as sourceColumns size is not equals to 6."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
