package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

/*
 * add common aggregates, i.e:- min, max, avg, sum, count
 * maybe include median, stddev and quantiles later
 * */

//FIXME GTP-223 :- Change from TransformerAggTrait to usual TransformerAggColTrait
//TODO : add target schema verification for attributeColumns

object GroupBySourceAggTarget extends TransformerAggTrait with Serializable {

  override def process(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      groupByColumns: Array[SourceColumn],
      aggregateColumns: Array[SourceColumn],
      targetColumns: Array[String]
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val rawEntityDF = sourceDF
    val recordCreationTimeCol = groupByColumns.last.columnName
    val tzDF = rawEntityDF.withColumn(
      recordCreationTimeCol,
      col(recordCreationTimeCol)
      + expr("INTERVAL 8 HOURS")
    )
    val castedDF = tzDF.withColumn(recordCreationTimeCol, to_date(col(recordCreationTimeCol)))
    val groupByCols = groupByColumns.map(srcCol => srcCol.columnName) ++ Array(
        CommonEnums.DATE_PARTITION_COLUMN
      )
    val requiredColumns = groupByCols ++ aggregateColumns.map(srcCol => srcCol.columnName)
    val repartitionedDF = castedDF
      .select(requiredColumns.map(c => col(c)): _*)
      .repartition(groupByCols.map(c => col(c)): _*)
    repartitionedDF.take(1)
    val groupedDF = repartitionedDF.groupBy(groupByCols.head, groupByCols.tail: _*)

    val aggsList = List("min", "max", "sum", "count", "avg")

    val tuplesList = aggregateColumns
      .flatMap(aggCol => aggsList.map(aggFunc => (aggCol.columnName, aggFunc)))

    val aggregatesDF = groupedDF.agg(tuplesList.head, tuplesList.tail: _*)
    val retDF = aggregateColumns.foldLeft(aggregatesDF) { (aggregatesDF, col) =>
      aggregatesDF.withColumn(col.columnName, lit(null))
    }

    val names = tuplesList.map(tpl => s"${tpl._2}(${tpl._1})")
    val zippedNames = names.zip(targetColumns)

    val renamedDF = zippedNames.foldLeft(retDF) { (retDF, tpl) =>
      retDF
        .withColumnRenamed(tpl._1, tpl._2)
    }

    renamedDF
  }

}
