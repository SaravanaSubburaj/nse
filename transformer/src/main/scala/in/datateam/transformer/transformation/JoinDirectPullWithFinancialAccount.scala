package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDirectPullWithFinancialAccount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  /*Working for following attributes
   * 1. billing_delivery_mode_code
   * 2. account_credit_limit
   * */

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 6 =>
        val financialAccountEntity = sourceColumns(1).entityName
        val attributeColumn = sourceColumns(0).columnName
        val financialAccountStatusCodeColumn = sourceColumns(1).columnName
        val financialAccountID2Column = sourceColumns(5).columnName
        val financialAccountSubscriberReferenceEntity = sourceColumns(2).entityName
        val subscriberReferenceSubscriberId = sourceColumns(2).columnName
        val expiryDate = sourceColumns(3).columnName
        val financialAccountID1Column = sourceColumns(4).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val financialAccountDF = sourceDFs(financialAccountEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(attributeColumn, financialAccountStatusCodeColumn, financialAccountID2Column)
          .repartitionAndSortWithInPartition(
            Array(col(financialAccountID2Column)),
            Array(col(financialAccountID2Column))
          )

        val financialAccountSubscriberReferenceDF = sourceDFs(financialAccountSubscriberReferenceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(financialAccountID1Column, subscriberReferenceSubscriberId, expiryDate)
          .filter(col(expiryDate).isNull)
          .drop(expiryDate)
          .repartitionAndSortWithInPartition(
            Array(col(financialAccountID1Column)),
            Array(col(financialAccountID1Column))
          )

        val joinedDF = financialAccountSubscriberReferenceDF
          .join(
            financialAccountDF,
            financialAccountSubscriberReferenceDF(financialAccountID1Column) === financialAccountDF(
                financialAccountID2Column
              ),
            "inner"
          )
          .filter(col(financialAccountStatusCodeColumn).isin(TransformerEnums.FINANCIAL_ACCOUNT_FILTER_LIST: _*))
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val repartitionedTargetDF = targetDF
          .repartitionAndSortWithInPartition(Array(col(profileJoinColumn)), Array(col(profileJoinColumn)))

        val resultJoinDF = repartitionedTargetDF
          .join(
            joinedDF,
            repartitionedTargetDF(profileJoinColumn) === joinedDF(entityJoinColumn),
            "left_outer"
          )
        //.drop(financialAccountDF(profileJoinColumn))

        val finalResultDF = resultJoinDF.withColumn(targetColumn, resultJoinDF(attributeColumn))
        finalResultDF.take(1)

        finalResultDF
      case _ =>
        logger.warn("JoinDirectPullWithFinancialAccount can not be applied as sourceColumns size is not equals to 2.")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
