package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.row_number
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetHandsetTypeName extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 14 =>
        val networkDataEntity = sourceColumns.head.entityName
        val networkVoiceEntity = sourceColumns(3).entityName
        val networkSmsEntity = sourceColumns(6).entityName
        val subscriberEntity = sourceColumns(12).entityName
        val deviceEntity = sourceColumns(9).entityName
        val dataMsisdn = sourceColumns.head.columnName
        val dataDeviceId = sourceColumns(1).columnName
        val dataTopLocationDate = sourceColumns(2).columnName
        val voiceMsisdn = sourceColumns(3).columnName
        val voiceDeviceId = sourceColumns(4).columnName
        val voiceTopLocationDate = sourceColumns(5).columnName
        val smsMsisdn = sourceColumns(6).columnName
        val smsDeviceId = sourceColumns(7).columnName
        val smsTopLocationDate = sourceColumns(8).columnName
        val deviceId = sourceColumns(9).columnName
        val deviceName = sourceColumns(10).columnName
        val subscriberStatusCode = sourceColumns(11).columnName
        val subscriberId = sourceColumns(12).columnName
        val msisdnValue = sourceColumns(13).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn

        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val deviceDf =
          TransformerHelpers.lookbackMonthly(sourceDFs, deviceEntity, partitionDate).select(deviceId, deviceName)

        val dataDf = sourceDFs(networkDataEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(dataDeviceId, dataMsisdn, dataTopLocationDate)

        val voiceDf = sourceDFs(networkVoiceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(voiceDeviceId, voiceMsisdn, voiceTopLocationDate)

        val smsDf = sourceDFs(networkSmsEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(smsDeviceId, smsMsisdn, smsTopLocationDate)

        val subscriberDf = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, subscriberStatusCode, msisdnValue)
          .filter(
            upper(col(subscriberStatusCode)) =!= "C"
            && upper(col(subscriberStatusCode)) =!= "L"
            && upper(col(subscriberStatusCode)) =!= "T"
          )
          .drop(subscriberStatusCode)

        val unionDf = dataDf
          .union(voiceDf)
          .union(smsDf)
          .repartitionAndSortWithInPartition(Array(col(dataMsisdn)), Array(col(dataTopLocationDate).desc))

        val window =
          Window.partitionBy(dataMsisdn).orderBy(col(dataTopLocationDate).desc)
        val groupedDF = unionDf
          .withColumn("rank", row_number().over(window))
          .select(dataMsisdn, dataDeviceId)
          .where("rank == 1")

        val joinWithDevice = groupedDF
          .join(
            deviceDf,
            groupedDF(dataDeviceId) === deviceDf(deviceId),
            "left_outer"
          )
          .drop(groupedDF(dataDeviceId))
          .drop(deviceId)

        val joinwithresult = subscriberDf
          .join(
            joinWithDevice,
            subscriberDf(msisdnValue) === joinWithDevice(dataMsisdn),
            "left_outer"
          )
          .drop(subscriberDf(msisdnValue))
          .drop(dataMsisdn)

        val resultDF = targetDF
          .join(
            joinwithresult,
            targetDF(profileJoinColumn) === joinwithresult(entityJoinColumn),
            "left_outer"
          )
          .drop(joinwithresult(entityJoinColumn))
          .withColumn(targetColumn, joinwithresult(deviceName))
        resultDF
      case _ =>
        logger.warn(
          "GetHandsetTypeName can not be applied as sourceColumns size is not equals to 12."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)

  }

}
