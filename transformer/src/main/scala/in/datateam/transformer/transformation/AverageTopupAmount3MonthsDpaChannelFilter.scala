package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object AverageTopupAmount3MonthsDpaChannelFilter extends TransformerAggColTrait with Serializable {
  val logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.size match {
      case 4 =>
        val reloadsDF = sourceDFs.get(sourceColumns.head.entityName).get
        val reloadChannelTypeCode = sourceColumns(3).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val filteredReloadsDF = reloadsDF
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(
                to_date(lit(partitionDate), Misc.dateFormat),
                -91
              )
          )
          .filter(lower(col(reloadChannelTypeCode)).isin("ax", "iv", "ud", "es", "pn", "eg", "rp"))
          .drop(reloadChannelTypeCode)

        calculateAverageMonthlyTopup(filteredReloadsDF, targetDF, sourceColumns, joinColumns, targetColumn)
      case _ =>
        logger.warn("You need to supply 4 columns to call AverageTopupAmount3MonthsDpaChannelFilter")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def calculateAverageMonthlyTopup(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val reloadAmtCol = sourceColumns(2).columnName

    val typechangeDF = sourceDF.withColumn(reloadDateCol, col(reloadDateCol).cast(DateType))

    val groupedDF = typechangeDF
      .select(col(subscriberIdCol), col(reloadDateCol), col(reloadAmtCol))
      .filter(
        col(reloadDateCol) >= Misc.getNthDate(partitionDate, -91)
        &&
        col(reloadDateCol) <= partitionDateMinusTwo
        &&
        col(reloadAmtCol) > 0
      )
      .groupBy(col(subscriberIdCol))
      .agg(
        round((sum(col(reloadAmtCol)) / countDistinct(col(reloadDateCol))), 2)
          .as(targetColumn + "_temp")
      )

    val joinedDF = targetDF
      .join(
        groupedDF,
        targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(joinColumns.get.head.entityJoinColumn),
        "left_outer"
      )
      .drop(groupedDF(joinColumns.get.head.entityJoinColumn))

    val retDF = joinedDF.withColumn(targetColumn, joinedDF(targetColumn + "_temp"))

    retDF

  }

}
