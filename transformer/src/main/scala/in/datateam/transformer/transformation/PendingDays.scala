package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.format
import java.time.temporal.ChronoUnit

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object PendingDays extends TransformerColTrait with Serializable {

  /**
    * Calculates PendingDays from Date column. Expects DOB in d/MM/yyyy.
    *
    * @param row           Input row
    * @param sourceColumns Date(String) : Column for Date, in d/MM/yyyy format.
    * @param targetColumn  PendingDays column (Integer) : Pending Days Left, initially null.
    * @return Row with computed PendingDays
    */
  def process(row: Row, sourceColumns: Array[SourceColumn], targetColumn: String): Row = {
    val dateVal = row.getAs[String](sourceColumns.head.columnName)
    val dateValParsed =
      LocalDate.parse(dateVal, format.DateTimeFormatter.ofPattern("d/MM/yyyy"))
    val pendingDays = ChronoUnit.DAYS.between(LocalDate.now(), dateValParsed)
    val schemaVal = row.schema
    val validatedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => pendingDays
        case x: Any => row.getAs[Any](x)
      }
    }
    val validatedRow = new GenericRowWithSchema(validatedArray, schemaVal)
    validatedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val sourceDFEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => process(row, sourceColumns, targetColumn))(
      sourceDFEncoder
    )
  }
}
