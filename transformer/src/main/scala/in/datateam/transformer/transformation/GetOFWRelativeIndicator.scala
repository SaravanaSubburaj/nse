package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetOFWRelativeIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 6 =>
        val billingOfferEntity = sourceColumns.head.entityName
        val billingOfferId = sourceColumns.head.columnName
        val billingOfferDescription = sourceColumns(1).columnName
        val billingOfferSaleExpirationDate = sourceColumns(2).columnName
        val assignedSubscriberReferenceEntity = sourceColumns(3).entityName
        val assignedBillingOfferId = sourceColumns(3).columnName
        val subscriberId = sourceColumns(4).columnName
        val subscriberAssignedBillingOfferStartDate = sourceColumns(5).columnName

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val billingOfferDF = sourceDFs(billingOfferEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(billingOfferId, billingOfferDescription, billingOfferSaleExpirationDate)
          .drop(billingOfferSaleExpirationDate)

        val assignedBillingOfferDF = sourceDFs(assignedSubscriberReferenceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(assignedBillingOfferId, subscriberId, subscriberAssignedBillingOfferStartDate)

        val window = Window.partitionBy(subscriberId).orderBy(col(subscriberAssignedBillingOfferStartDate).desc)
        val groupedDf = assignedBillingOfferDF
          .withColumn("rank", row_number().over(window))
          .select(assignedBillingOfferId, subscriberId)
          .where("rank == 1")

        val joinDF = billingOfferDF
          .join(
            groupedDf,
            billingOfferDF(billingOfferId) ===
              groupedDf(assignedBillingOfferId),
            "left_outer"
          )
          .drop(groupedDf(assignedBillingOfferId))

        val derivedDF = joinDF.withColumn(
          targetColumn + "_Temp",
          when(upper(col(billingOfferDescription)).like("%OFW%"), "1").otherwise("0")
        )

        val resultJoinDF = targetDF
          .join(
            derivedDF,
            targetDF(profileJoinColumn) === derivedDF(
                entityJoinColumn
              ),
            "left_outer"
          )
          .drop(derivedDF(entityJoinColumn))
        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, derivedDF(targetColumn + "_Temp"))

        finalResultDF
      case _ =>
        logger.warn("GetOFWRelativeIndicator can not be applied as sourceColumns size is not equals to 6")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
