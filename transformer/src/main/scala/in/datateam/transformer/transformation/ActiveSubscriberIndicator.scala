package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object ActiveSubscriberIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 2 =>
        val subscriberUsageStagingEntity = sourceColumns.head.entityName
        val usageSubscriberId = sourceColumns.head.columnName
        val usageTranscationDate = sourceColumns(1).columnName

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val usageStagingDF = sourceDFs(subscriberUsageStagingEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(usageSubscriberId, usageTranscationDate)
          .filter(
            to_date(col(usageTranscationDate))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -15)
            && to_date(col(usageTranscationDate)) <= to_date(lit(partitionDate))
          )
          .drop(usageTranscationDate)

        val groupedDF =
          usageStagingDF.groupBy(usageSubscriberId).agg(first(usageSubscriberId).as(usageSubscriberId + "Temp"))

        val calculateColumnDF = groupedDF
          .withColumn(targetColumn + "_temp", when(usageStagingDF(usageSubscriberId).isNotNull, 1).otherwise(null))

        val finalJoinDF = targetDF
          .join(calculateColumnDF, targetDF(profileJoinColumn) === calculateColumnDF(entityJoinColumn), "left_outer")
          .drop(calculateColumnDF(entityJoinColumn))

        val resultDF =
          finalJoinDF.withColumn(targetColumn, calculateColumnDF(targetColumn + "_temp"))

        resultDF
      case _ =>
        logger.warn(
          "ActiveSubscriberIndicator can not be applied when source columns is not equal to 2."
        )
        targetDF
    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }
}
