package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetLatestReloadDateDpaChannelFilter extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def getLatestReloadDate(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val reloadAmtCol = sourceColumns(2).columnName

    val resultDF = joinColumns match {
      case Some(x) =>
        val groupedDF = sourceDF
          .select(col(subscriberIdCol), col(reloadDateCol), col(reloadAmtCol))
          .filter(
            col(reloadDateCol) >= Misc.getNthDate(partitionDate, -91)
            &&
            col(reloadDateCol) <= partitionDateMinusTwo
            &&
            col(reloadAmtCol) > 0
          )
          .groupBy(col(subscriberIdCol))
          .agg(max(col(reloadDateCol)).as(targetColumn + "_temp"))
        val joinedDF = targetDF
          .join(
            groupedDF,
            targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(joinColumns.get.head.entityJoinColumn),
            "left_outer"
          )
          .drop(groupedDF(joinColumns.get.head.entityJoinColumn))
        joinedDF.withColumn(targetColumn, joinedDF(targetColumn + "_temp"))

      case None =>
        val groupedDF = targetDF
          .filter(
            col(reloadDateCol) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -91)
            &&
            col(reloadDateCol) <= partitionDateMinusTwo
            &&
            col(reloadAmtCol) > 0
          )
          .groupBy(col(subscriberIdCol))
          .agg(
            max(col(reloadDateCol)).as(targetColumn),
            first(CommonEnums.DATE_PARTITION_COLUMN).as(CommonEnums.DATE_PARTITION_COLUMN)
          )
        groupedDF
          .select(col(sourceColumns.head.columnName), col(targetColumn), col(CommonEnums.DATE_PARTITION_COLUMN))
    }
    resultDF
  }

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.size match {
      case 4 =>
        val sourceDF = sourceDFs.get(sourceColumns.head.entityName).get
        val reloadChannelTypeCode = sourceColumns(3).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val filteredReloadsDF = sourceDF
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -91
            )
          )
          .filter(lower(col(reloadChannelTypeCode)).isin("ax", "iv", "ud", "es", "pn", "eg", "rp"))
          .drop(reloadChannelTypeCode)
        getLatestReloadDate(filteredReloadsDF, targetDF, sourceColumns, joinColumns, targetColumn)
      case _ =>
        logger.warn(
          "GetLatestReloadDateDpaChannelFilter can not be " +
          "applied as sourceColumns size is not equals to 4."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
