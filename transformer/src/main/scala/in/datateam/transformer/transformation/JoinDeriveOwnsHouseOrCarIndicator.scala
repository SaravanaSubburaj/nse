package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.to_date
import org.apache.spark.sql.functions.when

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDeriveOwnsHouseOrCarIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  //    Used by ff attributes:
  //       1. owns_car_indicator
  //       2. owns_house_indicator

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit
      spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val returnDF = sourceColumns.size match {
      case 1 =>
        val attributeColumnEntity = sourceColumns.head.entityName
        val attributeName = sourceColumns.head.columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val sourceDF = sourceDFs(attributeColumnEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(entityJoinColumn, attributeName)

        val resultDF = targetDF
          .join(sourceDF, targetDF(profileJoinColumn) === sourceDF(entityJoinColumn), "left_outer")
          .withColumn(
            targetColumn,
            when(lower(col(attributeName)).isin(TransformerEnums.MORTGAGED, TransformerEnums.OWNED), "1")
              .otherwise("0")
          )
          .drop(sourceDF(entityJoinColumn))

        resultDF

      case _ =>
        logger.warn(
          "JoinDeriveOwnsHouseOrCarIndicator can not be applied as sourceColumns size is not equals to 2."
        )
        targetDF

    }

    returnDF.select(targetDF.columns.map(c => col(c)): _*)

  }

}
