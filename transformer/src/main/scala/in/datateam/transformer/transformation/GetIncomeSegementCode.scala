package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetIncomeSegementCode extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 2 =>
        val subscriberEntity = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val grossMonthlyIncome = sourceColumns(1).columnName

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val subscriberDF = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, grossMonthlyIncome)

        val derivedDF = subscriberDF.withColumn(
          targetColumn + "_Temp",
          when(col(grossMonthlyIncome) >= 154750, "AB")
            .when(col(grossMonthlyIncome) >= 50250 && col(grossMonthlyIncome) <= 154749, "C")
            .when(col(grossMonthlyIncome) >= 15917 && col(grossMonthlyIncome) < 50249, "D")
            .otherwise("E")
        )

        val resultJoinDF = targetDF
          .join(
            derivedDF,
            targetDF(profileJoinColumn) === derivedDF(
                entityJoinColumn
              ),
            "left_outer"
          )
          .drop(derivedDF(entityJoinColumn))
        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, derivedDF(targetColumn + "_Temp"))

        finalResultDF
      case _ =>
        logger.warn("GetIncomeSegementCode can not be applied as sourceColumns size is not equals to 2")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
