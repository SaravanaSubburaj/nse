package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.min

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object FilterByMinimumPartition extends TransformerColTrait with Serializable {

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    //FIXME: Find better way to pass partiton column
    val subscriberDatasetPartitionCol = extraParameter(TransformerEnums.EXISTING_PARTITION_COL)
    val minPartition = sourceDF
      .select(min(subscriberDatasetPartitionCol))
      .head()
      .getString(0)
    val filteredDF =
      sourceDF.filter(
        col(subscriberDatasetPartitionCol) === minPartition
      )
    filteredDF
  }

}
