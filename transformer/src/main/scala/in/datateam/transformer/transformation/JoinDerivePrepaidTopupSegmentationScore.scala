package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object JoinDerivePrepaidTopupSegmentationScore extends TransformerAggColTrait with Serializable {

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val partitionValue = extraParameter(TransformerEnums.PARTITION_VALUE)

    val entityName = sourceColumns.head.entityName
    val subsId = sourceColumns(0).columnName
    val model = sourceColumns(1).columnName
    val score = sourceColumns(2).columnName
    val entityJoinCol = joinColumn.get.head.entityJoinColumn
    val profileJoinCol = joinColumn.get.head.profileJoinColumn

    val aaModelDF = TransformerHelpers
      .lookbackMonthly(sourceDFs, entityName, partitionValue)
      .filter(upper(col(model)) === "TOPUP_SEGMENTATION")

    val aaModelScoreDF = aaModelDF
      .select(col(subsId), col(score), col(model))

    val resultDF = targetDF
      .join(
        aaModelScoreDF,
        targetDF(profileJoinCol) === aaModelScoreDF(entityJoinCol),
        "left_outer"
      )
      .drop(aaModelScoreDF(entityJoinCol))
      .withColumn(targetColumn, aaModelScoreDF(score))

    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }

}
