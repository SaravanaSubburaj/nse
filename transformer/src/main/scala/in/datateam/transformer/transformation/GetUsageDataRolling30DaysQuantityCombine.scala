package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object GetUsageDataRolling30DaysQuantityCombine extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val UsageDataRolling30DaysQuantityPrepaidColumns = sourceColumns.slice(0, 3)
    val UsageDataRolling30DaysQuantityPostpaidColumns = sourceColumns.slice(3, 6)
    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"
    val UsageDataRolling30DaysQuantityPrepaidDF = GetUsageDataRolling30DaysQuantity.process(
      sourceDFs,
      targetDF,
      UsageDataRolling30DaysQuantityPrepaidColumns,
      targetJoinColumn,
      targetColumnPrepaid
    )(spark, extraParameter)

    val UsageDataRolling30DaysQuantityPostpaidDF = GetUsageDataRolling30DaysQuantity.process(
      sourceDFs,
      UsageDataRolling30DaysQuantityPrepaidDF,
      UsageDataRolling30DaysQuantityPostpaidColumns,
      targetJoinColumn,
      targetColumnPostpaid
    )(spark, extraParameter)

    val UsageDataRolling30DaysQuantityDF = CombinePrepaidAndPostpaidOperator
      .process(
        UsageDataRolling30DaysQuantityPostpaidDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
        ),
        targetColumn + "_Temp"
      )(spark, extraParameter)
    val finalResultDF = UsageDataRolling30DaysQuantityDF.withColumn(
      targetColumn,
      when(
        UsageDataRolling30DaysQuantityDF(targetColumn + "_Temp").isNotNull,
        UsageDataRolling30DaysQuantityDF(targetColumn + "_Temp")
      ).otherwise(0)
    )

    finalResultDF
  }
}
