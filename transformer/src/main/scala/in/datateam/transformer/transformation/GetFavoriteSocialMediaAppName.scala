package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.substring
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.functions.to_date
import org.apache.spark.sql.functions.trim
import org.apache.spark.sql.functions.when

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.helper.Misc

object GetFavoriteSocialMediaAppName extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

    if (sourceColumns.length != 4) {
      logger.warn("GetFavoriteSocialMediaAppName requires 4 source columns")
      return targetDF
    }

    val radcomDF = sourceDFs(sourceColumns.head.entityName)

    val bytesUploadedCol = sourceColumns(0).columnName
    val msisdnCol = sourceColumns(1).columnName
    val fbJoinCol = msisdnCol + "_fb"
    val instaJoinCol = msisdnCol + "_insta"
    val viberJoinCol = msisdnCol + "_viber"
    val subActivityCol = sourceColumns(2).columnName
    val recordCreationTimeCol = sourceColumns(3).columnName

    val subMSISDNCol = targetJoinColumn.get.head.profileJoinColumn
    val radcomMSISDNCol = targetJoinColumn.get.head.entityJoinColumn

    val filteredByDatesDF = radcomDF.filter(
      (to_date(col(recordCreationTimeCol)) >= date_add(
          to_date(lit(partitionDate), Misc.dateFormat),
          -8
        )) && (to_date(col(recordCreationTimeCol)) <= date_add(
          to_date(lit(partitionDate), Misc.dateFormat),
          -1
        ))
    )

    val filteredByActivityDF = filteredByDatesDF.filter(
      col(msisdnCol).like(TransformerEnums.MSISDN_FILTER)
      &&
      lower(trim(col(subActivityCol)))
        .like(TransformerEnums.ACTIVITY_FILTER_VIBER)
      ||
      lower(trim(col(subActivityCol)))
        .like(TransformerEnums.ACTIVITY_FILTER_FB)
      ||
      lower(trim(col(subActivityCol)))
        .like(TransformerEnums.ACTIVITY_FILTER_FBCDN)
      ||
      lower(trim(col(subActivityCol)))
        .like(TransformerEnums.ACTIVITY_FILTER_INSTAGRAM)
    )

    val avgUploadedDataDF =
      filteredByActivityDF
        .withColumn(msisdnCol, substring(col(msisdnCol), 3, 10))
        .withColumn(TransformerEnums.AVG_UPLOADED_DATA, col(bytesUploadedCol) / 7 * 0.000001)
        .repartitionAndSortWithInPartition(Array(col(msisdnCol), col(subActivityCol)), Array())

    val scoresDF = avgUploadedDataDF
      .groupBy(msisdnCol, subActivityCol)
      .agg(
        sum(TransformerEnums.AVG_UPLOADED_DATA)
          .as("score")
      )

    val facebookDF = scoresDF
      .filter(
        col(subActivityCol).like(TransformerEnums.ACTIVITY_FILTER_FB) || col(subActivityCol)
          .like(TransformerEnums.ACTIVITY_FILTER_FBCDN)
      )
      .withColumnRenamed("score", "score_fb")
      .withColumnRenamed(msisdnCol, fbJoinCol)
      .groupBy(col(fbJoinCol))
      .agg(sum("score_fb"))

    val instagramDF = scoresDF
      .filter(col(subActivityCol).like(TransformerEnums.ACTIVITY_FILTER_INSTAGRAM))
      .withColumnRenamed("score", "score_insta")
      .withColumnRenamed(msisdnCol, instaJoinCol)
      .groupBy(col(instaJoinCol))
      .agg(sum("score_insta"))

    val viberDF = scoresDF
      .filter(col(subActivityCol).like(TransformerEnums.ACTIVITY_FILTER_VIBER))
      .withColumnRenamed("score", "score_viber")
      .withColumnRenamed(msisdnCol, viberJoinCol)
      .groupBy(col(viberJoinCol))
      .agg(sum("score_viber"))

    val joinedDF1 = targetDF
      .join(viberDF, targetDF(subMSISDNCol) === viberDF(viberJoinCol), "left_outer")
      .drop(viberJoinCol)

    val joinedDF2 = joinedDF1
      .join(instagramDF, joinedDF1(subMSISDNCol) === instagramDF(instaJoinCol), "left_outer")
      .drop(instaJoinCol)

    val joinedDF3 = joinedDF2
      .join(facebookDF, joinedDF2(subMSISDNCol) === facebookDF(fbJoinCol), "left_outer")
      .drop(fbJoinCol)

    val resDF = joinedDF3
      .withColumn(
        targetColumn,
        when(col("sum(score_viber)") >= 0.3, "VIBER")
          .when(col("sum(score_insta)") >= 0.11, "INSTAGRAM")
          .when(col("sum(score_fb)") >= 0.10, "FACEBOOK")
          .otherwise(null)
      )
      .drop("sum(score_viber)")
      .drop("sum(score_fb)")
      .drop("sum(score_insta)")

    resDF
  }

}

// 0 :- bytes uploaded
// 1 :- msisdn
// 2 :- activity
