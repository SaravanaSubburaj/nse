package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetContractTypeCode extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 3 =>
        val contractStagingEntity = sourceColumns.head.entityName
        val contractTypeCodeColumn = sourceColumns.head.columnName
        val subscriberId = sourceColumns(1).columnName
        val contractStartDate = sourceColumns(2).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val sourceDF = sourceDFs(contractStagingEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(contractTypeCodeColumn, subscriberId, contractStartDate)
          .repartitionAndSortWithInPartition(Array(col(subscriberId)), Array(col(contractStartDate).desc))

        val window =
          Window.partitionBy(subscriberId).orderBy(col(contractStartDate).desc)
        val groupedDF = sourceDF
          .withColumn("rank", row_number().over(window))
          .select(subscriberId, contractTypeCodeColumn)
          .where("rank == 1")

        val derivedDf = groupedDF
          .withColumn(
            targetColumn + "_temp",
            when(upper(col(contractTypeCodeColumn)) === "NL", "OB")
              .otherwise("LI")
          )
          .drop(contractTypeCodeColumn)

        val joinDF = targetDF
          .join(broadcast(derivedDf), targetDF(profileJoinColumn) === derivedDf(entityJoinColumn), "left_outer")
          .drop(derivedDf(entityJoinColumn))
        val resultDF =
          joinDF.withColumn(targetColumn, joinDF(targetColumn + "_temp"))
        resultDF
      case _ =>
        logger.warn(
          "GetContractTypeCode can not be applied as sourceColumns size is not equals to 3."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
