package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

/**
  * @author Saravana(saravana.subburaj@thedatateam.in)
  */
object JoinDerivePullIntTravelIndCombine extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val intTravelIndPrepaidColumns = sourceColumns.slice(0, 5)
    val intTravelIndPostpaidColumns = sourceColumns.slice(5, 10)

    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"

    val intTravelIndPrepaidDF = JoinDerivePullIntTravelInd.process(
      sourceDFs,
      targetDF,
      intTravelIndPrepaidColumns,
      targetJoinColumn,
      targetColumnPrepaid
    )(spark, extraParameter)
    val intTravelIndPostpaidDF = JoinDerivePullIntTravelInd.process(
      sourceDFs,
      intTravelIndPrepaidDF,
      intTravelIndPostpaidColumns,
      targetJoinColumn,
      targetColumnPostpaid
    )(spark, extraParameter)
    val intTravelIndDF = CombinePrepaidAndPostpaidOperator
      .process(
        intTravelIndPostpaidDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
        ),
        targetColumn
      )(spark, extraParameter)

    intTravelIndDF
  }
}
