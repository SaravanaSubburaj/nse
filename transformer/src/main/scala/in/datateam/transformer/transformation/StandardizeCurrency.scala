package in.datateam.transformer.transformation

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import spray.json._

import in.datateam.transformer.parser.StandardCurrency
import in.datateam.transformer.parser.StandardCurrencyJsonProtocol._
import in.datateam.utils.InvalidCurrencyFormatException
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.helper.FileSystemUtils

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object StandardizeCurrency extends TransformerColTrait with Serializable {

  def getAmountAndSign(amountSplitted: Array[String]): (String, String) = {
    if (amountSplitted(0).forall(_.isDigit)) {
      (amountSplitted(0), amountSplitted(1))
    } else if (amountSplitted(1).forall(_.isDigit)) {
      (amountSplitted(1), amountSplitted(0))
    } else {
      throw InvalidCurrencyFormatException(
        "Expected in format {currencySign currencyValue} " +
        "or {currencyValue currencySign}"
      )
    }
  }

  def processRow(
      row: Row,
      sourceColumns: Array[SourceColumn],
      targetColumn: String,
      currencyBroadcast: Broadcast[Map[String, String]]
    ): Row = {
    val amountWithSign = row.getAs[String](sourceColumns(0).columnName)
    val amountSplitted = amountWithSign.split(" ")
    if (amountSplitted.size != 2) {
      throw InvalidCurrencyFormatException(
        "Expected in format {currencySign currencyValue} " +
        "or {currencyValue currencySign}"
      )
    }
    val amountAndSign = getAmountAndSign(amountSplitted)
    val currencySign = currencyBroadcast.value(amountAndSign._2)
    val schemaVal = row.schema
    val processedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => s"${amountAndSign._1} ${currencySign}"
        case x: Any => row.getAs[Any](x)
      }
    }
    val processedRow = new GenericRowWithSchema(processedArray, schemaVal)
    processedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val broadcastDetail =
      broadcastDetails.filter(b => b.lookupName == "StandardCurrency")
    val currencyLookupString =
      FileSystemUtils.readFile(broadcastDetail.head.pathUrl)
    val standardCurrencyJson =
      currencyLookupString.parseJson.convertTo[StandardCurrency]
    val currencyMap =
      standardCurrencyJson.currencyLookup.toSeq
        .map(c => (c.currencySign, c.currencyCode))
        .toMap[String, String]
    val currencyBroadcast = spark.sparkContext.broadcast(currencyMap)
    implicit val sourceDFEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(
      row => processRow(row, sourceColumns, targetColumn, currencyBroadcast)
    )
  }

}
