package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.count
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetMostFreqAvailAllDayRoamSurfPrepaid extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.size match {
      case 7 =>
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val prepiadPromoAvailSubsId = sourceColumns(0).columnName
        val prepaidPromoAvailPromoId = sourceColumns(1).columnName
        val prepaidPromoPromoId = sourceColumns(2).columnName
        val prepaidPromoAvailPromoDesc = sourceColumns(3).columnName
        val prepaidPromoAvailPrmoChargeAmt = sourceColumns(4).columnName
        val prepaidPromoAvailServiceId = sourceColumns(6).columnName
        val prepaidPromoAvailPrmoOptDate = sourceColumns(5).columnName

        val prepaidPromoAvailStgDF = sourceDFs(sourceColumns(1).entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -90
            )
          )
          .select(
            prepiadPromoAvailSubsId,
            prepaidPromoAvailPromoId,
            prepaidPromoAvailPrmoChargeAmt,
            prepaidPromoAvailPrmoOptDate,
            prepaidPromoAvailServiceId
          )

        val prepaidPromoDF = sourceDFs(sourceColumns(3).entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -1
            )
          )
          .select(
            prepaidPromoAvailPromoId,
            prepaidPromoPromoId,
            prepaidPromoAvailPromoDesc
          )

        val joinDF = prepaidPromoAvailStgDF
          .join(
            broadcast(prepaidPromoDF),
            prepaidPromoAvailStgDF(prepaidPromoAvailPromoId) === prepaidPromoDF(prepaidPromoPromoId),
            "left_outer"
          )
          .drop(prepaidPromoDF(prepaidPromoPromoId))

        val filterRecs = joinDF
          .filter(
            col(prepaidPromoAvailPrmoOptDate) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -90
            )
            &&
            col(prepaidPromoAvailPrmoOptDate) < to_date(lit(partitionDate), Misc.dateFormat)
          )
          .filter(col(prepaidPromoAvailPrmoChargeAmt) > 0)
          .filter(col(prepaidPromoAvailServiceId).isNotNull)
          .filter(lower(col(prepaidPromoAvailPromoDesc)) rlike TransformerEnums.ROAM_SURF_PREPAID_FILTER)
          .repartitionAndSortWithInPartition(
            Array(col(prepiadPromoAvailSubsId), col(prepaidPromoAvailPromoDesc)),
            Array()
          )

        val groupedDF = filterRecs
          .groupBy(prepiadPromoAvailSubsId, prepaidPromoAvailPromoDesc)
          .agg(count(prepaidPromoAvailPromoDesc).alias("cnt"))
          .repartitionAndSortWithInPartition(Array(col(prepiadPromoAvailSubsId)), Array(col("cnt").desc))

        val rankedDF = groupedDF
          .withColumn(
            "rank",
            row_number().over(
              Window
                .partitionBy(col(prepiadPromoAvailSubsId))
                .orderBy(col("cnt").desc)
            )
          )
          .filter(col("rank") === 1)
          .select(prepiadPromoAvailSubsId, prepaidPromoAvailPromoDesc)
          .repartitionAndSortWithInPartition(
            Array(col(joinColumns.get.head.entityJoinColumn)),
            Array(col(joinColumns.get.head.entityJoinColumn))
          )

        val joinedDF = targetDF
          .join(
            rankedDF,
            targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(
                joinColumns.get.head.entityJoinColumn
              ),
            "left_outer"
          )
          .drop(groupedDF(joinColumns.get.head.entityJoinColumn))

        val resultantDF = joinedDF
          .drop(targetColumn)
          .withColumn(targetColumn, joinedDF(prepaidPromoAvailPromoDesc))
        resultantDF
      case _ =>
        logger.warn(
          "GetMostFreqAvailAllDayRoamSurfPrepaid can not be applied as " +
          "sourceColumns size is not equals to 7."
        )
        targetDF

    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }
}
