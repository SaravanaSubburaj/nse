package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lower

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object ConvertToLower extends TransformerColTrait with Serializable {

  /**
    * By default 1st column of the source column is converted to lower case
    *
    * @param sourceDF     - input DataFrame
    * @param sourceColumns - Column on which the transformation to lowercase is applied
    * @param targetColumn - New column formed with lowercase values
    * @return -  Dataframe with additional target column with lowercase values present
    */
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val colName = sourceColumns.head.columnName
    sourceDF.withColumn(targetColumn, lower(col(colName)))

  }
}
