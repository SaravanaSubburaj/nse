package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetUsageDataRolling30DaysQuantity extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 3 =>
        val entityName = sourceColumns.head.entityName
        val totalDataVolumneCount = sourceColumns.head.columnName
        val subscriberId = sourceColumns(1).columnName
        val usageTypeCode = sourceColumns(2).columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val sourceDF = sourceDFs(entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -30)
          )
          .select(totalDataVolumneCount, subscriberId, usageTypeCode)
          .filter(upper(col(usageTypeCode)) === "DATA")
          .drop(usageTypeCode)

        val groupDF = sourceDF
          .groupBy(subscriberId)
          .agg(sum(col(totalDataVolumneCount)).as("count"))

        val derivedDF =
          groupDF.withColumn(
            targetColumn + "_Temp",
            when(col("count").isNotNull, col("count") / (1024 * 1024)).otherwise(null)
          )

        val resultJoinDF = targetDF
          .join(
            derivedDF,
            targetDF(profileJoinColumn) === derivedDF(entityJoinColumn),
            "left_outer"
          )
          .drop(derivedDF(entityJoinColumn))
        val finalResultDF = resultJoinDF.withColumn(
          targetColumn,
          derivedDF(targetColumn + "_Temp")
        )

        finalResultDF
      case _ =>
        logger.warn(
          "GetUsageDataRolling30DaysQuantity can not be applied when source columns is not equal to 3."
        )
        targetDF
    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }
}
