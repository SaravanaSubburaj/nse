package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetMaritalStatusCode extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 5 =>
        val subscriberEntity = sourceColumns.head.entityName
        val contactEntity = sourceColumns(2).entityName
        val subscriberId = sourceColumns.head.columnName
        val mainContactId = sourceColumns(1).columnName
        val contactId = sourceColumns(2).columnName
        val maritalStatusCode = sourceColumns(3).columnName
        val paymentCatogeryCode = sourceColumns(4).columnName
        val partitionDate =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val subscriberDF = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, mainContactId, paymentCatogeryCode)
          .filter(upper(col(paymentCatogeryCode)) === "POST")

        val contactDf = sourceDFs(contactEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(contactId, maritalStatusCode)

        val joinDf = subscriberDF
          .join(
            contactDf,
            subscriberDF(mainContactId) === contactDf(contactId),
            "left_outer"
          )
          .drop(subscriberDF(mainContactId))
          .drop(contactDf(contactId))
        val resultJoinDF = targetDF
          .join(
            joinDf,
            targetDF(profileJoinColumn) === joinDf(entityJoinColumn),
            "left_outer"
          )
          .drop(subscriberDF(subscriberId))

        val finalResultDF =
          resultJoinDF.withColumn(targetColumn, contactDf(maritalStatusCode))

        finalResultDF
      case _ =>
        logger.warn(
          "GetMaritalStatusCode can not be applied as " +
          "sourceColumns size is not equals to 5."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }
}
