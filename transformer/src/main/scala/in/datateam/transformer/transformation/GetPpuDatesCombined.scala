package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object GetPpuDatesCombined extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val prepaidSourceColumns = sourceColumns.slice(0, 2)
    val postpaidSourceColumns = sourceColumns.slice(2, 4)

    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"

    val prepaidDF = GetPpuDates.process(
      sourceDFs,
      targetDF,
      prepaidSourceColumns,
      targetJoinColumn,
      targetColumnPrepaid
    )(spark, extraParameter): DataFrame

    val postpaidDF = GetPpuDates.process(
      sourceDFs,
      prepaidDF,
      postpaidSourceColumns,
      targetJoinColumn,
      targetColumnPostpaid
    )(spark, extraParameter): DataFrame

    val combinedDF = CombinePrepaidAndPostpaidOperator.process(
      postpaidDF,
      Array(
        SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
        SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
      ),
      targetColumn
    )(spark, extraParameter): DataFrame

    combinedDF

  }

}
