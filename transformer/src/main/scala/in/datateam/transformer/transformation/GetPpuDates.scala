package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.max

import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object GetPpuDates extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val subscriberKey = sourceColumns.head.columnName
    val ppuDateCol = sourceColumns(1).columnName

    val adhJoinCol = targetJoinColumn.get.head.entityJoinColumn
    val profileJoinCol = targetJoinColumn.get.head.profileJoinColumn

    val adhDF = sourceDFs(sourceColumns.head.entityName)
      .repartitionAndSortWithInPartition(Array(col(subscriberKey)), Array())

    val groupedDF = adhDF
      .groupBy(col(subscriberKey))
      .agg(max(ppuDateCol).as(targetColumn))

    val castedDF = groupedDF
      .withColumn(subscriberKey, groupedDF(subscriberKey).cast("string"))
      .repartitionAndSortWithInPartition(Array(col(adhJoinCol)), Array(col(adhJoinCol)))

    val joinedDF = targetDF
      .join(
        castedDF,
        targetDF(profileJoinCol) === castedDF(adhJoinCol),
        "left_outer"
      )
      .drop(castedDF(adhJoinCol))

    joinedDF
  }

}
