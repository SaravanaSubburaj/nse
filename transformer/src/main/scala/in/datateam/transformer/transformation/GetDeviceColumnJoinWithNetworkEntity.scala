package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger
import org.joda.time.DateTime

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter

object GetDeviceColumnJoinWithNetworkEntity extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 17 =>
        val networkDataEntity = sourceColumns.head.entityName
        val networkVoiceEntity = sourceColumns(3).entityName
        val networkSmsEntity = sourceColumns(6).entityName
        val subscriberEntity = sourceColumns(12).entityName
        val networkDataGnEntity = sourceColumns(15).entityName
        val deviceEntity = sourceColumns(9).entityName
        val dataMsisdn = sourceColumns.head.columnName
        val dataDeviceId = sourceColumns(1).columnName
        val dataTopLocationDate = sourceColumns(2).columnName
        val voiceMsisdn = sourceColumns(3).columnName
        val voiceDeviceId = sourceColumns(4).columnName
        val voiceTopLocationDate = sourceColumns(5).columnName
        val smsMsisdn = sourceColumns(6).columnName
        val smsDeviceId = sourceColumns(7).columnName
        val smsTopLocationDate = sourceColumns(8).columnName
        val deviceId = sourceColumns(9).columnName
        val AttributeColumn = sourceColumns(10).columnName
        val subscriberStatusCode = sourceColumns(11).columnName
        val subscriberId = sourceColumns(12).columnName
        val msisdnValue = sourceColumns(13).columnName
        val gnMsisdn = sourceColumns(14).columnName
        val gnDeviceId = sourceColumns(15).columnName
        val gnTopLocationDate = sourceColumns(16).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate =
          extraParameter(TransformerEnums.PARTITION_VALUE)
        val deviceDf =
          getDeviceDF(sourceDFs(deviceEntity))(spark, extraParameter)
            .select(deviceId, AttributeColumn)
            .filter(!trim(col(deviceId)).isin(""))

        val dataDf = sourceDFs(networkDataEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -30)
          )
          .select(dataDeviceId, dataMsisdn, dataTopLocationDate)
          .filter(col(dataDeviceId).isNotNull)
          .filter(!trim(col(dataDeviceId)).isin(""))

        val dataGroupedDF =
          dataDf.groupBy(col(dataMsisdn), col(dataDeviceId)).agg(max(dataTopLocationDate).as(dataTopLocationDate))

        val voiceDf = sourceDFs(networkVoiceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -30)
          )
          .select(voiceDeviceId, voiceMsisdn, voiceTopLocationDate)
          .filter(col(voiceDeviceId).isNotNull)
          .filter(!trim(col(voiceDeviceId)).isin(""))

        val voiceGroupedDf =
          voiceDf.groupBy(col(voiceMsisdn), col(voiceDeviceId)).agg(max(voiceTopLocationDate).as(voiceTopLocationDate))

        val smsDf = sourceDFs(networkSmsEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -30)
          )
          .select(smsDeviceId, smsMsisdn, smsTopLocationDate)
          .filter(col(smsDeviceId).isNotNull)
          .filter(!trim(col(smsDeviceId)).isin(""))

        val smsGroupedDf =
          smsDf.groupBy(col(voiceMsisdn), col(voiceDeviceId)).agg(max(smsTopLocationDate).as(smsTopLocationDate))

        val dataGnDf = sourceDFs(networkDataGnEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -30)
          )
          .select(gnDeviceId, gnMsisdn, gnTopLocationDate)
          .filter(col(gnDeviceId).isNotNull)
          .filter(!trim(col(gnDeviceId)).isin(""))

        val dataGnGroupedDF =
          dataGnDf.groupBy(col(gnMsisdn), col(gnDeviceId)).agg(max(gnTopLocationDate).as(gnTopLocationDate))

        val unionDf = dataGroupedDF
          .union(dataGnGroupedDF)
          .union(voiceGroupedDf)
          .union(smsGroupedDf)

        val window =
          Window.partitionBy(dataMsisdn).orderBy(col(dataTopLocationDate).desc)
        val groupedDF = unionDf
          .withColumn("rank", row_number().over(window))
          .select(dataMsisdn, dataDeviceId)
          .where("rank == 1")
          .select(dataMsisdn, dataDeviceId)

        val joinWithDevice = groupedDF
          .join(
            deviceDf,
            groupedDF(dataDeviceId) === deviceDf(deviceId)
          )
          .drop(deviceId)

        val resultDF = targetDF
          .join(
            joinWithDevice,
            targetDF(profileJoinColumn) === joinWithDevice(entityJoinColumn),
            "left_outer"
          )
          .drop(joinWithDevice(entityJoinColumn))
          .withColumn(targetColumn, joinWithDevice(AttributeColumn))

        resultDF
      case _ =>
        logger.warn(
          "GetDeviceColumnJoinWithNetworkEntity can not be applied" +
          " as sourceColumns size is not equals to 17."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)

  }

  def getDeviceDF(sourceDF: DataFrame)(implicit spark: SparkSession, extraParameter: Map[String, String]): DataFrame = {

    val partitionValue =
      extraParameter(TransformerEnums.PARTITION_VALUE)
    val currentPartitionFormatted = dateFormatter.parse(partitionValue)

    val dateTimeMonthSub1 = new DateTime(currentPartitionFormatted)
    val dateTimeMonthSub2 =
      new DateTime(currentPartitionFormatted).minusMonths(1)
    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val currentMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val previousMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val currentMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === currentMonthVal
    )
    val previousMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === previousMonthVal
    )

    val subscriberRevenueSummaryDF = if (!currentMonthDF.take(1).isEmpty) {
      currentMonthDF
    } else if (!previousMonthDF.take(1).isEmpty) {
      previousMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
    subscriberRevenueSummaryDF
  }

}
