package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDirectPullWithSmallEntity extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  /*Working for following attributes
   * 1. billing_offer_desc
   * 2. customer_facing_unit_type_description
   * */

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 1 =>
        val attributeColumnEntity = sourceColumns.head.entityName
        val attributeColumn = sourceColumns.head.columnName
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val attributeColumnDF = sourceDFs(attributeColumnEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(entityJoinColumn, attributeColumn)
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultJoinDF = targetDF
          .join(
            broadcast(attributeColumnDF),
            targetDF(profileJoinColumn) === attributeColumnDF(entityJoinColumn),
            "left_outer"
          )
          .drop(attributeColumnDF(entityJoinColumn))

        val finalResultDF = resultJoinDF.withColumn(targetColumn, resultJoinDF(attributeColumn))

        finalResultDF
      case _ =>
        logger.warn("JoinDirectPullWithSmallEntity can not be applied as sourceColumns size is not equals to 1.")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
