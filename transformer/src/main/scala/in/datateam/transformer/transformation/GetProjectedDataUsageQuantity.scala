package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetProjectedDataUsageQuantity extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    //    // TODO: Modify implementation with new logic.
    //    return targetDF
    val resultDF = sourceColumns.size match {
      case 17 =>
        val subscriberReferenceEntity = sourceColumns(15).entityName
        val subscriberId = sourceColumns(14).columnName
        val dailyPostpaidEntity = sourceColumns(0).entityName
        val postpaidSubscriberId = sourceColumns(0).columnName
        val totalDataVolumeCount = sourceColumns(1).columnName
        val financialInvoiceStagingEntity = sourceColumns(2).entityName
        val cycleRunDat = sourceColumns(2).columnName
        val periodCoverageStartDate = sourceColumns(3).columnName
        val invoiceStagingFinancialAccountId = sourceColumns(4).columnName
        val financialAccountId = sourceColumns(5).columnName
        val financialAccountEntity = sourceColumns(5).entityName
        val paymentCategoryCode = sourceColumns(7).columnName
        val usageTypeCode = sourceColumns(8).columnName
        val usageTransactionDate = sourceColumns(9).columnName
        val periodCoverageEndDate = sourceColumns(10).columnName
        val billNumber = sourceColumns(11).columnName
        val financialAccountStatusCode = sourceColumns(12).columnName
        val financialActivationDate = sourceColumns(13).columnName
        val ReferencefinancialAccountId = sourceColumns(16).columnName
        val expiryDate = sourceColumns(15).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val dailyPostpaidDF = sourceDFs
          .get(dailyPostpaidEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -30)
          )
          .select(postpaidSubscriberId, totalDataVolumeCount, paymentCategoryCode, usageTypeCode, usageTransactionDate)

        val financialInvoiceStagingNoFilterDF =
          TransformerHelpers.lookbackMonthly(sourceDFs, financialInvoiceStagingEntity, partitionDate)

        val financialInvoiceStagingDateArray =
          financialInvoiceStagingNoFilterDF.select(CommonEnums.DATE_PARTITION_COLUMN).take(1)

        val date = financialInvoiceStagingDateArray.size match {
          case 1 =>
            val pDate = financialInvoiceStagingDateArray.head.getAs(CommonEnums.DATE_PARTITION_COLUMN).toString
            YearMonth.parse(pDate, DateTimeFormatter.ofPattern(Misc.monthOnlyDateFormat)).atDay(1)
          case _ =>
            logger.warn(s"${financialInvoiceStagingEntity} entity is having 0 record.")
            LocalDate.parse(partitionDate, DateTimeFormatter.ofPattern(Misc.dateFormat))
        }
        val month = YearMonth.from(date)
        val firstDayofMonth = java.sql.Date.valueOf(month.atDay(1))
        val lastDayofMonth = java.sql.Date.valueOf(month.atEndOfMonth())

        val financialInvoiceStagingDF = financialInvoiceStagingNoFilterDF
          .select(
            cycleRunDat,
            periodCoverageStartDate,
            invoiceStagingFinancialAccountId,
            periodCoverageEndDate,
            billNumber
          )
          .withColumn(periodCoverageEndDate, col(periodCoverageEndDate).cast(DateType))
          .filter(col(periodCoverageEndDate) >= firstDayofMonth && col(periodCoverageEndDate) <= lastDayofMonth)

        val financialAccountDF = sourceDFs
          .get(financialAccountEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(financialAccountId, financialAccountStatusCode, financialActivationDate)
          .filter(upper(col(financialAccountStatusCode)) === "O" || upper(col(financialAccountStatusCode)) === "SUS")
          .drop(financialAccountStatusCode)

        val accountGroupDf = financialAccountDF
          .groupBy(financialAccountId)
          .agg(max(financialActivationDate).alias(financialActivationDate))
          .drop(financialActivationDate)

        val subscriberReferenceDf = sourceDFs
          .get(subscriberReferenceEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, ReferencefinancialAccountId, expiryDate)
          .filter(col(expiryDate).isNull)

        val joinSubscriberandFinancial = subscriberReferenceDf
          .join(
            accountGroupDf,
            subscriberReferenceDf(ReferencefinancialAccountId) === accountGroupDf(financialAccountId),
            "inner"
          )
          .drop(accountGroupDf(financialAccountId))

        val window = Window.partitionBy(invoiceStagingFinancialAccountId).orderBy(col(billNumber).desc)
        val maxBillDF = financialInvoiceStagingDF
          .withColumn("rank", row_number().over(window))
          .where("rank == 1")
          .select(invoiceStagingFinancialAccountId, cycleRunDat, periodCoverageStartDate, periodCoverageEndDate)

        val filterByStringDF = dailyPostpaidDF
          .filter(lower(col(usageTypeCode)).rlike(TransformerEnums.DATA))
          .filter(lower(col(paymentCategoryCode)).rlike(TransformerEnums.POST))

        val joinWithInvoiceStaging = joinSubscriberandFinancial
          .join(
            maxBillDF,
            joinSubscriberandFinancial(financialAccountId) === maxBillDF(invoiceStagingFinancialAccountId),
            "inner"
          )
          .drop(maxBillDF(invoiceStagingFinancialAccountId))

        val joinWithSubscriberReference = joinWithInvoiceStaging
          .join(
            filterByStringDF,
            joinWithInvoiceStaging(subscriberId) === filterByStringDF(postpaidSubscriberId),
            "left_outer"
          )
          .drop(joinWithInvoiceStaging(subscriberId))

        val typeChangeDF = joinWithSubscriberReference
          .withColumn(cycleRunDat, col(cycleRunDat).cast(DateType))
          .withColumn(usageTransactionDate, col(usageTransactionDate).cast(DateType))
          .withColumn(periodCoverageStartDate, col(periodCoverageStartDate).cast(DateType))

        val dateFilter = typeChangeDF.filter(
          col(usageTransactionDate) >= col(periodCoverageStartDate)
          && col(usageTransactionDate) <= (to_date(lit(partitionDate), Misc.dateFormat))
        )

        val tempTargetColumn = targetColumn + "_temp"
        val sumOfTotalDataVolumeCount = "sumOfTotalDataVolumeCount"
        val groupDF = dateFilter
          .groupBy(postpaidSubscriberId, periodCoverageStartDate, cycleRunDat)
          .agg(sum(col(totalDataVolumeCount)).as(sumOfTotalDataVolumeCount))

        val daysBtwCurrentAndPeriod = "daysBtwCurrentAndPeriod"
        val daysBtwCycleRunAndCurrent = "daysBtwCycleRunAndCurrent"
        val calculateDaysDF = groupDF
          .withColumn(
            daysBtwCurrentAndPeriod,
            datediff(to_date(lit(partitionDate), Misc.dateFormat), col(periodCoverageStartDate))
          )
          .withColumn(
            daysBtwCycleRunAndCurrent,
            datediff(to_date(lit(partitionDate), Misc.dateFormat), col(cycleRunDat))
          )
        val getTargetColumnValue = calculateDaysDF.withColumn(
          tempTargetColumn,
          (((col(sumOfTotalDataVolumeCount) / col(daysBtwCurrentAndPeriod)) * col(daysBtwCycleRunAndCurrent)) / (1024 * 1024))
        )
        val resultJoinDF = targetDF
          .join(
            getTargetColumnValue,
            targetDF(profileJoinColumn) === getTargetColumnValue(entityJoinColumn),
            "left_outer"
          )
          .drop(getTargetColumnValue(entityJoinColumn))

        val finalResultDF = resultJoinDF.withColumn(targetColumn, resultJoinDF(tempTargetColumn))
        finalResultDF
      case _ =>
        logger.warn(
          "GetProjectedDataUsageQuantity can not be applied as " +
          "sourceColumns size is not equals to 17."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
