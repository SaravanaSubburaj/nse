package in.datateam.transformer.transformation

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter
import org.apache.log4j.Logger
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date
import org.apache.spark.sql.functions.current_date
import org.apache.spark.sql.functions.year
import org.apache.spark.sql.functions.concat
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.IntegerType
import org.joda.time.DateTime

object JoinDeriveBirthDateCombine extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

    val subsEntityName = sourceColumns(0).entityName
    val subsId = sourceColumns(0).columnName
    val subsBirthDate = sourceColumns(1).columnName
    val subsEntityJoinColumn = targetJoinColumn.get(0).entityJoinColumn

    val aaModelEntityName = sourceColumns(2).entityName
    val aaModelSubsId = sourceColumns(2).columnName
    val aaModelAge = sourceColumns(3).columnName
    val aaModelModel = sourceColumns(4).columnName

    val aaModelEntityJoinColumn = targetJoinColumn.get(1).entityJoinColumn

    val partitionValueFormatted = dateFormatter.parse(partitionDate)
    val partitionValueFormatted1 =
      dateFormatter.format(partitionValueFormatted)

    val dateTimeMonthSub1 =
      new DateTime(partitionValueFormatted1).minusMonths(1)
    val dateTimeMonthSub2 =
      new DateTime(partitionValueFormatted1).minusMonths(2)

    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val firstMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val secondMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val profileJoinColumn = targetJoinColumn.get(0).profileJoinColumn

    val calculatedBirthDate = "calculated_birth_date"

    val subscriberDF = sourceDFs(subsEntityName)
      .filter(
        to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
        > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
      )
      .select(subsId, subsBirthDate)
      .repartitionAndSortWithInPartition(Array(col(subsId)), Array())

    val subscriberJoinedDF = targetDF
      .join(
        subscriberDF,
        targetDF(profileJoinColumn) === subscriberDF(subsEntityJoinColumn),
        "left_outer"
      )
      .drop(subscriberDF(subsEntityJoinColumn))

    val sourceDF = sourceDFs(aaModelEntityName).select(aaModelSubsId, aaModelAge)

    val firstMonthDF = sourceDF.filter(
      s"${CommonEnums.DATE_PARTITION_COLUMN} == '$firstMonthVal'" +
      s" AND (upper(${aaModelModel})='${TransformerEnums.AGE}')" +
      s" AND $aaModelSubsId IS NOT NULL"
    )

    val secondMonthDF = sourceDF.filter(
      s"${CommonEnums.DATE_PARTITION_COLUMN} == '$secondMonthVal'" +
      s" AND (upper(${aaModelModel})='${TransformerEnums.AGE}')" +
      s" AND $aaModelSubsId IS NOT NULL"
    )

    val aaModelDF = if (!firstMonthDF.take(1).isEmpty) {
      firstMonthDF
    } else if (!secondMonthDF.take(1).isEmpty) {
      secondMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }

    val aaModelTransformedDF = aaModelDF
      .withColumn(
        "calculated_birth_date",
        concat(year(current_date()).cast(IntegerType) - aaModelDF(aaModelAge).cast(IntegerType), lit("-01-01"))
      )

    val resultDF = subscriberJoinedDF
      .join(
        aaModelTransformedDF,
        subscriberJoinedDF(profileJoinColumn) === aaModelTransformedDF(aaModelEntityJoinColumn),
        "left_outer"
      )
      .drop(aaModelTransformedDF(aaModelEntityJoinColumn))

    val finalDF = CombinePrepaidAndPostpaidOperator
      .process(
        resultDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, subsBirthDate),
          SourceColumn(CommonEnums.PROFILE_MASTER, calculatedBirthDate)
        ),
        targetColumn
      )(spark, extraParameter)

    finalDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
