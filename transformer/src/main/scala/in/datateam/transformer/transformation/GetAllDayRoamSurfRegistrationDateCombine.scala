package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

/**
  * @author Saravana(saravana.subburaj@thedatateam.in)
  */
object GetAllDayRoamSurfRegistrationDateCombine extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val allDayRoamSurfRegistrationDatePrepaidColumns = sourceColumns.slice(0, 5)
    val allDayRoamSurfRegistrationDatePostpaidColumns =
      sourceColumns.slice(5, 8)
    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"

    val allDayRoamSurfRegistrationDatePrepaidDF =
      GetAllDayRoamSurfRegistrationDatePrepaid.process(
        sourceDFs,
        targetDF,
        allDayRoamSurfRegistrationDatePrepaidColumns,
        targetJoinColumn,
        targetColumnPrepaid
      )(spark, extraParameter)

    val allDayRoamSurfRegistrationDatePostpaidDF =
      GetAllDayRoamSurfRegistrationDatePostpaid.process(
        sourceDFs,
        allDayRoamSurfRegistrationDatePrepaidDF,
        allDayRoamSurfRegistrationDatePostpaidColumns,
        targetJoinColumn,
        targetColumnPostpaid
      )(spark, extraParameter)

    val allDayRoamSurfRegistrationDateDF = CombinePrepaidAndPostpaidOperator
      .process(
        allDayRoamSurfRegistrationDatePostpaidDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
        ),
        targetColumn
      )(spark, extraParameter)

    allDayRoamSurfRegistrationDateDF

  }

}
