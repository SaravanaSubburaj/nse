package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDirectWithBillingArrangement extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 9 =>
        val billingArrangementEntity = sourceColumns(2).entityName
        val financialAccountEntity = sourceColumns(1).entityName
        val financialAccountId = sourceColumns.head.columnName
        val attributeColumn = sourceColumns(2).columnName
        val financialAccountIdbillingArrangement = sourceColumns(3).columnName
        val financialStatusCode = sourceColumns(4).columnName
        val financialActivationDate = sourceColumns(5).columnName
        val financialSubscriberReferenceEntity = sourceColumns(6).entityName
        val subscriberReferenceSubscriberId = sourceColumns(6).columnName
        val expiryDate = sourceColumns(7).columnName
        val subscriberReferenceFinancialId = sourceColumns(8).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val subscriberReferenceDf = sourceDFs
          .get(financialSubscriberReferenceEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberReferenceSubscriberId, subscriberReferenceFinancialId, expiryDate)
          .filter(col(expiryDate).isNull)
          .drop(expiryDate)

        val financialAccountDF = sourceDFs(financialAccountEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(
            financialAccountId,
            financialStatusCode,
            financialActivationDate
          )
          .filter(
            upper(col(financialStatusCode)) === "O" || upper(
              col(financialStatusCode)
            ) === "SUS"
          )
          .drop(financialStatusCode)
          .repartitionAndSortWithInPartition(Array(col(financialAccountId)), Array(col(financialActivationDate)))

        val accountGroupDf = financialAccountDF
          .groupBy(financialAccountId)
          .agg(max(financialActivationDate).as(financialActivationDate))
          .drop(financialActivationDate)

        val joinWithSubscriberReference = subscriberReferenceDf
          .join(
            accountGroupDf,
            subscriberReferenceDf(subscriberReferenceFinancialId) ===
              accountGroupDf(financialAccountId),
            "left_outer"
          )
          .drop(subscriberReferenceDf(subscriberReferenceFinancialId))

        val billingArrangementDf = sourceDFs(billingArrangementEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -1
            )
          )
          .select(financialAccountIdbillingArrangement, attributeColumn)
          .repartitionAndSortWithInPartition(
            Array(col(financialAccountIdbillingArrangement)),
            Array(col(financialAccountIdbillingArrangement))
          )

        val joinDF = joinWithSubscriberReference
          .join(
            billingArrangementDf,
            joinWithSubscriberReference(financialAccountId) === billingArrangementDf(
                financialAccountIdbillingArrangement
              ),
            "inner"
          )
          .drop(billingArrangementDf(financialAccountIdbillingArrangement))
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultJoinDF = targetDF.join(
          joinDF,
          targetDF(profileJoinColumn) === joinDF(entityJoinColumn),
          "left_outer"
        )

        val finalResultDF =
          resultJoinDF.withColumn(targetColumn, resultJoinDF(attributeColumn))

        finalResultDF
      case _ =>
        logger.warn(
          "JoinDirectWithBillingArrangement can not be applied as sourceColumns size is not equals to 9."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }
}
