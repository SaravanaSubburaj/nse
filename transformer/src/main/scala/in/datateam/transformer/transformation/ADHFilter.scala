package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.max
import org.apache.spark.sql.functions.when

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object ADHFilter extends TransformerColTrait with Serializable {
  //SourceCols:-
  // 0 :- eventtypename
  // 1 :- freeallowanceamount
  // 2 :- chargeamount
  // 3 :- allowanceunitsapplied
  // 4 :- subscriberKey

  def filterDF(dataFrame: DataFrame, sourceColumns: Array[SourceColumn]): DataFrame = {
    val eventTypeCol = sourceColumns(0).columnName
    val freeAllowanceAmountCol = sourceColumns(1).columnName
    val chargeAmountCol = sourceColumns(2).columnName
    val allowanceUnitsAppliedCol = sourceColumns(3).columnName

    val eventFilteredDF = dataFrame
      .filter(
        s"${eventTypeCol} == '${TransformerEnums.SMS_CHARGE_FILTER}'" +
        s" OR ${eventTypeCol} == '${TransformerEnums.VOICE_CHARGE_FILTER}'" +
        s" OR ${eventTypeCol} == '${TransformerEnums.ADH_DATA_CHARGE_FILTER}' "
      )

    val amountColumnsFilteredDF = eventFilteredDF.filter(
      (
        (col(freeAllowanceAmountCol) === col(chargeAmountCol))
        &&
        (col(freeAllowanceAmountCol) > 0)
        &&
        (col(chargeAmountCol) > 0)
      )
      ||
      (
        col(allowanceUnitsAppliedCol).isNotNull
        &&
        (col(freeAllowanceAmountCol) =!= col(chargeAmountCol))
        &&
        (col(freeAllowanceAmountCol) > 0)
        &&
        (col(chargeAmountCol) > 0)
      )
    )

    amountColumnsFilteredDF

  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val eventTypeCol = sourceColumns(0).columnName
    val subscriberKeyCol = sourceColumns(4).columnName

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

    val filteredDF = filterDF(sourceDF, sourceColumns)
      .repartition(col(subscriberKeyCol))
    filteredDF.take(1)

    val addedDataDateDF = filteredDF.withColumn(
      TransformerEnums.LAST_DATA_PPU_DATE,
      when(
        col(eventTypeCol).isin(TransformerEnums.ADH_DATA_CHARGE_FILTER),
        partitionDate
      ).otherwise(null)
    )

    val addedCoreDateDF = addedDataDateDF
      .withColumn(
        TransformerEnums.LAST_CORE_PPU_DATE,
        when(
          col(eventTypeCol).isin(
            TransformerEnums.VOICE_CHARGE_FILTER,
            TransformerEnums.SMS_CHARGE_FILTER
          ),
          partitionDate
        ).otherwise(null)
      )

    val groupedDF = addedCoreDateDF
      .groupBy(col(subscriberKeyCol))
      .agg(
        max(col(TransformerEnums.LAST_DATA_PPU_DATE))
          .as(TransformerEnums.LAST_DATA_PPU_DATE),
        max(col(TransformerEnums.LAST_CORE_PPU_DATE))
          .as(TransformerEnums.LAST_CORE_PPU_DATE)
      )

    val addedDateDF = groupedDF.withColumn(
      CommonEnums.DATE_PARTITION_COLUMN,
      lit(partitionDate)
    )

    addedDateDF
  }

}
