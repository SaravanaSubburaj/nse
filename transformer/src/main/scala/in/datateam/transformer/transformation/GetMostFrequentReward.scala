package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetMostFrequentReward extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.size match {
      case 3 =>
        val rewardEntity = sourceColumns(0).entityName
        val rewardTypeColumn = sourceColumns(0).columnName
        val flagColumn = sourceColumns(1).columnName
        val msisdnColumn = sourceColumns(2).columnName
        val profileJoinColumn = joinColumns.get.head.profileJoinColumn
        val entityJoinColumn = joinColumns.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val sourceDF = sourceDFs(rewardEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            rewardTypeColumn,
            flagColumn,
            msisdnColumn,
            CommonEnums.DATE_PARTITION_COLUMN
          )

        val getLastTenDigitiNameDF = sourceDF
          .withColumn(msisdnColumn, substring(col(msisdnColumn), -10, 10))

        val filterDF = getLastTenDigitiNameDF
          .filter(col(flagColumn) === "1" || col(flagColumn) === "3")

        val transformedDF = filterDF
          .withColumn(
            rewardTypeColumn,
            when(lower(col(rewardTypeColumn)) === TransformerEnums.TELCO, "4")
              .when(
                lower(col(rewardTypeColumn)) === TransformerEnums.NON_TELCO,
                "3"
              )
              .when(lower(col(rewardTypeColumn)) === TransformerEnums.SMAC, "2")
              .when(
                lower(col(rewardTypeColumn)) === TransformerEnums.POINTS_EXCHANGE,
                "1"
              )
              .otherwise("0")
          )

        val groupBymsisdnFilterDF = transformedDF
          .filter(!(col(rewardTypeColumn) === "0"))
          .repartitionAndSortWithInPartition(
            Array(col(msisdnColumn), col(rewardTypeColumn)),
            Array(col(CommonEnums.DATE_PARTITION_COLUMN))
          )

        val groupBymsisdnDF = groupBymsisdnFilterDF
          .groupBy(msisdnColumn, rewardTypeColumn)
          .agg(
            max(CommonEnums.DATE_PARTITION_COLUMN).as(CommonEnums.DATE_PARTITION_COLUMN),
            count(rewardTypeColumn).alias("count")
          )
          .repartitionAndSortWithInPartition(Array(col(msisdnColumn)), Array(col("count").desc))

        val window = Window.partitionBy(msisdnColumn).orderBy(col("count").desc)
        val groupedDF = groupBymsisdnDF
          .withColumn("rank", rank().over(window))
          .select(
            rewardTypeColumn,
            msisdnColumn,
            CommonEnums.DATE_PARTITION_COLUMN
          )
          .where("rank == 1")
          .repartitionAndSortWithInPartition(Array(col(msisdnColumn)), Array(col(CommonEnums.DATE_PARTITION_COLUMN)))

        val window1 = Window
          .partitionBy(msisdnColumn)
          .orderBy(CommonEnums.DATE_PARTITION_COLUMN)
        val higherPartitionValueDf = groupedDF
          .withColumn("rank", rank().over(window1))
          .filter(col("rank") === 1)
          .select(msisdnColumn, rewardTypeColumn)
          .repartitionAndSortWithInPartition(Array(col(msisdnColumn)), Array(col(rewardTypeColumn).desc))

        val tempTargetColumn = targetColumn + "_temp"
        val higherRewardValueDF = higherPartitionValueDf
          .groupBy(msisdnColumn)
          .agg(max(rewardTypeColumn).as(tempTargetColumn))
          .withColumn(
            tempTargetColumn,
            when(col(tempTargetColumn) === "4", (TransformerEnums.TELCO).toUpperCase)
              .when(col(tempTargetColumn) === "3", (TransformerEnums.NON_TELCO).toUpperCase)
              .when(col(tempTargetColumn) === "2", (TransformerEnums.SMAC).toUpperCase)
              .when(
                col(tempTargetColumn) === "1",
                (TransformerEnums.POINTS_EXCHANGE).toUpperCase
              )
              .otherwise(null)
          )

        val resultJoinDF = targetDF
          .join(
            higherRewardValueDF,
            targetDF(profileJoinColumn) === higherRewardValueDF(
                entityJoinColumn
              ),
            "left_outer"
          )
          .drop(higherRewardValueDF(entityJoinColumn))

        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, resultJoinDF(tempTargetColumn))

        finalResultDF
      case _ =>
        logger.warn(
          "GetMostFrequentReward can not be applied as sourceColumns size is not equals to 3."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
