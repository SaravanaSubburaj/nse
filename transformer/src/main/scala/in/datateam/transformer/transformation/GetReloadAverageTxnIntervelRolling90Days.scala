package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetReloadAverageTxnIntervelRolling90Days extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 4 =>
        val dailyReloadSummaryEntity = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val reloadDate = sourceColumns(1).columnName
        val totalReloadAmount = sourceColumns(2).columnName
        val reloadChannelTypeCode = sourceColumns(3).columnName

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val subscriberDF = sourceDFs(dailyReloadSummaryEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(subscriberId, reloadDate, totalReloadAmount)
          .filter(col(totalReloadAmount) > 0)
          .filter(!lower(col(reloadChannelTypeCode)).isin("tp", "sl"))
          .drop(totalReloadAmount)
          .drop(reloadChannelTypeCode)

        val window = Window.partitionBy(subscriberId).orderBy(col(reloadDate).desc)
        val groupedDF = subscriberDF.withColumn("prev_date", lag(col(reloadDate), -1).over(window))

        val derivedDF = groupedDF.withColumn("days_count", datediff(col(reloadDate), col("prev_date")))

        val finalDF =
          derivedDF
            .withColumn("daysCount", when(col("days_count") >= 1, col("days_count")).otherwise(null))
            .drop("days_count")

        val calculatedDF = finalDF
          .groupBy(subscriberId)
          .agg(countDistinct(col(reloadDate)).as("distinctCount"), sum(col("daysCount")).as("Sum"))

        val getAttrbuteValueDF =
          calculatedDF.withColumn(targetColumn + "_Temp", col("Sum") / (col("distinctCount") - 1))

        val resultJoinDF = targetDF
          .join(
            getAttrbuteValueDF,
            targetDF(profileJoinColumn) === getAttrbuteValueDF(
                entityJoinColumn
              ),
            "left_outer"
          )
          .drop(getAttrbuteValueDF(entityJoinColumn))

        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, getAttrbuteValueDF(targetColumn + "_Temp"))

        finalResultDF
      case _ =>
        logger.warn(
          "GetReloadAverageTxnIntervelRolling90Days can not be applied as sourceColumns size is not equals to 4"
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
