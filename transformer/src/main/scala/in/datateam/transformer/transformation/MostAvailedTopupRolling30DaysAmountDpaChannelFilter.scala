package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object MostAvailedTopupRolling30DaysAmountDpaChannelFilter extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val logger = Logger.getLogger(getClass.getName)

    val resultDF = sourceColumns.size match {
      case 6 => {
        val reloadsDF = sourceDFs.get(sourceColumns.head.entityName).get
        val reloadChannelTypeCode = sourceColumns(5).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val filteredReloadsDF = reloadsDF
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(
                to_date(lit(partitionDate), Misc.dateFormat),
                -31
              )
          )
          .filter(lower(col(reloadChannelTypeCode)).isin("ax", "iv", "ud", "es", "pn", "eg", "rp"))
          .drop(reloadChannelTypeCode)

        calculateMostAvailedTopup(filteredReloadsDF, targetDF, sourceColumns, joinColumns, targetColumn)
      }
      case _ => {
        logger.warn("You need to supply 6 columns for MostAvailedTopupRolling30DaysAmountDpaChannelFilter")
        targetDF
      }
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def calculateMostAvailedTopup(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val reloadAmtCol = sourceColumns(2).columnName
    val totalReloadCountCol = sourceColumns(3).columnName
    val reloadDenominationAmountCol = sourceColumns(4).columnName

    val typechangeDF = sourceDF.withColumn(reloadDateCol, col(reloadDateCol).cast(DateType))

    val groupedDF = typechangeDF
      .select(
        col(subscriberIdCol),
        col(reloadDateCol),
        col(reloadAmtCol),
        col(totalReloadCountCol),
        col(reloadDenominationAmountCol)
      )
      .filter(
        col(reloadDateCol) >= Misc.getNthDate(partitionDate, -31)
        &&
        col(reloadDateCol) <= partitionDateMinusTwo
        &&
        col(reloadAmtCol) > 0
      )
      .withColumn(reloadDenominationAmountCol, col(reloadDenominationAmountCol).cast("double"))
      .groupBy(col(subscriberIdCol), col(reloadDenominationAmountCol))
      .agg(
        sum(col(totalReloadCountCol))
          .as(targetColumn + "_temp")
      )

    val rankedDF = groupedDF
      .withColumn(
        "rank",
        row_number().over(
          Window
            .partitionBy(col(subscriberIdCol))
            .orderBy(col(targetColumn + "_temp").desc, col(reloadDenominationAmountCol).desc)
        )
      )
      .filter(col("rank") === 1)

    val joinedDF = targetDF
      .join(
        rankedDF,
        targetDF(joinColumns.get.head.profileJoinColumn) === rankedDF(joinColumns.get.head.entityJoinColumn),
        "left_outer"
      )
      .drop(rankedDF(joinColumns.get.head.entityJoinColumn))

    val retDF = joinedDF.withColumn(targetColumn, joinedDF(reloadDenominationAmountCol))

    retDF

  }
}
