package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object TrimWhiteSpacesMultiCol extends TransformerColTrait with Serializable {

  /**
    * A clone of the existing trim function to be used when you need to trim multiple/all cols.
    *
    * @param sourceDF      Input DF
    * @param sourceColumns List of columns to trim, trim all StringType columns if blank/None-ish.
    * @param targetColumn  Pass in None/dummy columns, won't be used, but specify it to maintain format of
    *                      existing trait.
    * @return DF with white spaces trimmed in specified columns.
    */
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val stringCols = sourceDF.schema.filter(x => x.dataType == StringType)
    val stringColNames = stringCols.map(x => x.name)
    if (sourceColumns.head.columnName == "*") {
      stringColNames.foldLeft(sourceDF)(
        (srcDF, colName) => srcDF.withColumn(colName, trim(col(colName)))
      )
    } else {
      sourceColumns.foldLeft(sourceDF)(
        (srcDF, srcCol) => srcDF.withColumn(srcCol.columnName, trim(col(srcCol.columnName)))
      )
    }

  }

}
