package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date
import org.apache.spark.sql.functions.when

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.transformer.helper.TransformerHelpers.lookbackMonthly
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDeriveWithOptOutIndicator extends TransformerAggColTrait with Serializable {

  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val returnDF = sourceColumns.size match {
      case 1 =>
        val attributeColumnEntity = sourceColumns.head.entityName
        val attributeColumn = sourceColumns.head.columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val attributeColumnEntityDF = lookbackMonthly(sourceDFs, attributeColumnEntity, partitionDate)
          .select(attributeColumn, entityJoinColumn)

        val repartitionTargetDF = targetDF
          .repartitionAndSortWithInPartition(Array(col(profileJoinColumn)), Array(col(profileJoinColumn)))

        val resultDF = repartitionTargetDF
          .join(
            attributeColumnEntityDF,
            repartitionTargetDF(profileJoinColumn) === attributeColumnEntityDF(entityJoinColumn),
            "left_outer"
          )
          .withColumn(
            targetColumn,
            when(col(attributeColumn).isNotNull, "1")
              .otherwise("0")
          )
          .drop(attributeColumnEntityDF(entityJoinColumn))

        resultDF

      case _ =>
        logger.warn(
          "JoinDeriveWithOptOutIndicator can not be applied as sourceColumns size is not equals to 2."
        )
        targetDF

    }

    returnDF.select(targetDF.columns.map(c => col(c)): _*)

  }
}
