package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetTenureCount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 3 =>
        val subscriberEntityName = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val activationDate = sourceColumns(1).columnName
        val churnDate = sourceColumns(2).columnName

        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val sourceDF = sourceDFs(subscriberEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, activationDate, churnDate, CommonEnums.DATE_PARTITION_COLUMN)
          .withColumn(activationDate, col(activationDate).cast(DateType))
          .withColumn(churnDate, col(churnDate).cast(DateType))

        val derivedDF = sourceDF
          .withColumn(
            targetColumn + "_Temp",
            when(
              col(churnDate).isNull,
              ceil((datediff(col(CommonEnums.DATE_PARTITION_COLUMN), col(activationDate))) / 365)
            ).otherwise(ceil((datediff(col(churnDate), col(activationDate))) / 365))
          )
          .drop(CommonEnums.DATE_PARTITION_COLUMN)

        val resultJoinDF = targetDF
          .join(
            derivedDF,
            targetDF(profileJoinColumn)
              === derivedDF(entityJoinColumn),
            "left_outer"
          )
          .drop(derivedDF(entityJoinColumn))
        val resultDf = resultJoinDF.withColumn(targetColumn, derivedDF(targetColumn + "_Temp"))
        resultDf
      case _ =>
        logger.warn("GetTenureCount can not be applied as sourceColumns size is not equals to 3")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
