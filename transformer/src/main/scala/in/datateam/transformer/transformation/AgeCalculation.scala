package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.format

import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import org.apache.log4j.Logger

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object AgeCalculation extends TransformerColTrait with Serializable {

  /**
    * Calculates age from Date of Birth column. Expects DOB in yyyy-MM-dd HH:mm:ss.SSS.
    *
    * @param row           Input row
    * @param sourceColumns Date of Birth (String) : Column for DOB, in yyyy-MM-dd HH:mm:ss.SSS format.
    * @param targetColumn  Age column (Integer) : Age in years, initially null.
    * @return Row with computed age
    */
  val logger: Logger = Logger.getLogger("AgeCalculation")

  def processRow(row: Row, sourceColumns: Array[SourceColumn], targetColumn: String): Row = {
    val dateVal = row.getAs[String](sourceColumns(0).columnName)
    //    FIXME: Remove var
    var ageVar = row.getAs[Any](targetColumn)

    val dateValParsed = Try(
      LocalDateTime.parse(
        dateVal,
        format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
      )
    ).toOption

    if (dateValParsed.isDefined) {
      ageVar = Period
        .between(dateValParsed.get.toLocalDate, LocalDate.now())
        .getYears
    } else {
      logger.warn(
        "cadup.DateTimeParseException occurred in AgeCalculation, incorrect date format encountered," +
        "please make sure format is yyyy-MM-dd HH:mm:ss.SSS"
      )
    }

    val schemaVal = row.schema
    val validatedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => ageVar
        case x: Any => row.getAs[Any](x)
      }
    }
    val validatedRow = new GenericRowWithSchema(validatedArray, schemaVal)
    validatedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    implicit val sourceDFEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => processRow(row, sourceColumns, targetColumn))
  }
}
