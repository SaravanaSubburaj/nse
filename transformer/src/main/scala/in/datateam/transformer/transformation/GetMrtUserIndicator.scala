package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetMrtUserIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 12 =>
        val voiceEntityName = sourceColumns.head.entityName
        val voiceMsisdnNumber = sourceColumns.head.columnName
        val voiceCellId = sourceColumns(1).columnName
        val smsEntityName = sourceColumns(2).entityName
        val smsMsisdnNumber = sourceColumns(2).columnName
        val smsCellId = sourceColumns(3).columnName
        val buildingReferenceEntity = sourceColumns(4).entityName
        val locationAreaCode = sourceColumns(4).columnName
        val buildingTypeName = sourceColumns(5).columnName
        val buildingGroupName = sourceColumns(6).columnName
        val cdrAFirstSac = sourceColumns(7).columnName
        val sdrSac = sourceColumns(8).columnName
        val inbuildingRefernceCellId = sourceColumns(9).columnName
        val sdrLac = sourceColumns(10).columnName
        val cdrAFirstLac = sourceColumns(11).columnName

        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val voiceEntityDF = sourceDFs(voiceEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(voiceMsisdnNumber, voiceCellId, cdrAFirstSac, cdrAFirstLac)

        val getVoiceMsisdnValueDF = voiceEntityDF
          .withColumn("lac", voiceEntityDF(cdrAFirstLac))
          .withColumn(voiceMsisdnNumber, substring(col(voiceMsisdnNumber), -10, 10))
          .withColumn("cellNew", when(col(voiceCellId) === 0, col(cdrAFirstSac)).otherwise(col(voiceCellId)))
          .drop(cdrAFirstLac)
          .drop(cdrAFirstSac)
          .drop(voiceCellId)
          .select("lac", voiceMsisdnNumber, "cellNew")

        val smsEntityDF = sourceDFs(smsEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(smsMsisdnNumber, smsCellId, sdrSac, sdrLac)

        val getSmsMsisdnValue = smsEntityDF
          .withColumn(voiceMsisdnNumber, substring(col(smsMsisdnNumber), -10, 10))
          .withColumn("cellNew", when(col(smsCellId).isNull, col(sdrSac)).otherwise(col(smsCellId)))
          .drop(smsMsisdnNumber)
          .drop(smsCellId)
          .drop(sdrSac)

        val buildingReferenceDF = sourceDFs(buildingReferenceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(locationAreaCode, buildingTypeName, buildingGroupName, inbuildingRefernceCellId)
          .filter(lower(col(buildingTypeName)) === "mrt")

        val unionDF = getVoiceMsisdnValueDF
          .union(getSmsMsisdnValue)

        val joinWithBuildingReference = buildingReferenceDF.join(
          unionDF,
          buildingReferenceDF(locationAreaCode) === unionDF("lac") &&
          buildingReferenceDF(inbuildingRefernceCellId) === unionDF("cellNew"),
          "left_outer"
        )

        val getMrtIndicatorDF =
          joinWithBuildingReference.withColumn("mrtIndicator", when(col(buildingGroupName).isNull, "0").otherwise("1"))

        val groupDF = getMrtIndicatorDF
          .groupBy(voiceMsisdnNumber)
          .agg(sum("mrtIndicator").as("finalMrtIndicator"))

        val finalDf = groupDF.withColumn(targetColumn + "_temp", when(col("finalMrtIndicator") > 0, "1").otherwise("0"))

        val resultJoinDF = targetDF
          .join(
            finalDf,
            targetDF(profileJoinColumn) === finalDf(entityJoinColumn),
            "left_outer"
          )
          .drop(finalDf(entityJoinColumn))

        val getResultDF = resultJoinDF.withColumn(
          targetColumn + "_Result",
          when(col(targetColumn + "_temp").isNull, "0").otherwise("1")
        )

        val finalResultDF = getResultDF.withColumn(
          targetColumn,
          getResultDF(targetColumn + "_Result")
        )

        finalResultDF
      case _ =>
        logger.warn(
          "GetMrtUserIndicator can not be applied as sourceColumns size is not equals to 7."
        )
        targetDF
    }

    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
