package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger
import org.joda.time.DateTime

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter

object GetArpuCategoryCode extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  /** Calculting Arpu category code from monthly postpaid or prepaid revenue summary
    *
    * @param targetDF      joined DF with subscriber and monthly either prepaid or postpaidSubscriberRevenueSummary
    * @param sourceColumns grossServiceRevenueIndicative column
    * @param targetColumn  populate value in arpuCategoryCodePostpaid
    * @return ResultDF with Calulated Arpu code in target column
    */
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val returnDF = sourceColumns.length match {
      case 2 =>
        val sourceEntity = sourceColumns.head.entityName
        val sourceCol = sourceColumns.head.columnName
        val subscriberIdCol = sourceColumns(1).columnName
        val entityJoinCol = joinColumn.get.head.entityJoinColumn
        val profileJoinCol = joinColumn.get.head.profileJoinColumn
        val partitionValue =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val calculatedDF = TransformerHelpers
          .lookbackMonthly(sourceDFs, sourceEntity, partitionValue)
          .select(sourceCol, subscriberIdCol)
          .repartitionAndSortWithInPartition(Array(col(subscriberIdCol)), Array())

        val sourceTempCol = sourceCol + "_Temp"
        val groupDF = calculatedDF
          .groupBy(subscriberIdCol)
          .agg(sum(sourceCol).alias(sourceTempCol))
        val getResultCol = groupDF
          .withColumn(
            targetColumn + "_temp",
            when(col(sourceTempCol) >= 8000, "A")
              .when(col(sourceTempCol) >= 7000 && col(sourceTempCol) <= 7999, "B")
              .when(col(sourceTempCol) >= 6000 && col(sourceTempCol) <= 6999, "C")
              .when(col(sourceTempCol) >= 5000 && col(sourceTempCol) <= 5999, "D")
              .when(col(sourceTempCol) >= 4000 && col(sourceTempCol) <= 4999, "E")
              .when(col(sourceTempCol) >= 3000 && col(sourceTempCol) <= 3999, "F")
              .when(col(sourceTempCol) >= 2000 && col(sourceTempCol) <= 2999, "G")
              .when(col(sourceTempCol) >= 1000 && col(sourceTempCol) <= 1999, "H")
              .when(col(sourceTempCol) >= 300 && col(sourceTempCol) <= 999, "I")
              .otherwise("J")
          )
          .drop(sourceCol, sourceTempCol)
          .repartitionAndSortWithInPartition(Array(col(entityJoinCol)), Array(col(entityJoinCol)))

        val resultDF = targetDF
          .join(
            getResultCol,
            targetDF(profileJoinCol) === getResultCol(entityJoinCol),
            "left_outer"
          )
          .drop(getResultCol(entityJoinCol))
          .withColumnRenamed(targetColumn + "_temp", targetColumn)
        resultDF

      case _ =>
        logger.warn(
          "GetArpuCategoryCode can not be applied when source columns is not equal to 2."
        )
        targetDF
    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    returnDF.select(targetColumns.map(c => col(c)): _*)
  }

  def getSubscriberRevenueSummaryDF(
      sourceDF: DataFrame
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionValue = extraParameter(TransformerEnums.PARTITION_VALUE)
    val currentPartitionFormatted = dateFormatter.parse(partitionValue)

    val dateTimeMonthSub1 = new DateTime(currentPartitionFormatted)
    val dateTimeMonthSub2 =
      new DateTime(currentPartitionFormatted).minusMonths(1)
    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val currentMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val previousMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val currentMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === currentMonthVal
    )
    val previousMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === previousMonthVal
    )

    val subscriberRevenueSummaryDF = if (!currentMonthDF.take(1).isEmpty) {
      currentMonthDF
    } else if (!previousMonthDF.take(1).isEmpty) {
      previousMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
    subscriberRevenueSummaryDF
  }
}
