package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Transformation arguments, sourceColumns need to be ordered:
  * index 0: groupBy_1 (relSubscriberId)
  * index 1: filter_1 (reloadDate)
  * index 2: filter_2, sum  (totalReloadAmount)
  */
object TotalDailyTopupRolling30DaysAmount extends TransformerAggColTrait with Serializable {
  val logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.length match {
      case 3 =>
        val reloadsDF = sourceDFs(sourceColumns.head.entityName)

        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val filteredReloadsDF = reloadsDF.filter(
          to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -91
            )
        )

        calculateTotalDailyTopupRolling(
          filteredReloadsDF,
          targetDF,
          sourceColumns,
          joinColumns,
          targetColumn
        )
      case _ =>
        logger.warn(
          "You need to supply 3 columns to call TotalDailyTopupRolling30Days"
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def calculateTotalDailyTopupRolling(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val totalReloadAmountCol = sourceColumns(2).columnName

    val typechangeDF = sourceDF
      .withColumn(reloadDateCol, col(reloadDateCol).cast(DateType))
      .select(subscriberIdCol, reloadDateCol, totalReloadAmountCol)
      .filter(
        col(reloadDateCol) >= Misc.getNthDate(partitionDate, -31)
        &&
        col(reloadDateCol) <= partitionDateMinusTwo
        &&
        col(totalReloadAmountCol) > 0
      )
      .repartitionAndSortWithInPartition(Array(col(subscriberIdCol)), Array(col(totalReloadAmountCol)))

    val groupedDF = typechangeDF
      .groupBy(col(subscriberIdCol))
      .agg(sum(col(totalReloadAmountCol)).as(targetColumn + "_temp"))

    val joinedDF = targetDF
      .join(
        groupedDF,
        targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(
            joinColumns.get.head.entityJoinColumn
          ),
        "left_outer"
      )
      .drop(groupedDF(joinColumns.get.head.entityJoinColumn))

    val retDF =
      joinedDF.withColumn(targetColumn, joinedDF(targetColumn + "_temp"))

    retDF

  }

}
