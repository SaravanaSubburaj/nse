package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Created by RamaKrishnaa(ramakrishnaa.palanisamy@thedatateam.in).
  */
//TODO Code is used only for availment attributes, but need to change the logic
//TODO  so that if we can supply filter conditions externally, we can make it more generic.

object GetAvailmentIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 3 =>
        val subsIdSubsAssignBillOffer = sourceColumns.head.columnName
        val billingOfferDescription = sourceColumns(1).columnName
        val billOfferStartDate = sourceColumns(2).columnName
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val subsAssignBillOfferDF = sourceDFs(sourceColumns.head.entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            subsIdSubsAssignBillOffer,
            billingOfferDescription,
            billOfferStartDate
          )

        val result = subsAssignBillOfferDF
          .filter(
            col(billOfferStartDate) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
            &&
            col(billOfferStartDate) < to_date(lit(partitionDate), Misc.dateFormat)
          )
          .withColumn(
            targetColumn,
            when(
              lower(col(billingOfferDescription))
              rlike TransformerEnums.EASY_ROAM_INDICATOR_FILTER,
              "1"
            ).otherwise("0")
          )
          .withColumnRenamed(targetColumn, targetColumn + "_temp")
          .select(subsIdSubsAssignBillOffer, targetColumn + "_temp")
          .repartitionAndSortWithInPartition(Array(col(subsIdSubsAssignBillOffer)), Array(col(targetColumn + "_temp")))

        val result1 =
          result
            .groupBy(subsIdSubsAssignBillOffer)
            .agg(max(targetColumn + "_temp").alias(targetColumn + "_temp"))
            .repartitionAndSortWithInPartition(
              Array(col(joinColumn.get.head.entityJoinColumn)),
              Array(col(joinColumn.get.head.entityJoinColumn))
            )

        val joinedDF = targetDF
          .join(
            result1,
            targetDF(joinColumn.get.head.profileJoinColumn) === result(
                joinColumn.get.head.entityJoinColumn
              ),
            "left_outer"
          )
          .drop(result(joinColumn.get.head.entityJoinColumn))
        val resultantDF = joinedDF
          .withColumn(targetColumn, joinedDF(targetColumn + "_temp"))
        resultantDF

      case _ =>
        logger.warn(
          "Get Easy roam Avail Indicator can not be applied as sourceColumn" +
          "s size is not equals to 3."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }

}
