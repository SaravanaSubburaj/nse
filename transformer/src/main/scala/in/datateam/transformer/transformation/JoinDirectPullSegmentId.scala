package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object JoinDirectPullSegmentId extends TransformerAggColTrait with Serializable {

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val partitionValue = extraParameter(TransformerEnums.PARTITION_VALUE)
    val sourceColumnSubs1 = sourceColumns(0).columnName
    val sourceColumnSubs2 = sourceColumns(2).columnName
    val sourceColumnSubs3 = sourceColumns(4).columnName
    val sourceColumnSubs4 = sourceColumns(6).columnName
    val sourceColumnSubs5 = sourceColumns(8).columnName

    val sourceColumnDPA1 = sourceColumns(1).columnName
    val sourceColumnDPA2 = sourceColumns(3).columnName
    val sourceColumnDPA3 = sourceColumns(5).columnName
    val sourceColumnDPA4 = sourceColumns(7).columnName
    val sourceColumnDPA5 = sourceColumns(9).columnName
    val sourceColumnDPA6 = sourceColumns(10).columnName

    val entitySubsDF = sourceDFs(sourceColumns(0).entityName)
      .select(
        sourceColumnSubs1,
        sourceColumnSubs2,
        sourceColumnSubs3,
        sourceColumnSubs4,
        sourceColumnSubs5,
        targetJoinColumn.get.head.entityJoinColumn
      )
      .filter(col(CommonEnums.DATE_PARTITION_COLUMN) === partitionValue)
    val entityDPADF = sourceDFs(sourceColumns(1).entityName)
      .select(
        sourceColumnDPA1,
        sourceColumnDPA2,
        sourceColumnDPA3,
        sourceColumnDPA4,
        sourceColumnDPA5,
        sourceColumnDPA6
      )

    val joinedDF = entitySubsDF
      .join(
        broadcast(entityDPADF),
        entitySubsDF(sourceColumnSubs1) === entityDPADF(sourceColumnDPA1)
        && entitySubsDF(sourceColumnSubs2) === entityDPADF(sourceColumnDPA2)
        && entitySubsDF(sourceColumnSubs3) === entityDPADF(sourceColumnDPA3)
        && entitySubsDF(sourceColumnSubs4) === entityDPADF(sourceColumnDPA4)
        && entitySubsDF(sourceColumnSubs5) === entityDPADF(sourceColumnDPA5),
        "inner"
      )
      .select(targetJoinColumn.get.head.entityJoinColumn, sourceColumnDPA6)
      .repartitionAndSortWithInPartition(
        Array(col(targetJoinColumn.get.head.entityJoinColumn)),
        Array(col(targetJoinColumn.get.head.entityJoinColumn))
      )

    val resultantDF = targetDF
      .join(
        joinedDF,
        joinedDF(targetJoinColumn.get.head.entityJoinColumn)
          === targetDF(targetJoinColumn.get.head.profileJoinColumn),
        "left_outer"
      )
      .drop(joinedDF(targetJoinColumn.get.head.entityJoinColumn))
      .drop(targetDF(targetColumn))
      .withColumn(targetColumn, joinedDF(sourceColumnDPA6))

    resultantDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
