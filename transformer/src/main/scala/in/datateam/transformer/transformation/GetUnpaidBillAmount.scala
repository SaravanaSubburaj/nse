package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetUnpaidBillAmount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 11 =>
        val financialInvoiceStagingEntity = sourceColumns.head.entityName
        val financialAccount = sourceColumns(1).entityName
        val financialAccountIdInvoiceStaging = sourceColumns.head.columnName
        val financialAccountId = sourceColumns(1).columnName
        val previousBalanceAmount = sourceColumns(2).columnName
        val financialActivitiesAmount = sourceColumns(4).columnName
        val billNumber = sourceColumns(5).columnName
        val financialAccountStatusCode = sourceColumns(6).columnName
        val financialActivationDate = sourceColumns(7).columnName
        val financialSubscriberReferenceEntity = sourceColumns(8).entityName
        val subscriberReferenceSubscriberId = sourceColumns(8).columnName
        val expiryDate = sourceColumns(9).columnName
        val subscriberReferenceFinancialId = sourceColumns(10).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val subscriberReferenceDf = sourceDFs
          .get(financialSubscriberReferenceEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberReferenceSubscriberId, subscriberReferenceFinancialId, expiryDate)
          .filter(col(expiryDate).isNull)
          .drop(expiryDate)

        val financialInvoiceStagingDF =
          TransformerHelpers
            .lookbackMonthly(sourceDFs, financialInvoiceStagingEntity, partitionDate)
            .select(financialAccountIdInvoiceStaging, previousBalanceAmount, financialActivitiesAmount, billNumber)
            .repartitionAndSortWithInPartition(
              Array(col(financialAccountIdInvoiceStaging)),
              Array(col(billNumber).desc)
            )

        val window = Window
          .partitionBy(financialAccountIdInvoiceStaging)
          .orderBy(col(billNumber).desc)
        val groupedDF = financialInvoiceStagingDF
          .withColumn("rank", rank().over(window))
          .select(
            financialAccountIdInvoiceStaging,
            previousBalanceAmount,
            financialActivitiesAmount
          )
          .where("rank == 1")

        val financialAccountDf = sourceDFs(financialAccount)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .filter(
            trim(upper(col(financialAccountStatusCode))) === "O" || trim(
              upper(col(financialAccountStatusCode))
            ) === "SUS"
          )
          .drop(financialAccountStatusCode)
          .select(
            financialAccountId,
            financialActivationDate
          )

        val accountGroupDf = financialAccountDf
          .groupBy(financialAccountId)
          .agg(max(financialActivationDate).as(financialActivationDate))
          .drop(financialActivationDate)

        val joinWithSubscriberReference = subscriberReferenceDf
          .join(
            accountGroupDf,
            subscriberReferenceDf(subscriberReferenceFinancialId)
              === accountGroupDf(financialAccountIdInvoiceStaging),
            "left_outer"
          )
          .drop(subscriberReferenceDf(subscriberReferenceFinancialId))

        val tempTargetColum = targetColumn + "_temp"
        val derivedDF = groupedDF
          .withColumn(
            tempTargetColum,
            col(previousBalanceAmount) + col(financialActivitiesAmount)
          )
          .drop(previousBalanceAmount, financialActivitiesAmount)

        val joinDF = joinWithSubscriberReference
          .join(
            derivedDF,
            joinWithSubscriberReference(financialAccountId) === derivedDF(
                financialAccountIdInvoiceStaging
              ),
            "inner"
          )
          .drop(derivedDF(financialAccountIdInvoiceStaging))
          .drop(accountGroupDf(financialAccountId))

        val resultJoinDF = targetDF.join(joinDF, targetDF(profileJoinColumn) === joinDF(entityJoinColumn), "left_outer")

        val finalResultDF = resultJoinDF.withColumn(targetColumn, resultJoinDF(tempTargetColum))
        finalResultDF
      case _ =>
        logger.warn(
          "GetUnpaidBillAmount can not be applied as sourceColumns size is not equals to 8"
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
