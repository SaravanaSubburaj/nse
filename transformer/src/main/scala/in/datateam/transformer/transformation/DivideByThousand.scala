package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object DivideByThousand extends TransformerColTrait with Serializable {

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceDF
      .withColumn(
        targetColumn,
        (col(targetColumn).cast("long") / 1000).cast("timestamp")
      )
    resultDF
  }

}
