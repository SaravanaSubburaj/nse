package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetAllDayRoamSurfRegistrationDatePrepaid extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 5 =>
        val availmentDateEntity = sourceColumns(0).entityName
        val availmentDateColumn = sourceColumns(0).columnName
        val subscriberIdColumnAvailmentStaging = sourceColumns(2).columnName
        val prepaidPromoEntity = sourceColumns(1).entityName
        val prepaidPromoDescription = sourceColumns(1).columnName
        val prepaidPromoId = sourceColumns(4).columnName
        val prePaidPromoAvailmentStagingID = sourceColumns(3).columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColoumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val sourceDFAvailmentStaging = sourceDFs(availmentDateEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            availmentDateColumn,
            subscriberIdColumnAvailmentStaging,
            prePaidPromoAvailmentStagingID
          )

        val sourceDFPrepaidPromo = sourceDFs(prepaidPromoEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
              >= to_date(lit(partitionDate), Misc.dateFormat)
          )
          .select(prepaidPromoDescription, prepaidPromoId)

        val dateFilterDF = sourceDFAvailmentStaging.filter(
          col(availmentDateColumn) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          &&
          col(availmentDateColumn) < to_date(lit(partitionDate), Misc.dateFormat)
        )

        val joinWithPromo = dateFilterDF
          .join(
            broadcast(sourceDFPrepaidPromo),
            dateFilterDF(prePaidPromoAvailmentStagingID) === sourceDFPrepaidPromo(prepaidPromoId),
            "inner"
          )
          .drop(sourceDFPrepaidPromo(prepaidPromoId))

        val serviceNameFilterDF = joinWithPromo
          .filter(lower(col(prepaidPromoDescription)).rlike(TransformerEnums.ROAM_SURF_PREPAID_FILTER))
          .repartitionAndSortWithInPartition(
            Array(col(subscriberIdColumnAvailmentStaging)),
            Array(col(availmentDateColumn).desc)
          )

        val groupDF = serviceNameFilterDF
          .groupBy(subscriberIdColumnAvailmentStaging)
          .agg(max(availmentDateColumn).as(targetColumn + "_temp"))
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultJoinDF = targetDF
          .join(
            groupDF,
            targetDF(profileJoinColoumn) === groupDF(entityJoinColumn),
            "left_outer"
          )
          .drop(groupDF(entityJoinColumn))

        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, resultJoinDF(targetColumn + "_temp"))

        finalResultDF
      case _ =>
        logger.warn(
          "GetAllDayRoamSurfRegistrationDatePrePiad can not be applied as " +
          "sourceColumns size is not equals to 5."
        )
        targetDF
    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }
}
