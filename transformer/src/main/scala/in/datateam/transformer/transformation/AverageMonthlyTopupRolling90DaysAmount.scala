package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Transformation arguments, sourceColumns need to be ordered:
  * index 0: groupBy_1 (relSubscriberId)
  * index 1: filter_1 (reloadDate)
  * index 2: filter_2, sum,divide by 3 and round-off  (totalReloadAmount)
  */
object AverageMonthlyTopupRolling90DaysAmount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def getMonthlyTopupAmount(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val reloadAmtCol = sourceColumns(2).columnName

    val resultDF = joinColumns match {
      case Some(x) => {
        val typechangeDF = sourceDF

        val groupedDF = typechangeDF
          .select(col(subscriberIdCol), col(reloadDateCol), col(reloadAmtCol))
          .filter(
            col(reloadDateCol) >= Misc.getNthDate(partitionDate, -91)
            &&
            col(reloadDateCol) <= partitionDateMinusTwo
            &&
            col(reloadAmtCol) > 0
          )
          .groupBy(col(subscriberIdCol))
          .agg(round((sum(col(reloadAmtCol)) / 3), 2).as(targetColumn + "_tmp"), count(col(reloadAmtCol)))

        val joinedDF = targetDF
          .join(
            groupedDF,
            targetDF(joinColumns.get.head.profileJoinColumn) === groupedDF(joinColumns.get.head.entityJoinColumn),
            "left_outer"
          )
          .drop(groupedDF(joinColumns.get.head.entityJoinColumn))

        val retDF = joinedDF.withColumn(targetColumn, joinedDF(targetColumn + "_tmp"))

        retDF
      }
      case None => {
        val groupedDF = sourceDF
          .filter(
            col(reloadDateCol) >= date_add(to_date(lit(partitionDate), Misc.dateFormat), -91)
            &&
            col(reloadDateCol) <= date_add(to_date(lit(partitionDate), Misc.dateFormat), -2)
            &&
            col(reloadAmtCol) > 0
          )
          .groupBy(col(subscriberIdCol))
          .agg(
            round((sum(col(reloadAmtCol)) / 3), 2).as(targetColumn),
            first(CommonEnums.DATE_PARTITION_COLUMN).as(CommonEnums.DATE_PARTITION_COLUMN)
          )

        groupedDF
          .select(col(sourceColumns.head.columnName), col(targetColumn), col(CommonEnums.DATE_PARTITION_COLUMN))
      }
    }
    resultDF
  }

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val sourceDF = sourceDFs.get(sourceColumns.head.entityName).get

    val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

    val filteredReloadsDF = sourceDF.filter(
      to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(to_date(lit(partitionDate), Misc.dateFormat), -91)
    )

    val resultDF = sourceColumns.size match {

      case 3 =>
        getMonthlyTopupAmount(filteredReloadsDF, targetDF, sourceColumns, joinColumns, targetColumn)

      case _ =>
        logger.warn("AverageMonthlyTopupRolling90DaysAmount requires 3 source columns")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
