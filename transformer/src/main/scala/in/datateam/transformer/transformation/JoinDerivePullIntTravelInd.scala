package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.functions.when

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object JoinDerivePullIntTravelInd extends TransformerAggColTrait with Serializable {

  /**
    * This function checks if the billingOfferDescription is of type roam and usageNetworkRoamingTypeCode is "r".
    * if so then the international_traveller_indicator gets the flag 1 else 0
    *
    * @param joinedDF - the DF got by joining the entity daily_postpaid_subscriber_usage_staging,
    *                 DailyPrepaidSubscriberUsage
    * @return
    */
  /**
    *
    * @param sourceDFs        - The 2 DF's which need to be joined based on the direct-pull attribute
    * @param targetDF         - The profile DF
    * @param sourceColumns    - columns (direct-pull attributes) from 2 DF's based on which the join of 2 DF's are done
    * @param targetJoinColumn - The column based on value which the joined DF and the target DF are joined
    * @return - Joined DF with target columns added to the target DF
    */
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

    val subSubscriberId = sourceColumns(0).columnName
    val assignedBillingOffersSubscriberId = sourceColumns(1).columnName
    val usageSubscriberId = sourceColumns(2).columnName
    val usageRoamingTypeCode = sourceColumns(3).columnName
    val assignedBillingOffersBillingOfferDescription = sourceColumns(4).columnName

    val entityDFSub =
      sourceDFs(sourceColumns(0).entityName).select(col(subSubscriberId))

    val entityDFAssignedBillingOffers = sourceDFs(sourceColumns(1).entityName)
      .filter(col(CommonEnums.DATE_PARTITION_COLUMN) === partitionDate)
      .select(
        col(assignedBillingOffersSubscriberId),
        col(assignedBillingOffersBillingOfferDescription)
      )

    val entityDFUsage = sourceDFs(sourceColumns(2).entityName)
      .filter(col(CommonEnums.DATE_PARTITION_COLUMN) === partitionDate)
      .select(col(usageSubscriberId), col(usageRoamingTypeCode))

    val profileJoinCol = targetJoinColumn.get.head.profileJoinColumn
    val entityJoinCol = targetJoinColumn.get.head.entityJoinColumn

    // resultantDF will be removed on try catch implementation
    val joinDF1 = entityDFSub
      .join(
        entityDFAssignedBillingOffers,
        entityDFSub(subSubscriberId) === entityDFAssignedBillingOffers(
            assignedBillingOffersSubscriberId
          ),
        "inner"
      )
      .drop(entityDFAssignedBillingOffers(assignedBillingOffersSubscriberId))

    val joinDF2 = joinDF1
      .join(
        entityDFUsage,
        joinDF1(subSubscriberId) === entityDFUsage(usageSubscriberId),
        "inner"
      )
      .drop(entityDFUsage(usageSubscriberId))

    val flagDF = joinDF2
      .withColumn(
        "flag",
        when(
          lower(col(usageRoamingTypeCode)) === "r" &&
          lower(col(assignedBillingOffersBillingOfferDescription))
            .like("%roam%"),
          1
        ).otherwise(0)
      )
      .repartitionAndSortWithInPartition(Array(col(subSubscriberId)), Array(col("flag")))

    val aggDF =
      flagDF.groupBy(subSubscriberId).agg(sum(col("flag")).as("flag_count"))

    val finDF = aggDF.withColumn(
      targetColumn,
      when(col("flag_count") > 0, 1).otherwise(0)
    )

    val resultDF = targetDF
      .join(
        finDF,
        targetDF(profileJoinCol) === finDF(entityJoinCol),
        "left_outer"
      )
      .drop(finDF(entityJoinCol))
      .withColumn(targetColumn, finDF(targetColumn))

    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)

  }
}

//0:- globeSubscriber.subscriberId
//1:- SubscriberAssignedBillingOffer.subscriber.Id
//2:- globeDailyPostpaidSubscriberUsageStaging.subscriberId
//3:- globeDailyPostpaidSubscriberUsageStaging.usageNetworkRoamingTypeCode
//4:- SubscriberAssignedBillingOffer.billingOfferDescription

//join subscriber and subscriber_assigned_billing_offer on subscriber_id
//daily_postpaid_subscriber_usage_staging and subscriber on subscriber_id
//tag the subscriber with 1 if both condition below hold, else 0
//
//Usage:
//LOWER(usage_network_roaming_type_code) = 'r'
//
//Billing Offer:
//lower(billing_offer_desc) LIKE '%roam%'
