package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object JoinDirectPullSubscriberTypeDescription extends TransformerAggColTrait with Serializable {

  /**
    *
    * @param sourceDFs        - The 2 DF's which need to be joined based on the direct-pull attribute
    * @param targetDF         - The profile DF
    * @param sourceColumns    - columns (direct-pull attributes) from 2 DF's based on which the join of 2 DF's are done
    * @param targetJoinColumn - The column based on value which the joined DF and the target DF are joined
    * @return - Joined DF with target columns added to the target DF
    */
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val partitionValue = extraParameter(TransformerEnums.PARTITION_VALUE)

    val sourceColumnProduct = sourceColumns(0).columnName
    val sourceColumnSubs = sourceColumns(1).columnName
    val productDescriptionColumn = sourceColumns(2).columnName
    val entityProductDF: DataFrame = sourceDFs(sourceColumns(0).entityName)
      .select(sourceColumnProduct, productDescriptionColumn)
      .filter(col(CommonEnums.DATE_PARTITION_COLUMN) === partitionValue)
    val entitySubsDF = sourceDFs(sourceColumns(1).entityName)
      .select(sourceColumnSubs, targetJoinColumn.get.head.entityJoinColumn)
      .filter(col(CommonEnums.DATE_PARTITION_COLUMN) === partitionValue)
    val filteredTargetDF = targetDF

    val joinedDF = entitySubsDF
      .join(
        broadcast(entityProductDF),
        entitySubsDF(sourceColumnSubs) === entityProductDF(sourceColumnProduct),
        "inner"
      )
      .select(targetJoinColumn.get.head.entityJoinColumn, productDescriptionColumn)
      .withColumnRenamed(productDescriptionColumn, targetColumn)
      .repartitionAndSortWithInPartition(
        Array(col(targetJoinColumn.get.head.entityJoinColumn)),
        Array(col(targetJoinColumn.get.head.entityJoinColumn))
      )

    val resultantDF = filteredTargetDF
      .join(
        joinedDF,
        filteredTargetDF(targetJoinColumn.get.head.profileJoinColumn)
          === joinedDF(targetJoinColumn.get.head.entityJoinColumn),
        "left_outer"
      )
      .drop(joinedDF(targetJoinColumn.get.head.entityJoinColumn))
      .drop(filteredTargetDF(targetColumn))

    resultantDF.select(targetDF.columns.map(c => col(c)): _*)

  }
}
