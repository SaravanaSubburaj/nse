package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object CombineActiveSubscriberIndicator extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val dailyPrepaidSubscriberUsgaeStagingColumns = sourceColumns.slice(0, 2)

    val dailyPostpaidSubscriberUsageStagingColumns = sourceColumns.slice(2, 4)

    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"

    val dailyPrepaidSubscriberUsgaeStagingDF = ActiveSubscriberIndicator.process(
      sourceDFs,
      targetDF,
      dailyPrepaidSubscriberUsgaeStagingColumns,
      targetJoinColumn,
      targetColumnPrepaid
    )(spark, extraParameter)

    val dailyPostpaidSubscriberUsageStagingDF = ActiveSubscriberIndicator.process(
      sourceDFs,
      dailyPrepaidSubscriberUsgaeStagingDF,
      dailyPostpaidSubscriberUsageStagingColumns,
      targetJoinColumn,
      targetColumnPostpaid
    )(spark, extraParameter)

    val dailySubscriberUsageStagingDF = CombinePrepaidAndPostpaidOperator
      .process(
        dailyPostpaidSubscriberUsageStagingDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
        ),
        targetColumn + "_Temp"
      )(spark, extraParameter)

    val finalResultDF = dailySubscriberUsageStagingDF.withColumn(
      targetColumn,
      when(dailySubscriberUsageStagingDF(targetColumn + "_Temp").isNotNull, 1).otherwise(0)
    )
    finalResultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
