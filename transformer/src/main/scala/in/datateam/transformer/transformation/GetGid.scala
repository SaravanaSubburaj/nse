package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetGid extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 4 =>
        val uniqueNewEntity = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val uniqueGidColumn = sourceColumns(1).columnName
        val confidentNewEntity = sourceColumns(2).entityName
        val confidentGidColumn = sourceColumns(2).columnName
        val suspectsNewEntity = sourceColumns(3).entityName
        val suspectsGidColumn = sourceColumns(3).columnName

        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn

        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val uniqueNewEntityDF = sourceDFs(uniqueNewEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, uniqueGidColumn)

        val groupedUnion = uniqueNewEntityDF.groupBy(subscriberId).agg(first(uniqueGidColumn).as(uniqueGidColumn))

        val confidentNewEntityDF = sourceDFs(confidentNewEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, confidentGidColumn)

        val groupedConfidents =
          confidentNewEntityDF.groupBy(subscriberId).agg(first(confidentGidColumn).as(confidentGidColumn))

        val suspectsNewEntityDF = sourceDFs(suspectsNewEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, suspectsGidColumn)

        val groupedSuspects =
          suspectsNewEntityDF.groupBy(subscriberId).agg(first(suspectsGidColumn).as(suspectsGidColumn))

        val unionDF = groupedUnion.union(groupedConfidents).union(groupedSuspects)

        val renameDF = unionDF.withColumnRenamed(uniqueGidColumn, uniqueGidColumn + "_Temp")
        val resultDF = targetDF
          .join(
            renameDF,
            targetDF(profileJoinColumn) === renameDF(entityJoinColumn),
            "left_outer"
          )
          .drop(renameDF(entityJoinColumn))
          .withColumn(targetColumn, renameDF(uniqueGidColumn + "_Temp"))
        resultDF

      case _ =>
        logger.warn(
          "GetGid can not be applied as sourceColumns size is not equals to 4."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
