package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetRewardIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val resultDF = sourceColumns.size match {
      case 2 =>
        val omsPortalColumn = sourceColumns(0).columnName
        val rewardEntity = sourceColumns(1).entityName
        val msisdnColumn = sourceColumns(1).columnName
        val profileJoinColumn = joinColumn.get.head.profileJoinColumn
        val entityJoinColumn = joinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val sourceDF = sourceDFs(rewardEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(omsPortalColumn, msisdnColumn)

        val getLastTenDigitiNameDF = sourceDF
          .withColumn(msisdnColumn, substring(col(msisdnColumn), -10, 10))

        val tempColumn = targetColumn + "_temp1"
        val transformedDF = getLastTenDigitiNameDF
          .withColumn(
            tempColumn,
            when(col(omsPortalColumn) === "3", "1")
              .otherwise("0")
          )
          .select(msisdnColumn, tempColumn)
          .repartitionAndSortWithInPartition(Array(col(msisdnColumn)), Array(col(tempColumn)))

        val tempTargetColumn = targetColumn + "_temp"
        val takeMaxIndicator = transformedDF
          .groupBy(msisdnColumn)
          .agg(max(tempColumn).alias(tempTargetColumn))
        val resultJoinDF = targetDF
          .join(
            takeMaxIndicator,
            targetDF(profileJoinColumn) === takeMaxIndicator(entityJoinColumn),
            "left_outer"
          )
          .drop(takeMaxIndicator(entityJoinColumn))
        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, resultJoinDF(tempTargetColumn))
        finalResultDF
      case _ =>
        logger.warn(
          "GetRewardIndicator can not be applied when sourceColumns size is not equal to 3"
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
