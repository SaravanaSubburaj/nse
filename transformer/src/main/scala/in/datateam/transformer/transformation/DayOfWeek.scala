package in.datateam.transformer.transformation

import java.time.LocalDateTime
import java.time.format
import java.time.format.TextStyle
import java.util.Locale

import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import org.apache.log4j.Logger

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object DayOfWeek extends TransformerColTrait with Serializable {

  /**
    * Finds the day of the week from an input date column, expects date column to be in yyyy-MM-dd HH:mm:ss.SSS format.
    *
    * @param row           Input row
    * @param sourceColumns Date column : A date column, needs to be in yyyy-MM-dd HH:mm:ss.SSS format.
    * @param targetColumn  DayOfWeek column (String) : Indicates the day of the week as String 'Friday', 'Saturday' etc.
    * @return Row with day of week specified.
    */
  def processRows(row: Row, sourceColumns: Array[SourceColumn], targetColumn: String): Row = {

    val logger: Logger = Logger.getLogger("DayOfWeek")

    val dateVal = row.getAs[String](sourceColumns(0).columnName)
    val dateValOpt = Try(
      LocalDateTime.parse(
        dateVal,
        format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
      )
    ).toOption
    //    FIXME: Remove var
    var dayOfWeekVar = row.getAs[String](targetColumn)

    if (dateValOpt.isDefined) {
      val dateValParsed = dateValOpt.get
      dayOfWeekVar = dateValParsed.getDayOfWeek
        .getDisplayName(TextStyle.FULL, Locale.ENGLISH)
    } else {
      logger.warn(
        "cadup.DateTimeParseException occurred in DayOfWeek, incorrect date format encountered," +
        "please make sure format is yyyy-MM-dd HH:mm:ss.SSS"
      )
    }

    val schemaVal = row.schema
    val validatedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => dayOfWeekVar
        case x: Any => row.getAs[Any](x)
      }
    }
    val validatedRow = new GenericRowWithSchema(validatedArray, schemaVal)
    validatedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val sourceEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => processRows(row, sourceColumns, targetColumn))(
      sourceEncoder
    )
  }

}
