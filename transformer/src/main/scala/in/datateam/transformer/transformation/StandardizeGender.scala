package in.datateam.transformer.transformation

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import spray.json._

import in.datateam.transformer.parser.StandardGender
import in.datateam.transformer.parser.StandardGenderJsonProtocol._
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.helper.FileSystemUtils

object StandardizeGender extends TransformerColTrait with Serializable {

  /**
    * Standardizing Gender for sourceColumn and will update in targetColumn
    *
    * @param row             Input Row
    * @param sourceColumns   for which we have to lookup Gender
    * @param targetColumn    in which it will update Standard Gender
    * @param genderBroadcast a broadcast variable for Standard Genders
    * @return will return Row that will have Standard Gender
    *         only if it will match otherwise it will return a empty Value.
    */
  def processRow(
      row: Row,
      sourceColumns: Array[SourceColumn],
      targetColumn: String,
      genderBroadcast: Broadcast[Map[String, Array[String]]]
    ): Row = {
    val gender: String = row.getAs[String](sourceColumns.head.columnName)
    val genderMatch: String = genderBroadcast.value
      .map(x => {
        if (x._2.contains(gender)) x._1 else ""
      })
      .mkString
    val schemaVal: StructType = row.schema
    val processedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => genderMatch
        case x: Any => row.getAs[Any](x)
      }
    }
    val processedRow = new GenericRowWithSchema(processedArray, schemaVal)
    processedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val broadcastDetail: Seq[LookupDetail] =
      broadcastDetails.filter(b => b.lookupName == "StandardGender")
    val genderLookUpString: String =
      FileSystemUtils.readFile(broadcastDetail.head.pathUrl)
    val standardGenderJson: StandardGender =
      genderLookUpString.parseJson.convertTo[StandardGender]
    val genderMap: Map[String, Array[String]] =
      standardGenderJson.genderLookup.toSeq
        .map(x => (x.gender, x.genderOption))
        .toMap[String, Array[String]]
    val genderBroadcast: Broadcast[Map[String, Array[String]]] =
      spark.sparkContext.broadcast(genderMap)
    implicit val sourceRowDF: ExpressionEncoder[Row] = RowEncoder(
      sourceDF.schema
    )
    sourceDF.map(
      row => processRow(row, sourceColumns, targetColumn, genderBroadcast)
    )

  }

}
