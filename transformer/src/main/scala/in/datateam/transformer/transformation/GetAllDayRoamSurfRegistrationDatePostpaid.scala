package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetAllDayRoamSurfRegistrationDatePostpaid extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 3 =>
        val assignedBillingOfferEntity = sourceColumns(0).entityName
        val assignedBIllingSubscriberId = sourceColumns(0).columnName
        val billingOfferDiscrptionColumn = sourceColumns(1).columnName
        val assignedBIllingOfferStartDate = sourceColumns(2).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val assignedBillingOfferEntityDF = sourceDFs(assignedBillingOfferEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            assignedBIllingSubscriberId,
            assignedBIllingOfferStartDate,
            billingOfferDiscrptionColumn
          )

        val dateFilterDF = assignedBillingOfferEntityDF.filter(
          col(assignedBIllingOfferStartDate) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          &&
          col(assignedBIllingOfferStartDate) < to_date(lit(partitionDate), Misc.dateFormat)
        )
        val offerDescriptionFilterDF = dateFilterDF
          .filter(lower(col(billingOfferDiscrptionColumn)).rlike(TransformerEnums.ALL_ROAM_SURF_FILTER))
          .repartitionAndSortWithInPartition(
            Array(col(assignedBIllingSubscriberId)),
            Array(col(assignedBIllingOfferStartDate))
          )

        val groupbyDF = offerDescriptionFilterDF
          .groupBy(assignedBIllingSubscriberId)
          .agg(max(assignedBIllingOfferStartDate).as(targetColumn + "_temp"))
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultJoinDF = targetDF
          .join(
            groupbyDF,
            targetDF(profileJoinColumn) === groupbyDF(entityJoinColumn),
            "left_outer"
          )
          .drop(groupbyDF(entityJoinColumn))

        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, resultJoinDF(targetColumn + "_temp"))

        finalResultDF
      case _ =>
        logger.warn(
          "GetAllDayRoamSurfRegistrationDatePostpaid can not be applied as sourceColumn" +
          "s size is not equals to 3."
        )
        targetDF
    }
    val targetColumns: List[String] = targetDF.columns.toList ++ List(
        targetColumn
      )
    resultDF.select(targetColumns.map(c => col(c)): _*)
  }
}
