package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.when

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

/**
  * Created by Prabhu GS (prabhu.gs@thedatateam.in).
  */
object SumOfMultipleColumns extends TransformerColTrait with Serializable {

  /**
    * This function computes sum of sourceColumns of a row and stores the result in targetColumn
    *
    * @param sourceDF         the source dataframe to be processed
    * @param sourceColumns    An array of SourceColumn
    * @param targetColumn     String containing the target column name
    * @param spark            Spark Session
    * @param extraParameter   Extra Parameter Map
    * @param broadcastDetails LookupDetails if any
    * @return transformed dataframe
    */
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    sourceDF
      .withColumn(
        targetColumn,
        sourceColumns
          .map(sourceColumn => col(sourceColumn.columnName))
          .toSeq
          .reduce { (a, b) =>
            val (aDouble, bDouble) = (a.cast("double"), b.cast("double"))
            when(aDouble.isNotNull && bDouble.isNotNull, aDouble + bDouble)
              .when(aDouble.isNotNull && bDouble.isNull, aDouble)
              .when(aDouble.isNull && bDouble.isNotNull, bDouble)
              .otherwise(aDouble)
          }
      )
  }
}
