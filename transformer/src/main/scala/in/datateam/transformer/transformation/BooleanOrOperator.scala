package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.when

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

/**
  * Created by Prabhu GS (prabhu.gs@thedatateam.in).
  */
object BooleanOrOperator extends TransformerColTrait with Serializable {

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    sourceDF
      .withColumn(
        targetColumn,
        sourceColumns
          .map(sourceColumn => col(sourceColumn.columnName))
          .toSeq
          .reduce { (a, b) =>
            val (aBool, bBool) = (a.cast("boolean"), b.cast("boolean"))
            when(aBool.isNotNull && bBool.isNotNull, aBool || bBool)
              .when(aBool.isNotNull && bBool.isNull, aBool)
              .when(aBool.isNull && bBool.isNotNull, bBool)
              .otherwise(aBool)
          }
      )
  }
}
