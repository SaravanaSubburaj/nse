package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

/**
  * @author Saravana(saravana.subburaj@thedatateam.in)
  */
object GetArpuCategoryCodeCombine extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val arpuCategoryCodePrepaidColumns = sourceColumns.slice(0, 2)
    val arpuCategoryCodePostpaidColumns = sourceColumns.slice(2, 4)
    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"
    val arpuCategoryCodePrepaidDF =
      GetArpuCategoryCode.process(
        sourceDFs,
        targetDF,
        arpuCategoryCodePrepaidColumns,
        joinColumn,
        targetColumnPrepaid
      )(spark, extraParameter)

    val arpuCategoryCodePostpaidDF = GetArpuCategoryCode.process(
      sourceDFs,
      arpuCategoryCodePrepaidDF,
      arpuCategoryCodePostpaidColumns,
      joinColumn,
      targetColumnPostpaid
    )(spark, extraParameter)

    val arpuCategoryCodeDF = CombinePrepaidAndPostpaidOperator
      .process(
        arpuCategoryCodePostpaidDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
        ),
        targetColumn
      )(spark, extraParameter)

    arpuCategoryCodeDF
  }

}
