package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetLteCapabaleIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 2 =>
        val subscriberEntity = sourceColumns.head.entityName
        val lteCapabable = sourceColumns.head.columnName
        val subscriberId = sourceColumns(1).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val sourceDF = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(lteCapabable, subscriberId)

        val derivedDf =
          sourceDF.withColumn(targetColumn + "_temp", when(upper(col(lteCapabable)) === "LTE", "1").otherwise("0"))

        val joinDF = targetDF
          .join(
            derivedDf,
            targetDF(profileJoinColumn) === derivedDf(entityJoinColumn),
            "left_outer"
          )
          .drop(derivedDf(entityJoinColumn))
          .drop(derivedDf(lteCapabable))
        val resultDF =
          joinDF.withColumn(targetColumn, joinDF(targetColumn + "_temp"))
        resultDF

      case _ =>
        logger.warn(
          "GetLteCapabaleIndicator can not be applied" +
          " as sourceColumns size is not equals to 2."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)

  }
}
