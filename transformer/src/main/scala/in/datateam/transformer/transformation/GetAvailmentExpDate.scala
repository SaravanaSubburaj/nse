package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetAvailmentExpDate extends TransformerAggColTrait with Serializable {

  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 4 =>
        val subsIdSubscriAssignBillOffer = sourceColumns.head.columnName
        val billingOfferDesc = sourceColumns(1).columnName
        val subsAssigndBillOfferStartDate = sourceColumns(2).columnName
        val subsAssigndBillOfferEndDate = sourceColumns(3).columnName
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val subsAssingBillOfferDF = sourceDFs(sourceColumns.head.entityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            subsIdSubscriAssignBillOffer,
            billingOfferDesc,
            subsAssigndBillOfferStartDate,
            subsAssigndBillOfferEndDate
          )

        val result = subsAssingBillOfferDF
          .filter(
            col(subsAssigndBillOfferStartDate) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
            &&
            col(subsAssigndBillOfferStartDate) < to_date(lit(partitionDate), Misc.dateFormat)
          )
          .withColumn(
            targetColumn,
            when(
              lower(col(billingOfferDesc))
              rlike TransformerEnums.EASY_ROAM_INDICATOR_FILTER,
              col(subsAssigndBillOfferEndDate)
            ).otherwise(null)
          )
          .withColumnRenamed(targetColumn, targetColumn + "_temp")
          .select(subsIdSubscriAssignBillOffer, targetColumn + "_temp")
          .repartitionAndSortWithInPartition(
            Array(col(subsIdSubscriAssignBillOffer)),
            Array(col(targetColumn + "_temp"))
          )

        val result1 =
          result
            .groupBy(subsIdSubscriAssignBillOffer)
            .agg(max(targetColumn + "_temp").alias(targetColumn + "_temp"))
            .repartitionAndSortWithInPartition(
              Array(col(joinColumn.get.head.entityJoinColumn)),
              Array(col(joinColumn.get.head.entityJoinColumn))
            )

        val joinedDF = targetDF
          .join(
            result1,
            targetDF(joinColumn.get.head.profileJoinColumn) === result(
                joinColumn.get.head.entityJoinColumn
              ),
            "left_outer"
          )
          .drop(result(joinColumn.get.head.entityJoinColumn))

        val resultantDF = joinedDF
          .withColumn(targetColumn, joinedDF(targetColumn + "_temp"))
        resultantDF

      case _ =>
        logger.warn(
          "Get Easy roam Avail Latest Exp Date can not be applied as sourceColumn" +
          "s size is not equals to 4."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
