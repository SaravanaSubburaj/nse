package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.min

import org.apache.log4j.Logger

import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object JoinDirectPullOnSubscriberId extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 2 =>
        val entityName = sourceColumns.head.entityName
        val attributeColumn = sourceColumns.head.columnName
        val subscriberId = sourceColumns(1).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val entityDf = sourceDFs(entityName)
        val minPartition = entityDf.select(min(CommonEnums.DATE_PARTITION_COLUMN)).head().getString(0)

        val filteredDF = entityDf
          .filter(col(CommonEnums.DATE_PARTITION_COLUMN) === minPartition)
          .select(attributeColumn, subscriberId)
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultJoinDF = targetDF
          .join(
            filteredDF,
            targetDF(profileJoinColumn) === entityDf(entityJoinColumn),
            "left_outer"
          )
          .drop(filteredDF(entityJoinColumn))

        val finalResultDF =
          resultJoinDF.withColumn(targetColumn, resultJoinDF(attributeColumn))
        finalResultDF
      case _ =>
        logger.warn(
          "JoinDirectPullOnSubscriberId can not be applied as sourceColumns size is not equals to 2."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
