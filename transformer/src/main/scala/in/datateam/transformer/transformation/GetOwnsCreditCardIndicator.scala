package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger
import org.joda.time.DateTime

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter

object GetOwnsCreditCardIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 10 =>
        val financialAcoountEntity = sourceColumns.head.entityName
        val accountFinancialId = sourceColumns.head.columnName
        val financialAccountStatusCode = sourceColumns(1).columnName
        val financialActivationDate = sourceColumns(2).columnName
        val subscriberReferenceEntity = sourceColumns(3).entityName
        val subscriberReferenceSubscriberId = sourceColumns(3).columnName
        val expiryDate = sourceColumns(4).columnName
        val subscriberReferenceFinancialId = sourceColumns(5).columnName
        val paymentStagingEntity = sourceColumns(6).entityName
        val paymentFinancialId = sourceColumns(6).columnName
        val paymentMethodCode = sourceColumns(7).columnName
        val subscriberEntity = sourceColumns(8).entityName
        val subscriberId = sourceColumns(8).columnName
        val paymentCatogeryCode = sourceColumns(9).columnName

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val subscriberDf = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, paymentCatogeryCode)

        val paymentStagingDf = getPaymentStaging(sourceDFs(paymentStagingEntity))(spark, extraParameter)
          .select(paymentFinancialId, paymentMethodCode)

        val subscriberReferenceDf = sourceDFs(subscriberReferenceEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberReferenceSubscriberId, subscriberReferenceFinancialId, expiryDate)
          .filter(col(expiryDate).isNull)
          .drop(expiryDate)

        val financialAccountDf = sourceDFs(financialAcoountEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(
            accountFinancialId,
            financialAccountStatusCode,
            financialActivationDate
          )
          .filter(
            upper(col(financialAccountStatusCode)) === "O" || upper(
              col(financialAccountStatusCode)
            ) === "SUS"
          )
          .drop(financialAccountStatusCode)
          .repartitionAndSortWithInPartition(Array(col(accountFinancialId)), Array(col(financialActivationDate)))

        val accountGroupDf = financialAccountDf
          .groupBy(accountFinancialId)
          .agg(max(financialActivationDate).as(financialActivationDate))
          .drop(financialActivationDate)

        val joinWithSubscriberReference = subscriberReferenceDf
          .join(
            accountGroupDf,
            subscriberReferenceDf(subscriberReferenceFinancialId) ===
              accountGroupDf(accountFinancialId),
            "left_outer"
          )
          .drop(subscriberReferenceDf(subscriberReferenceFinancialId))

        val paymentFilterDf = paymentStagingDf.filter(substring(col(paymentMethodCode), 0, 2).like("%CC"))

        val groupedDF = paymentFilterDf
          .groupBy(paymentFinancialId)
          .agg(count(col(paymentMethodCode)).as(paymentMethodCode + "_Count"))

        val joinDf = joinWithSubscriberReference
          .join(
            groupedDF,
            joinWithSubscriberReference(accountFinancialId) === groupedDF(paymentFinancialId),
            "inner"
          )
          .drop(groupedDF(paymentFinancialId))

        val joinWithSubscriber = subscriberDf
          .join(
            joinDf,
            subscriberDf(subscriberId)
              === joinDf(subscriberReferenceSubscriberId),
            "left_outer"
          )
          .drop(joinDf(subscriberReferenceSubscriberId))

        val filterOutPrepaidDF = joinWithSubscriber.filter(upper(col(paymentCatogeryCode)) === "POST")

        val calculatedDF = filterOutPrepaidDF.withColumn(
          targetColumn + "_Temp",
          when(col(paymentMethodCode + "_Count") > 0, "1").otherwise("0")
        )
        val resultJoinDF = targetDF
          .join(
            calculatedDF,
            targetDF(profileJoinColumn) === calculatedDF(entityJoinColumn),
            "left_outer"
          )
          .drop(calculatedDF(entityJoinColumn))
        val finalResultDF = resultJoinDF.withColumn(
          targetColumn,
          calculatedDF(targetColumn + "_temp")
        )

        finalResultDF

      case _ =>
        logger.warn("GetOwnsCreditCardIndicator can not be applied as sourceColumns size is not equals to 10")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def getPaymentStaging(
      sourceDF: DataFrame
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionValue = extraParameter(TransformerEnums.PARTITION_VALUE)
    val currentPartitionFormatted = dateFormatter.parse(partitionValue)

    val dateTimeMonthSub1 = new DateTime(currentPartitionFormatted)
    val dateTimeMonthSub2 =
      new DateTime(currentPartitionFormatted).minusMonths(1)
    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val currentMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val previousMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val currentMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === currentMonthVal
    )
    val previousMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === previousMonthVal
    )

    val PaymentStagingDF = if (!currentMonthDF.take(1).isEmpty) {
      currentMonthDF
    } else if (!previousMonthDF.take(1).isEmpty) {
      previousMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
    PaymentStagingDF
  }
}
