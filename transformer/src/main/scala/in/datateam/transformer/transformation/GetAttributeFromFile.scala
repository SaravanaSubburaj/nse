package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

object GetAttributeFromFile extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val partitionValue = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val joinColumns = targetJoinColumn.get
    val targetColPrepaid = targetColumn + "_prepaid"
    val targetColPostpaid = targetColumn + "_postpaid"

    val filePrepaidDF = TransformerHelpers
      .lookbackMonthly(sourceDFs, sourceColumns(0).entityName, partitionValue)
      .withColumnRenamed(targetColumn, targetColPrepaid)

    val filePostpaidDF = TransformerHelpers
      .lookbackMonthly(sourceDFs, sourceColumns(1).entityName, partitionValue)
      .withColumnRenamed(targetColumn, targetColPostpaid)
      .withColumnRenamed(targetColumn, targetColPostpaid)

    val prepaidSrcCols = Array(SourceColumn(sourceColumns(0).entityName, targetColPrepaid))
    val postpaidSrcCols = Array(SourceColumn(sourceColumns(1).entityName, targetColPostpaid))

    val prepaidJoinCols = Array(joinColumns(0))
    val postpaidJoinCols = prepaidJoinCols

    val targetPrepaidDF =
      JoinDirectPullWithProfile.process(
        Map(sourceColumns(0).entityName -> filePrepaidDF),
        targetDF,
        prepaidSrcCols,
        Option(prepaidJoinCols),
        targetColPrepaid
      )(
        spark,
        extraParameter
      )

    val targetPostpaidDF =
      JoinDirectPullWithProfile.process(
        Map(sourceColumns(1).entityName -> filePostpaidDF),
        targetPrepaidDF,
        postpaidSrcCols,
        Option(postpaidJoinCols),
        targetColPostpaid
      )(spark, extraParameter)

    val combinedDF = CombinePrepaidAndPostpaidOperator.process(
      targetPostpaidDF,
      Array(
        SourceColumn(CommonEnums.PROFILE_MASTER, targetColPrepaid),
        SourceColumn(CommonEnums.PROFILE_MASTER, targetColPostpaid)
      ),
      targetColumn
    )(spark, extraParameter)

    combinedDF
  }
}
