package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDeriveMdmTypeCode extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit
      spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val returnDF = sourceColumns.size match {
      case 3 =>
        val uniquesEntityName = sourceColumns(0).entityName
        val uniquesAttributeName = sourceColumns(0).columnName

        val confidentsEntityName = sourceColumns(1).entityName
        val confidentsAttributeName = sourceColumns(1).columnName

        val suspectsEntityName = sourceColumns(2).entityName
        val suspectsAttributeName = sourceColumns(2).columnName

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val uniquesDF = sourceDFs(uniquesEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(uniquesAttributeName)
          .distinct()
          .withColumn(targetColumn + "_temp", lit("U"))

        val confidentsDF = sourceDFs(confidentsEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(confidentsAttributeName)
          .distinct()
          .withColumn(targetColumn + "_temp", lit("C"))

        val suspectsDF = sourceDFs(suspectsEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(suspectsAttributeName)
          .distinct()
          .withColumn(targetColumn + "_temp", lit("S"))

        val mdmDF = uniquesDF.union(confidentsDF).union(suspectsDF)

        val resultDF = targetDF
          .join(mdmDF, targetDF(profileJoinColumn) === mdmDF(entityJoinColumn), "left_outer")
          .withColumn(targetColumn, mdmDF(targetColumn + "_temp"))
          .drop(mdmDF(entityJoinColumn))

        resultDF

      case _ =>
        logger.warn(
          "JoinDeriveMdmTypeCode can not be applied as sourceColumns size is not equals to 2."
        )
        targetDF

    }

    returnDF.select(targetDF.columns.map(c => col(c)): _*)

  }

}
