package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

/**
  * @author Saravana(saravana.subburaj@thedatateam.in)
  */
object GetMostFreqAvailAllDayRoamSurfCombine extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val mostFreqAvailAllDayRoamSurfPrepaidColumns = sourceColumns.slice(0, 7)
    val mostFreqAvailAllDayRoamSurfPostpaidColumns = sourceColumns.slice(7, 10)
    val targetColumnPrepaid = targetColumn + "_prepaid"
    val targetColumnPostpaid = targetColumn + "_postpaid"

    val mostFreqAvailAllDayRoamSurfPrepaidDF =
      GetMostFreqAvailAllDayRoamSurfPrepaid.process(
        sourceDFs,
        targetDF,
        mostFreqAvailAllDayRoamSurfPrepaidColumns,
        joinColumns,
        targetColumnPrepaid
      )(spark, extraParameter)

    val mostFreqAvailAllDayRoamSurfPostpaidDF =
      GetMostFreqAvailAllDayRoamSurfPostpaid.process(
        sourceDFs,
        mostFreqAvailAllDayRoamSurfPrepaidDF,
        mostFreqAvailAllDayRoamSurfPostpaidColumns,
        joinColumns,
        targetColumnPostpaid
      )(spark, extraParameter)

    val mostFreqAvailAllDayRoamSurfDF = CombinePrepaidAndPostpaidOperator
      .process(
        mostFreqAvailAllDayRoamSurfPostpaidDF,
        Array(
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPrepaid),
          SourceColumn(CommonEnums.PROFILE_MASTER, targetColumnPostpaid)
        ),
        targetColumn
      )(spark, extraParameter)

    mostFreqAvailAllDayRoamSurfDF

  }

}
