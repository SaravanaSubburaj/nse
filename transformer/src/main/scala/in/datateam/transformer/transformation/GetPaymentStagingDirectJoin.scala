package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger
import org.joda.time.DateTime

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter

object GetPaymentStagingDirectJoin extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 9 =>
        val paymentStagingEntity = sourceColumns.head.entityName
        val finanicalAccountEntity = sourceColumns(2).entityName
        val paymentFinancialAccountId = sourceColumns(1).columnName
        val attributeColumn = sourceColumns.head.columnName
        val accountFinancialId = sourceColumns(2).columnName
        val financialAccountStatusCode = sourceColumns(4).columnName
        val financialActivationDate = sourceColumns(5).columnName
        val financialSubscriberReferenceEntity = sourceColumns(6).entityName
        val subscriberReferenceSubscriberId = sourceColumns(6).columnName
        val expiryDate = sourceColumns(7).columnName
        val subscriberReferenceFinancialId = sourceColumns(8).columnName
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val paymentStagingDf = getPaymentStaging(sourceDFs(paymentStagingEntity))(spark, extraParameter)
          .select(attributeColumn, paymentFinancialAccountId)

        val subscriberReferenceDf = sourceDFs
          .get(financialSubscriberReferenceEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberReferenceSubscriberId, subscriberReferenceFinancialId, expiryDate)
          .filter(col(expiryDate).isNull)
          .drop(expiryDate)

        val financialAccountDf = sourceDFs(finanicalAccountEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(
            accountFinancialId,
            financialAccountStatusCode,
            financialActivationDate
          )
          .filter(
            upper(col(financialAccountStatusCode)) === "O" || upper(
              col(financialAccountStatusCode)
            ) === "SUS"
          )
          .drop(financialAccountStatusCode)
          .repartitionAndSortWithInPartition(Array(col(accountFinancialId)), Array(col(financialActivationDate)))

        val accountGroupDf = financialAccountDf
          .groupBy(accountFinancialId)
          .agg(max(financialActivationDate).as(financialActivationDate))
          .drop(financialActivationDate)

        val joinWithSubscriberReference = subscriberReferenceDf
          .join(
            accountGroupDf,
            subscriberReferenceDf(subscriberReferenceFinancialId) ===
              accountGroupDf(accountFinancialId),
            "left_outer"
          )
          .drop(subscriberReferenceDf(subscriberReferenceFinancialId))

        val joinDf = joinWithSubscriberReference
          .join(
            paymentStagingDf,
            joinWithSubscriberReference(accountFinancialId) === paymentStagingDf(paymentFinancialAccountId),
            "inner"
          )
          .drop(paymentStagingDf(paymentFinancialAccountId))
          .drop(accountFinancialId)
          .repartitionAndSortWithInPartition(Array(col(subscriberReferenceSubscriberId)), Array(col(attributeColumn)))

        val groupDf = joinDf
          .groupBy(subscriberReferenceSubscriberId)
          .agg(max(attributeColumn).as(targetColumn + "_temp"))
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultJoinDF = targetDF
          .join(
            groupDf,
            targetDF(profileJoinColumn) === groupDf(entityJoinColumn),
            "left_outer"
          )
          .drop(joinDf(entityJoinColumn))

        val finalResultDF = resultJoinDF.withColumn(
          targetColumn,
          resultJoinDF(targetColumn + "_temp")
        )

        finalResultDF
      case _ =>
        logger.warn(
          "GetPaymentStagingDirectJoin can not be applied as sourceColumns size is not equals to 9."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }

  def getPaymentStaging(
      sourceDF: DataFrame
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionValue = extraParameter(TransformerEnums.PARTITION_VALUE)
    val currentPartitionFormatted = dateFormatter.parse(partitionValue)

    val dateTimeMonthSub1 = new DateTime(currentPartitionFormatted)
    val dateTimeMonthSub2 =
      new DateTime(currentPartitionFormatted).minusMonths(1)
    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val currentMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val previousMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val currentMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === currentMonthVal
    )
    val previousMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === previousMonthVal
    )

    val PaymentStagingDF = if (!currentMonthDF.take(1).isEmpty) {
      currentMonthDF
    } else if (!previousMonthDF.take(1).isEmpty) {
      previousMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
    PaymentStagingDF
  }
}
