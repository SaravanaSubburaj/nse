package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
trait TransformerTrait

trait TransformerRowTrait {
  def process(row: Row, sourceColumns: Array[SourceColumn], targetColumn: String): Row
}

trait TransformerColTrait {

  def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame
}

trait TransformerAggColTrait {

  def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame
}

trait TransformerAggTrait {

  def process(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      groupByColumns: Array[SourceColumn],
      aggregateColumns: Array[SourceColumn],
      targetColumns: Array[String]
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame
}
