package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object TrimWhiteSpaces extends TransformerColTrait with Serializable {

  /**
    * Trimming White Space from leading and trailing in the sourceColumn and returning DF.
    *
    * @param sourceDF  Input DF
    * @param sourceColumns Column in which have to Trim White Spaces
    * @param targetColumn  Column which is set for target here sourceColumn and target column will be same.
    * @return          DF with Trimmed white spaces from leading and trailing in the targetColumn.
    */
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    sourceDF.withColumn(
      sourceColumns.head.columnName,
      trim(sourceDF(sourceColumns.head.columnName))
    )
  }
}
