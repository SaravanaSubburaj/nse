package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.concat_ws

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object ConcatenateColumns extends TransformerColTrait with Serializable {
  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    sourceDF.withColumn(
      targetColumn,
      concat_ws(
        " ",
        sourceColumns.map(srcColumn => col(srcColumn.columnName)).toSeq: _*
      )
    )
  }
}
