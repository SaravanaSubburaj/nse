package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object GetMRF extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 3 =>
        val partitionValue =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val chargeStagingEntity = sourceColumns.head.entityName
        val subscriberId = sourceColumns(1).columnName
        val revenueTypeCode = sourceColumns.head.columnName
        val chargeTotalAmount = sourceColumns(2).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn

        val sourceDf = TransformerHelpers
          .lookbackMonthly(sourceDFs, chargeStagingEntity, partitionValue)
          .select(subscriberId, chargeTotalAmount, revenueTypeCode)

        val filterDf = sourceDf
          .filter(trim(upper(col(revenueTypeCode))) === "RC")
          .drop(revenueTypeCode)
          .repartitionAndSortWithInPartition(Array(col(subscriberId)), Array())

        val groupDF = filterDf.groupBy(subscriberId).agg(sum(chargeTotalAmount).alias(targetColumn + "_temp"))
        val joinDF = targetDF
          .join(
            groupDF,
            targetDF(profileJoinColumn) === groupDF(entityJoinColumn),
            "left_outer"
          )
          .drop(groupDF(entityJoinColumn))
        val resultDF =
          joinDF.withColumn(targetColumn, joinDF(targetColumn + "_temp"))
        resultDF
      case _ =>
        logger.warn(
          "GetMRF can not be applied as sourceColumns size is not equals to 3."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
