package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.row_number
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger
import org.joda.time.DateTime

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter

object GetHandsetLteIndicator extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 8 =>
        val networkDataEntity = sourceColumns.head.entityName
        val subscriberEntity = sourceColumns(6).entityName
        val deviceEntity = sourceColumns(3).entityName
        val dataMsisdn = sourceColumns.head.columnName
        val dataDeviceId = sourceColumns(1).columnName
        val dataTopLocationDate = sourceColumns(2).columnName
        val deviceId = sourceColumns(3).columnName
        val is4GIndicator = sourceColumns(4).columnName
        val subscriberStatusCode = sourceColumns(5).columnName
        val subscriberId = sourceColumns(6).columnName
        val msisdnValue = sourceColumns(7).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
        val deviceDf = getDeviceDF(sourceDFs.get(deviceEntity).get)(spark, extraParameter)
          .select(deviceId, is4GIndicator)

        val dataDf = sourceDFs(networkDataEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(dataDeviceId, dataMsisdn, dataTopLocationDate)
          .repartitionAndSortWithInPartition(Array(col(dataMsisdn)), Array(col(dataTopLocationDate).desc))

        val subscriberDf = sourceDFs(subscriberEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(subscriberId, subscriberStatusCode, msisdnValue)
          .filter(
            upper(col(subscriberStatusCode)) =!= "C"
            && upper(col(subscriberStatusCode)) =!= "N"
            && upper(col(subscriberStatusCode)) =!= "T"
          )
          .drop(subscriberStatusCode)

        val window =
          Window.partitionBy(dataMsisdn).orderBy(col(dataTopLocationDate).desc)
        val groupedDF = dataDf
          .withColumn("rank", row_number().over(window))
          .select(dataMsisdn, dataDeviceId)
          .where("rank == 1")
        val joinWithDevice = groupedDF
          .join(
            deviceDf,
            groupedDF(dataDeviceId) === deviceDf(deviceId),
            "left_outer"
          )
          .drop(groupedDF(dataDeviceId))
          .drop(deviceId)
        val derivedDf =
          joinWithDevice.withColumn(
            targetColumn + "_temp",
            when(upper(col(is4GIndicator)) === "Y", "1").otherwise("0")
          )

        val joinwithresult = subscriberDf
          .join(
            derivedDf,
            subscriberDf(dataMsisdn) === derivedDf(msisdnValue),
            "left_outer"
          )
          .drop(derivedDf(msisdnValue))
          .drop(profileJoinColumn)
          .drop(subscriberStatusCode)
          .repartitionAndSortWithInPartition(Array(col(entityJoinColumn)), Array(col(entityJoinColumn)))

        val resultDF = targetDF
          .join(
            joinwithresult,
            targetDF(profileJoinColumn) === joinwithresult(entityJoinColumn),
            "left_outer"
          )
          .drop(joinwithresult(entityJoinColumn))
          .withColumn(targetColumn, joinwithresult(targetColumn + "_temp"))
        resultDF
      case _ =>
        logger.warn(
          "GetHandsetLteIndicator can not be applied as sourceColumns size is not equals to 8."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def getDeviceDF(sourceDF: DataFrame)(implicit spark: SparkSession, extraParameter: Map[String, String]): DataFrame = {

    val partitionValue = extraParameter.get(TransformerEnums.PARTITION_VALUE).get
    val currentPartitionFormatted = dateFormatter.parse(partitionValue)

    val dateTimeMonthSub1 = new DateTime(currentPartitionFormatted)
    val dateTimeMonthSub2 =
      new DateTime(currentPartitionFormatted).minusMonths(1)
    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val currentMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val previousMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val currentMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === currentMonthVal
    )
    val previousMonthDF = sourceDF.filter(
      col(CommonEnums.DATE_PARTITION_COLUMN) === previousMonthVal
    )

    val subscriberRevenueSummaryDF = if (!currentMonthDF.take(1).isEmpty) {
      currentMonthDF
    } else if (!previousMonthDF.take(1).isEmpty) {
      previousMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
    subscriberRevenueSummaryDF
  }
}
