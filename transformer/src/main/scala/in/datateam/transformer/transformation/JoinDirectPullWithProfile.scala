package in.datateam.transformer.transformation

import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object JoinDirectPullWithProfile extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  /**
    * This transformer can be used to extract attribute with direct join with target dataframe.
    * Provide broadcastFlag as "true" in transformationOption in case involved entity is less 10 MB.
    * sample config
    * {
    *   "attributeName": "attribute_name",
    *   "sourceColumns": [
    *     {"entityName": "entity_1", "columnName": "entity_attribute_name"},
    *   ],
    *   "joinColumns": [
    *     {"entityJoinColumn": "entity_1_col_1", "profileJoinColumn": "profile_col_1"},
    *     {"entityJoinColumn": "entity_1_col_2", "profileJoinColumn": "profile_col_2"}
    *   ],
    *   "transformationOption": [
    *     {"key": "broadcastFlag", "value": "true"}
    *   ]
    *   "transformationFunction": "JoinDirectPullWithEntityAndProfile"
    * }
    *
    */
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 1 =>
        val entityName = sourceColumns.head.entityName
        val attributeColumn = sourceColumns.head.columnName
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val broadcastFlag = extraParameter.getOrElse("broadcastFlag", "false").toLowerCase == "true"
        val joinColumns = targetJoinColumn.get
        val requiredColumns = Array(attributeColumn) ++ joinColumns.map(joinColumn => joinColumn.entityJoinColumn)
        val entityDF = sourceDFs(entityName)

        val partitionValueDF = entityDF.select(CommonEnums.DATE_PARTITION_COLUMN)
        val partitionValue = if (!partitionValueDF.take(1).isEmpty) {
          partitionValueDF.head().getString(0)
        } else {
          partitionDate
        }
        val validFormatters = Array(Misc.dateFormatter, Misc.monthOnlyDateFormatter, Misc.hourlyDateFormatter)
        val parseResults = validFormatters.map(x => Try(x.parse(partitionValue))).map(x => x.isSuccess)
        val requiredDateFormat = parseResults.indexOf(true) match {
          case 0 => Misc.dateFormat
          case 1 => Misc.monthOnlyDateFormat
          case 2 => Misc.hourlyDateFormat
        }

        val filterDF = requiredDateFormat match {
          case Misc.monthOnlyDateFormat => entityDF.select(requiredColumns.map(c => col(c)): _*)
          case _ =>
            entityDF
              .filter(
                to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
                > date_add(to_date(lit(partitionDate), requiredDateFormat), -1)
              )
              .select(requiredColumns.map(c => col(c)): _*)

        }

        val joinCondition = joinColumns
          .map(
            joinColumn => targetDF(joinColumn.profileJoinColumn) === filterDF(joinColumn.entityJoinColumn)
          )
          .reduce((a, b) => a && b)

        val joinedAllColumnDF = if (broadcastFlag) {
          targetDF.join(broadcast(filterDF), joinCondition, "left_outer")
        } else {
          targetDF.join(filterDF, joinCondition, "left_outer")
        }

        val joinedDF = joinColumns.foldLeft(joinedAllColumnDF) { (iDF: DataFrame, joinColumn) =>
          iDF.drop(filterDF(joinColumn.entityJoinColumn))
        }

        val finalResultDF =
          joinedDF.withColumn(targetColumn, joinedDF(attributeColumn))
        finalResultDF
      case _ =>
        logger.warn(
          "JoinDirectPullWithProfile can not be applied as sourceColumns size is not equals to 1."
        )
        targetDF
    }
    if (targetDF.columns.contains(targetColumn)) {
      resultDF.select(targetDF.columns.map(c => col(c)): _*)
    } else {
      resultDF
    }
  }
}
