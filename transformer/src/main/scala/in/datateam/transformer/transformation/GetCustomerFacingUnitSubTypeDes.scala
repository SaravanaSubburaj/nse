package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.date_add
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.to_date

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetCustomerFacingUnitSubTypeDes extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.size match {
      case 1 =>
        val attributeColumnEntity = sourceColumns.head.entityName
        val customerFacingUnitSubTypeDescription = sourceColumns(0).columnName
        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
        val entityJoinColumns = targetJoinColumn.get
        val profileJoinColumns = targetJoinColumn.get
        val customerFacingUnitTypeCodeAttr = entityJoinColumns(0).entityJoinColumn
        val customerFacingUnitSubTypeCodeAttr = entityJoinColumns(1).entityJoinColumn
        val customerFacingUnitTypeCodeProf = profileJoinColumns(0).profileJoinColumn
        val customerFacingUnitSubTypeCodeProf = profileJoinColumns(1).profileJoinColumn

        val attributeColumnDF = sourceDFs(attributeColumnEntity)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -1)
          )
          .select(
            customerFacingUnitTypeCodeAttr,
            customerFacingUnitSubTypeDescription,
            customerFacingUnitSubTypeCodeAttr
          )

        val resultJoinDF = targetDF
          .join(
            broadcast(attributeColumnDF),
            targetDF(customerFacingUnitTypeCodeProf) === attributeColumnDF(customerFacingUnitTypeCodeAttr) && targetDF(
              customerFacingUnitSubTypeCodeProf
            ) === attributeColumnDF(customerFacingUnitSubTypeCodeAttr),
            "left_outer"
          )
          .drop(attributeColumnDF(customerFacingUnitTypeCodeAttr))
          .drop(attributeColumnDF(customerFacingUnitSubTypeCodeAttr))

        val finalResultDF = resultJoinDF.withColumn(targetColumn, resultJoinDF(customerFacingUnitSubTypeDescription))

        finalResultDF
      case _ =>
        logger.warn("JoinDirectPullWithEntity can not be applied as sourceColumns size is not equals to 1.")
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)

  }
}
