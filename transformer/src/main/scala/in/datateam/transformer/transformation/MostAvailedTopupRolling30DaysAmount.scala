package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

/**
  * Transformation arguments, sourceColumns need to be ordered:
  * index 0: groupBy_1 (relSubscriberId)
  * index 1: filter (reloadDate)
  * index 2: filter (totalReloadAmount)
  * index 3: sum (totalReloadCount)
  * index 4: groupBy_2 (reloadDenominationAmount)
  */
//TODO fix the logic , this is a copy, it does not have logic specific to the attribute

object MostAvailedTopupRolling30DaysAmount extends TransformerAggColTrait with Serializable {
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val logger = Logger.getLogger(getClass.getName)

    val resultDF = sourceColumns.length match {
      case 5 => {
        val reloadsDF = sourceDFs(sourceColumns.head.entityName)

        val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)

        val filteredReloadsDF = reloadsDF.filter(
          to_date(col(CommonEnums.DATE_PARTITION_COLUMN)) >= date_add(
              to_date(lit(partitionDate), Misc.dateFormat),
              -91
            )
        )

        calculateMostAvailedTopup(
          filteredReloadsDF,
          targetDF,
          sourceColumns,
          joinColumns,
          targetColumn
        )
      }
      case _ => {
        logger.warn(
          "You need to supply 5 columns for MostAvailedTopupRolling30DaysAmount"
        )
        targetDF
      }
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }

  def calculateMostAvailedTopup(
      sourceDF: DataFrame,
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      joinColumns: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String]
    ): DataFrame = {

    val partitionDate = extraParameter(TransformerEnums.PARTITION_VALUE)
    val partitionDateMinusTwo = Misc.getNthDate(partitionDate, -2)

    val subscriberIdCol = sourceColumns(0).columnName
    val reloadDateCol = sourceColumns(1).columnName
    val reloadAmtCol = sourceColumns(2).columnName
    val totalReloadCountCol = sourceColumns(3).columnName
    val reloadDenominationAmountCol = sourceColumns(4).columnName

    val typechangeDF =
      sourceDF.withColumn(reloadDateCol, col(reloadDateCol).cast(DateType))

    val groupedDF = typechangeDF
      .select(
        col(subscriberIdCol),
        col(reloadDateCol),
        col(reloadAmtCol),
        col(totalReloadCountCol),
        col(reloadDenominationAmountCol)
      )
      .filter(
        col(reloadDateCol) >= Misc.getNthDate(partitionDate, -31)
        &&
        col(reloadDateCol) <= partitionDateMinusTwo
        &&
        col(reloadAmtCol) > 0
      )
      .groupBy(col(subscriberIdCol), col(reloadDenominationAmountCol))
      .agg(
        sum(col(totalReloadCountCol))
          .as(targetColumn + "_temp")
      )
      .repartitionAndSortWithInPartition(Array(col(subscriberIdCol)), Array(col(targetColumn + "_temp").desc))

    val rankedDF = groupedDF
      .withColumn(
        "rank",
        row_number().over(
          Window
            .partitionBy(col(subscriberIdCol))
            .orderBy(col(targetColumn + "_temp").desc)
        )
      )
      .filter(col("rank") === 1)
      .repartitionAndSortWithInPartition(
        Array(col(joinColumns.get.head.entityJoinColumn)),
        Array(col(joinColumns.get.head.entityJoinColumn))
      )

    val joinedDF = targetDF
      .join(
        rankedDF,
        targetDF(joinColumns.get.head.profileJoinColumn) === rankedDF(
            joinColumns.get.head.entityJoinColumn
          ),
        "left_outer"
      )
      .drop(rankedDF(joinColumns.get.head.entityJoinColumn))

    val retDF =
      joinedDF.withColumn(targetColumn, joinedDF(reloadDenominationAmountCol))

    retDF

  }
}
