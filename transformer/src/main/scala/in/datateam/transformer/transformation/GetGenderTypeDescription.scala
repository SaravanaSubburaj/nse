package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.upper
import org.apache.spark.sql.functions.when

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.transformer.transformation.AAModelLookupTransform.logger
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object GetGenderTypeDescription extends TransformerAggColTrait with Serializable {

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 3 =>
        val partitionValue =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val entityName = sourceColumns.head.entityName
        val subsId = sourceColumns.head.columnName
        val model = sourceColumns(1).columnName
        val score = sourceColumns(2).columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val aaModelDF = TransformerHelpers
          .lookbackMonthly(sourceDFs, entityName, partitionValue)
          .filter(upper(col(model)) === TransformerEnums.GENDER_TYPE_SCORE)
          .select(subsId, model, score)

        val aaModelScoreDF = aaModelDF
        val calculateDF = aaModelScoreDF.withColumn(
          targetColumn + "Temp",
          when(upper(col(score)) === "F", "FEMALE")
            .when(upper(col(score)) === "M", "MALE")
            .otherwise(null)
        )

        val resultDF = targetDF
          .join(
            calculateDF,
            targetDF(profileJoinColumn) === calculateDF(entityJoinColumn),
            "left_outer"
          )
          .drop(calculateDF(entityJoinColumn))
          .withColumn(targetColumn, calculateDF(targetColumn + "Temp"))
        resultDF
      case _ =>
        logger.warn(
          "GetGenderTypeDescription can not be applied" +
          " as sourceColumns size is not equals to 3."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
