package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.format
import java.time.temporal.ChronoUnit

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object NumberOfDaysSpent extends TransformerColTrait with Serializable {

  /**
    * Calculates DaysSpent from Date column. Expects date in d/MM/yyyy.
    *
    * @param row Input row
    * @param sourceColumns Date (String) : Column for Date, in d/MM/yyyy format.
    * @param targetColumn DaySpent column (Integer) : No of days Spent, initially null.
    * @return Row with computed DaySpent
    */
  def processRow(row: Row, sourceColumns: Array[SourceColumn], targetColumn: String): Row = {
    val dateVal = row.getAs[String](sourceColumns(0).columnName)
    var DaySpent = row.getAs[Any](targetColumn)
    val dateValParsed =
      LocalDate.parse(dateVal, format.DateTimeFormatter.ofPattern("d/MM/yyyy"))

    DaySpent = ChronoUnit.DAYS.between(dateValParsed, LocalDate.now())

    val schemaVal = row.schema
    val validatedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => DaySpent
        case x: Any => row.getAs[Any](x)
      }
    }
    val validatedRow = new GenericRowWithSchema(validatedArray, schemaVal)
    validatedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    implicit val sourceDFEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(row => processRow(row, sourceColumns, targetColumn))
  }
}
