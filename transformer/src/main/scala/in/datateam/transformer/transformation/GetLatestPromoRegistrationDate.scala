package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetLatestPromoRegistrationDate extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 6 =>
        val subscriberAvailmentStagingEntity = sourceColumns(0).entityName
        val promoAvailmentDateColumn = sourceColumns(0).columnName
        val availmentSubscriberId = sourceColumns(1).columnName
        val promoOperationId = sourceColumns(2).columnName
        val availmentChannelCode = sourceColumns(3).columnName
        val serviceId = sourceColumns(4).columnName
        val promoDenominationValue = sourceColumns(5).columnName
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val partitionDate = extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val sourceAvailmentDF = sourceDFs
          .get(subscriberAvailmentStagingEntity)
          .get
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
            > date_add(to_date(lit(partitionDate), Misc.dateFormat), -90)
          )
          .select(
            promoAvailmentDateColumn,
            availmentSubscriberId,
            promoOperationId,
            availmentChannelCode,
            serviceId,
            promoDenominationValue
          )

        val filterDF = sourceAvailmentDF.filter(
          col(promoAvailmentDateColumn) >
          date_add(to_date(lit(partitionDate), Misc.dateFormat), -180)
        )

        val sourceAvailmentFilterDF = filterDF
          .filter(
            col(promoOperationId).isin("1", "6", "39") &&
            (col(availmentChannelCode) =!= "1001" || col(serviceId) === "2374")
          )

        val groupDF = sourceAvailmentFilterDF
          .withColumn(
            "rank",
            row_number()
              .over(
                Window
                  .partitionBy(col(availmentSubscriberId))
                  .orderBy(col(promoAvailmentDateColumn).desc, col(promoDenominationValue).desc)
              )
          )
          .filter(col("rank") === 1)
          .withColumnRenamed(promoAvailmentDateColumn, targetColumn + "_temp")

        val joinDF = targetDF
          .join(groupDF, targetDF(profileJoinColumn) === groupDF(entityJoinColumn), "left_outer")
          .drop(groupDF(entityJoinColumn))

        val resultDF = joinDF.withColumn(targetColumn, joinDF(targetColumn + "_temp"))
        resultDF

      case _ =>
        logger.warn(
          "GetLatestPromoRegistrationDate can not be applied" +
          " as sourceColumns size is not equals to 2."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
