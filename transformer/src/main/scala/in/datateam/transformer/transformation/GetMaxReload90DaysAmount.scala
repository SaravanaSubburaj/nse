package in.datateam.transformer.transformation
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc

object GetMaxReload90DaysAmount extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)
  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val resultDF = sourceColumns.length match {
      case 5 =>
        val reloadSummaryEntityName = sourceColumns.head.entityName
        val subscriberId = sourceColumns.head.columnName
        val totalReloadAmount = sourceColumns(1).columnName
        val reloadChannelTypecode = sourceColumns(2).columnName
        val reloadDenominationValue = sourceColumns(3).columnName
        val reloadDate = sourceColumns(4).columnName
        val datePartition = extraParameter(TransformerEnums.PARTITION_VALUE)

        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val sourceDf = sourceDFs(reloadSummaryEntityName)
          .filter(
            to_date(col(CommonEnums.DATE_PARTITION_COLUMN))
              >= date_add(to_date(lit(datePartition), Misc.dateFormat), -90)
          )
          .select(subscriberId, reloadChannelTypecode, reloadDenominationValue, totalReloadAmount)
          .filter(col(totalReloadAmount) > 0)
          .drop(totalReloadAmount)
          .filter(lower(col(reloadChannelTypecode)).isin("ax", "iv", "ud", "es", "pn", "eg", "rp"))
          .drop(reloadChannelTypecode)
          .filter(
            to_date(col(reloadDate)) >= date_add(to_date(lit(datePartition), Misc.dateFormat), -90)
            &&
            to_date(col(reloadDate)) <= to_date(lit(datePartition))
          )

        val groupedDf = sourceDf.groupBy(subscriberId).agg(max(col(reloadDenominationValue)).as(targetColumn + "_Temp"))

        val resultJoinDF = targetDF
          .join(
            groupedDf,
            targetDF(profileJoinColumn) === groupedDf(
                entityJoinColumn
              ),
            "left_outer"
          )
          .drop(groupedDf(entityJoinColumn))

        val finalResultDF = resultJoinDF
          .withColumn(targetColumn, groupedDf(targetColumn + "_Temp"))

        finalResultDF
      case _ =>
        logger.warn(
          "GetMaxReload90DaysAmount can not be applied as sourceColumns size is not equals to 5."
        )
        targetDF
    }
    resultDF.select(targetDF.columns.map(c => col(c)): _*)
  }
}
