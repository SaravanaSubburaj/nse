package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DateType

import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object FilterProfiles extends TransformerColTrait with Serializable {

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {

    val brandTypeCodeCol = sourceColumns(0).columnName
    val subscriberStatusCodeCol = sourceColumns(1).columnName
    val msisdnValueCol = sourceColumns(2).columnName

    val lastPromoRegDateCol = sourceColumns(3).columnName
    val lastDataPpuDateCol = sourceColumns(4).columnName
    val lastCorePpuDateCol = sourceColumns(5).columnName

    //filter profile acc. to condition
    val filteredProfiles = sourceDF
      .filter(
        lower(trim(col(brandTypeCodeCol))).isin("tm", "ghp", "ghp-prepaid")
        && !lower(trim(col(subscriberStatusCodeCol))).isin("c", "l", "t")
      )
      .withColumn(lastPromoRegDateCol, col(lastPromoRegDateCol).cast(DateType))
      .withColumn(lastDataPpuDateCol, col(lastDataPpuDateCol).cast(DateType))
      .withColumn(lastCorePpuDateCol, col(lastCorePpuDateCol).cast(DateType))

    val retDF = filteredProfiles.withColumn(
      msisdnValueCol,
      concat(lit("63"), col(msisdnValueCol))
    )
    retDF
  }

}
