package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

import org.apache.log4j.Logger

import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.helper.TransformerHelpers
import in.datateam.transformer.helper.DataFrameUtils.DataFrameOp
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

object AAModelLookupTransform extends TransformerAggColTrait with Serializable {
  val logger: Logger = Logger.getLogger(getClass.getName)

  override def process(
      sourceDFs: Map[String, DataFrame],
      targetDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetJoinColumn: Option[Array[JoinColumn]],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastsDetails: LookupDetail*
    ): DataFrame = {
    val returnDF = sourceColumns.size match {
      case 3 =>
        val partitionValue =
          extraParameter.get(TransformerEnums.PARTITION_VALUE).get

        val entityName = sourceColumns.head.entityName
        val subsId = sourceColumns.head.columnName
        val model = sourceColumns(1).columnName
        val score = sourceColumns(2).columnName
        val entityJoinColumn = targetJoinColumn.get.head.entityJoinColumn
        val profileJoinColumn = targetJoinColumn.get.head.profileJoinColumn

        val filterCondition = targetColumn match {

          case "prepaid_topup_segmentation_score" =>
            s"upper(${model}) == '${TransformerEnums.TOPUP_SEGMENTATION}'"
          case "interest_segmentation_score" =>
            s"(upper(${model}) == '${TransformerEnums.POSTPAID_INTEREST_SEGMENTATION}'" +
            s"OR upper(${model}) == '${TransformerEnums.PREPAID_INTEREST_SEGMENTATION}')"
          case "postpaid_clv_segment_name" =>
            s"upper(${model}) == '${TransformerEnums.POST_CLV_FILTER}'"
          case "prepaid_churn_propensity_decile" =>
            s"upper(${model}) == '${TransformerEnums.PREPAID_CHURN_FILTER}'"
          case "prepaid_churn_propensity_score" =>
            s"upper(${model}) == '${TransformerEnums.PREPAID_CHURN_PROPENSITY_SCORE}'"
          case "postpaid_churn_propensity_score" =>
            s"(upper(${model}) == '${TransformerEnums.POSTPAID_SI_CHURN_LI_SCORE}'" +
            s" OR upper(${model}) == '${TransformerEnums.POSTPAID_SI_CHURN_OB_SCORE}')"
          case "postpaid_churn_propensity_decile" =>
            s"(upper(${model}) == '${TransformerEnums.POSTPAID_SI_CHURN_LI}'" +
            s" OR upper(${model}) == '${TransformerEnums.POSTPAID_SI_CHURN_OB}')"
          case "airtime_loan_10_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_10}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_15_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_15}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_20_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_20}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_25_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_25}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_30_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_30}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_40_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_40}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_5_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_5}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "airtime_loan_50_availer_topup_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.AIRTIME_SCORE_50}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "content_games_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.CONTENT_GAMES_SCORE}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "content_music_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.CONTENT_MUSIC_SCORE}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "content_video_propensity_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.CONTENT_MOVIES_SCORE}'" +
            s" AND upper(${subsId}) IS NOT NULL)"
          case "roaming_propensity_decile" =>
            s"(lower(${model}) IN ('prepaid_roaming', 'postpaid_roaming')" +
            s" AND ${subsId} IS NOT NULL)"
          case "roaming_propensity_score" =>
            s"(lower(${model}) IN ('prepaid_roaming_score', 'postpaid_roaming_score')" +
            s" AND ${subsId} IS NOT NULL)"
          case "prepaid_income_score" =>
            s"(upper(${model}) LIKE '${TransformerEnums.PREPAID_INCOME_SCORE}'" +
            s" AND ${subsId} IS NOT NULL)"

        }

        val aaModelScoreDF = TransformerHelpers
          .lookbackMonthly(sourceDFs, entityName, partitionValue)
          .filter(filterCondition)
          .select(subsId, model, score)
          .repartitionAndSortWithInPartition(Array(col(subsId)), Array())

        val resultDF = targetDF
          .join(
            aaModelScoreDF,
            targetDF(profileJoinColumn) === aaModelScoreDF(entityJoinColumn),
            "left_outer"
          )
          .drop(aaModelScoreDF(entityJoinColumn))
          .withColumn(targetColumn, aaModelScoreDF(score))
        resultDF
      case _ =>
        logger.warn(
          "AAModelLookupTransform can not be applied as sourceColumns size is not equals to 3."
        )
        targetDF
    }
    returnDF.select(targetDF.columns.map(c => col(c)): _*)
  }

}
