package in.datateam.transformer.transformation

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types.StructField

import spray.json._

import in.datateam.transformer.parser.StandardMaritalStatus
import in.datateam.transformer.parser.StandardMaritalStatusJsonProtocol._
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.helper.FileSystemUtils

object StandardizeMaritalStatus extends TransformerColTrait with Serializable {

  /**
    * Standardizing Marital Status for sourceColumn and will update in targetColumn
    *
    * @param row                    Input Row
    * @param sourceColumns          for which we have to lookup Marital Status
    * @param targetColumn           in which it will update Standard Marital Status
    * @param maritalStatusBroadcast a broadcast variable for Standard Marital Status
    * @return will return Row that will have Standard Marital Status
    *         only if it will match otherwise it will return a empty Value.
    */
  def processRow(
      row: Row,
      sourceColumns: Array[SourceColumn],
      targetColumn: String,
      maritalStatusBroadcast: Broadcast[Map[String, String]]
    ): Row = {

    val maritalStatus = row.getAs[String](sourceColumns.head.columnName)
    val maritalStatusMatch =
      maritalStatusBroadcast.value.getOrElse(maritalStatus, "")
    val schemaVal = row.schema

    val processedArray: Array[Any] = schemaVal.fields.map { structField: StructField =>
      structField.name match {
        case `targetColumn` => maritalStatusMatch
        case x: Any => row.getAs[Any](x)
      }
    }
    val processedRow = new GenericRowWithSchema(processedArray, schemaVal)
    processedRow
  }

  override def process(
      sourceDF: DataFrame,
      sourceColumns: Array[SourceColumn],
      targetColumn: String
    )(
      implicit spark: SparkSession,
      extraParameter: Map[String, String],
      broadcastDetails: LookupDetail*
    ): DataFrame = {

    val broadcastDetail =
      broadcastDetails.filter(b => b.lookupName == "StandardMaritalStatus")
    val maritalStatusLookupString =
      FileSystemUtils.readFile(broadcastDetail.head.pathUrl)
    val standardMaritalStatusJson =
      maritalStatusLookupString.parseJson.convertTo[StandardMaritalStatus]
    val maritalStatusMap = standardMaritalStatusJson.maritalStatusLookup
      .flatMap(x => {
        for {
          value <- x.possibleValues
        } yield value -> x.maritalStatus
      }.toMap)
      .toMap

    val maritalStatusBroadcast = spark.sparkContext.broadcast(maritalStatusMap)
    implicit val sourceDFEncoder = RowEncoder(sourceDF.schema)
    sourceDF.map(
      row => processRow(row, sourceColumns, targetColumn, maritalStatusBroadcast)
    )

  }

}
