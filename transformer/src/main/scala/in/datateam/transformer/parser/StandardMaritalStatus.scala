package in.datateam.transformer.parser

import spray.json.DefaultJsonProtocol

case class MaritalStatus(maritalStatus: String, possibleValues: Array[String])

case class StandardMaritalStatus(maritalStatusLookup: Array[MaritalStatus])

object StandardMaritalStatusJsonProtocol extends DefaultJsonProtocol {
  implicit val maritalStatusFormat = jsonFormat2(MaritalStatus)
  implicit val standardMaritalStatusFormat = jsonFormat1(StandardMaritalStatus)
}
