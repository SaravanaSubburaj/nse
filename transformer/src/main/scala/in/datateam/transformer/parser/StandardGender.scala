package in.datateam.transformer.parser

import spray.json.DefaultJsonProtocol

case class Gender(gender: String, genderOption: Array[String])

case class StandardGender(genderLookup: Array[Gender])

object StandardGenderJsonProtocol extends DefaultJsonProtocol {
  implicit val genderFormat = jsonFormat2(Gender)
  implicit val standardGenderFormat = jsonFormat1(StandardGender)
}
