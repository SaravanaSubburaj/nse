package in.datateam.transformer.parser

import spray.json.DefaultJsonProtocol

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
case class Currency(currencyCode: String, currencyName: String, currencySign: String)

case class StandardCurrency(currencyLookup: Array[Currency])

object StandardCurrencyJsonProtocol extends DefaultJsonProtocol {
  implicit val currencyFormat = jsonFormat3(Currency)
  implicit val standardCurrencyFormat = jsonFormat1(StandardCurrency)
}
