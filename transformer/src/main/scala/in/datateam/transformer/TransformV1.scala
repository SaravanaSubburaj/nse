package in.datateam.transformer

import scala.reflect.runtime.{universe => ru}

import org.apache.spark.sql.AnalysisException
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit

import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger

import in.datateam.fury.AuditAndLineageManager
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.transformer.transformation.TransformerAggColTrait
import in.datateam.transformer.transformation.TransformerAggTrait
import in.datateam.transformer.transformation.TransformerColTrait
import in.datateam.transformer.transformation.TransformerRowTrait
import in.datateam.transformer.transformation.TransformerTrait
import in.datateam.utils.configparser.AttributeTransformationUnit
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils
import in.datateam.utils.helper.FileSystemUtils.getCheckpointDir
import in.datateam.utils.helper.Misc

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object TransformV1 {
  val logger: Logger = Logger.getLogger(getClass.getName)

  def transformDataFrame(
      config: Config,
      targetDF: DataFrame,
      sourceDFs: Map[String, DataFrame],
      partition: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {
    val runtime = ru.runtimeMirror(this.getClass.getClassLoader)
    implicit val validDFEncoder = RowEncoder(targetDF.schema)
    implicit val extraParameters = Map(
      TransformerEnums.PARTITION_VALUE -> partition,
      //FIXME: Find better way to pass partiton column
      TransformerEnums.EXISTING_PARTITION_COL -> config.getExistingPartitionColumn()
    )
    val writeInterval = 10
    val tempTargetDF = if (config.configType == CommonEnums.PROFILE) {
      getXAttributes(config, targetDF, 0, writeInterval)
    } else {
      targetDF
    }
    val transformationsDetails: Array[AttributeTransformationUnit] =
      getTransformationDetailsWithNoEmptyDFSource(config, sourceDFs)
    val transformedDF = transformationsDetails.zipWithIndex.foldLeft[DataFrame](tempTargetDF) {
      (preDF: DataFrame, transformationDetailsWithIndex: (AttributeTransformationUnit, Int)) =>
        val (transformationDetails: AttributeTransformationUnit, index: Int) = transformationDetailsWithIndex
        val transformationOptionMap = transformationDetails.getTransformationOptionsMap()
        implicit val extraParametersWithTransformationOptions = extraParameters ++ transformationOptionMap

        val handlerValue = s"in.datateam.transformer.transformation.${transformationDetails.transformationFunction}"
        val module = runtime.staticModule(handlerValue)

        val lineageCols: List[String] = getAuditAndLineageColumns(config)

        val objectInstance = runtime.reflectModule(module).instance

        val postDF = try {
          objectInstance match {
            case transformationFunction: TransformerRowTrait =>
              preDF.map(
                row =>
                  transformationFunction
                    .process(row, transformationDetails.sourceColumns, transformationDetails.attributeName)
              )
            case transformationFunction: TransformerColTrait =>
              transformationFunction.process(
                preDF,
                transformationDetails.sourceColumns,
                transformationDetails.attributeName
              )(spark, extraParametersWithTransformationOptions, config.lookupDetails.getOrElse(Array()).toSeq: _*)
            case transformationFunction: TransformerAggColTrait =>
              transformationFunction.process(
                sourceDFs,
                preDF,
                transformationDetails.sourceColumns,
                transformationDetails.joinColumns,
                transformationDetails.attributeName
              )(spark, extraParametersWithTransformationOptions)
            case transformationFunction: TransformerAggTrait =>
              transformationFunction.process(
                preDF,
                preDF,
                transformationDetails.groupByColumns.get,
                transformationDetails.sourceColumns,
                transformationDetails.attributeColumns.get
              )(spark, extraParametersWithTransformationOptions)
            case _ =>
              throw new ClassNotFoundException(
                handlerValue + " is not an instance of " + classOf[TransformerTrait].getName
              )
          }
        } catch {
          case analysisException: AnalysisException =>
            logger.info(s"CDP_10001 Calculating ${transformationDetails.attributeName} failed.")
            logger.info(s"Reason for failure message: ${analysisException.message}")
            logger.info("Stacktrace: " + analysisException.getStackTrace.map(x => x.toString).mkString("\n"))

            preDF
        }
        auditAndLineageManager.process(
          preDF,
          postDF,
          transformationDetails.transformationFunction,
          transformationDetails.attributeName,
          config,
          partition,
          lineageCols: _*
        )
        val interval = 5
        val iipostDF = if (((index + 1) % interval == 0) && config.configType == CommonEnums.PROFILE) {
          // Writing data after calculation of 5 attributes to avoid memory issue in smaller executors.
          val att = config.transformationsDetails.toList.slice(index - writeInterval + 1, index + 1)
          logger.info(s"Writing attributes ${att.map(x => x.attributeName).mkString(", ")}")
          dagShortener(postDF, config)
        } else {
          postDF
        }

        val ipostDF = if (((index + 1) % writeInterval == 0) && config.configType == CommonEnums.PROFILE) {
          // Writing data after calculation of 5 attributes to avoid memory issue in smaller executors.
          val att = config.transformationsDetails.toList.slice(index - writeInterval + 1, index + 1)
          val allColumns = getAllColumns(config, index - writeInterval + 1, writeInterval)
          logger.info(s"Writing attributes ${att.map(x => x.attributeName).mkString(", ")}")
          logger.info(s"Columns index : ${index} ${postDF.columns.mkString(", ")}")
          val attPrefix = s"${index - writeInterval + 1}_${index + 1}"
          val partDF = postDF.select(allColumns.map(c => col(c)): _*)
          writePartAttributes(partDF, config, attPrefix)
          val iTempTargetDF = getXAttributes(config, targetDF, index + 1, writeInterval)
          iTempTargetDF
        } else {
          iipostDF
        }
        ipostDF
    }
    if (config.configType == CommonEnums.PROFILE) {
      val attrSize = config.transformationsDetails.size
      val lastAttributeCount = config.transformationsDetails.size % writeInterval
      if (lastAttributeCount != 0) {
        val attPrefix = s"${attrSize - lastAttributeCount}_${attrSize}"
        writePartAttributes(transformedDF, config, attPrefix)
      }
    }
    transformedDF
  }

  def process(
      config: Config,
      targetDF: DataFrame,
      sourceDFs: Map[String, DataFrame],
      partitionValue: String
    )(
      implicit spark: SparkSession,
      auditAndLineageManager: AuditAndLineageManager
    ): DataFrame = {
    val updatedPartitionValue = config.sourceDetails.head.partitionDetails match {
      case Some(_) =>
        Misc.getFormattedPartitionDate(
          config.sourceDetails.head.partitionDetails.get.partitionFormat
            .mkString(CommonEnums.PARTITION_DELIMITER.toString),
          config.sourceDetails.head.partitionDetails.get.partitionType,
          partitionValue
        )
      case None => Misc.getFormattedPartitionDate(partitionValue)
    }

    val intermediateDF = config.configType match {
      case CommonEnums.PROFILE =>
        val directColumns = config.columnMapping.map(c => c.targetColumn)
        val joinColumns = getJoinColumns(config)
        val allColumns = (directColumns ++ joinColumns).distinct.map(c => col(c))
        writePartAttributes(targetDF.select(allColumns: _*), config, "direct")
        targetDF
      case CommonEnums.ENTITY => targetDF
    }

    val transformedDF = transformDataFrame(config, intermediateDF, sourceDFs, updatedPartitionValue)
    val finalDF: DataFrame = if (config.configType == CommonEnums.ENTITY) {
      logger.info("Transformation Completed.")
      transformedDF
    } else {
      logger.info("Attribute mapping Completed.")
      joinPartAttributes(config, partitionValue)(spark)
    }
    finalDF
  }

  def getAuditAndLineageColumns(config: Config): List[String] = {
    val lineageCols: List[String] = config.configType match {
      case CommonEnums.ENTITY =>
        List(CommonEnums.DATE_PARTITION_COLUMN) ++
        config.sourceDetails.head.primaryColumns.get.toList
      case CommonEnums.PROFILE => List(CommonEnums.DATE_PARTITION_COLUMN, config.targetSchema.head.columnName)
    }
    lineageCols
  }

  def getJoinColumns(config: Config): List[String] = {
    val columns: List[String] = config.transformationsDetails.filter { transformationUnit =>
        transformationUnit.joinColumns.isDefined
      }.map { transformationUnit =>
        transformationUnit.joinColumns.get.map { joinColumns =>
          joinColumns.profileJoinColumn
        }
      }.flatten.toList ++ List(CommonEnums.DATE_PARTITION_COLUMN) ++ getAuditAndLineageColumns(config)
    columns.distinct
  }

  def writePartAttributes(
      dataFrame: DataFrame,
      config: Config,
      attributePrefix: String
    )(
      implicit spark: SparkSession
    ): Unit = {
    val partitionColumns = List(CommonEnums.DATE_PARTITION_COLUMN)
    val partAttributesPathStr = getCheckpointDir() + "/attributes/" + attributePrefix
    logger.info(s"Writing ${attributePrefix} columns into ${partAttributesPathStr}")
    dataFrame.write
      .partitionBy(partitionColumns: _*)
      .mode(SaveMode.Overwrite)
      .format("parquet")
      .save(partAttributesPathStr)
  }

  def joinPartAttributes(config: Config, partitionValue: String)(implicit spark: SparkSession): DataFrame = {
    val partAttributesPathStr = getCheckpointDir() + "/attributes/"
    val fs = FileSystemUtils.getFileSystem(partAttributesPathStr)
    val partAttributesPath = fs.listStatus(new Path(partAttributesPathStr)).map { path =>
      partAttributesPathStr + path.getPath.getName
    }
    val allDFs = partAttributesPath.map { path =>
      spark.read.format("parquet").load(path)
    }
    val joinColumns = getJoinColumns(config)
    val repartitionCols = joinColumns.slice(0, joinColumns.size).map(c => col(c))
    val firstDF = allDFs.head
      .repartition(repartitionCols: _*)
      .sortWithinPartitions(repartitionCols: _*)
    firstDF.take(1)
    val joinedDF = allDFs.tail.foldLeft(firstDF) { (targetDF, partAttributeDF) =>
      val partDF = partAttributeDF
        .repartition(repartitionCols: _*)
        .sortWithinPartitions(repartitionCols: _*)
      partDF.take(1)
      targetDF.join(partDF, joinColumns, "left_outer")
    }
    val populatedColumns = joinedDF.columns
    val emptyColumns = config.targetSchema.filterNot { targetSchemaUnit =>
      populatedColumns.contains(targetSchemaUnit.columnName)
    }
    val finalDF: DataFrame = emptyColumns.foldLeft(joinedDF) { (tempDF, schemaUnit) =>
      tempDF.withColumn(schemaUnit.columnName, lit("").cast(schemaUnit.dataType))
    }
    val allColumns = config.getAllColumns.map(columnName => col(columnName))
    val finalDFWithDateColumn = finalDF
      .withColumn(CommonEnums.DATE_PARTITION_COLUMN, lit(partitionValue))
      .select(allColumns: _*)
    finalDFWithDateColumn
  }

  def getAllColumns(config: Config, index: Int, interval: Int)(implicit spark: SparkSession): List[String] = {
    val joinColumns = getJoinColumns(config)
    val requiredColumns = config.transformationsDetails.slice(index, index + interval).map(c => c.attributeName)
    val allColumns = joinColumns ++ requiredColumns
    allColumns
  }

  def getXAttributes(
      config: Config,
      sourceDF: DataFrame,
      index: Int,
      interval: Int
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val joinColumns = getJoinColumns(config)
    val allColumns = getAllColumns(config, index, interval)
    val repartitionCols = joinColumns.slice(0, joinColumns.size).map(c => col(c))
    val reqDF = sourceDF
      .select(allColumns.map(c => col(c)): _*)
      .repartition(repartitionCols: _*)
      .sortWithinPartitions(repartitionCols: _*)
    reqDF.persist()
    reqDF.take(1)
    reqDF
  }

  def writePartAttributes(dataFrame: DataFrame, config: Config)(implicit spark: SparkSession): DataFrame = {
    val partitionColumns = List(CommonEnums.DATE_PARTITION_COLUMN)
    FileSystemUtils.writeTempData(dataFrame, config, partitionColumns)
    FileSystemUtils.readTempData(dataFrame.schema)
  }

  def dagShortener(dataFrame: DataFrame, config: Config)(implicit spark: SparkSession): DataFrame = {
    val joinColumns = getJoinColumns(config)
    val repartitionCols = joinColumns.slice(0, joinColumns.size).map(c => col(c))
    val repartitionedDF = dataFrame
      .repartition(repartitionCols: _*)
      .sortWithinPartitions(repartitionCols: _*)
      .persist()
    repartitionedDF.take(1)
    repartitionedDF
  }

  def getTransformationDetailsWithNoEmptyDFSource(
      config: Config,
      sourceDFs: Map[String, DataFrame]
    )(
      implicit spark: SparkSession
    ): Array[AttributeTransformationUnit] = {
    val emptyDFList: List[String] = sourceDFs.filter {
      case (_, dataFrame) =>
        dataFrame.columns.isEmpty
    }.map { case (entityName, _) => entityName }.toList

    val transformationDetailsArray = config.transformationsDetails.filter {
      (attributeTransformationUnit: AttributeTransformationUnit) =>
        attributeTransformationUnit.sourceColumns
          .filter(sourceColumn => emptyDFList.contains(sourceColumn.entityName))
          .isEmpty
    }

    config.transformationsDetails.filter { (attributeTransformationUnit: AttributeTransformationUnit) =>
      !attributeTransformationUnit.sourceColumns
        .filter(sourceColumn => emptyDFList.contains(sourceColumn.entityName))
        .isEmpty
    }.foreach { (attributeTransformationUnit: AttributeTransformationUnit) =>
      logger.info(s"Skipping attribute ${attributeTransformationUnit.attributeName} as source path doesn't exist.")
    }

    transformationDetailsArray
  }
}
