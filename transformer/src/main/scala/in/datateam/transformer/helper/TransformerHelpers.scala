package in.datateam.transformer.helper

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger
import org.joda.time.DateTime

import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.Misc.dateFormatter
import in.datateam.utils.helper.Misc.monthOnlyDateFormatter

object TransformerHelpers {
  val logger: Logger = Logger.getLogger(getClass.getName)

  //TODO :- could take a parameter 'n' to look back 'n' months
  def lookbackMonthly(
      sourceDFs: Map[String, DataFrame],
      entityName: String,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val partitionValueFormatted = dateFormatter.parse(partitionValue)
    val partitionValueFormatted1 =
      dateFormatter.format(partitionValueFormatted)

    val dateTimeMonthSub0 =
      new DateTime(partitionValueFormatted1)
    val dateTimeMonthSub1 =
      new DateTime(partitionValueFormatted1).minusMonths(1)
    val dateTimeMonthSub2 =
      new DateTime(partitionValueFormatted1).minusMonths(2)

    val MonthSub0 = dateFormatter.format(dateTimeMonthSub0.toDate)
    val MonthSub1 = dateFormatter.format(dateTimeMonthSub1.toDate)
    val MonthSub2 = dateFormatter.format(dateTimeMonthSub2.toDate)

    val currentMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub0))
    val firstMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub1))
    val secondMonthVal =
      monthOnlyDateFormatter.format(dateFormatter.parse(MonthSub2))

    val sourceDF =
      sourceDFs.get(entityName).get

    val currentMonthDF = sourceDF.filter(
      s"${CommonEnums.DATE_PARTITION_COLUMN} == '${currentMonthVal}'"
    )

    val firstMonthDF = sourceDF.filter(
      s"${CommonEnums.DATE_PARTITION_COLUMN} == '${firstMonthVal}'"
    )

    val secondMonthDF = sourceDF.filter(
      s"${CommonEnums.DATE_PARTITION_COLUMN} == '${secondMonthVal}'"
    )

    val returnDF = if (!currentMonthDF.take(1).isEmpty) {
      currentMonthDF
    } else if (!firstMonthDF.take(1).isEmpty) {
      firstMonthDF
    } else if (!secondMonthDF.take(1).isEmpty) {
      secondMonthDF
    } else {
      val schemaEmptyDF = sourceDF.schema
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schemaEmptyDF)
    }
    returnDF
  }

}
