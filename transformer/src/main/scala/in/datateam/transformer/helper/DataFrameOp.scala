package in.datateam.transformer.helper

import org.apache.spark.sql.Column
import org.apache.spark.sql.DataFrame

import in.datateam.transformer.enums.TransformerEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object DataFrameUtils {

  implicit class DataFrameOp(val df: DataFrame) {

    def customRepartition(c: Column*): DataFrame = {
      if (TransformerEnums.REPARTITION_AND_SORT) {
        df.repartition(c: _*)
      } else {
        df
      }
    }

    def customSortWithInPartition(c: Column*): DataFrame = {
      if (TransformerEnums.REPARTITION_AND_SORT) {
        df.sortWithinPartitions(c: _*)
      } else {
        df
      }
    }

    def repartitionAndSortWithInPartition(repartitionColumns: Array[Column], sortColumns: Array[Column]): DataFrame = {
      if (TransformerEnums.REPARTITION_AND_SORT) {
        val repartitionedDF =
          sortColumns.size match {
            case 0 => df.repartition(repartitionColumns: _*)
            case _ => df.repartition(repartitionColumns: _*).sortWithinPartitions(sortColumns: _*)
          }
        repartitionedDF.take(1)
        repartitionedDF
      } else {
        df
      }
    }

//    def repartitionAndSortWithInPartition1(repartitionColumns: Column*)(sortColumns: Column*): DataFrame = {
//      if (TransformerEnums.REPARTITION_AND_SORT) {
//        val repartitionedDF = df.repartition(repartitionColumns: _*).sortWithinPartitions(sortColumns: _*)
//        repartitionedDF.take(1)
//        repartitionedDF
//      } else {
//        df
//      }
//    }
  }

}
