package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
class SumOfMultipleColumnsTest extends FlatSpec with SparkTest {
  "SumOfMultipleColumnsTest" should "calculate sum of all columns" in withSparkSession { spark: SparkSession =>
    val sampleList = List(
      Row("16.915", "1", null),
      Row("0", "26.616", null),
      Row("12.606", "1", null),
      Row("1", "1", null),
      Row(null, "1", null),
      Row("1", null, null),
      Row("1", "1", null),
      Row(null, null, null)
    )

    val schema = StructType(
      List(
        StructField("col_1", StringType),
        StructField("col_2", StringType),
        StructField("col_3", StringType)
      )
    )
    val sourceColumns =
      Array(SourceColumn("sample", "col_1"), SourceColumn("sample", "col_2"))
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleList),
      schema
    )
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val calculatedDF =
      SumOfMultipleColumns.process(sampleDF, sourceColumns, "col_3")(
        spark,
        extraParameters
      )
    val collectCalculatedDF = calculatedDF.collect()

    val ExpectedDF = Array(
      Row("16.915", "1", 17.915),
      Row("0", "26.616", 26.616),
      Row("12.606", "1", 13.606),
      Row("1", "1", 2.0),
      Row(null, "1", 1.0),
      Row("1", null, 1.0),
      Row("1", "1", 2.0),
      Row(null, null, null)
    )
    assertResult(collectCalculatedDF)(ExpectedDF)
  }
}
