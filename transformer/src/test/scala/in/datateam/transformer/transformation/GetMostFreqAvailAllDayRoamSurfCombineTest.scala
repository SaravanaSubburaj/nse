package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMostFreqAvailAllDayRoamSurfCombineTest extends FlatSpec with SparkTest {
  "GetMostFreqAvailAllDayRoamSurfCombineTest" should "Test prepaid and postpaid availment_most_availed_all_day_roam_surf_variant_90days_name combined in a single attribute" in withSparkSession {

    spark: SparkSession =>
      val sourceColumns = Array(
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "subscriberId"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "prepaidPromoId"),
        SourceColumn("PrepaidPromo", "prepaidPromoId"),
        SourceColumn("PrepaidPromo", "prepaidPromoDescription"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "promoChargeAmount"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "promoOperationDatetime"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "serviceId"),
        SourceColumn("SubscriberAssignedBillingOffer", "subscriberId"),
        SourceColumn("SubscriberAssignedBillingOffer", "billingOfferDescription"),
        SourceColumn("SubscriberAssignedBillingOffer", "subscriberAssignedBillingOfferStartDate")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subscriber_id"))
      )
      val date = "2019-01-16"
      val subscriberList = List(
        Row("1", date),
        Row("2", date),
        Row("3", date)
      )
      val subscriberDFSchema = StructType(
        List(
          StructField("subscriber_id", StringType, nullable = false),
          StructField("date", StringType, nullable = false)
        )
      )

      val globePrepaidPromoAvailmentStagingSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("prepaidPromoId", dataType = StringType),
          StructField("promoChargeAmount", dataType = DoubleType),
          StructField("serviceId", dataType = StringType),
          StructField("promoOperationDatetime", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )
      val prepaidPromoSchema = StructType(
        List(
          StructField("prepaidPromoId", dataType = StringType),
          StructField("prepaidPromoDescription", dataType = StringType),
          StructField("date", StringType, nullable = false)
        )
      )

      val subscriberAssignedBillingOfferSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("billingOfferDescription", dataType = StringType),
          StructField("subscriberAssignedBillingOfferStartDate", dataType = StringType),
          StructField("date", StringType, nullable = false)
        )
      )

      val globePrepaidPromoAvailmentStagingList = List(
        Row("1", "GS50_BASE3", 5.0, "S1", "2018-12-01 18:16:46", date),
        Row("2", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", date),
        Row("1", "GS50_BASE3", 3.0, "S1", "2018-12-01 18:16:46", date),
        Row("2", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", date),
        Row("1", "GS50_BASE3", 3.0, "S1", "2018-12-01 18:16:46", date),
        Row("3", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", date),
        Row("1", "GS50_BASE2", 3.0, "S1", "2018-12-01 18:16:46", date),
        Row("1", "GS50_BASE3", 3.0, "S1", "2018-12-01 18:16:46", date),
        Row("4", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", date)
      )

      val prepaidPromoList = List(
        Row("FREE_WNP2_2GB3", "ALLNETPROMO_USSD_30D", date),
        Row("ALLNETPROMO_USSD_30D3", "ALLNETPROMO_USSD_50D", date),
        Row("GS50_BASE2", "EASYROAMSURF150", date),
        Row("GS50_BASE3", "ROAMSURF150", date)
      )
      val subscriberAssignedBillingOfferList = List(
        Row("1", "roam surf 149", "2018-12-01", date),
        Row("2", "roam surf 149", "2018-03-13", date),
        Row("1", "roam surf 149", "2018-12-01", date),
        Row("2", "roam surf 249", "2018-03-13", date),
        Row("1", "roam surf 249", "2018-12-01", date),
        Row("2", "roam surf 349", "2018-03-13", date)
      )

      val profileList = List(Row("1"), Row("2"), Row("3"))
      val profileSchema = StructType(
        List(
          StructField("subscriber_id", StringType)
        )
      )

      val subscriberDF = spark.createDataFrame(spark.sparkContext.parallelize(subscriberList), subscriberDFSchema)

      val globePrepaidPromoAvailmentStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globePrepaidPromoAvailmentStagingList),
        globePrepaidPromoAvailmentStagingSchema
      )

      val prepaidPromoDF = spark.createDataFrame(
        spark.sparkContext.parallelize(prepaidPromoList),
        prepaidPromoSchema
      )

      val subscriberAssignedBillingOfferDF = spark.createDataFrame(
        spark.sparkContext.parallelize(subscriberAssignedBillingOfferList),
        subscriberAssignedBillingOfferSchema
      )

      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileSchema)

      implicit val extraParameters: Map[String, String] = Map(TransformerEnums.PARTITION_VALUE -> date)

      val sourceDFs = Map(
        "Subscriber"                        -> subscriberDF,
        "GlobePrepaidPromoAvailmentStaging" -> globePrepaidPromoAvailmentStagingDF,
        "PrepaidPromo"                      -> prepaidPromoDF,
        "SubscriberAssignedBillingOffer"    -> subscriberAssignedBillingOfferDF
      )
      val targetColumn = "availment_most_availed_all_day_roam_surf_variant_90days_name"

      val getMostFreqAvailAllDayRoamSurfCombineDF = GetMostFreqAvailAllDayRoamSurfCombine
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actual = getMostFreqAvailAllDayRoamSurfCombineDF.collect()
      val expected = Array(
        Row("3", "EASYROAMSURF150"),
        Row("1", "ROAMSURF150"),
        Row("2", "EASYROAMSURF150")
      )
      assertResult(expected)(actual)
  }
}
