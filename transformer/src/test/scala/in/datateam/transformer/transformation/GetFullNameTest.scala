package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class GetFullNameTest extends FlatSpec with SparkTest {
  "GetFullName Test1" should "Return Row with fullName based on firstName and lastName column" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleList = List(
        Row("Allen", "John", null),
        Row("Rakesh", "Sharma", null),
        Row("David ", "William", null),
        Row("Akash", "Raj", "Akash Raj")
      )
      val schema = StructType(
        List(
          StructField("firstName", StringType),
          StructField("lastName", StringType),
          StructField("fullName", StringType, nullable = true)
        )
      )
      val sourceColumns = Array(
        SourceColumn("sample", "firstName"),
        SourceColumn("sample", "lastName")
      )
      val sampleDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleList),
        schema
      )
      val calculatedDF = ConcatenateColumns
        .process(sampleDF, sourceColumns, "fullName")(spark, extraParameters)
      val collectCalculatedDF = calculatedDF.collect()
      val ExpectedDF = List(
        Row("Allen", "John", "Allen John"),
        Row("Rakesh", "Sharma", "Rakesh Sharma"),
        Row("David ", "William", "David  William"),
        Row("Akash", "Raj", "Akash Raj")
      )
      assertResult(ExpectedDF)(collectCalculatedDF)
  }

  "GetFullName Test2" should "Return Row with fullName based on first,last and middleName column" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleList = List(
        Row("Allen", "Rayan", "John", null),
        Row("Rakesh", "Roshan", "Sharma", null),
        Row("David ", "Sam", "William", null),
        Row("Akash", "Singh", "Raj", "Akash Singh Raj")
      )
      val schema = StructType(
        List(
          StructField("firstName", StringType),
          StructField("middleName", StringType),
          StructField("lastName", StringType),
          StructField("fullName", StringType, nullable = true)
        )
      )
      val sourceColumns = Array(
        SourceColumn("sample", "firstName"),
        SourceColumn("sample", "middleName"),
        SourceColumn("sample", "lastName")
      )
      val sampleDF = spark
        .createDataFrame(spark.sparkContext.parallelize(sampleList), schema)
        .alias("sample")
      val calculatedDF = ConcatenateColumns
        .process(sampleDF, sourceColumns, "fullName")(spark, extraParameters)
      val collectCalculatedDF = calculatedDF.collect()
      val ExpectedDF = List(
        Row("Allen", "Rayan", "John", "Allen Rayan John"),
        Row("Rakesh", "Roshan", "Sharma", "Rakesh Roshan Sharma"),
        Row("David ", "Sam", "William", "David  Sam William"),
        Row("Akash", "Singh", "Raj", "Akash Singh Raj")
      )
      assertResult(ExpectedDF)(collectCalculatedDF)
  }

  "GetFullName Test3" should "Return Row with fullName based on multiple column" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleList = List(
        Row("Allen", "R", "Rayan", "John", null),
        Row("Rakesh", "V", "Roshan", "Sharma", null),
        Row("David ", "A", "Sam", "William", null),
        Row("Akash", "M", "Singh", "Raj", "Akash M Singh Raj")
      )
      val schema = StructType(
        List(
          StructField("firstName", StringType),
          StructField("Column", StringType),
          StructField("middleName", StringType),
          StructField("lastName", StringType),
          StructField("fullName", StringType, nullable = true)
        )
      )
      val sourceColumns = Array(
        SourceColumn("sample", "firstName"),
        SourceColumn("sample", "Column"),
        SourceColumn("sample", "middleName"),
        SourceColumn("sample", "lastName")
      )
      val sampleDF = spark
        .createDataFrame(spark.sparkContext.parallelize(sampleList), schema)
        .alias("sample")
      val calculatedDF = ConcatenateColumns
        .process(sampleDF, sourceColumns, "fullName")(spark, extraParameters)
      val collectCalculatedDF = calculatedDF.collect()
      val ExpectedDF = List(
        Row("Allen", "R", "Rayan", "John", "Allen R Rayan John"),
        Row("Rakesh", "V", "Roshan", "Sharma", "Rakesh V Roshan Sharma"),
        Row("David ", "A", "Sam", "William", "David  A Sam William"),
        Row("Akash", "M", "Singh", "Raj", "Akash M Singh Raj")
      )
      assertResult(ExpectedDF)(collectCalculatedDF)
  }

}
