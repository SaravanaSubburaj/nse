package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class StandardizeDatesTest extends FlatSpec with SparkTest {
  "StandardizeDatesTest" should "test StandardizeDates implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sample_rows = List(
      Row("1", "Person_P", "07-06-1995 08:15:00.000"),
      Row("2", "Person_C", "14/02/1996 08:35:00.000"),
      Row("3", "Person_A", "        2015-12-06"),
      Row("4", "Person_M", " 1994/02/11"),
      Row("5", "Person_X", "")
    )

    val schema = StructType(
      List(
        StructField("ID", StringType, nullable = false),
        StructField("Name", StringType, nullable = false),
        StructField("DOB", StringType, nullable = false)
      )
    )
    val sourceColumns = Array(SourceColumn("sample", "DOB"))
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      schema
    )
    val validatedDF = StandardizeDatesCol
      .process(sampleDF, sourceColumns, "DOB")(spark, extraParameters)
    val collectedValidatedDF = validatedDF.collect()

    val expectedDF = List(
      Row("1", "Person_P", "1995-06-07 08:15:00.000"),
      Row("2", "Person_C", "1996-02-14 08:35:00.000"),
      Row("3", "Person_A", "2015-12-06 00:00:00.000"),
      Row("4", "Person_M", "1994-02-11 00:00:00.000"),
      Row("5", "Person_X", "")
    )

    assertResult(expectedDF)(collectedValidatedDF)

  }

}
