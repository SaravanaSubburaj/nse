package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetArpuCatogeryCodeTest extends FlatSpec with SparkTest {
  "join test" should "Test join operation 1" in withSparkSession { spark: SparkSession =>
    val sampleList = List(Row("1", "2019-01-16"), Row("2", "2019-01-16"))
    val schema = StructType(
      List(
        StructField("subsSubscriberID", StringType, nullable = false),
        StructField("date", StringType, nullable = false)
      )
    )
    val profileList = List(Row("1"), Row("2"))
    val profileListSchema =
      StructType(List(StructField("subsSubscriberID", StringType)))
    val sourceColumns = Array(
      SourceColumn(
        "monthlyPostpaidSubscriberRevenueSummary",
        "grossServiceRevenueIndicative"
      ),
      SourceColumn("monthlyPostpaidSubscriberRevenueSummary", "subscriberID")
    )
    val targetColumn = "arpuCategoryCodePostpaid"
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleList),
      schema
    )
    val sampleListSource = List(
      Row("306545", "2", "2019-01"),
      Row("7158", "1", "2019-01"),
      Row("7158", "1", "2018-12")
    )
    val schemaSourceDF = StructType(
      List(
        StructField("grossServiceRevenueIndicative", StringType),
        StructField("subscriberid", StringType),
        StructField("date", StringType)
      )
    )

    val joinColumns =
      Option(Array(JoinColumn("subscriberid", "subsSubscriberID")))

    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleDFSource = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleListSource),
      schemaSourceDF
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val sourceDF = Array(
      ("subscriber", sampleDF),
      ("monthlyPostpaidSubscriberRevenueSummary", sampleDFSource)
    ).toMap
    val df = GetArpuCategoryCode
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF =
      df.select("subsSubscriberID", "arpuCategoryCodePostpaid").collect()
    val expectedList = List(Row("1", "B"), Row("2", "A"))
    assertResult(expectedList)(collectedDF)
  }

  "join test" should "Test join operation 2" in withSparkSession { spark: SparkSession =>
    val sampleList = List(Row("1", "2019-01-16"), Row("2", "2019-01-16"))
    val schema = StructType(
      List(
        StructField("subsSubscriberID", StringType, nullable = false),
        StructField("date", StringType, nullable = false)
      )
    )
    val profileList = List(Row("1"), Row("2"))
    val profileListSchema =
      StructType(List(StructField("subsSubscriberID", StringType)))
    val sourceColumns = Array(
      SourceColumn(
        "monthlyPostpaidSubscriberRevenueSummary",
        "grossServiceRevenueIndicative"
      ),
      SourceColumn("monthlyPostpaidSubscriberRevenueSummary", "subscriberID")
    )
    val targetColumn = "arpuCategoryCodePostpaid"
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleList),
      schema
    )
    val sampleListSource =
      List(Row("306545", "2", "2018-12"), Row("7158", "1", "2018-12"))
    val schemaSourceDF = StructType(
      List(
        StructField("grossServiceRevenueIndicative", StringType),
        StructField("subscriberid", StringType),
        StructField("date", StringType)
      )
    )

    val joinColumns =
      Option(Array(JoinColumn("subscriberid", "subsSubscriberID")))

    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleDFSource = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleListSource),
      schemaSourceDF
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val sourceDF = Array(
      ("subscriber", sampleDF),
      ("monthlyPostpaidSubscriberRevenueSummary", sampleDFSource)
    ).toMap
    val df = GetArpuCategoryCode
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF =
      df.select("subsSubscriberID", "arpuCategoryCodePostpaid").collect()
    val expectedList = List(Row("1", "B"), Row("2", "A"))
    assertResult(expectedList)(collectedDF)
  }
}
