package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

class StandardizeGenderTest extends FlatSpec with SparkTest {

  "StandardizedGenderTest" should "test StandardizedGender Implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleList = List(
      Row("ID1", "Male"),
      Row("ID2", "Female"),
      Row("ID3", "other"),
      Row("ID4", "MALE"),
      Row("ID5", "abc")
    )
    val schema = StructType(
      List(
        StructField("RowID", StringType, nullable = false),
        StructField("Gender", StringType, nullable = false)
      )
    )
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleList),
      schema
    )

    val loopupDetail = LookupDetail(
      "StandardGender",
      "json",
      "local",
      "transformer/src/test/resources/Gender.json"
    )
    val sourceColumns = Array(SourceColumn("sample", "Gender"))
    val transformedDF = StandardizeGender
      .process(sampleDF, sourceColumns, "Gender")(
        spark,
        extraParameters,
        loopupDetail
      )
    val collectedTransformedDF = transformedDF.collect()
    val expectedCollectedValidatedDF = List(
      Row("ID1", "M"),
      Row("ID2", "F"),
      Row("ID3", "O"),
      Row("ID4", "M"),
      Row("ID5", "")
    )
    assertResult(expectedCollectedValidatedDF)(collectedTransformedDF)

  }

}
