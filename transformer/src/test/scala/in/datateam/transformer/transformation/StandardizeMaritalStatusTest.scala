package in.datateam.transformer.transformation

import java.io.File

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

class StandardizeMaritalStatusTest extends FlatSpec with SparkTest {
  "StandardizeMaritalStatusTest" should "test StandardizeMaritalStatus implementation" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val records = List(
        Row("Yamilet Andersen", "married"),
        Row("Parker James", "single"),
        Row("Nathanial Moreno", "separated"),
        Row("Jared Kane", "mq")
      )

      val schema = StructType(
        List(
          StructField("name", StringType, nullable = false),
          StructField("marital_status", StringType, nullable = false)
        )
      )

      val testDF =
        spark.createDataFrame(spark.sparkContext.parallelize(records), schema)
      val maritalStatusMapFilePath = new File(
        "transformer/src/test/resources/marital_status.json"
      ).getAbsolutePath
      val broadcastDetails = Seq(
        LookupDetail(
          "StandardMaritalStatus",
          "json",
          "local",
          maritalStatusMapFilePath
        )
      )
      val sourceColumns = Array(SourceColumn("sample", "marital_status"))
      val transformedDF =
        StandardizeMaritalStatus.process(
          testDF,
          sourceColumns,
          "marital_status"
        )(spark, extraParameters, broadcastDetails: _*)

      val collectedTransformedDF = transformedDF.collect()

      val expectedCollectedValidatedDF = List(
        Row("Yamilet Andersen", "Married"),
        Row("Parker James", "Single"),
        Row("Nathanial Moreno", "Separated"),
        Row("Jared Kane", "")
      )

      assertResult(expectedCollectedValidatedDF)(collectedTransformedDF)

  }
}
