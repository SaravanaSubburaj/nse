package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetLatestPromoRegistrationNameTest extends FlatSpec with SparkTest {
  "GetLatestPromoRegistrationNameTest" should "Get Latest Promo registraion name" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters = Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(Row("1", ""))
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("GetLatestPromoRegistrationName", StringType)
        )
      )
      val subcriberEntityList = List(
        Row("1")
      )
      val subcriberEntityListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType)
        )
      )
      val globePrepaidPromoAvailmentStaging = List(
        Row("1", "2019-01-07", "112", "1", "1092", "1234", "10", "2019-01-16"),
        Row("1", "2019-01-07", "112", "6", "1001", "2374", "15", "2019-01-16")
      )
      val globePrepaidPromoAvailmentStagingSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("promoRegistrationDatetime", StringType),
          StructField("prepaidPromoId", StringType),
          StructField("promoOperationId", StringType),
          StructField("availmentChannelCode", StringType),
          StructField("serviceId", StringType),
          StructField("promoDenominationValue", StringType),
          StructField("date", StringType)
        )
      )
      val PrepaidPromo = List(
        Row("112", "roamsurf40GB", "2019-01-16")
      )
      val PrepaidPromoSchema = StructType(
        List(
          StructField("prepaidPromoId", StringType),
          StructField("prepaidPromoDescription", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileListSchema)
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val globePrepaidPromoAvailmentStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globePrepaidPromoAvailmentStaging),
        globePrepaidPromoAvailmentStagingSchema
      )
      val PrepaidPromoDF = spark.createDataFrame(spark.sparkContext.parallelize(PrepaidPromo), PrepaidPromoSchema)
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("globePrepaidPromoAvailmentStaging", globePrepaidPromoAvailmentStagingDF),
        ("PrepaidPromo", PrepaidPromoDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("globePrepaidPromoAvailmentStaging", "promoRegistrationDatetime"),
        SourceColumn("PrepaidPromo", "prepaidPromoDescription"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "subscriberID"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "prepaidPromoId"),
        SourceColumn("PrepaidPromo", "prepaidPromoId"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "promoOperationId"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "availmentChannelCode"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "serviceId"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "promoDenominationValue")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subsSubscriberID"))
      )
      val targetColumn = "GetLatestPromoRegistrationName"
      val df = GetLatestPromoRegistrationName
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)
      val collectedDF = df.collect().toList
      val expectList = List(
        Row("1", "roamsurf40GB")
      )
      assertResult(expectList)(collectedDF)
  }
}
