package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDeriveWithOptOutIndicatorTest extends FlatSpec with SparkTest {

  "JoinDeriveWithOptOutIndicatorTest" should "join and derive from opt_out Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val contactEntityList = List(
        Row("9050182781", "2019-01-16"),
        Row("9050210019", "2019-01-16"),
        Row(null, "2019-01-16")
      )
      val contactEntityListSchema = StructType(
        List(
          StructField("mobtel", StringType),
          StructField("date", StringType)
        )
      )

      val profileEntityList = List(
        Row("9050182781", ""),
        Row("9050210019", ""),
        Row("9050210028", "")
      )
      val profileEntityListSchema = StructType(
        List(
          StructField("msisdn_value", StringType),
          StructField("opt_out_indicator", StringType)
        )
      )

      val contactDF = spark.createDataFrame(
        spark.sparkContext.parallelize(contactEntityList),
        contactEntityListSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileEntityList),
        profileEntityListSchema
      )

      val sourceDFs = Array(("opt_out", contactDF)).toMap

      val sourceColumns = Array(
        SourceColumn("opt_out", "mobtel")
      )

      val joinColumns = Option(Array(JoinColumn("mobtel", "msisdn_value")))

      val targetColumn = "opt_out_indicator"

      val joinedDF = JoinDeriveWithOptOutIndicator
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actualList = joinedDF.sort("msisdn_value").collect().toList

      val expectedList = List(
        Row("9050182781", "1"),
        Row("9050210019", "1"),
        Row("9050210028", "0")
      )

      assertResult(expectedList)(actualList)

  }

}
