package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetGrossMonthlyIncomeAmountTest extends FlatSpec with SparkTest {
  "GetHandsetTypeNameTest" should "Get the monthly income amount" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(
      Row("1", "11221", ""),
      Row("2", "11222", ""),
      Row("3", "11223", ""),
      Row("4", "11224", ""),
      Row("5", "11225", "")
    )
    val profileListSchema = StructType(
      List(
        StructField("subsSubscriberID", StringType),
        StructField("msisdn_value", StringType),
        StructField("postpaid_gross_monthly_income_amount", StringType)
      )
    )
    val subcriberEntityList = List(
      Row("1", "11221", null, "2019-01-16"),
      Row("2", "11222", "40", "2019-01-16"),
      Row("3", "11223", "50", "2019-01-16"),
      Row("4", "11224", "55", "2019-01-16"),
      Row("5", "11225", "22", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subsSubscriberID", StringType),
        StructField("msisdn_value", StringType),
        StructField("grossMonthlyIncomeAmount", StringType, nullable = true),
        StructField("date", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val sourceDF = Array(("Subscriber", subsDF)).toMap
    val sourceColumns = Array(
      SourceColumn("Subscriber", "subsSubscriberID"),
      SourceColumn("Subscriber", "grossMonthlyIncomeAmount")
    )
    val joinColumns =
      Option(Array(JoinColumn("subsSubscriberID", "subsSubscriberID")))
    val targetColumn = "postpaid_gross_monthly_income_amount"
    val df = GetGrossMonthlyIncomeAmount
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF = df.sort("subsSubscriberID").collect().toList
    val expectList = List(
      Row("1", "11221", "0.00"),
      Row("2", "11222", "40"),
      Row("3", "11223", "50"),
      Row("4", "11224", "55"),
      Row("5", "11225", "22")
    )
    assertResult(expectList)(collectedDF)
  }
}
