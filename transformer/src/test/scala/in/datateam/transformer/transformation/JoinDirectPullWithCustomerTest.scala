package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullWithCustomerTest extends FlatSpec with SparkTest {
  "JoinDirectPullWithCustomerTest" should "join and direct pull from customer Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val customerEntityList = List(
        Row("1", "tabs@gmail.com", "2019-01-16"),
        Row("2", "donn@gmail.com", "2019-01-16"),
        Row("3", "dean@gmail.com", "2019-01-16")
      )
      val customerEntityListSchema = StructType(
        List(
          StructField("customerId", StringType),
          StructField("CustomerEmailAddressText", StringType),
          StructField("date", StringType)
        )
      )

      val profileEntityList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", "")
      )
      val profileEntityListSchema = StructType(
        List(
          StructField("customer_id", StringType),
          StructField("customer_email_address_text", StringType)
        )
      )

      val customerDF = spark.createDataFrame(
        spark.sparkContext.parallelize(customerEntityList),
        customerEntityListSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileEntityList),
        profileEntityListSchema
      )

      val sourceDFs = Array(("customer", customerDF)).toMap

      val sourceColumns = Array(
        SourceColumn("customer", "CustomerEmailAddressText")
      )

      val joinColumns = Option(Array(JoinColumn("customerId", "customer_id")))

      val targetColumn = "customer_email_address_text"

      val joinedDF = JoinDirectPullWithCustomer
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actualList = joinedDF.sort("customer_id").collect().toList

      val expectedList = List(
        Row("1", "tabs@gmail.com"),
        Row("2", "donn@gmail.com"),
        Row("3", "dean@gmail.com")
      )

      assertResult(expectedList)(actualList)

  }

}
