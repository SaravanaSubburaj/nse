package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetOwnsCreditCardIndicatorTest extends FlatSpec with SparkTest {
  "GetOwnsCreditCardIndicatorTest" should "get Owns credit card indicator" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("customer_id", StringType),
          StructField("owns_credit_card_indicator", StringType)
        )
      )

      val subcriberEntityList = List(Row("1"), Row("2"))
      val subcriberEntityListSchema =
        StructType(List(StructField("customerId", StringType)))
      val paymentStaging = List(
        Row("112", "2018-12-18", "2019-01"),
        Row("113", "2018-12-16", "2019-01"),
        Row("114", "2018-12-15", "2019-01"),
        Row("115", "2018-12-14", "2019-01"),
        Row("116", "2018-12-11", "2019-01")
      )
      val paymentStagingSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("paymentDate", StringType),
          StructField("date", StringType)
        )
      )

      val FinancialAccount = List(
        Row("112", "1", "O", "2019-01-01", "2019-01-16"),
        Row("112", "1", "SUS", "2019-01-01", "2019-01-15"),
        Row("113", "2", "SUS", "2019-01-02", "2019-01-16"),
        Row("114", "3", "SUS", "2019-01-02", "2019-01-16"),
        Row("115", "4", "O", "2019-01-03", "2019-01-16"),
        Row("116", "5", "O", "2019-01-04", "2019-01-16")
      )
      val FinancialAccountSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("customerId", StringType),
          StructField("financialAccountStatusCode", StringType),
          StructField("financialAccountActivationDate", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val paymentDf = spark.createDataFrame(
        spark.sparkContext.parallelize(paymentStaging),
        paymentStagingSchema
      )
      val financialAccountDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccount),
        FinancialAccountSchema
      )
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("financialAccount", financialAccountDF),
        ("paymentStaging", paymentDf)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("paymentStaging", "paymentDate"),
        SourceColumn("paymentStaging", "financialAccountId"),
        SourceColumn("financialAccount", "financialAccountId"),
        SourceColumn("financialAccount", "customerId"),
        SourceColumn("financialAccount", "financialAccountStatusCode"),
        SourceColumn("financialAccount", "financialAccountActivationDate")
      )
      val joinColumns = Option(Array(JoinColumn("customerId", "customer_id")))
      val targetColumn = "owns_credit_card_indicator"
      val df = GetPaymentStagingDirectJoin
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("customer_id").collect().toList
      val expectList = List(
        Row("1", "2018-12-18"),
        Row("2", "2018-12-16"),
        Row("3", "2018-12-15"),
        Row("4", "2018-12-14"),
        Row("5", "2018-12-11")
      )
      assertResult(expectList)(calulatedDF)
  }
}
