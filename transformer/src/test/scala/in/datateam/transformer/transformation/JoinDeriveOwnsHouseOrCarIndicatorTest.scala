package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDeriveOwnsHouseOrCarIndicatorTest extends FlatSpec with SparkTest {

  "GetOwnsHouseIndicatorTest" should "join and derive from adh_contact Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val adhContactEntityList = List(
        Row("1", "mortgaged", "2019-01-16"),
        Row("2", "owned", "2019-01-16"),
        Row("3", "sample", "2019-01-16")
      )
      val contactEntityListSchema = StructType(
        List(
          StructField("contactID", StringType),
          StructField("residencyType", StringType),
          StructField("date", StringType)
        )
      )

      val profileEntityList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", "")
      )
      val profileEntityListSchema = StructType(
        List(
          StructField("main_contact_id", StringType),
          StructField("owns_house_indicator", StringType)
        )
      )

      val contactDF = spark.createDataFrame(
        spark.sparkContext.parallelize(adhContactEntityList),
        contactEntityListSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileEntityList),
        profileEntityListSchema
      )

      val sourceDFs = Array(("adh_contact", contactDF)).toMap

      val sourceColumns = Array(
        SourceColumn("adh_contact", "residencyType")
      )

      val joinColumns = Option(Array(JoinColumn("contactId", "main_contact_id")))

      val targetColumn = "owns_house_indicator"

      val joinedDF = JoinDeriveOwnsHouseOrCarIndicator
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actualList = joinedDF.sort("main_contact_id").collect().toList

      val expectedList = List(
        Row("1", "1"),
        Row("2", "1"),
        Row("3", "0")
      )

      assertResult(expectedList)(actualList)

  }

  "GetOwnsCarIndicatorTest" should "join and derive from adh_contact Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val contactEntityList = List(
        Row("1", "mortgaged", "2019-01-16"),
        Row("2", "owned", "2019-01-16"),
        Row("3", "sample", "2019-01-16")
      )
      val contactEntityListSchema = StructType(
        List(
          StructField("contactID", StringType),
          StructField("contactCarOwnershipTypeValue", StringType),
          StructField("date", StringType)
        )
      )

      val profileEntityList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", "")
      )
      val profileEntityListSchema = StructType(
        List(
          StructField("main_contact_id", StringType),
          StructField("owns_car_indicator", StringType)
        )
      )

      val contactDF = spark.createDataFrame(
        spark.sparkContext.parallelize(contactEntityList),
        contactEntityListSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileEntityList),
        profileEntityListSchema
      )

      val sourceDFs = Array(("contact", contactDF)).toMap

      val sourceColumns = Array(
        SourceColumn("contact", "contactCarOwnershipTypeValue")
      )

      val joinColumns = Option(Array(JoinColumn("contactID", "main_contact_id")))

      val targetColumn = "owns_car_indicator"

      val joinedDF = JoinDeriveOwnsHouseOrCarIndicator
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actualList = joinedDF.sort("main_contact_id").collect().toList

      val expectedList = List(
        Row("1", "1"),
        Row("2", "1"),
        Row("3", "0")
      )

      assertResult(expectedList)(actualList)

  }

}
