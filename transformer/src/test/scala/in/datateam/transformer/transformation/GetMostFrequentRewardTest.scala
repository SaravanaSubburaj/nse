package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMostFrequentRewardTest extends FlatSpec with SparkTest {
  "GetMostFrequentReward" should "Get Latest Frequent Reward" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val partitionDate = "2019-03-04"
    val rewardList = List(Row("telco", "1", "3258985874", partitionDate))
    val rewardSchema = StructType(
      List(
        StructField("rewardtype", StringType),
        StructField("flags", StringType),
        StructField("msisdn", StringType),
        StructField("date", StringType)
      )
    )

    val profileList = List(Row("1", "3258985874", null, partitionDate))
    val profileSchema = StructType(
      List(
        StructField("subscriberid", StringType),
        StructField("msisdn_value", StringType),
        StructField("mostfrequent", StringType),
        StructField("date", StringType)
      )
    )
    val rewardDF = spark.createDataFrame(
      spark.sparkContext.parallelize(rewardList),
      rewardSchema
    )
    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileSchema
    )

    val sourceDF = Array(("MillipedeRewardTransactions", rewardDF)).toMap
    val sourceColumns = Array(
      SourceColumn("MillipedeRewardTransactions", "rewardtype"),
      SourceColumn("MillipedeRewardTransactions", "flags"),
      SourceColumn("MillipedeRewardTransactions", "msisdn")
    )
    val joinColumns = Option(Array(JoinColumn("msisdn", "msisdn_value")))
    val targetColumn = "mostfrequent"
    val df = GetMostFrequentReward
      .process(sourceDF, targetDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collecteddf = df.collect().toList
    val expectList = List(Row("1", "3258985874", "telco", "2019-03-04"))
    assertResult(expectList)(collecteddf)
  }
}
