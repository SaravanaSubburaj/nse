package in.datateam.transformer.transformation

import java.sql.Timestamp
import java.time.Instant

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types._

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class ReloadSummaryAttributesTest extends FlatSpec with SparkTest {

  def getTimestampFromEpoch(epochSecond: Long): Timestamp = {
    //small func to get timestamp
    Timestamp.from(Instant.ofEpochSecond(epochSecond))
  }

  "GetLatestReloadDateTest" should "test GetLatestReloadDate implementation" in withSparkSession {

    spark: SparkSession =>
      val partitionDate: String = "2019-01-16"
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> partitionDate)

      val ts1: Timestamp = getTimestampFromEpoch(1547444451) //Jan. 14, 2019
      val ts1Delayed: Timestamp = getTimestampFromEpoch(1547455251) //Jan. 14, 2019, but delayed
      val ts2: Timestamp = getTimestampFromEpoch(1544745600) //Dec. 14, 2018
      val ts3: Timestamp = getTimestampFromEpoch(1546850451) //Jan. 7, 2019
      val ts4: Timestamp = getTimestampFromEpoch(1544517651) //Dec. 11, 2018
      val ts5: Timestamp = getTimestampFromEpoch(1539247251) //Oct. 11, 2018, should be filtered by condition

      val sample_rows = List(
        Row("1", "Person_P", ts1, 10.0, partitionDate),
        Row("1", "Person_P", ts3, 30.0, partitionDate),
        Row("2", "Person_C", ts2, 20.0, partitionDate),
        Row("2", "Person_C", ts4, 60.0, partitionDate),
        Row("3", "Person_M", ts2, 40.0, partitionDate),
        Row("3", "Person_M", ts1, 70.0, partitionDate),
        Row("3", "Person_M", ts1Delayed, 20.0, partitionDate),
        Row("4", "Person_N", ts5, 100.0, partitionDate), //outside range, should be filtered
        Row("5", "Person_O", ts3, 0.0, partitionDate) //has zero recharge amount, should be filtered
      )

      val profile_rows = List(
        Row("1", "Person_P", "", partitionDate),
        // Date col represents partitionDate and will be used for filter conditions
        Row("2", "Person_C", "", partitionDate),
        Row("3", "Person_M", "", partitionDate),
        Row("4", "Person_N", "", partitionDate),
        Row("5", "Person_O", "", partitionDate)
      )

      val sampleSchema = StructType(
        List(
          StructField("RowID", StringType),
          StructField("Name", StringType),
          StructField("RechargeTS", TimestampType),
          StructField("RechargeAmt", DoubleType),
          StructField("date", StringType)
        )
      )

      val profileSchema = StructType(
        List(
          StructField("profRowID", StringType),
          StructField("profName", StringType),
          StructField("LatestRecharge", StringType),
          StructField("date", StringType)
        )
      )

      //val encoder = RowEncoder(schema)
      val sampleDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sample_rows),
        sampleSchema
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profile_rows),
        profileSchema
      )

      var transformedDF = GetLatestReloadDate.process(
        Map[String, DataFrame]("sampleDF" -> sampleDF),
        profileDF,
        Array(
          SourceColumn("sampleDF", "RowID"),
          SourceColumn("sampleDF", "RechargeTS"),
          SourceColumn("sampleDF", "RechargeAmt")
        ),
        Option(Array(JoinColumn("RowID", "profRowID"))),
        "LatestRecharge"
      )(spark, extraParameters)

      transformedDF = transformedDF.sort(col("profRowID"))
      val transformedCollectedDF = transformedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .collect()

      val expectedDF = List(
        Row("1", "Person_P", ts1, partitionDate),
        Row("2", "Person_C", ts2, partitionDate),
        Row("3", "Person_M", ts1Delayed, partitionDate),
        Row("4", "Person_N", null, partitionDate),
        Row("5", "Person_O", null, partitionDate)
      )
      assertResult(expectedDF)(transformedCollectedDF)
  }

  "AverageMonthlyTopupRolling90DaysAmountTest" should
  "test AverageMonthlyTopupRolling90DaysAmount implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val ts1: Timestamp = getTimestampFromEpoch(1547444451) //Jan. 14, 2019
    val ts1Delayed: Timestamp = getTimestampFromEpoch(1547455251) //Jan. 14, 2019, but delayed
    val ts2: Timestamp = getTimestampFromEpoch(1544745600) //Dec. 14, 2018
    val ts3: Timestamp = getTimestampFromEpoch(1546850451) //Jan. 7, 2019
    val ts4: Timestamp = getTimestampFromEpoch(1544517651) //Dec. 11, 2018
    val ts5: Timestamp = getTimestampFromEpoch(1539247251) //Oct. 11, 2018, should be filtered by condition
    val partitionDate: String = "2019-01-16"

    val sample_rows = List(
      Row("1", "Person_P", ts1, 10.0, partitionDate),
      Row("1", "Person_P", ts3, 30.0, partitionDate),
      Row("2", "Person_C", ts2, 20.0, partitionDate),
      Row("2", "Person_C", ts4, 60.0, partitionDate),
      Row("3", "Person_M", ts2, 40.0, partitionDate),
      Row("3", "Person_M", ts1, 70.0, partitionDate),
      Row("3", "Person_M", ts1Delayed, 20.0, partitionDate),
      Row("4", "Person_N", ts5, 100.0, partitionDate), //outside range, should be filtered
      Row("5", "Person_O", ts3, 0.0, partitionDate) //has zero recharge amount, should be filtered
    )

    val profile_rows = List(
      Row("1", "Person_P", "", partitionDate),
      // Date col represents partitionDate and will be used for filter conditions
      Row("2", "Person_C", "", partitionDate),
      Row("3", "Person_M", "", partitionDate),
      Row("4", "Person_N", "", partitionDate),
      Row("5", "Person_O", "", partitionDate)
    )

    val sampleSchema = StructType(
      List(
        StructField("RowID", StringType),
        StructField("Name", StringType),
        StructField("RechargeTS", TimestampType),
        StructField("RechargeAmt", DoubleType),
        StructField("date", StringType)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("profRowID", StringType),
        StructField("profName", StringType),
        StructField("MRT90Amount", StringType),
        StructField("date", StringType)
      )
    )

    //val encoder = RowEncoder(schema)
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      sampleSchema
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profile_rows),
      profileSchema
    )

    var transformedDF = AverageMonthlyTopupRolling90DaysAmount.process(
      Map[String, DataFrame]("sampleDF" -> sampleDF),
      profileDF,
      Array(
        SourceColumn("sampleDF", "RowID"),
        SourceColumn("sampleDF", "RechargeTS"),
        SourceColumn("sampleDF", "RechargeAmt")
      ),
      Option(Array(JoinColumn("RowID", "profRowID"))),
      "MRT90Amount"
    )(spark, extraParameters)

    transformedDF = transformedDF.sort(col("profRowID"))
    val transformedCollectedDF = transformedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .collect()

    val expectedDF = List(
      Row("1", "Person_P", 13.33, partitionDate),
      Row("2", "Person_C", 26.67, partitionDate),
      Row("3", "Person_M", 43.33, partitionDate),
      Row("4", "Person_N", null, partitionDate),
      Row("5", "Person_O", null, partitionDate)
    )

    assertResult(expectedDF)(transformedCollectedDF)
  }

  "AverageTopupAmount3MonthsTest" should "test AverageTopupAmount3Months implementation" in withSparkSession {

    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val ts1: Timestamp = getTimestampFromEpoch(1547444451) //Jan. 14, 2019
      val ts1Delayed: Timestamp = getTimestampFromEpoch(1547455251) //Jan. 14, 2019, but delayed
      val ts2: Timestamp = getTimestampFromEpoch(1544745600) //Dec. 14, 2018
      val ts3: Timestamp = getTimestampFromEpoch(1546850451) //Jan. 7, 2019
      val ts4: Timestamp = getTimestampFromEpoch(1544517651) //Dec. 11, 2018
      val ts5: Timestamp = getTimestampFromEpoch(1539247251) //Oct. 11, 2018, should be filtered by condition

      val partitionDate: String = "2019-01-16"

      val sample_rows = List(
        Row("1", "Person_P", ts1, 10.0, partitionDate),
        Row("1", "Person_P", ts3, 30.0, partitionDate),
        Row("1", "Person_P", ts2, 50.0, partitionDate),
        Row("2", "Person_C", ts2, 20.0, partitionDate),
        Row("2", "Person_C", ts4, 60.0, partitionDate),
        Row("3", "Person_M", ts2, 40.0, partitionDate),
        Row("3", "Person_M", ts1, 70.0, partitionDate),
        Row("3", "Person_M", ts1Delayed, 20.0, partitionDate),
        Row("4", "Person_N", ts5, 100.0, partitionDate), //outside range, should be filtered
        Row("5", "Person_O", ts3, 0.0, partitionDate) //has zero recharge amount, should be filtered
      )

      val profile_rows = List(
        Row("1", "Person_P", "", partitionDate),
        // Date col represents partitionDate and will be used for filter conditions
        Row("2", "Person_C", "", partitionDate),
        Row("3", "Person_M", "", partitionDate),
        Row("4", "Person_N", "", partitionDate),
        Row("5", "Person_O", "", partitionDate)
      )

      val sampleSchema = StructType(
        List(
          StructField("RowID", StringType),
          StructField("Name", StringType),
          StructField("RechargeTS", TimestampType),
          StructField("RechargeAmt", DoubleType),
          StructField("date", StringType)
        )
      )

      val profileSchema = StructType(
        List(
          StructField("profRowID", StringType),
          StructField("profName", StringType),
          StructField("AvgTopupAmount", StringType),
          StructField("date", StringType)
        )
      )

      //val encoder = RowEncoder(schema)
      val sampleDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sample_rows),
        sampleSchema
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profile_rows),
        profileSchema
      )

      var transformedDF = AverageTopupAmount3Months.process(
        Map[String, DataFrame]("sampleDF" -> sampleDF),
        profileDF,
        Array(
          SourceColumn("sampleDF", "RowID"),
          SourceColumn("sampleDF", "RechargeTS"),
          SourceColumn("sampleDF", "RechargeAmt")
        ),
        Option(Array(JoinColumn("RowID", "profRowID"))),
        "AvgTopupAmount"
      )(spark, extraParameters)

      transformedDF = transformedDF.sort(col("profRowID"))
      val transformedCollectedDF = transformedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .collect()

      val expectedDF = List(
        Row("1", "Person_P", 30.00, partitionDate),
        Row("2", "Person_C", 40.00, partitionDate),
        Row("3", "Person_M", 65.00, partitionDate),
        Row("4", "Person_N", null, partitionDate),
        Row("5", "Person_O", null, partitionDate)
      )

      assertResult(expectedDF)(transformedCollectedDF)
  }

  "TotalDailyTopupRolling30DaysAmountTest" should
  "test TotalDailyTopupRolling30DaysAmount implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val ts1: Timestamp = getTimestampFromEpoch(1547444451) //Jan. 14, 2019
    val ts1Delayed: Timestamp = getTimestampFromEpoch(1547455251) //Jan. 14, 2019, but delayed
    val ts2: Timestamp = getTimestampFromEpoch(1544745600) //Dec. 14, 2018
    val ts3: Timestamp = getTimestampFromEpoch(1546850451) //Jan. 7, 2019
    val ts4: Timestamp = getTimestampFromEpoch(1544517651) //Dec. 11, 2018
    val ts5: Timestamp = getTimestampFromEpoch(1539247251) //Oct. 11, 2018, should be filtered by condition

    val partitionDate: String = "2019-01-16"

    val sample_rows = List(
      Row("1", "Person_P", ts1, 10.0, partitionDate),
      Row("1", "Person_P", ts3, 30.0, partitionDate),
      Row("2", "Person_C", ts2, 20.0, partitionDate),
      Row("2", "Person_C", ts4, 60.0, partitionDate),
      Row("3", "Person_M", ts2, 40.0, partitionDate),
      Row("3", "Person_M", ts1, 70.0, partitionDate),
      Row("3", "Person_M", ts1Delayed, 20.0, partitionDate),
      Row("4", "Person_N", ts5, 100.0, partitionDate), //outside range, should be filtered
      Row("5", "Person_O", ts3, 0.0, partitionDate) //has zero recharge amount, should be filtered
    )

    val profile_rows = List(
      Row("1", "Person_P", "", partitionDate),
      // Date col represents partitionDate and will be used for filter conditions
      Row("2", "Person_C", "", partitionDate),
      Row("3", "Person_M", "", partitionDate),
      Row("4", "Person_N", "", partitionDate),
      Row("5", "Person_O", "", partitionDate)
    )

    val sampleSchema = StructType(
      List(
        StructField("RowID", StringType),
        StructField("Name", StringType),
        StructField("RechargeTS", TimestampType),
        StructField("RechargeAmt", DoubleType),
        StructField("date", StringType)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("profRowID", StringType),
        StructField("profName", StringType),
        StructField("TDTR30Amount", StringType),
        StructField("date", StringType)
      )
    )

    //val encoder = RowEncoder(schema)
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      sampleSchema
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profile_rows),
      profileSchema
    )

    var transformedDF = TotalDailyTopupRolling30DaysAmount.process(
      Map[String, DataFrame]("sampleDF" -> sampleDF),
      profileDF,
      Array(
        SourceColumn("sampleDF", "RowID"),
        SourceColumn("sampleDF", "RechargeTS"),
        SourceColumn("sampleDF", "RechargeAmt")
      ),
      Option(Array(JoinColumn("RowID", "profRowID"))),
      "TDTR30Amount"
    )(spark, extraParameters)

    transformedDF = transformedDF.sort(col("profRowID"))
    val transformedCollectedDF = transformedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .collect()

    val expectedDF = List(
      Row("1", "Person_P", 40, partitionDate),
      Row("2", "Person_C", null, partitionDate),
      Row("3", "Person_M", 90, partitionDate),
      Row("4", "Person_N", null, partitionDate),
      Row("5", "Person_O", null, partitionDate)
    )

    assertResult(expectedDF)(transformedCollectedDF)
  }

  "MaxDailyTopupRolling30DaysAmountTest" should
  "test MaxDailyTopupRolling30DaysAmount implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val ts1: Timestamp = getTimestampFromEpoch(1547444451) //Jan. 14, 2019
    val ts1Delayed: Timestamp = getTimestampFromEpoch(1547455251) //Jan. 14, 2019, but delayed
    val ts2: Timestamp = getTimestampFromEpoch(1544745600) //Dec. 14, 2018
    val ts3: Timestamp = getTimestampFromEpoch(1546850451) //Jan. 7, 2019
    val ts4: Timestamp = getTimestampFromEpoch(1544517651) //Dec. 11, 2018
    val ts5: Timestamp = getTimestampFromEpoch(1539247251) //Oct. 11, 2018, should be filtered by condition

    val ts6: Timestamp = getTimestampFromEpoch(1546473600) // Jan, 3, 2019

    val partitionDate: String = "2019-01-16"

    val sample_rows = List(
      Row("1", "Person_P", ts1, 10.0, partitionDate),
      Row("1", "Person_P", ts3, 30.0, partitionDate),
      Row("2", "Person_C", ts2, 20.0, partitionDate),
      Row("2", "Person_C", ts4, 60.0, partitionDate),
      Row("3", "Person_M", ts6, 40.0, partitionDate),
      Row("3", "Person_M", ts1, 70.0, partitionDate),
      Row("3", "Person_M", ts1Delayed, 20.0, partitionDate),
      Row("4", "Person_N", ts5, 100.0, partitionDate), //outside range, should be filtered
      Row("5", "Person_O", ts3, 0.0, partitionDate) //has zero recharge amount, should be filtered
    )

    val profile_rows = List(
      Row("1", "Person_P", "", partitionDate),
      // Date col represents partitionDate and will be used for filter conditions
      Row("2", "Person_C", "", partitionDate),
      Row("3", "Person_M", "", partitionDate),
      Row("4", "Person_N", "", partitionDate),
      Row("5", "Person_O", "", partitionDate)
    )

    val sampleSchema = StructType(
      List(
        StructField("RowID", StringType),
        StructField("Name", StringType),
        StructField("RechargeTS", TimestampType),
        StructField("RechargeAmt", DoubleType),
        StructField("date", StringType)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("profRowID", StringType),
        StructField("profName", StringType),
        StructField("MDTR30Amount", StringType),
        StructField("date", StringType)
      )
    )

    //val encoder = RowEncoder(schema)
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      sampleSchema
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profile_rows),
      profileSchema
    )

    var transformedDF = MaxDailyTopupRolling30DaysAmount.process(
      Map[String, DataFrame]("sampleDF" -> sampleDF),
      profileDF,
      Array(
        SourceColumn("sampleDF", "RowID"),
        SourceColumn("sampleDF", "RechargeTS"),
        SourceColumn("sampleDF", "RechargeAmt")
      ),
      Option(Array(JoinColumn("RowID", "profRowID"))),
      "MDTR30Amount"
    )(spark, extraParameters)

    transformedDF = transformedDF.sort(col("profRowID"))
    val transformedCollectedDF = transformedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .collect()

    val expectedDF = List(
      Row("1", "Person_P", 30, partitionDate),
      Row("2", "Person_C", null, partitionDate),
      Row("3", "Person_M", 90, partitionDate),
      Row("4", "Person_N", null, partitionDate),
      Row("5", "Person_O", null, partitionDate)
    )

    assertResult(expectedDF)(transformedCollectedDF)
  }

  "MostAvailedTopupRolling30DaysAmountTest" should
  "test MostAvailedTopupRolling30DaysAmount implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

    val ts1: Timestamp = getTimestampFromEpoch(1547444451) //Jan. 14, 2019
    val ts1Delayed: Timestamp = getTimestampFromEpoch(1547455251) //Jan. 14, 2019, but delayed
    val ts2: Timestamp = getTimestampFromEpoch(1544745600) //Dec. 14, 2018
    val ts3: Timestamp = getTimestampFromEpoch(1546850451) //Jan. 7, 2019
    val ts4: Timestamp = getTimestampFromEpoch(1544517651) //Dec. 11, 2018
    val ts5: Timestamp = getTimestampFromEpoch(1539247251) //Oct. 11, 2018, should be filtered by condition

    val ts6: Timestamp = getTimestampFromEpoch(1546473600) // Jan, 3, 2019

    val partitionDate: String = "2019-01-16"

    val sample_rows = List(
      Row("1", "Person_P", ts1, 50.0, 1, 50.0, partitionDate),
      Row("1", "Person_P", ts3, 50.0, 2, 25.0, partitionDate),
      Row("2", "Person_C", ts2, 25.0, 1, 25.0, partitionDate),
      Row("2", "Person_C", ts4, 200.0, 4, 50.0, partitionDate),
      Row("3", "Person_M", ts6, 150.0, 3, 50.0, partitionDate),
      Row("3", "Person_M", ts1, 50.0, 2, 25.0, partitionDate),
      Row("3", "Person_M", ts1Delayed, 200.0, 4, 80.0, partitionDate),
      Row("4", "Person_N", ts5, 25.0, 1, 25.0, partitionDate), //outside range, should be filtered
      Row("5", "Person_O", ts3, 0.0, 0, 0.0, partitionDate) //has zero recharge amount, should be filtered
    )

    val profile_rows = List(
      Row("1", "Person_P", "", partitionDate),
      // Date col represents partitionDate and will be used for filter conditions
      Row("2", "Person_C", "", partitionDate),
      Row("3", "Person_M", "", partitionDate),
      Row("4", "Person_N", "", partitionDate),
      Row("5", "Person_O", "", partitionDate)
    )

    val sampleSchema = StructType(
      List(
        StructField("RowID", StringType),
        StructField("Name", StringType),
        StructField("RechargeTS", TimestampType),
        StructField("RechargeAmt", DoubleType),
        StructField("RechargeCount", IntegerType),
        StructField("RechargeDen", DoubleType),
        StructField("date", StringType)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("profRowID", StringType),
        StructField("profName", StringType),
        StructField("MATR30Amount", StringType),
        StructField("date", StringType)
      )
    )

    //val encoder = RowEncoder(schema)
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      sampleSchema
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profile_rows),
      profileSchema
    )

    var transformedDF = MostAvailedTopupRolling30DaysAmount.process(
      Map[String, DataFrame]("sampleDF" -> sampleDF),
      profileDF,
      Array(
        SourceColumn("sampleDF", "RowID"),
        SourceColumn("sampleDF", "RechargeTS"),
        SourceColumn("sampleDF", "RechargeAmt"),
        SourceColumn("sampleDF", "RechargeCount"),
        SourceColumn("sampleDF", "RechargeDen")
      ),
      Option(Array(JoinColumn("RowID", "profRowID"))),
      "MATR30Amount"
    )(spark, extraParameters)

    transformedDF = transformedDF.sort(col("profRowID"))
    val transformedCollectedDF = transformedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .collect()

    val expectedDF = List(
      Row("1", "Person_P", 25.0, partitionDate),
      Row("2", "Person_C", null, partitionDate),
      Row("3", "Person_M", 80.0, partitionDate),
      Row("4", "Person_N", null, partitionDate),
      Row("5", "Person_O", null, partitionDate)
    )

    assertResult(expectedDF)(transformedCollectedDF)
  }

}
