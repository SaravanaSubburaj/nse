package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class CombineActiveSubscriberIndicatorTest extends FlatSpec with SparkTest {
  "CombineActiveSubscriberIndicatorTest" should "Test if a subscriber is active or not" in withSparkSession {
    spark: SparkSession =>
      val sourceColumns = Array(
        SourceColumn("GlobeDailyPrepaidSubscriberUsage", "subscriberId"),
        SourceColumn("GlobeDailyPrepaidSubscriberUsage", "usageTransactionDate"),
        SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "subscriberId"),
        SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "usageTransactionDate")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subscriber_id"))
      )
      val date = "2019-01-24"

      val prepaidUsageSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("usageTransactionDate", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )
      val postpaidUsageSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("usageTransactionDate", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )
      val subscriberSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )

      val subsriberList = List(
        Row("1", date),
        Row("2", date),
        Row("3", date),
        Row("5", date),
        Row("7", date),
        Row("9", date),
        Row("10", date),
        Row("11", date)
      )
      val profileSchema = StructType(
        List(
          StructField("subscriber_id", StringType),
          StructField("active_subscriber_indicator", StringType)
        )
      )

      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("5", ""),
        Row("7", ""),
        Row("9", ""),
        Row("10", ""),
        Row("11", "")
      )

      val prepaidUsageList = List(
        Row("1", "2019-01-13", date),
        Row("2", "2019-01-12", date),
        Row("3", "2019-01-07", date)
      )
      val postpaidUsageList = List(
        Row("5", "2019-01-04", date),
        Row("6", "2019-01-23", date),
        Row("7", "2019-01-13", date)
      )
      val subscriberDF = spark.createDataFrame(spark.sparkContext.parallelize(subsriberList), subscriberSchema)
      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileSchema)
      val prepaidUsageDF = spark.createDataFrame(spark.sparkContext.parallelize(prepaidUsageList), prepaidUsageSchema)
      val postpaidUsageDF =
        spark.createDataFrame(spark.sparkContext.parallelize(postpaidUsageList), postpaidUsageSchema)

      val sourceDFs = Map(
        "subscriber"                               -> subscriberDF,
        "GlobeDailyPrepaidSubscriberUsage"         -> prepaidUsageDF,
        "GlobeDailyPostpaidSubscriberUsageStaging" -> postpaidUsageDF
      )
      implicit val extraParameters: Map[String, String] = Map(TransformerEnums.PARTITION_VALUE -> date)

      val targetColumn = "active_subscriber_indicator"

      val activeSubscriberIndicatorDF = CombineActiveSubscriberIndicator
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actual = activeSubscriberIndicatorDF.collect()
      val expected = Array(
        Row("7", 1),
        Row("11", 0),
        Row("3", 0),
        Row("5", 0),
        Row("9", 0),
        Row("1", 1),
        Row("10", 0),
        Row("2", 1)
      )
      assertResult(expected)(actual)

  }
}
