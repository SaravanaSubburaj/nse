package in.datateam.transformer.transformation

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.scalatest.FlatSpec

class JoinDeriveBirthDateCombineTest extends FlatSpec with SparkTest {

  "JoinDeriveBirthDateCombineTest" should "combine prepaid and postpaid birth_date" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val aaModelData = List(
        Row(
          "10663",
          "0003909257",
          "33",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "AGE",
          "2018-12"
        ),
        Row(
          "10550",
          "0003909204",
          "48",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "AGE",
          "2018-12"
        )
      )

      val aaModelSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(aaModelData),
          aaModelSchema
        )

      val subscriberData = List(
        Row(
          "10123",
          "1995-01-18",
          "2019-01-16"
        ),
        Row(
          "10567",
          "1975-05-24",
          "2019-01-16"
        )
      )

      val subscriberSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("birthDate", StringType),
          StructField("date", StringType)
        )
      )

      val subscriberDF = spark.createDataFrame(
        spark.sparkContext.parallelize(subscriberData),
        subscriberSchema
      )

      val sampleProfList = List(
        Row("10663", ""),
        Row("10550", ""),
        Row("10123", ""),
        Row("10567", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subscriber_id", StringType),
          StructField("birth_date", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map(
        "GlobeSubscriber" -> subscriberDF,
        "AAModelScore"    -> aaModelDF
      )

      val sourceColumns = Array(
        SourceColumn("GlobeSubscriber", "subscriberId"),
        SourceColumn("GlobeSubscriber", "birthDate"),
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "score"),
        SourceColumn("AAModelScore", "model")
      )

      val targetColumn: String = "birth_date"
      val targetJoinColumn = Option(
        Array(
          JoinColumn("subscriberId", "subscriber_id"),
          JoinColumn("subsId", "subscriber_id")
        )
      )

      val joinedDF = JoinDeriveBirthDateCombine.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = List(
        Row("10123", "1995-01-18"),
        Row("10550", "1971-01-01"),
        Row("10567", "1975-05-24"),
        Row("10663", "1986-01-01")
      )

      assertResult(expectedDF)(actualDF)
  }

}
