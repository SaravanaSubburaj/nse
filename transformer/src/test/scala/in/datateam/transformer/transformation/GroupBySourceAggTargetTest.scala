package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class GroupBySourceAggTargetTest extends FlatSpec with SparkTest {
  "GroupBySourceAggTargetTest" should "test entity aggregation transformer" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val dateA = new java.sql.Date(2019, 1, 13)
      val dateB = new java.sql.Date(2019, 1, 14)
      val dateC = new java.sql.Date(2019, 1, 15)

      val testData = List(
        Row("A", 10, 10, dateA, "2019-01-16"),
        Row("C", 30, 10, dateC, "2019-01-16"),
        Row("A", 42, 10, dateA, "2019-01-16"),
        Row("B", 53, 10, dateB, "2019-01-16"),
        Row("C", 64, 10, dateC, "2019-01-16"),
        Row("A", 56, 10, dateA, "2019-01-16"),
        Row("B", 12, 10, dateB, "2019-01-16"),
        Row("B", 20, 10, dateB, "2019-01-16"),
        Row("C", 65, 10, dateC, "2019-01-16")
      )

      val resultData = List(
        Row("A", dateA, "2019-01-16", 10, 56, 108, 3, 36.0, 10, 10, 30, 3, 10.0, null, null),
        Row("B", dateB, "2019-01-16", 12, 53, 85, 3, 28.333333333333332, 10, 10, 30, 3, 10.0, null, null),
        Row("C", dateC, "2019-01-16", 30, 65, 159, 3, 53.0, 10, 10, 30, 3, 10.0, null, null)
      )

      val resultSchema = StructType(
        List(
          StructField("Key", StringType),
          StructField("recordCreationTime", DateType),
          StructField("date", StringType),
          StructField("Min_Value1", IntegerType),
          StructField("Max_Value1", IntegerType),
          StructField("Sum_Value1", IntegerType),
          StructField("Count_Value1", IntegerType),
          StructField("Avg_Value1", DoubleType),
          StructField("Min_Value2", IntegerType),
          StructField("Max_Value2", IntegerType),
          StructField("Sum_Value2", IntegerType),
          StructField("Count_Value2", IntegerType),
          StructField("Avg_Value2", DoubleType),
          StructField("Value1", DoubleType),
          StructField("Value2", DoubleType)
        )
      )

      val resultDF = spark.createDataFrame(spark.sparkContext.parallelize(resultData), resultSchema)

      val testSchema = StructType(
        List(
          StructField("Key", StringType),
          StructField("Value1", IntegerType),
          StructField("Value2", IntegerType),
          StructField("recordCreationTime", DateType),
          StructField("date", StringType)
        )
      )

      val testDF = spark.createDataFrame(spark.sparkContext.parallelize(testData), testSchema)
      val groupByCols = Array(SourceColumn("testEntity", "Key"), SourceColumn("testEntity", "recordCreationTime"))

      val aggCols = Array(SourceColumn("testEntity", "Value1"), SourceColumn("testEntity", "Value2"))
      val targetCols = Array(
        "Min_Value1",
        "Max_Value1",
        "Sum_Value1",
        "Count_Value1",
        "Avg_Value1",
        "Min_Value2",
        "Max_Value2",
        "Sum_Value2",
        "Count_Value2",
        "Avg_Value2"
      )

      val actualResultDF =
        GroupBySourceAggTarget.process(testDF, testDF, groupByCols, aggCols, targetCols)(spark, extraParameters)

      assertResult(resultDF.sort("Key").collect().toList)(actualResultDF.sort("Key").collect().toList)

  }

}
