package in.datateam.transformer.transformation

import java.io.File

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.LookupDetail
import in.datateam.utils.configparser.SourceColumn

class StandardizeCurrencyTest extends FlatSpec with SparkTest {
  "StandardizeCurrencyTest" should "test StandardizeCurrency implementation" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sample_rows =
        List(Row("1", "Rs. 100"), Row("2", "$ 50"), Row("3", "S$ 600"))

      val schema = StructType(
        List(
          StructField("RowID", StringType, nullable = false),
          StructField("Amount", StringType, nullable = false)
        )
      )
      val sourceColumns = Array(SourceColumn("sample", "Amount"))
      val sampleDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sample_rows),
        schema
      )
      val currencyMapFilePath =
        new File("transformer/src/test/resources/currency.json").getAbsolutePath
      val broadcastsDetails = Seq(
        LookupDetail("StandardCurrency", "json", "local", currencyMapFilePath)
      )
      val transformedDF = StandardizeCurrency
        .process(sampleDF, sourceColumns, "Amount")(
          spark,
          extraParameters,
          broadcastsDetails: _*
        )
      val collectedTransformedDF = transformedDF.collect()

      val expectedCollectedValidatedDF =
        List(Row("1", "100 INR"), Row("2", "50 USD"), Row("3", "600 SGD"))

      assertResult(expectedCollectedValidatedDF)(collectedTransformedDF)

  }

}
