package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMRFTest extends FlatSpec with SparkTest {
  "GetMRFTest" should "get sum of charge_total_amount" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList =
      List(Row("1", ""), Row("2", ""), Row("3", ""), Row("4", ""))
    val profileListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("MRF", StringType)
      )
    )

    val subcriberEntityList = List(
      Row("1", "2019-01-16"),
      Row("2", "2019-01-16"),
      Row("3", "2019-01-16"),
      Row("4", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("date", StringType)
      )
    )

    val attributeEntity = List(
      Row("RC", "1", "2.5", "2019-01"),
      Row("RC", "1", "2.5", "2019-01"),
      Row("RC", "2", "3.5", "2019-01"),
      Row("RC", "2", "2.5", "2019-01"),
      Row("RC", "3", "2.5", "2019-01"),
      Row("RC", "4", "2.5", "2019-01")
    )
    val attributeEntitySchema = StructType(
      List(
        StructField("revenueTypeCode", StringType),
        StructField("subscriberId", StringType),
        StructField("chargeTotalAmount", StringType),
        StructField("date", StringType)
      )
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val entityDF = spark.createDataFrame(
      spark.sparkContext.parallelize(attributeEntity),
      attributeEntitySchema
    )
    val sourceDF =
      Array(("GlobeSubscriber", subsDF), ("chargeStaging", entityDF)).toMap
    val sourceColumns = Array(
      SourceColumn("chargeStaging", "revenueTypeCode"),
      SourceColumn("chargeStaging", "subscriberId"),
      SourceColumn("chargeStaging", "chargeTotalAmount")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriberId")))
    val targetColumn = "MRF"
    val df = GetMRF
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("subscriberId").collect().toList
    val expectList =
      List(Row("1", 5.0), Row("2", 6.0), Row("3", 2.5), Row("4", 2.5))
    assertResult(expectList)(calulatedDF)
  }
}
