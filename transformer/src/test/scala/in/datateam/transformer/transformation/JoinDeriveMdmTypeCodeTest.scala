package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDeriveMdmTypeCodeTest extends FlatSpec with SparkTest {

  "JoinDeriveMdmTypeCodeTest" should "join and derive from mdm entities" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

    val uniquesEntityList = List(
      Row("1", "2019-01-16"),
      Row("2", "2019-01-16"),
      Row("3", "2019-01-16")
    )

    val confidentsEntityList = List(
      Row("4", "2019-01-16"),
      Row("5", "2019-01-16"),
      Row("6", "2019-01-16")
    )

    val suspectsEntityList = List(
      Row("7", "2019-01-16"),
      Row("8", "2019-01-16"),
      Row("9", "2019-01-16")
    )

    val uniquesEntityListSchema = StructType(
      List(
        StructField("subs_id", StringType),
        StructField("date", StringType)
      )
    )

    val confidentsEntityListSchema = StructType(
      List(
        StructField("subs_id", StringType),
        StructField("date", StringType)
      )
    )

    val suspectsEntityListSchema = StructType(
      List(
        StructField("subs_id", StringType),
        StructField("date", StringType)
      )
    )

    val uniquesDF = spark.createDataFrame(
      spark.sparkContext.parallelize(uniquesEntityList),
      uniquesEntityListSchema
    )

    val confidentsDF = spark.createDataFrame(
      spark.sparkContext.parallelize(confidentsEntityList),
      confidentsEntityListSchema
    )

    val suspectsDF = spark.createDataFrame(
      spark.sparkContext.parallelize(suspectsEntityList),
      suspectsEntityListSchema
    )

    val profileEntityList = List(
      Row(1, ""),
      Row(2, ""),
      Row(3, ""),
      Row(4, ""),
      Row(5, ""),
      Row(6, ""),
      Row(7, ""),
      Row(8, ""),
      Row(9, ""),
      Row(10, ""),
      Row(11, ""),
      Row(12, "")
    )

    val profileEntityListSchema = StructType(
      List(
        StructField("subscriber_id", IntegerType),
        StructField("mdm_type_code", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileEntityList),
      profileEntityListSchema
    )

    val sourceDFs = Array(
      ("uniques_new", uniquesDF),
      ("confidents_new", confidentsDF),
      ("suspects_new", suspectsDF)
    ).toMap

    val sourceColumns = Array(
      SourceColumn("uniques_new", "subs_id"),
      SourceColumn("confidents_new", "subs_id"),
      SourceColumn("suspects_new", "subs_id")
    )

    val joinColumns = Option(Array(JoinColumn("subs_id", "subscriber_id")))

    val targetColumn = "mdm_type_code"

    val joinedDF = JoinDeriveMdmTypeCode
      .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

    val actualList = joinedDF.sort("subscriber_id").collect().toList

    val expectedList = List(
      Row(1, "U"),
      Row(2, "U"),
      Row(3, "U"),
      Row(4, "C"),
      Row(5, "C"),
      Row(6, "C"),
      Row(7, "S"),
      Row(8, "S"),
      Row(9, "S"),
      Row(10, null),
      Row(11, null),
      Row(12, null)
    )

    assertResult(expectedList)(actualList)

  }

}
