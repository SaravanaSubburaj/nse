package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetRewardIndicatorTest extends FlatSpec with SparkTest {
  "join test" should "Test join operation" in withSparkSession { spark: SparkSession =>
    val partitionDate = "2019-03-04"
    val rewardList = List(
      Row("1", "631234306545", "2018-12-07", partitionDate),
      Row("2", "631234306546", "2018-12-07", partitionDate),
      Row("3", "631234306547", "2018-12-07", partitionDate),
      Row("306545", "631234306548", "2018-12-07", partitionDate)
    )
    val rewardSchema = StructType(
      List(
        StructField("omsPortal", StringType),
        StructField("msisdn", StringType),
        StructField("etlInsertionTS", StringType),
        StructField("date", StringType)
      )
    )

    val profileList = List(
      Row("1", "1234306545", null, partitionDate),
      Row("2", "1234306546", null, partitionDate),
      Row("3", "1234306547", null, partitionDate),
      Row("4", "1234306548", null, partitionDate),
      Row("5", "1234306549", null, partitionDate)
    )
    val profileSchema = StructType(
      List(
        StructField("subscriberid", StringType),
        StructField("msisdn_value", StringType),
        StructField("reward", StringType),
        StructField("date", StringType)
      )
    )
    val rewardDF = spark.createDataFrame(
      spark.sparkContext.parallelize(rewardList),
      rewardSchema
    )
    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileSchema
    )
    val sourceDF = Array(("MillipedeRewardTransactions", rewardDF)).toMap
    val sourceColumns = Array(
      SourceColumn("MillipedeRewardTransactions", "omsPortal"),
      SourceColumn("MillipedeRewardTransactions", "msisdn")
    )
    val joinColumns = Option(Array(JoinColumn("msisdn", "msisdn_value")))
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-03-04")

    val targetColumn = "reward"
    val df = GetRewardIndicator
      .process(sourceDF, targetDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF = df
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .collect()
      .toList
    val expectList = List(
      Row("3", "1234306547", "1", partitionDate),
      Row("1", "1234306545", "0", partitionDate),
      Row("2", "1234306546", "0", partitionDate),
      Row("5", "1234306549", null, partitionDate),
      Row("4", "1234306548", "0", partitionDate)
    )
    assertResult(expectList)(collectedDF)
  }
}
