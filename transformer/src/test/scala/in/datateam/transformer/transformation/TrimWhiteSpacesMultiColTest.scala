package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class TrimWhiteSpacesMultiColTest extends FlatSpec with SparkTest {

  "TrimWhiteSpacesMultiColTest" should "trim multiple columns" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

    val sampleList = List(
      Row(" Allen", "John ", "  123"),
      Row("Allen     ", " John", "456  "),
      Row("        Allen ", " John ", "  123"),
      Row(" All en ", "  John ", "456  "),
      Row("Allen", "John", "1 2 3"),
      Row("Allen ", "John", "  123")
    )
    val schema = StructType(
      List(
        StructField("firstName", StringType),
        StructField("lastName", StringType),
        StructField("miscID", StringType)
      )
    )
    val sourceColumns = Array(SourceColumn("sample", "*"))
    val sampleDF = spark
      .createDataFrame(spark.sparkContext.parallelize(sampleList), schema)
      .alias("sample")
    val calculatedDF = TrimWhiteSpacesMultiCol
      .process(sampleDF, sourceColumns, "")(spark, extraParameters)
    val collectCalculatedDf = calculatedDF.collect()

    val expectedDF = List(
      Row("Allen", "John", "123"),
      Row("Allen", "John", "456"),
      Row("Allen", "John", "123"),
      Row("All en", "John", "456"),
      Row("Allen", "John", "1 2 3"),
      Row("Allen", "John", "123")
    )
    assertResult(expectedDF)(collectCalculatedDf)
  }
}
