package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetUnpaidBillAmountTest extends FlatSpec with SparkTest {
  "GetUnpaidBillAmountTest" should "calculate attribute unpaid billed amount" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("unpaid_billed_amount", StringType)
        )
      )

      val subcriberEntityList = List(Row("1"))
      val subcriberEntityListSchema =
        StructType(List(StructField("subsSubscriberID", StringType)))
      val FinancialAccountInvoiceStaging = List(
        Row("201.01", "112", "202.25", "2158", "2019-01"),
        Row("202.01", "113", "205.25", "2159", "2019-01"),
        Row("21.01", "114", "22.25", "2160", "2019-01"),
        Row("301.01", "115", "302.25", "2161", "2019-01"),
        Row("401.01", "116", "402.25", "2162", "2019-01")
      )
      val FinancialAccountInvoiceStagingSchema = StructType(
        List(
          StructField("previousBalanceAmount", StringType),
          StructField("financialAccountId", StringType),
          StructField("financialActivitiesAmount", StringType),
          StructField("billNumber", StringType),
          StructField("date", StringType)
        )
      )

      val FinancialAccount = List(
        Row("112", "1", "O", "2019-01-01", "2019-01-16"),
        Row("112", "1", "SUS", "2019-01-01", "2019-01-15"),
        Row("113", "2", "SUS", "2019-01-02", "2019-01-16"),
        Row("114", "3", "SUS", "2019-01-02", "2019-01-16"),
        Row("115", "4", "O", "2019-01-03", "2019-01-16"),
        Row("116", "5", "O", "2019-01-04", "2019-01-16")
      )
      val FinancialAccountSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("customerId", StringType),
          StructField("financialAccountStatusCode", StringType),
          StructField("financialAccountActivationDate", StringType),
          StructField("date", StringType)
        )
      )
      val FinancialAccountSubscriberReference = List(
        Row("112", "1", null, "2019-01-16"),
        Row("112", "1", null, "2019-01-15"),
        Row("113", "2", null, "2019-01-16"),
        Row("114", "3", null, "2019-01-16"),
        Row("114", "4", null, "2019-01-16"),
        Row("115", "5", null, "2019-01-16")
      )
      val FinancialAccountSubscriberReferenceSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("subscriberId", StringType),
          StructField("expirationDate", StringType, nullable = true),
          StructField("date", StringType)
        )
      )
      val subscriberFinancialReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountSubscriberReference),
        FinancialAccountSubscriberReferenceSchema
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val financialDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountInvoiceStaging),
        FinancialAccountInvoiceStagingSchema
      )
      val subscriberReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccount),
        FinancialAccountSchema
      )
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("financialAccount", subscriberReferenceDF),
        ("FinancialAccountInvoiceStaging", financialDF),
        ("FinancialAccountSubscriberReference", subscriberFinancialReferenceDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("FinancialAccountInvoiceStaging", "financialAccountId"),
        SourceColumn("financialAccount", "financialAccountId"),
        SourceColumn("FinancialAccountInvoiceStaging", "previousBalanceAmount"),
        SourceColumn("financialAccount", "customerId"),
        SourceColumn(
          "FinancialAccountInvoiceStaging",
          "financialActivitiesAmount"
        ),
        SourceColumn("FinancialAccountInvoiceStaging", "billNumber"),
        SourceColumn("financialAccount", "financialAccountStatusCode"),
        SourceColumn("financialAccount", "financialAccountActivationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "subscriberId"),
        SourceColumn("FinancialAccountSubscriberReference", "expirationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "financialAccountId")
      )
      val joinColumns = Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
      val targetColumn = "unpaid_billed_amount"
      val df = GetUnpaidBillAmount
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subsSubscriberID").collect().toList
      val expectList = List(
        Row("1", 403.26),
        Row("2", 407.26),
        Row("3", 43.260000000000005),
        Row("4", 43.260000000000005),
        Row("5", 603.26)
      )
      assertResult(expectList)(calulatedDF)
  }

}
