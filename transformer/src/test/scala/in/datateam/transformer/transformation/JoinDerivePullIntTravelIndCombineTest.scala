package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDerivePullIntTravelIndCombineTest extends FlatSpec with SparkTest {
  "JoinDerivePullIntTravelIndCombineTest" should "Test prepaid and postpaid international_traveller_indicator combined in a single attribute" in withSparkSession {
    spark: SparkSession =>
      val sourceColumns = Array(
        SourceColumn("GlobeSubscriber", "subscriberId"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "subscriberId"),
        SourceColumn("GlobeDailyPrepaidSubscriberUsage", "subscriberId"),
        SourceColumn("GlobeDailyPrepaidSubscriberUsage", "usageNetworkRoamingTypeCode"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "serviceName"),
        SourceColumn("GlobeSubscriber", "subscriberId"),
        SourceColumn("SubscriberAssignedBillingOffer", "subscriberId"),
        SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "subscriberId"),
        SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "usageNetworkRoamingTypeCode"),
        SourceColumn("SubscriberAssignedBillingOffer", "billingOfferDescription")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subscriber_id"))
      )
      val date = "2019-01-16"

      val subscriberList = List(
        Row("1", date),
        Row("2", date),
        Row("3", date)
      )
      val subscriberSchema = StructType(
        List(
          StructField("subscriber_id", StringType, nullable = false),
          StructField("date", StringType, nullable = false)
        )
      )
      val globeSubscriberList = List(
        Row("1", date),
        Row("2", date),
        Row("3", date)
      )
      val globeSubscriberSchema = StructType(
        List(
          StructField("subscriberId", StringType, nullable = false),
          StructField("date", StringType, nullable = false)
        )
      )
      val globePrepaidPromoAvailmentStagingSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("serviceName", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )
      val globeDailyPrepaidSubscriberUsageSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("usageNetworkRoamingTypeCode", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )

      val subscriberAssignedBillingOfferSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("billingOfferDescription", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )

      val globeDailyPostpaidSubscriberUsageStagingSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("usageNetworkRoamingTypeCode", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )

      val profileSchema = StructType(
        List(StructField("subscriber_id", StringType))
      )

      val globePrepaidPromoAvailmentStagingList = List(
        Row("1", "GS50_BASE", date),
        Row("2", "roaming", date),
        Row("3", "GS50_BASE", date)
      )

      val globeDailyPrepaidSubscriberUsageList = List(
        Row("1", "H", date),
        Row("2", "R", date),
        Row("3", "R", date)
      )

      val subscriberAssignedBillingOfferList = List(
        Row("1", "Roam Surf 15 Days (P4999)", date),
        Row("2", "Roam Saver", date),
        Row("3", "Add On - Easy Roam 149", date)
      )

      val globeDailyPostpaidSubscriberUsageStagingList = List(
        Row("1", "H", date),
        Row("2", "R", date),
        Row("3", "R", date)
      )

      val profileList = List(Row("1"), Row("2"), Row("3"))

      val subscriberDF =
        spark.createDataFrame(spark.sparkContext.parallelize(subscriberList), subscriberSchema)

      val globeSubscriberDF =
        spark.createDataFrame(spark.sparkContext.parallelize(globeSubscriberList), globeSubscriberSchema)

      val globePrepaidPromoAvailmentStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globePrepaidPromoAvailmentStagingList),
        globePrepaidPromoAvailmentStagingSchema
      )

      val globeDailyPrepaidSubscriberUsageDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globeDailyPrepaidSubscriberUsageList),
        globeDailyPrepaidSubscriberUsageSchema
      )

      val subscriberAssignedBillingOfferDF = spark.createDataFrame(
        spark.sparkContext.parallelize(subscriberAssignedBillingOfferList),
        subscriberAssignedBillingOfferSchema
      )
      val globeDailyPostpaidSubscriberUsageStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globeDailyPostpaidSubscriberUsageStagingList),
        globeDailyPostpaidSubscriberUsageStagingSchema
      )

      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileSchema)

      val sourceDFs = Map(
        "Subscriber"                               -> subscriberDF,
        "GlobeSubscriber"                          -> globeSubscriberDF,
        "GlobePrepaidPromoAvailmentStaging"        -> globePrepaidPromoAvailmentStagingDF,
        "GlobeDailyPrepaidSubscriberUsage"         -> globeDailyPrepaidSubscriberUsageDF,
        "SubscriberAssignedBillingOffer"           -> subscriberAssignedBillingOfferDF,
        "GlobeDailyPostpaidSubscriberUsageStaging" -> globeDailyPostpaidSubscriberUsageStagingDF
      )

      implicit val extraParameters: Map[String, String] = Map(TransformerEnums.PARTITION_VALUE -> date)

      val targetColumn = "international_traveller_indicator"

      val joinDerivePullIntTravelIndCombineDF = JoinDerivePullIntTravelIndCombine
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actual = joinDerivePullIntTravelIndCombineDF.collect()
      val expected = Array(
        Row("3", 0),
        Row("1", 0),
        Row("2", 1)
      )
      assertResult(expected)(actual)
  }
}
