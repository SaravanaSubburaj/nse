package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectWithBillingArrangementTest extends FlatSpec with SparkTest {
  "JoinDirectWithBillingArrangementTest" should "join and direct pull from BillingArrangement" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("bill_email", StringType)
        )
      )

      val subcriberEntityList = List(Row("1"), Row("2"))
      val subcriberEntityListSchema =
        StructType(List(StructField("subsSubscriberID", StringType)))
      val BillingArrangement = List(
        Row("112", "Gmail", "2019-01-16"),
        Row("113", "Gmail", "2019-01-16"),
        Row("114", "Gmail", "2019-01-16"),
        Row("115", "Ymail", "2019-01-16"),
        Row("116", "Ymail", "2019-01-16")
      )
      val BillingArrangementSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("billingArrangementMediaEmailText", StringType),
          StructField("date", StringType)
        )
      )

      val FinancialAccount = List(
        Row("112", "1", "O", "2019-01-01", "2019-01-16"),
        Row("112", "1", "SUS", "2019-01-01", "2019-01-15"),
        Row("113", "2", "SUS", "2019-01-02", "2019-01-16"),
        Row("114", "3", "SUS", "2019-01-02", "2019-01-16"),
        Row("115", "4", "O", "2019-01-03", "2019-01-16"),
        Row("116", "5", "O", "2019-01-04", "2019-01-16")
      )
      val FinancialAccountSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("customerId", StringType),
          StructField("financialAccountStatusCode", StringType),
          StructField("financialAccountActivationDate", StringType),
          StructField("date", StringType)
        )
      )
      val FinancialAccountSubscriberReference = List(
        Row("112", "1", null, "2019-01-16"),
        Row("112", "1", null, "2019-01-15"),
        Row("113", "2", null, "2019-01-16"),
        Row("114", "3", null, "2019-01-16"),
        Row("114", "4", null, "2019-01-16"),
        Row("115", "5", null, "2019-01-16")
      )
      val FinancialAccountSubscriberReferenceSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("subscriberId", StringType),
          StructField("expirationDate", StringType, nullable = true),
          StructField("date", StringType)
        )
      )
      val subscriberFinancialReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountSubscriberReference),
        FinancialAccountSubscriberReferenceSchema
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val BillingArrangementDF = spark.createDataFrame(
        spark.sparkContext.parallelize(BillingArrangement),
        BillingArrangementSchema
      )
      val subscriberReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccount),
        FinancialAccountSchema
      )
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("financialAccount", subscriberReferenceDF),
        ("BillingArrangement", BillingArrangementDF),
        ("FinancialAccountSubscriberReference", subscriberFinancialReferenceDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("financialAccount", "financialAccountId"),
        SourceColumn("financialAccount", "customerId"),
        SourceColumn("BillingArrangement", "billingArrangementMediaEmailText"),
        SourceColumn("BillingArrangement", "financialAccountId"),
        SourceColumn("financialAccount", "financialAccountStatusCode"),
        SourceColumn("financialAccount", "financialAccountActivationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "subscriberId"),
        SourceColumn("FinancialAccountSubscriberReference", "expirationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "financialAccountId")
      )
      val joinColumns = Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
      val targetColumn = "bill_email"
      val df = JoinDirectWithBillingArrangement
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calculatedDF = df.sort("subsSubscriberID").collect().toList
      val expectList = List(
        Row("1", "Gmail"),
        Row("2", "Gmail"),
        Row("3", "Gmail"),
        Row("4", "Gmail"),
        Row("5", "Ymail")
      )
      assertResult(expectList)(calculatedDF)
  }
}
