package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectFinancialInvoiceStagingTest extends FlatSpec with SparkTest {
  "JoinDirectFinancialInvoiceStagingTest" should "join and direct pull from invoiceStaging" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("period_coverage_end_date", StringType)
        )
      )

      val subcriberEntityList = List(Row("1"), Row("2"))
      val subcriberEntityListSchema =
        StructType(List(StructField("subsSubscriberID", StringType)))
      val FinancialAccountInvoiceStaging = List(
        Row("112", "2018-12-18", "1158", "2019-01"),
        Row("113", "2018-12-16", "1159", "2019-01"),
        Row("114", "2018-12-15", "1160", "2019-01"),
        Row("115", "2018-12-14", "1161", "2019-01"),
        Row("116", "2018-12-11", "1162", "2019-01")
      )
      val FinancialAccountInvoiceStagingSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("periodCoverageEndDate", StringType),
          StructField("billNumber", StringType),
          StructField("date", StringType)
        )
      )

      val FinancialAccount = List(
        Row("112", "1", "O", "2019-01-01", "2019-01-16"),
        Row("112", "1", "SUS", "2019-01-01", "2019-01-15"),
        Row("113", "2", "SUS", "2019-01-02", "2019-01-16"),
        Row("114", "3", "SUS", "2019-01-02", "2019-01-16"),
        Row("115", "4", "O", "2019-01-03", "2019-01-16"),
        Row("116", "5", "O", "2019-01-04", "2019-01-16")
      )
      val FinancialAccountSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("customerId", StringType),
          StructField("financialAccountStatusCode", StringType),
          StructField("financialAccountActivationDate", StringType),
          StructField("date", StringType)
        )
      )
      val FinancialAccountSubscriberReference = List(
        Row("112", "1", null, "2019-01-16"),
        Row("112", "1", null, "2019-01-15"),
        Row("113", "2", null, "2019-01-16"),
        Row("114", "3", null, "2019-01-16"),
        Row("114", "4", null, "2019-01-16"),
        Row("115", "5", null, "2019-01-16")
      )
      val FinancialAccountSubscriberReferenceSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("subscriberId", StringType),
          StructField("expirationDate", StringType, nullable = true),
          StructField("date", StringType)
        )
      )
      val subscriberFinancialReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountSubscriberReference),
        FinancialAccountSubscriberReferenceSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val financialDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountInvoiceStaging),
        FinancialAccountInvoiceStagingSchema
      )
      val subscriberReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccount),
        FinancialAccountSchema
      )
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("financialAccount", subscriberReferenceDF),
        ("FinancialAccountInvoiceStaging", financialDF),
        ("FinancialAccountSubscriberReference", subscriberFinancialReferenceDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("FinancialAccountInvoiceStaging", "financialAccountId"),
        SourceColumn("financialAccount", "financialAccountId"),
        SourceColumn("FinancialAccountInvoiceStaging", "periodCoverageEndDate"),
        SourceColumn("financialAccount", "customerId"),
        SourceColumn("FinancialAccountInvoiceStaging", "billNumber"),
        SourceColumn("financialAccount", "financialAccountStatusCode"),
        SourceColumn("financialAccount", "financialAccountActivationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "subscriberId"),
        SourceColumn("FinancialAccountSubscriberReference", "expirationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "financialAccountId")
      )
      val joinColumns = Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
      val targetColumn = "period_coverage_end_date"
      val df = JoinDirectFinancialInvoiceStaging
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calculatedDF = df.sort("subsSubscriberID").collect().toList
      val expectList = List(
        Row("1", "2018-12-18"),
        Row("2", "2018-12-16"),
        Row("3", "2018-12-15"),
        Row("4", "2018-12-15"),
        Row("5", "2018-12-14")
      )
      assertResult(expectList)(calculatedDF)
  }
}
