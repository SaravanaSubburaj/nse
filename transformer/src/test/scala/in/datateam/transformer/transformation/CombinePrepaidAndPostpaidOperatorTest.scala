package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
class CombinePrepaidAndPostpaidOperatorTest extends FlatSpec with SparkTest {
  "CombinePrepaidAndPostpaidOperatorTest" should "put not value from two columns" in withSparkSession {
    spark: SparkSession =>
      val sampleList = List(
        Row("1", null, null),
        Row(null, "1", null),
        Row("1", null, null),
        Row(null, null, null)
      )

      val schema = StructType(
        List(
          StructField("col_1", StringType),
          StructField("col_2", StringType),
          StructField("col_3", StringType)
        )
      )
      val sourceColumns =
        Array(SourceColumn("sample", "col_1"), SourceColumn("sample", "col_2"))
      val sampleDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleList),
        schema
      )
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val calculatedDF = CombinePrepaidAndPostpaidOperator
        .process(sampleDF, sourceColumns, "col_3")(spark, extraParameters)
      val collectCalculatedDF = calculatedDF.collect()

      val ExpectedDF = Array(Row("1"), Row("1"), Row("1"), Row(null))
      assertResult(collectCalculatedDF)(ExpectedDF)
  }
}
