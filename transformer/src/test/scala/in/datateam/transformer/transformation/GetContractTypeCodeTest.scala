package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetContractTypeCodeTest extends FlatSpec with SparkTest {
  "GetContractTypeCodeTest" should "Get Contract_type_code derived attribute" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("contract_type_code", StringType)
        )
      )

      val subcriberEntityList = List(Row("1", "2019-01-16"))
      val subcriberEntityListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("date", StringType)
        )
      )

      val attributeEntity = List(
        Row("NL", "1", "2019-01-15", "2019-01-16"),
        Row("NL", "2", "2019-01-15", "2019-01-16"),
        Row("L", "3", "2019-01-15", "2019-01-16"),
        Row("L", "4", "2019-01-15", "2019-01-16"),
        Row("NL", "5", "2019-01-15", "2019-01-16")
      )
      val attributeEntitySchema = StructType(
        List(
          StructField("contractTypeCode", StringType),
          StructField("subscriberId", StringType),
          StructField("contractStartDate", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val entityDF = spark.createDataFrame(
        spark.sparkContext.parallelize(attributeEntity),
        attributeEntitySchema
      )
      val sourceDF = Array(
        ("GlobeSubscriber", subsDF),
        ("subscriberContractStaging", entityDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("subscriberContractStaging", "contractTypeCode"),
        SourceColumn("subscriberContractStaging", "subscriberId"),
        SourceColumn("subscriberContractStaging", "contractStartDate")
      )
      val joinColumns =
        Option(Array(JoinColumn("subscriberId", "subscriberId")))
      val targetColumn = "contract_type_code"
      val df = GetContractTypeCode
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subscriberId").collect().toList
      val expectList = List(
        Row("1", "OB"),
        Row("2", "OB"),
        Row("3", "LI"),
        Row("4", "LI"),
        Row("5", "OB")
      )
      assertResult(expectList)(calulatedDF)
  }
}
