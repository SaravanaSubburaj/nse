package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetFavoriteSocialMediaAppNameTest extends FlatSpec with SparkTest {
  "GetFavoriteSocialMediaAppNameTest" should "test FavoriteSocialMediaAppName implementation" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val testData = List(
        Row("639120001", "xyz.fbcdn.net", 9290808L, "2019-01-13", "2019-01-13"),
        Row("639120002", "www.facebook.com", 2721486L, "2019-01-12", "2019-01-13"),
        Row("639120003", "messages.viber.com", 21906294L, "2019-01-11", "2019-01-13"),
        Row("639120001", "media.viber.com", 426079L, "2019-01-11", "2019-01-13"),
        Row("639120002", "www.instagram.com", 1330099L, "2019-01-12", "2019-01-13"),
        Row("639120003", "videos.instagram.com", 7523794L, "2019-01-13", "2019-01-13"),
        Row("639120001", "games.facebook.com", 1975965L, "2019-01-12", "2019-01-13"),
        Row("639120002", "media.viber.com", 832755L, "2019-01-11", "2019-01-13"),
        Row("639120003", "igcdn.instagram.com", 6225024L, "2019-01-13", "2019-01-13"),
        Row("639120001", "uvw.fbcdn.net", 8011839L, "2019-01-11", "2019-01-13"),
        Row("639120002", "xyz.fbcdn.net", 5848343L, "2019-01-12", "2019-01-13"),
        Row("639120003", "xyz.fbcdn.net", 6138173L, "2019-01-13", "2019-01-13")
      )

      val testProfileData = List(
        Row("9120001", null),
        Row("9120002", null),
        Row("9120003", null)
      )

      val ingestedRadcomSchema = StructType(
        List(
          StructField("msisdn", StringType, false),
          StructField("subscriberActivity", StringType),
          StructField("bytesUploaded", LongType),
          StructField("recordCreationTime", StringType),
          StructField("date", StringType)
        )
      )

      val profileSchema = StructType(
        List(
          StructField("msisdn_value", StringType, false),
          StructField("favorite_social_media_app_name", StringType)
        )
      )

      val sourceColumns = Array(
        SourceColumn("radcom", "bytesUploaded"),
        SourceColumn("radcom", "msisdn"),
        SourceColumn("radcom", "subscriberActivity"),
        SourceColumn("radcom", "recordCreationTime")
      )

      val joinColumns = Option(
        Array(
          JoinColumn("msisdn", "msisdn_value")
        )
      )

      val testDF = spark.createDataFrame(spark.sparkContext.parallelize(testData), ingestedRadcomSchema)
      //      val testDF = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], ingestedRadcomSchema) // Case for empty source DF
      val testProfileDF = spark.createDataFrame(spark.sparkContext.parallelize(testProfileData), profileSchema)

      val sourceDFs = Map[String, DataFrame]("radcom" -> testDF)

      val expectedProfileData = List(
        Row("9120002", "INSTAGRAM"),
        Row("9120001", "FACEBOOK"),
        Row("9120003", "VIBER")
      )

      val expectedProfile = spark.createDataFrame(spark.sparkContext.parallelize(expectedProfileData), profileSchema)

      val resultProfile = GetFavoriteSocialMediaAppName
        .process(sourceDFs, testProfileDF, sourceColumns, joinColumns, "favorite_social_media_app_name")(
          spark,
          extraParameters
        )

      assertResult(expectedProfile.collect.toList)(resultProfile.collect.toList)

  }

}
