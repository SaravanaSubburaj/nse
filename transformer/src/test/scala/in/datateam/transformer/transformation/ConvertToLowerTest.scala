package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class ConvertToLowerTest extends FlatSpec with SparkTest {
  "ConvertToLower" should "Convert all the columns of a DF specified to lowercase" in withSparkSession {
    spark: SparkSession =>
      val sampleList = List(
        Row("PRASAD", "Saralaya", null),
        Row("Amit", "Ankalkoti", null),
        Row("DAVID", "William", null),
        Row("Akash", "Raj", null)
      )

      val schema = StructType(
        List(
          StructField("firstName", StringType),
          StructField("lastName", StringType),
          StructField("LowerCase", StringType, nullable = true)
        )
      )
      val sourceColumn = Array(
        SourceColumn("sample", "firstName"),
        SourceColumn("sample", "lastName")
      )
      val sampleDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleList),
        schema
      )
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val calculatedDF =
        ConvertToLower.process(sampleDF, sourceColumn, "LowerCase")(
          spark,
          extraParameters
        )
      val collectCalculatedDF = calculatedDF.collect()

      val ExpectedDF = Array(
        Row("PRASAD", "Saralaya", "prasad"),
        Row("Amit", "Ankalkoti", "amit"),
        Row("DAVID", "William", "david"),
        Row("Akash", "Raj", "akash")
      )
      assertResult(collectCalculatedDF)(ExpectedDF)
  }
}
