package in.datateam.transformer.transformation
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetUsageDataRolling30DaysQuantityCombineTest extends FlatSpec with SparkTest {
  "GetUsageDataRolling30DaysQuantityCombineTest" should "test for usage30days quantity for prepaid and postpaid" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", ""),
        Row("7", ""),
        Row("8", ""),
        Row("9", ""),
        Row("10", ""),
        Row("11", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("usage_data_rolling_30_days_quantity", StringType)
        )
      )

      val dailyPrepaidSubscriberUsageStaging = List(
        Row("11234", "1", "data", "2019-01-16"),
        Row("112678", "1", "data", "2019-01-16"),
        Row("1131123", "2", "data", "2019-01-16"),
        Row("114778", "3", "data", "2019-01-16"),
        Row("115123", "4", "data", "2019-01-16"),
        Row("116098", "5", "data", "2019-01-16")
      )
      val dailyPrepaidSubscriberUsageStagingSchema = StructType(
        List(
          StructField("totalDataVolumeCount", StringType),
          StructField("subscriberId", StringType),
          StructField("usageTypeCode", StringType, nullable = true),
          StructField("date", StringType)
        )
      )
      val dailyPostpaidSubscriberUsageStaging = List(
        Row("11221", "7", "data", "2019-01-16"),
        Row("11245", "7", "data", "2019-01-16"),
        Row("11367", "8", "data", "2019-01-16"),
        Row("114112", "9", "data", "2019-01-16"),
        Row("11476", "10", "data", "2019-01-16"),
        Row("115099", "11", "data", "2019-01-16")
      )
      val dailyPostpaidSubscriberUsageStagingSchema = StructType(
        List(
          StructField("totalDataVolumeCount", StringType),
          StructField("subscriberId", StringType),
          StructField("usageTypeCode", StringType, nullable = true),
          StructField("date", StringType)
        )
      )
      val dailyPrepaidSubscriberUsageStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(dailyPrepaidSubscriberUsageStaging),
        dailyPrepaidSubscriberUsageStagingSchema
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )

      val dailyPostpaidSubscriberUsageStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(dailyPostpaidSubscriberUsageStaging),
        dailyPostpaidSubscriberUsageStagingSchema
      )
      val sourceDF = Array(
        ("daily_prepaid_subscriber_usage_staging", dailyPrepaidSubscriberUsageStagingDF),
        ("daily_postpaid_subscriber_usage_staging", dailyPostpaidSubscriberUsageStagingDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("daily_prepaid_subscriber_usage_staging", "totalDataVolumeCount"),
        SourceColumn("daily_prepaid_subscriber_usage_staging", "subscriberId"),
        SourceColumn("daily_prepaid_subscriber_usage_staging", "usageTypeCode"),
        SourceColumn("daily_postpaid_subscriber_usage_staging", "totalDataVolumeCount"),
        SourceColumn("daily_postpaid_subscriber_usage_staging", "subscriberId"),
        SourceColumn("daily_postpaid_subscriber_usage_staging", "usageTypeCode")
      )
      val joinColumns = Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
      val targetColumn = "usage_data_rolling_30_days_quantity"
      val df = GetUsageDataRolling30DaysQuantityCombine
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subsSubscriberID").collect().toList
      val expectList = List(
        Row("1", "0.11817169189453125"),
        Row("10", "0.010944366455078125"),
        Row("11", "0.10976696014404297"),
        Row("2", "1.0787229537963867"),
        Row("3", "0.10946083068847656"),
        Row("4", "0.10978984832763672"),
        Row("5", "0.11071968078613281"),
        Row("7", "0.021425247192382812"),
        Row("8", "0.010840415954589844"),
        Row("9", "0.10882568359375")
      )
      assertResult(expectList)(calulatedDF)
  }

}
