package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class AAModelAttributesTest extends FlatSpec with SparkTest {

  "GetInterestSegmentationScoreTest" should "process for prepaid and postpaid " +
  "interest segment score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "POSTPAID_INTEREST_SEGMENTATION",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("interest_segmentation_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "interest_segmentation_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetPostpaidChurnPropensityDecileTest" should "Get postpaid_churn_propensity_decile attribute" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "postpaid_si_churn_li",
          "2018-12"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(Row("10663", "1"))

      val profileSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("postpaid_churn_propensity_decile", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "postpaid_churn_propensity_decile"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = AAModelLookupTransform.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(Row("10663", "1"))
      assertResult(expectedDF)(actualDF)
  }

  "GetPostpaidClvSegmentNameTest" should "process for Postpaid CLV model " +
  "interest segment score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "POSTPAID_CLV",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("postpaid_clv_segment_name", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "postpaid_clv_segment_name"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetPostPaidPropensityScoreTest" should "Get postpaid_churn_propensity_score attribute" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "postpaid_si_churn_li_score",
          "2018-12"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(Row("10663", "1"))

      val profileSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("postpaid_churn_propensity_score", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "postpaid_churn_propensity_score"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = AAModelLookupTransform.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(Row("10663", "1"))
      assertResult(expectedDF)(actualDF)
  }

  "GetPrepaidChurnPropensityScoreTest" should "process for PREPAID_CHURN_SCORE model " +
  "interest segment score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "PREPAID_CHURN_SCORE",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("prepaid_churn_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "prepaid_churn_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetPrepaidChurnPropertyDecileTest" should "process for PREPAID_SCORE model " +
  "interest segment score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "PREPAID_CHURN",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("prepaid_churn_propensity_decile", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "prepaid_churn_propensity_decile"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "JoinDeriveprepaid_topup_segmentation_scoreTest" should "process for current month" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2019-01"
        ),
        Row(
          "12388",
          "0004145778",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2019-01"
        ),
        Row(
          "4922",
          "000162810",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2019-01"
        ),
        Row(
          "6367",
          "000146482",
          "5",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2019-01"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(
        Row("Prasad", "10663", ""),
        Row("Amit", "202", ""),
        Row("David", "4922", ""),
        Row("Akash", "471", ""),
        Row("Sharma", "6367", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("prepaid_topup_segmentation_score", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "prepaid_topup_segmentation_score"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = AAModelLookupTransform.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(
        Row("Akash", "471", null),
        Row("Amit", "202", null),
        Row("David", "4922", null),
        Row("Prasad", "10663", null),
        Row("Sharma", "6367", null)
      )
      assertResult(expectedDF)(actualDF)

  }

  "JoinDeriveprepaid_topup_segmentation_scoreTest" should "process for month - 1 " in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-12"
        ),
        Row(
          "12388",
          "0004145778",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-12"
        ),
        Row(
          "4922",
          "000162810",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-11"
        ),
        Row(
          "202",
          "000162810",
          "4",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-12"
        ),
        Row(
          "6367",
          "000146482",
          "5",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-12"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(
        Row("Prasad", "10663", ""),
        Row("Amit", "202", ""),
        Row("David", "4922", ""),
        Row("Akash", "471", ""),
        Row("Sharma", "6367", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("prepaid_topup_segmentation_score", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "prepaid_topup_segmentation_score"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = AAModelLookupTransform.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(
        Row("Akash", "471", null),
        Row("Amit", "202", "4"),
        Row("David", "4922", null),
        Row("Prasad", "10663", null),
        Row("Sharma", "6367", "5")
      )
      assertResult(expectedDF)(actualDF)

  }

  "JoinDeriveprepaid_topup_segmentation_scoreTest" should "process for month - 2 " in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-02"
        ),
        Row(
          "12388",
          "0004145778",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-01"
        ),
        Row(
          "4922",
          "000162810",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-10"
        ),
        Row(
          "202",
          "000162810",
          "4",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-11"
        ),
        Row(
          "6367",
          "000146482",
          "5",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-11"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(
        Row("Prasad", "10663", ""),
        Row("Amit", "202", ""),
        Row("David", "4922", ""),
        Row("Akash", "471", ""),
        Row("Sharma", "6367", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("prepaid_topup_segmentation_score", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "prepaid_topup_segmentation_score"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = AAModelLookupTransform.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(
        Row("Akash", "471", null),
        Row("Amit", "202", "4"),
        Row("David", "4922", null),
        Row("Prasad", "10663", null),
        Row("Sharma", "6367", "5")
      )
      assertResult(expectedDF)(actualDF)

  }

  "GetRoamingPropensityScoreTest" should "roaming propensity score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "GHP",
        "1",
        "18JUL2018:14:35:19",
        "POSTPAID_ROAMING_SCORE",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "PREPAID_ROAMING_SCORE",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("roaming_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "roaming_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetRoamingPropensityDecileTest" should "roaming propensity decile" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "GHP",
        "1",
        "18JUL2018:14:35:19",
        "POSTPAID_ROAMING",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "PREPAID_ROAMING",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("roaming_propensity_decile", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "roaming_propensity_decile"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetContentMoviesScoreTest" should "content movies score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "GHP",
        "1",
        "18JUL2018:14:35:19",
        "TM_CONTENT_MOVIES_SCORE",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "TM_CONTENT_MOVIES_SCORE",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""), Row("10551", ""), Row("10552", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("content_video_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "content_video_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10551", null), Row("10552", null), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetContentMusicScoreTest" should "content music score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "GHP",
        "1",
        "18JUL2018:14:35:19",
        "GHP_CONTENT_MUSIC_SCORE",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "TM_CONTENT_MUSIC_SCORE",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("content_music_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "content_music_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetContentGamesScoreTest" should "content games score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "GHP",
        "1",
        "18JUL2018:14:35:19",
        "GHP_CONTENT_GAMES_SCORE",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "TM_CONTENT_GAMES_SCORE",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("content_games_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "content_games_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest50" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_50",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_50",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_50_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_50_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest5" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_5",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_5",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_5_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_5_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest40" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_40",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_40",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_40_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_40_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest30" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_30",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_30",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_30_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_30_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest25" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_25",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_25",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_25_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_25_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest20" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_20",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_20",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_20_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_20_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest15" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_15",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_15",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_15_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_15_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

  "GetAirTimeScoreTest10" should "process for prepaid " +
  "airtime score" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleAAModelScoreData = List(
      Row(
        "10663",
        "0003909257",
        "1",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_GP_10",
        "2018-12"
      ),
      Row(
        "10550",
        "0003909204",
        "3",
        "TM-PREPAID",
        "1",
        "18JUL2018:14:35:19",
        "AIRTIME_SCORE_TM_10",
        "2018-12"
      )
    )

    val aaModelScoreSchema = StructType(
      List(
        StructField("subsId", StringType),
        StructField("msisdn", StringType),
        StructField("score", StringType),
        StructField("brand", StringType),
        StructField("updateDy", StringType),
        StructField("updateDttp", StringType),
        StructField("model", StringType),
        StructField("date", StringType)
      )
    )

    val aaModelScoreDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleAAModelScoreData),
        aaModelScoreSchema
      )

    val sampleProfList = List(Row("10663", ""), Row("10550", ""))

    val profileSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("airtime_loan_10_availer_topup_propensity_score", StringType)
      )
    )

    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleProfList),
      profileSchema
    )

    val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

    val sourceColumns = Array(
      SourceColumn("AAModelScore", "subsId"),
      SourceColumn("AAModelScore", "model"),
      SourceColumn("AAModelScore", "score")
    )

    val targetColumn: String = "airtime_loan_10_availer_topup_propensity_score"
    val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

    val joinedDF = AAModelLookupTransform.process(
      sourceDFs,
      targetDF,
      sourceColumns,
      targetJoinColumn,
      targetColumn
    )(spark, extraParameters)

    val actualDF = joinedDF
      .select(profileSchema.fieldNames.map(c => col(c)): _*)
      .sort(profileSchema.fieldNames.head)
      .collect()

    val expectedDF = Array(Row("10550", "3"), Row("10663", "1"))
    assertResult(expectedDF)(actualDF)
  }

}
