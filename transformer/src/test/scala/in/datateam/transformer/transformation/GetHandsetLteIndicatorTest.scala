package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetHandsetLteIndicatorTest extends FlatSpec with SparkTest {
  "GetHandsetLteIndicatorTest" should "check for Lte indicator for subscriber" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", "11221", "L", ""),
        Row("2", "11222", "L", ""),
        Row("3", "11223", "L", ""),
        Row("4", "11224", "L", ""),
        Row("5", "11225", "N", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("msisdn_value", StringType),
          StructField("subscriber_status_code", StringType),
          StructField("handset_lte_capable_indicator", StringType)
        )
      )
      val subcriberEntityList = List(
        Row("1", "11221", "L", "2019-01-16"),
        Row("2", "11222", "L", "2019-01-16"),
        Row("3", "11223", "L", "2019-01-16"),
        Row("4", "11224", "L", "2019-01-16"),
        Row("5", "11225", "N", "2019-01-16")
      )
      val subcriberEntityListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("msisdnValue", StringType),
          StructField("subscriberStatusCode", StringType),
          StructField("date", StringType)
        )
      )
      val networkDaily = List(
        Row("11221", "112", "2018-12-06T16:00:00.000Z", "2019-01-16"),
        Row("11221", "112", "2018-12-06T16:00:00.000Z", "2019-01-16"),
        Row("11222", "114", "2018-12-06T16:00:00.000Z", "2019-01-16"),
        Row("11223", "116", "2018-12-06T16:00:00.000Z", "2019-01-16"),
        Row("11224", "118", "2018-12-06T16:00:00.000Z", "2019-01-16"),
        Row("11225", "119", "2018-12-06T16:00:00.000Z", "2019-01-16")
      )
      val networkDailySchema = StructType(
        List(
          StructField("msisdnValue", StringType),
          StructField("deviceID", StringType),
          StructField("topLocationDate", StringType),
          StructField("date", StringType)
        )
      )
      val device = List(
        Row("112", "Y", "2019-01"),
        Row("114", "Y", "2019-01"),
        Row("116", "Y", "2019-01"),
        Row("118", "Y", "2019-01"),
        Row("119", "Y", "2019-01")
      )
      val deviceSchema = StructType(
        List(
          StructField("deviceId", StringType),
          StructField("is4gIndicator", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val networkDailyDF = spark.createDataFrame(
        spark.sparkContext.parallelize(networkDaily),
        networkDailySchema
      )
      val deviceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(device),
        deviceSchema
      )
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("DailyNetworkDataGITransactionTopLocation", networkDailyDF),
        ("Device", deviceDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("DailyNetworkDataGITransactionTopLocation", "msisdnValue"),
        SourceColumn("DailyNetworkDataGITransactionTopLocation", "deviceID"),
        SourceColumn(
          "DailyNetworkDataGITransactionTopLocation",
          "topLocationDate"
        ),
        SourceColumn("Device", "deviceId"),
        SourceColumn("Device", "is4gIndicator"),
        SourceColumn("Subscriber", "subscriberStatusCode"),
        SourceColumn("Subscriber", "subscriberId"),
        SourceColumn("Subscriber", "msisdnValue")
      )
      val joinColumns = Option(Array(JoinColumn("msisdnValue", "msisdn_value")))
      val targetColumn = "handset_lte_capable_indicator"
      val df = GetHandsetLteIndicator
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val collectedDF = df.sort("msisdn_value").collect().toList
      val expectList = List(
        Row("1", "11221", "L", "1"),
        Row("2", "11222", "L", "1"),
        Row("3", "11223", "L", "1"),
        Row("4", "11224", "L", "1"),
        Row("5", "11225", "N", null)
      )
      assertResult(expectList)(collectedDF)
  }
}
