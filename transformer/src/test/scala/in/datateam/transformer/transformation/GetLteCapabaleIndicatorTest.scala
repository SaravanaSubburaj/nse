package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetLteCapabaleIndicatorTest extends FlatSpec with SparkTest {
  "GetLteCapabaleIndicatorTest" should "direct derived LteIndicator" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(
      Row("1", ""),
      Row("2", ""),
      Row("3", ""),
      Row("4", ""),
      Row("5", "")
    )
    val profileListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("sim_lte_capable_indicator", StringType)
      )
    )

    val subcriberEntityList = List(
      Row("1", "LTE", "2019-01-16"),
      Row("2", "3G", "2019-01-16"),
      Row("3", "3G", "2019-01-16"),
      Row("4", "2G", "2019-01-16"),
      Row("5", "LTE", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("simTechnologyDescription", StringType),
        StructField("date", StringType)
      )
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val sourceDF = Array(("GlobeSubscriber", subsDF)).toMap
    val sourceColumns = Array(
      SourceColumn("GlobeSubscriber", "simTechnologyDescription"),
      SourceColumn("GlobeSubscriber", "subscriberId")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriberId")))
    val targetColumn = "sim_lte_capable_indicator"
    val df = GetLteCapabaleIndicator
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("subscriberId").collect().toList
    val expectList = List(
      Row("1", "1"),
      Row("2", "0"),
      Row("3", "0"),
      Row("4", "0"),
      Row("5", "1")
    )
    assertResult(expectList)(calulatedDF)
  }
}
