package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetTenureCountTest extends FlatSpec with SparkTest {
  "GetTenureCountTest" should "get tenure count" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList =
      List(Row("1", ""), Row("2", ""), Row("3", ""), Row("4", ""))
    val profileListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("tenure_count", StringType)
      )
    )

    val subcriberEntityList = List(
      Row("1", "2019-01-01", "2019-06-16", "2019-01-16"),
      Row("2", "2019-01-01", "2019-07-16", "2019-01-16"),
      Row("3", "2019-01-01", "2018-01-16", "2019-01-16"),
      Row("4", "2019-01-01", "2018-11-16", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("activationDate", StringType),
        StructField("churnDate", StringType),
        StructField("date", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )

    val sourceDF =
      Array(("GlobeSubscriber", subsDF)).toMap
    val sourceColumns = Array(
      SourceColumn("GlobeSubscriber", "subscriberId"),
      SourceColumn("GlobeSubscriber", "activationDate"),
      SourceColumn("GlobeSubscriber", "churnDate")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriberId")))
    val targetColumn = "tenure_count"
    val df = GetTenureCount
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("subscriberId").collect().toList
    val expectList =
      List(Row("1", 1), Row("2", 1), Row("3", 0), Row("4", 0))
    assertResult(expectList)(calulatedDF)
  }
}
