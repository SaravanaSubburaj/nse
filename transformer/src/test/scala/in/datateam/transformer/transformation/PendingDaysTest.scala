package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.temporal.ChronoUnit

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class PendingDaysTest extends FlatSpec with SparkTest {
  "PendingDaysTest" should "test PendingDays implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sample_rows = List(
      Row("1", "Person_P", "07/06/2019", null),
      Row("2", "Person_C", "14/02/2029", null),
      Row("3", "Person_M", "11/02/2020", null),
      Row("4", "Person_P", "07/03/1995", null),
      Row("5", "Person_Q", "20/01/2004", null),
      Row("6", "Person_R", "11/09/1997", null)
    )

    val schema = StructType(
      List(
        StructField("RowID", StringType, nullable = false),
        StructField("Name", StringType, nullable = false),
        StructField("Date", StringType, nullable = false),
        StructField("PendingDays", LongType, nullable = true)
      )
    )

    val sourceColumns = Array(SourceColumn("sample", "Date"))
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      schema
    )
    val calculatedDF = PendingDays
      .process(sampleDF, sourceColumns, "PendingDays")(spark, extraParameters)
    val collectedValidatedDF = calculatedDF.collect()

    def count(date: String): Long = {
      val dateval = LocalDate.parse(date)
      val Count = ChronoUnit.DAYS.between(LocalDate.now(), dateval)
      Count
    }

    val expectedCollectedValidatedDF = List(
      Row("1", "Person_P", "07/06/2019", count("2019-06-07")),
      Row("2", "Person_C", "14/02/2029", count("2029-02-14")),
      Row("3", "Person_M", "11/02/2020", count("2020-02-11")),
      Row("4", "Person_P", "07/03/1995", count("1995-03-07")),
      Row("5", "Person_Q", "20/01/2004", count("2004-01-20")),
      Row("6", "Person_R", "11/09/1997", count("1997-09-11"))
    )

    assertResult(expectedCollectedValidatedDF)(collectedValidatedDF)
  }

}
