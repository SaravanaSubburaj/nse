package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class ADHFilterTest extends FlatSpec with SparkTest {
  "ADHFilterTest" should "filter ADH event data" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleData = List(
      Row("Globe SMS Charge", 10, 10, 2, "1"),
      Row("Globe Voice Charge", 20, 30, 3, "2"),
      Row("Globe Data Traffic Charge", 30, 49, 4, "3"),
      Row("Globe Data Traffic Charge", 40, 23, null, "4"),
      Row("Globe Voice Charge", 0, 12, 2, "5"),
      Row("Globe SMS Charge", 50, 50, 3, "6"),
      Row("Globe Broadband Charge", 60, 60, 7, "7")
    )

    val adhSchema = StructType(
      List(
        StructField("eventType", StringType),
        StructField("freeAllowanceAmount", IntegerType),
        StructField("chargeAmount", IntegerType),
        StructField("allowanceUnitsApplied", IntegerType),
        StructField("subscriberKey", StringType)
      )
    )

    val adhDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(sampleData),
        adhSchema
      )

    val sourceColumns = Array(
      SourceColumn("mutables_usagepostpaidmutable", "eventType"),
      SourceColumn("mutables_usagepostpaidmutable", "freeAllowanceAmount"),
      SourceColumn("mutables_usagepostpaidmutable", "chargeAmount"),
      SourceColumn("mutables_usagepostpaidmutable", "allowanceUnitsApplied"),
      SourceColumn("mutables_usagepostpaidmutable", "subscriberkey")
    )

    val processedDF =
      ADHFilter
        .process(adhDF, sourceColumns, "last_core_ppu_date")(
          spark,
          extraParameters
        )
        .sort("subscriberkey")

    val expectedData = Array(
      Row("1", null, "2019-01-16", "2019-01-16"),
      Row("2", null, "2019-01-16", "2019-01-16"),
      Row("3", "2019-01-16", null, "2019-01-16"),
      Row("6", null, "2019-01-16", "2019-01-16")
    )

    val expectedSchema = StructType(
      List(
        StructField("subscriberkey", StringType),
        StructField("last_data_ppu_date", StringType),
        StructField("last_core_ppu_date", StringType),
        StructField("date", StringType)
      )
    )

    val expectedDF =
      spark
        .createDataFrame(
          spark.sparkContext.parallelize(expectedData),
          expectedSchema
        )
        .sort("subscriberkey")

    assertResult(expectedDF.collectAsList())(processedDF.collectAsList())
  }

}
