package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMostFreqAvailAllDayRoamSurfPrepaidTest extends FlatSpec with SparkTest {
  "join test" should "Test join operation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val subscriberEntitiyInput = List(
      Row(
        "1",
        "sunil",
        "saini",
        "12/01/2018",
        "ghp",
        "515023409285162",
        "9175882420",
        "GSM",
        "2019-01-16"
      ),
      Row(
        "2",
        "prabhu",
        "gs",
        "12/02/2018",
        "ghp",
        "515023409285163",
        "9175882421",
        "GSM",
        "2019-01-16"
      ),
      Row(
        "3",
        "Ram",
        "Kri",
        "12/01/2018",
        "ghp",
        "515023409285162",
        "9175882420",
        "GSM",
        "2019-01-16"
      ),
      Row(
        "4",
        "Ramk",
        "Kri",
        "12/02/2018",
        "ghp",
        "515023409285163",
        "9175882421",
        "GSM",
        "2019-01-16"
      )
    )

    val schema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("firstName", StringType),
        StructField("lastName", StringType),
        StructField("activationDate", StringType),
        StructField("brandType", StringType),
        StructField("imsi", StringType),
        StructField("msisdn", StringType),
        StructField("subscriptionType", StringType),
        StructField("date", StringType)
      )
    )

    val prepaidPromoAvailment = List(
      Row("1", "GS50_BASE3", 5.0, "S1", "2018-12-01 18:16:46", "2019-01-16"),
      Row("2", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", "2019-01-16"),
      Row("1", "GS50_BASE3", 3.0, "S1", "2018-12-01 18:16:46", "2019-01-16"),
      Row("2", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", "2019-01-16"),
      Row("1", "GS50_BASE3", 3.0, "S1", "2018-12-01 18:16:46", "2019-01-16"),
      Row("3", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", "2019-01-16"),
      Row("1", "GS50_BASE2", 3.0, "S1", "2018-12-01 18:16:46", "2019-01-16"),
      Row("1", "GS50_BASE3", 3.0, "S1", "2018-12-01 18:16:46", "2019-01-16"),
      Row("4", "GS50_BASE2", 5.0, "S2", "2018-12-01 18:16:46", "2019-01-16")
    )

    val prepaidPromoAvailmentSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("prepaidPromoId", StringType),
        StructField("promoChargeAmount", DoubleType),
        StructField("serviceId", StringType),
        StructField("promoOPerationDatetime", StringType),
        StructField("date", StringType)
      )
    )

    val prepaidPromo = List(
      Row("FREE_WNP2_2GB3", "ALLNETPROMO_USSD_30D", "2019-01-16"),
      Row("ALLNETPROMO_USSD_30D3", "ALLNETPROMO_USSD_50D", "2019-01-16"),
      Row("GS50_BASE2", "EASYROAMSURF150", "2019-01-16"),
      Row("GS50_BASE3", "ROAMSURF150", "2019-01-16")
    )
    val prepaidPromoSchema = StructType(
      List(
        StructField("prepaidPromoID", StringType),
        StructField("prepaidPromoDescription", StringType),
        StructField("date", StringType)
      )
    )

    val date = "2019-01-16"
    val targetList = List(
      Row("3", "EASYROAMSURF150", date),
      Row("1", "ROAMSURF150", date),
      Row("4", "EASYROAMSURF150", date),
      Row("2", "EASYROAMSURF150", date)
    )

    val schema2 = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField(
          "mostFrequentAvailedAllayRoamSurfVariantName",
          StringType
        ),
        StructField("date", StringType)
      )
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext.parallelize(subscriberEntitiyInput),
      schema
    )
    val prePromoAvailDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(prepaidPromoAvailment),
        prepaidPromoAvailmentSchema
      )
    val prePrmoDF = spark.createDataFrame(
      spark.sparkContext.parallelize(prepaidPromo),
      prepaidPromoSchema
    )
    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(targetList),
      schema2
    )
    val sourceDF = Array(
      ("Subscriber", subsDF),
      ("globePrepaidPromoAvailmentStaging", prePromoAvailDF),
      ("PrepaidPromo", prePrmoDF)
    ).toMap
    val sourceColumns = Array(
      SourceColumn("globePrepaidPromoAvailmentStaging", "subscriberId"),
      SourceColumn("globePrepaidPromoAvailmentStaging", "prepaidPromoId"),
      SourceColumn("PrepaidPromo", "prepaidPromoID"),
      SourceColumn("PrepaidPromo", "prepaidPromoDescription"),
      SourceColumn("globePrepaidPromoAvailmentStaging", "promoChargeAmount"),
      SourceColumn(
        "globePrepaidPromoAvailmentStaging",
        "promoOPerationDatetime"
      ),
      SourceColumn("globePrepaidPromoAvailmentStaging", "serviceId")
    )

    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriberId")))
    val targetColumn = "mostFrequentAvailedAllayRoamSurfVariantName"
    val df = GetMostFreqAvailAllDayRoamSurfPrepaid
      .process(sourceDF, targetDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collecteDF = df
      .select(schema2.fieldNames.map(c => col(c)): _*)
      .sort(schema2.fieldNames.head)
      .collect()
      .toList
    val expectList = List(
      Row("1", "ROAMSURF150", date),
      Row("2", "EASYROAMSURF150", date),
      Row("3", "EASYROAMSURF150", date),
      Row("4", "EASYROAMSURF150", date)
    )
    assertResult(expectList)(collecteDF)
  }
}
