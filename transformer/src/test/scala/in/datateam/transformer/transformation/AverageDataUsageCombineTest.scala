package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class AverageDataUsageCombineTest extends FlatSpec with SparkTest {
  "AverageDataUsageCombineTest" should "Test prepaid and postpaid usage_average_data_quantity_rolling_90days_mb " +
  "combined in a single attribute" in withSparkSession { spark: SparkSession =>
    val sourceColumns = Array(
      SourceColumn("GlobeDailyPrepaidSubscriberUsage", "subscriberId"),
      SourceColumn("GlobeDailyPrepaidSubscriberUsage", "usageTypeCode"),
      SourceColumn("GlobeDailyPrepaidSubscriberUsage", "totalDataVolumeCount"),
      SourceColumn("GlobeDailyPrepaidSubscriberUsage", "totalChargeableRoundedUnitCount"),
      SourceColumn("GlobeDailyPrepaidSubscriberUsage", "usageTransactionDate"),
      SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "subscriberId"),
      SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "usageTypeCode"),
      SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "totalDataVolumeCount"),
      SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "totalChargeableRoundedUnitCount"),
      SourceColumn("GlobeDailyPostpaidSubscriberUsageStaging", "usageTransactionDate")
    )
    val joinColumns = Option(
      Array(JoinColumn("subscriberId", "subscriber_id"))
    )
    val date = "2019-01-16"
    val subscriberList = List(
      Row("1", date),
      Row("2", date),
      Row("3", date)
    )
    val subscriberDFSchema = StructType(
      List(
        StructField("subscriber_id", StringType, nullable = false),
        StructField("date", StringType, nullable = false)
      )
    )

    val globeDailyPrepaidSubscriberUsageSchema = StructType(
      List(
        StructField("subscriberId", dataType = StringType),
        StructField("usageTypeCode", dataType = StringType),
        StructField("totalDataVolumeCount", dataType = IntegerType),
        StructField("totalChargeableRoundedUnitCount", dataType = IntegerType),
        StructField("usageTransactionDate", dataType = StringType),
        StructField("date", dataType = StringType, nullable = false)
      )
    )
    val globeDailyPostpaidSubscriberUsageStagingSchema = StructType(
      List(
        StructField("subscriberId", dataType = StringType),
        StructField("usageTypeCode", dataType = StringType),
        StructField("totalDataVolumeCount", dataType = IntegerType),
        StructField("totalChargeableRoundedUnitCount", dataType = IntegerType),
        StructField("usageTransactionDate", dataType = StringType),
        StructField("date", dataType = StringType, nullable = false)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("subscriber_id", StringType)
      )
    )

    val globeDailyPrepaidSubscriberUsageList = List(
      Row("1", "SMS", 10000, 2000, "2019-01-04", date),
      Row("2", "DATA", 13000, 3000, "2019-01-12", date),
      Row("3", "DATA", 16000, 4000, "2019-01-07", date)
    )
    val globeDailyPostpaidSubscriberUsageStagingList = List(
      Row("1", "SMS", 11000, 5000, "2019-01-04", date),
      Row("2", "DATA", 15000, 4000, "2019-01-12", date),
      Row("3", "DATA", 17000, 1000, "2019-01-07", date)
    )

    val profileList = List(Row("1"), Row("2"), Row("3"))

    val subscriberDF = spark.createDataFrame(spark.sparkContext.parallelize(subscriberList), subscriberDFSchema)

    val globeDailyPrepaidSubscriberUsageDF = spark.createDataFrame(
      spark.sparkContext.parallelize(globeDailyPrepaidSubscriberUsageList),
      globeDailyPrepaidSubscriberUsageSchema
    )

    val globeDailyPostpaidSubscriberUsageStagingDF = spark.createDataFrame(
      spark.sparkContext.parallelize(globeDailyPostpaidSubscriberUsageStagingList),
      globeDailyPostpaidSubscriberUsageStagingSchema
    )

    val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileSchema)

    implicit val extraParameters: Map[String, String] = Map(TransformerEnums.PARTITION_VALUE -> date)

    val sourceDFs = Map(
      "Subscriber"                               -> subscriberDF,
      "GlobeDailyPrepaidSubscriberUsage"         -> globeDailyPrepaidSubscriberUsageDF,
      "GlobeDailyPostpaidSubscriberUsageStaging" -> globeDailyPostpaidSubscriberUsageStagingDF
    )
    val targetColumn = "usage_average_data_quantity_rolling_90days_mb"

    val averageDataUsageCombineDF = AverageDataUsageCombine
      .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

    val actual = averageDataUsageCombineDF.sort(col("subscriber_id")).collect()
    val expected = Array(
      Row("1", null),
      Row("2", 0.004132588704427083),
      Row("3", 0.005086263020833333)
    )
    assertResult(expected)(actual)
  }
}
