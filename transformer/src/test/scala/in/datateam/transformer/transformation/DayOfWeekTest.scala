package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class DayOfWeekTest extends FlatSpec with SparkTest {
  "DayOfWeekTest" should "test DayOfWeek implementation" in withSparkSession { spark: SparkSession =>
    val sample_rows = List(
      Row("1", "Person_P", "1995-06-07 00:00:00.000", ""),
      Row("2", "Person_C", "1995-02-14 00:00:00.000", ""),
      Row("3", "Person_M", "1994-02-11 00:00:00.000", ""),
      Row("4", "Person_X", "1994/02/11", ""),
      Row("5", "Person_Y", "11/02/1994", "")
    )

    val schema = StructType(
      List(
        StructField("RowID", StringType, nullable = false),
        StructField("Name", StringType, nullable = false),
        StructField("DOB", StringType, nullable = false),
        StructField("Day_of_week", StringType, nullable = true)
      )
    )

    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      schema
    )
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val transformedDF = DayOfWeek
      .process(sampleDF, Array(SourceColumn("sample", "DOB")), "Day_of_week")(
        spark,
        extraParameters
      )
    val collectedTransformedDF = transformedDF.collect()

    val expectedCollectedValidatedDF = List(
      Row("1", "Person_P", "1995-06-07 00:00:00.000", "Wednesday"),
      Row("2", "Person_C", "1995-02-14 00:00:00.000", "Tuesday"),
      Row("3", "Person_M", "1994-02-11 00:00:00.000", "Friday"),
      Row("4", "Person_X", "1994/02/11", ""),
      Row("5", "Person_Y", "11/02/1994", "")
    )

    assertResult(expectedCollectedValidatedDF)(collectedTransformedDF)

  }
}
