package in.datateam.transformer.transformation
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

/**
  * @author Saravana(saravana.subburaj@thedatateam.in)
  */

class GetArpuCategoryCodeCombineTest extends FlatSpec with SparkTest {
  "GetArpuCategoryCodeCombineTest" should "Test prepaid and postpaid arpu_category_code combined in a single attribute" in withSparkSession {
    spark: SparkSession =>
      val sourceColumns = Array(
        SourceColumn("MonthlyPrepaidSubscriberRevenueSummary", "grossServiceRevenueIndicative"),
        SourceColumn("MonthlyPrepaidSubscriberRevenueSummary", "subscriberId"),
        SourceColumn("MonthlyPostpaidSubscriberRevenueSummary", "grossServiceRevenueIndicative"),
        SourceColumn("MonthlyPostpaidSubscriberRevenueSummary", "subscriberId")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subscriber_id"))
      )

      val subscriberList = List(
        Row("1", "2019-01-16"),
        Row("2", "2019-01-16"),
        Row("3", "2019-01-16")
      )
      val subscriberSchema = StructType(
        List(
          StructField("subscriber_id", StringType, nullable = false),
          StructField("date", StringType, nullable = false)
        )
      )
      val monthlyPrepaidSubscriberRevenueSummarySchema = StructType(
        List(
          StructField("grossServiceRevenueIndicative", dataType = StringType),
          StructField("subscriberId", dataType = StringType),
          StructField("date", dataType = StringType)
        )
      )

      val monthlyPostpaidSubscriberRevenueSummarySchema = StructType(
        List(
          StructField("grossServiceRevenueIndicative", dataType = StringType),
          StructField("subscriberId", dataType = StringType),
          StructField("date", dataType = StringType)
        )
      )

      val monthlyPrepaidSubscriberRevenueSummaryList = List(
        Row("90000", "1", "2019-01"),
        Row("6500", "2", "2019-01"),
        Row("5555", "3", "2019-01")
      )

      val monthlyPostpaidSubscriberRevenueSummaryList = List(
        Row("60000", "1", "2019-01"),
        Row("4500", "2", "2019-01"),
        Row("5000", "3", "2019-01")
      )

      val subscriberDF = spark.createDataFrame(spark.sparkContext.parallelize(subscriberList), subscriberSchema)

      val monthlyPrepaidSubscriberRevenueSummaryDF = spark.createDataFrame(
        spark.sparkContext.parallelize(monthlyPrepaidSubscriberRevenueSummaryList),
        monthlyPrepaidSubscriberRevenueSummarySchema
      )

      val monthlyPostpaidSubscriberRevenueSummaryDF = spark.createDataFrame(
        spark.sparkContext.parallelize(monthlyPostpaidSubscriberRevenueSummaryList),
        monthlyPostpaidSubscriberRevenueSummarySchema
      )

      implicit val extraParameters: Map[String, String] = Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val profileList = List(Row("1"), Row("2"), Row("3"))
      val profileSchema = StructType(
        List(
          StructField("subscriber_id", StringType)
        )
      )

      val sourceDFs = Map(
        "Subscriber"                              -> subscriberDF,
        "MonthlyPrepaidSubscriberRevenueSummary"  -> monthlyPrepaidSubscriberRevenueSummaryDF,
        "MonthlyPostpaidSubscriberRevenueSummary" -> monthlyPostpaidSubscriberRevenueSummaryDF
      )

      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileSchema)

      val targetColumn = "arpu_category_code"
      val arpuCategoryCodeCombineDF = GetArpuCategoryCodeCombine
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actual = arpuCategoryCodeCombineDF.collect()

      val expected = List(
        Row("3", "D"),
        Row("1", "A"),
        Row("2", "C")
      )

      assertResult(expected)(actual)
  }
}
