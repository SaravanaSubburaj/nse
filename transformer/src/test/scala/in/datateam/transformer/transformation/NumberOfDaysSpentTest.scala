package in.datateam.transformer.transformation

import java.time.LocalDate
import java.time.temporal.ChronoUnit

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class NumberOfDaysSpentTest extends FlatSpec with SparkTest {
  "DaySpentTest" should "test DaySpent implementation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sample_rows = List(
      Row("1", "Person_P", "07/06/1995", null),
      Row("2", "Person_C", "14/02/1995", null),
      Row("3", "Person_M", "11/02/1994", null),
      Row("4", "Person_P", "01/01/2019", null),
      Row("5", "Person_Q", "01/01/2020", null),
      Row("6", "Person_R", "03/05/2030", null)
    )

    val schema = StructType(
      List(
        StructField("RowID", StringType, nullable = false),
        StructField("Name", StringType, nullable = false),
        StructField("Date", StringType, nullable = false),
        StructField("DaySpent", LongType, nullable = true)
      )
    )

    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_rows),
      schema
    )
    val validatedDF = NumberOfDaysSpent
      .process(sampleDF, Array(SourceColumn("sample", "Date")), "DaySpent")(
        spark,
        extraParameters
      )
    val collectedValidatedDF = validatedDF.collect()

    def count(date: String): Long = {
      val dateval = LocalDate.parse(date)
      val Count = ChronoUnit.DAYS.between(dateval, LocalDate.now())
      Count
    }

    val expectedCollectedValidatedDF = List(
      Row("1", "Person_P", "07/06/1995", count("1995-06-07")),
      Row("2", "Person_C", "14/02/1995", count("1995-02-14")),
      Row("3", "Person_M", "11/02/1994", count("1994-02-11")),
      Row("4", "Person_P", "01/01/2019", count("2019-01-01")),
      Row("5", "Person_Q", "01/01/2020", count("2020-01-01")),
      Row("6", "Person_R", "03/05/2030", count("2030-05-03"))
    )

    assertResult(expectedCollectedValidatedDF)(collectedValidatedDF)
  }
}
