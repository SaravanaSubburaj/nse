package in.datateam.transformer.transformation

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types._

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn
import in.datateam.utils.enums.CommonEnums

class JoinDerivePullIntTravelIndTest extends FlatSpec with SparkTest {

  "JoinDerivePullIntTravelIndPostpaidTest" should
  "test JoinDerivePullIntTravelInd implementation" in withSparkSession { (spark: SparkSession) =>
    val partitionDate: String = "2019-01-16"
    implicit val extraParameters = Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sample_assigned_billing_offer = List(
      Row("1", "Person_P", "Data", partitionDate),
      Row("1", "Person_P", "SuperRoamPlusData", partitionDate),
      Row("2", "Person_C", "SMSunlimited", partitionDate),
      Row("2", "Person_C", "UnlimitedRoamCalls", partitionDate),
      Row("3", "Person_M", "SMSunlimited", partitionDate),
      Row("3", "Person_M", "StandardData", partitionDate),
      Row("3", "Person_M", "StandardVoice", partitionDate),
      Row("4", "Person_N", "StandardVoice", partitionDate),
      Row("5", "Person_O", "StandardVoice", partitionDate)
    )

    val sample_subs = List(
      Row("1", "Person_P"),
      Row("2", "Person_C"),
      Row("3", "Person_M"),
      Row("4", "Person_N"),
      Row("5", "Person_O")
    )

    val sample_usage = List(
      Row("1", "Person_P", "r", partitionDate),
      Row("2", "Person_C", "s", partitionDate),
      Row("3", "Person_M", "s", partitionDate),
      Row("4", "Person_N", "t", partitionDate),
      Row("5", "Person_O", "r", partitionDate)
    )

    val profile_rows = List(
      Row("1", "Person_P", partitionDate, null), // Date col represents partitionDate and
      // will be used for filter conditions
      Row("2", "Person_C", partitionDate, null),
      Row("3", "Person_M", partitionDate, null),
      Row("4", "Person_N", partitionDate, null),
      Row("5", "Person_O", partitionDate, null)
    )

    val sampleAssignedBillingOfferSchema = StructType(
      List(
        StructField("RowID", StringType),
        StructField("Name", StringType),
        StructField("OfferDesc", StringType),
        StructField("date", StringType)
      )
    )

    val sampleUsageSchema = StructType(
      List(
        StructField("RowID", StringType),
        StructField("Name", StringType),
        StructField("UsageType", StringType),
        StructField("date", StringType)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("profRowID", StringType),
        StructField("profName", StringType),
        StructField("partitionDate", StringType),
        StructField("IntlTravelIndicator", IntegerType, true)
      )
    )

    val sampleSubscriberSchema = StructType(
      List(StructField("RowID", StringType), StructField("Name", StringType))
    )

    val sampleAssignedBillingOfferDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_assigned_billing_offer),
      sampleAssignedBillingOfferSchema
    )

    val sampleSubscriberDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_subs),
      sampleSubscriberSchema
    )

    val sampleUsageDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sample_usage),
      sampleUsageSchema
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profile_rows),
      profileSchema
    )

    val transformedDF = JoinDerivePullIntTravelInd.process(
      Map[String, DataFrame](
        "sample_subs"                   -> sampleSubscriberDF,
        "sample_usage"                  -> sampleUsageDF,
        "sample_assigned_billing_offer" -> sampleAssignedBillingOfferDF
      ),
      profileDF,
      Array(
        SourceColumn("sample_subs", "RowID"),
        SourceColumn("sample_assigned_billing_offer", "RowID"),
        SourceColumn("sample_usage", "RowID"),
        SourceColumn("sample_usage", "UsageType"),
        SourceColumn("sample_assigned_billing_offer", "OfferDesc")
      ),
      Option(Array(JoinColumn("RowID", "profRowID"))),
      "IntlTravelIndicatorPostpaid"
    )(spark, extraParameters)

    val addIntlTravelIndicatorPrepaidDF = transformedDF
      .withColumn("IntlTravelIndicatorPrepaid", lit("").cast("double"))

    val combinedIntlTravelIndicatorDF = CombinePrepaidAndPostpaidOperator
      .process(
        addIntlTravelIndicatorPrepaidDF,
        Array(
          SourceColumn(
            CommonEnums.PROFILE_MASTER,
            "IntlTravelIndicatorPrepaid"
          ),
          SourceColumn(
            CommonEnums.PROFILE_MASTER,
            "IntlTravelIndicatorPostpaid"
          )
        ),
        "IntlTravelIndicator"
      )(spark, extraParameters)

    val sortedTransformedDF =
      combinedIntlTravelIndicatorDF.sort(col("profRowID"))
    val transformedCollectedDF = sortedTransformedDF.collect()

    val expectedDF = List(
      Row("1", "Person_P", partitionDate, 1),
      Row("2", "Person_C", partitionDate, 0),
      Row("3", "Person_M", partitionDate, 0),
      Row("4", "Person_N", partitionDate, 0),
      Row("5", "Person_O", partitionDate, 0)
    )

    assertResult(expectedDF)(transformedCollectedDF)
  }

}
