package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class AAModelLookupWithBrandFilterTest extends FlatSpec with SparkTest {
  "AAModelLookupWithBrandFilterTest" should "test AAmodel which have brand filter" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "NCR - National Capital Region|METRO MANILA|PASAY CITY|Barangay",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "WORK_LOCATION",
          "2018-12"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(Row("10663", ""))

      val profileSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("work_barangay_name", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score"),
        SourceColumn("AAModelScore", "brand")
      )

      val targetColumn: String = "work_barangay_name"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = AAModelLookupWithBrandFilter.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(Row("10663", "Barangay"))
      assertResult(expectedDF)(actualDF)
  }
}
