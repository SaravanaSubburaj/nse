package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullWithPaymentCategoryTest extends FlatSpec with SparkTest {
  "JoinDirectPullWithPaymentCategoryTest" should "join and direct pull from payment category Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val contactEntityList = List(
        Row("1", "Prepaid Payment", "2019-01-16"),
        Row("2", "Postpaid Payment", "2019-01-16"),
        Row("3", "Hybrid", "2019-01-16")
      )
      val contactEntityListSchema = StructType(
        List(
          StructField("paymentCategoryCode", StringType),
          StructField("paymentCategoryDescription", StringType),
          StructField("date", StringType)
        )
      )

      val profileEntityList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", "")
      )
      val profileEntityListSchema = StructType(
        List(
          StructField("payment_category_code", StringType),
          StructField("payment_category_description", StringType)
        )
      )

      val paymentCategoryDF = spark.createDataFrame(
        spark.sparkContext.parallelize(contactEntityList),
        contactEntityListSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileEntityList),
        profileEntityListSchema
      )

      val sourceDFs = Array(("payment_category", paymentCategoryDF)).toMap

      val sourceColumns = Array(
        SourceColumn("payment_category", "paymentCategoryDescription")
      )

      val joinColumns = Option(Array(JoinColumn("paymentCategoryCode", "payment_category_code")))

      val targetColumn = "payment_category_description"

      val joinedDF = JoinDirectPullWithPaymentCategory
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actualList = joinedDF.sort("payment_category_code").collect().toList

      val expectedList = List(
        Row("1", "Prepaid Payment"),
        Row("2", "Postpaid Payment"),
        Row("3", "Hybrid")
      )

      assertResult(expectedList)(actualList)

  }

}
