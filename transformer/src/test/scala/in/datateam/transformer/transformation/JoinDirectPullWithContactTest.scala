package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullWithContactTest extends FlatSpec with SparkTest {
  "JoinDirectPullWithContactTest" should "join and direct pull from contact Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val contactEntityList = List(
        Row("1", "Data Engineer", "2019-01-16"),
        Row("2", "Data Architect", "2019-01-16"),
        Row("3", "Data Scientist", "2019-01-16")
      )
      val contactEntityListSchema = StructType(
        List(
          StructField("contactId", StringType),
          StructField("contactOccupationText", StringType),
          StructField("date", StringType)
        )
      )

      val profileEntityList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", "")
      )
      val profileEntityListSchema = StructType(
        List(
          StructField("main_contact_id", StringType),
          StructField("occupation_text", StringType)
        )
      )

      val customerDF = spark.createDataFrame(
        spark.sparkContext.parallelize(contactEntityList),
        contactEntityListSchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileEntityList),
        profileEntityListSchema
      )

      val sourceDFs = Array(("contact", customerDF)).toMap

      val sourceColumns = Array(
        SourceColumn("contact", "contactOccupationText")
      )

      val joinColumns = Option(Array(JoinColumn("contactID", "main_contact_id")))

      val targetColumn = "occupation_text"

      val joinedDF = JoinDirectPullWithContact
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actualList = joinedDF.sort("main_contact_id").collect().toList

      val expectedList = List(
        Row("1", "Data Engineer"),
        Row("2", "Data Architect"),
        Row("3", "Data Scientist")
      )

      assertResult(expectedList)(actualList)

  }

}
