package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullSegmentIdTest extends FlatSpec with SparkTest {

  "JoinDirectPullTestDPA" should "Take 2 dataframes make a Join based on Multiple Col name match" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleSubsList = List(
        Row(
          "1",
          "GRP",
          "sub-GRP",
          "pre-cust1",
          "pre-sub-cust1",
          "PR-101",
          "2019-01-16"
        ),
        Row(
          "2",
          "GRS",
          "sub-GRS",
          "pre-cust2",
          "pre-sub-cust2",
          "PR-102",
          "2019-01-16"
        ),
        Row(
          "3",
          "GPS",
          "sub-GRS",
          "pre-cust2",
          "pre-sub-cust2",
          "PR-102",
          "2019-01-16"
        )
      )

      val schemaSubs = StructType(
        List(
          StructField("subsSubscriberId", StringType),
          StructField("brandTypeCode", StringType),
          StructField("brandSubTypeCode", StringType),
          StructField("customerFacingUnitTypeCode", StringType),
          StructField("customerFacingUnitSubTypecode", StringType),
          StructField("productTypeCode", StringType),
          StructField("date", StringType)
        )
      )
      val sampleSubsDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleSubsList),
        schemaSubs
      )

      val sampleSegList = List(
        Row("100", "GRP", "sub-GRP", "pre-cust1", "pre-sub-cust1", "PR-101"),
        Row("200", "GRS", "sub-GRS", "pre-cust2", "pre-sub-cust2", "PR-102"),
        Row("300", "APS", "sub-APS", "ATI", "sub-ATI", "PR-103")
      )

      val schemaSegm = StructType(
        List(
          StructField("segmentId", StringType),
          StructField("subscriberBrand", StringType),
          StructField("subscriberSubBrand", StringType),
          StructField("customerTypeKey", StringType),
          StructField("customerSubTypeKey", StringType),
          StructField("subscriberTypeKey", StringType)
        )
      )
      val sampleSegDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleSegList),
        schemaSegm
      )

      val sampleProfList = List(
        Row("1", "Prasad", ""),
        Row("2", "Amit", ""),
        Row("6", "david", ""),
        Row("4", "Akash", ""),
        Row("5", "Sharma", "")
      )

      val schemaProf = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("subsName", StringType),
          StructField("segmentId", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        schemaProf
      )

      val sourceDFs =
        Map("subscriber" -> sampleSubsDF, "segment" -> sampleSegDF)
      val sourceColumns = Array(
        SourceColumn("subscriber", "brandTypeCode"),
        SourceColumn("segment", "subscriberBrand"),
        SourceColumn("subscriber", "brandSubTypeCode"),
        SourceColumn("segment", "subscriberSubBrand"),
        SourceColumn("subscriber", "customerFacingUnitTypeCode"),
        SourceColumn("segment", "customerTypeKey"),
        SourceColumn("subscriber", "customerFacingUnitSubTypecode"),
        SourceColumn("segment", "customerSubTypeKey"),
        SourceColumn("subscriber", "productTypeCode"),
        SourceColumn("segment", "subscriberTypeKey"),
        SourceColumn("segment", "segmentId")
      )
      val targetColumn: String = "segmentId"

      val targetJoinColumn =
        Option(Array(JoinColumn("subsSubscriberId", "subscriberId")))
      val joinedDF = JoinDirectPullSegmentId
        .process(
          sourceDFs,
          targetDF,
          sourceColumns,
          targetJoinColumn,
          targetColumn
        )(spark, extraParameters)
      val actualDF = joinedDF
        .sort("subscriberId")
        .sort(schemaProf.fieldNames.head)
        .collect()
        .toList

      val expectedDF = List(
        Row("1", "Prasad", "100"),
        Row("2", "Amit", "200"),
        Row("4", "Akash", null),
        Row("5", "Sharma", null),
        Row("6", "david", null)
      )
      assertResult(actualDF)(expectedDF)

  }
}
