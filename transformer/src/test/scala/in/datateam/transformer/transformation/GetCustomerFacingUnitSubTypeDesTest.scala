package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetCustomerFacingUnitSubTypeDesTest extends FlatSpec with SparkTest {
  "JoinDirectPullWithEntityTest" should "join and direct pull from invoiceStaging" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", "K", "G", "", "2019-01-16"),
        Row("2", "L", "H", "", "2019-01-16"),
        Row("3", "K", "I", "", "2019-01-16"),
        Row("4", "M", "J", "", "2019-01-16"),
        Row("5", "N", "O", "", "2019-01-16")
      )
      val profileListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("customer_facing_unit_type_code", StringType),
          StructField("customer_facing_unit_sub_type_code", StringType),
          StructField("customer_facing_unit_sub_type_description", StringType),
          StructField("date", StringType)
        )
      )

      val attributeEntity = List(
        Row("K", "30GB", "G", "2019-01-16", "2019-01-16"),
        Row("L", "2GB", "H", "2019-01-16", "2019-01-16"),
        Row("K", "30GB", "I", "2019-01-16", "2019-01-16"),
        Row("M", "40GB", "J", "2019-01-16", "2019-01-16"),
        Row("N", "50GB", "O", "2019-01-16", "2019-01-16")
      )
      val attributeEntitySchema = StructType(
        List(
          StructField("customerFacingUnitTypeCode", StringType),
          StructField("customerFacingUnitSubTypeDescription", StringType),
          StructField("customerFacingUnitSubTypeCode", StringType),
          StructField("sourceInsertDate", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val entityDF = spark.createDataFrame(
        spark.sparkContext.parallelize(attributeEntity),
        attributeEntitySchema
      )
      val sourceDF = Array(
        ("customerFacingUnitSubType", entityDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn(
          "customerFacingUnitSubType",
          "customerFacingUnitSubTypeDescription"
        )
      )
      val joinColumns =
        Option(
          Array(
            JoinColumn("customerFacingUnitTypeCode", "customer_facing_unit_type_code"),
            JoinColumn("customerFacingUnitSubTypeCode", "customer_facing_unit_sub_type_code")
          )
        )
      val targetColumn = "customer_facing_unit_sub_type_description"
      val df = GetCustomerFacingUnitSubTypeDes
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calculatedDF = df.sort("subscriberId").collect().toList
      val expectList = List(
        Row("1", "K", "G", "30GB", "2019-01-16"),
        Row("2", "L", "H", "2GB", "2019-01-16"),
        Row("3", "K", "I", "30GB", "2019-01-16"),
        Row("4", "M", "J", "40GB", "2019-01-16"),
        Row("5", "N", "O", "50GB", "2019-01-16")
      )
      assertResult(expectList)(calculatedDF)
  }
}
