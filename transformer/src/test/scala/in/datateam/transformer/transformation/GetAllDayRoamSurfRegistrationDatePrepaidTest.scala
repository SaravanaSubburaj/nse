package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetAllDayRoamSurfRegistrationDatePrepaidTest extends FlatSpec with SparkTest {
  "GetAllDayRoamSurfRegistrationDatePrepaidTest" should "get roam " +
  "surf Reistration Date for PrepaidTest" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(Row("1", ""))
    val profileListSchema = StructType(
      List(
        StructField("subsSubscriberID", StringType),
        StructField("allDayRoamSurfLastRegistrationDatePrepaid", StringType)
      )
    )
    val subscriberEntityList = List(Row("1"))
    val subscriberEntityListSchema =
      StructType(List(StructField("subsSubscriberID", StringType)))

    val globePrepaidPromoAvailmentStaging =
      List(Row("1", "2019-01-07", "112", "2019-01-16"))
    val globePrepaidPromoAvailmentStagingSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("promoRegistrationDatetime", StringType),
        StructField("prepaidPromoId", StringType),
        StructField("date", StringType)
      )
    )
    val PrepaidPromo = List(Row("112", "roamsurf", "2019-01-16"))
    val PrepaidPromoSchema = StructType(
      List(
        StructField("prePaidPromoID", StringType),
        StructField("prepaidPromoDescription", StringType),
        StructField("date", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subscriberEntityList),
      subscriberEntityListSchema
    )
    val globePrepaidPromoAvailmentStagingDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(globePrepaidPromoAvailmentStaging),
        globePrepaidPromoAvailmentStagingSchema
      )
    val PrepaidPromoDF = spark.createDataFrame(
      spark.sparkContext.parallelize(PrepaidPromo),
      PrepaidPromoSchema
    )
    val sourceDF = Array(
      ("Subscriber", subsDF),
      (
        "globePrepaidPromoAvailmentStaging",
        globePrepaidPromoAvailmentStagingDF
      ),
      ("PrepaidPromo", PrepaidPromoDF)
    ).toMap
    val sourceColumns = Array(
      SourceColumn(
        "globePrepaidPromoAvailmentStaging",
        "promoRegistrationDatetime"
      ),
      SourceColumn("PrepaidPromo", "prepaidPromoDescription"),
      SourceColumn("globePrepaidPromoAvailmentStaging", "subscriberID"),
      SourceColumn("globePrepaidPromoAvailmentStaging", "prepaidPromoId"),
      SourceColumn("PrepaidPromo", "prePaidPromoID")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
    val targetColumn = "allDayRoamSurfLastRegistrationDatePrepaid"
    val df = GetAllDayRoamSurfRegistrationDatePrepaid
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF = df
      .select(profileListSchema.fieldNames.map(c => col(c)): _*)
      .collect()
      .toList
    val expectList = List(Row("1", "2019-01-07"))
    assertResult(expectList)(collectedDF)
  }
}
