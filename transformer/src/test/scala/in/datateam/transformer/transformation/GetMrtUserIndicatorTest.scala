package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMrtUserIndicatorTest extends FlatSpec with SparkTest {
  "GetMrtUserIndicatorTest" should "should indicates for Mrt user" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList =
      List(Row("9598874741", ""), Row("9598874742", ""), Row("9598874743", ""), Row("9598874744", ""))
    val profileListSchema = StructType(
      List(
        StructField("msisdn_value", StringType),
        StructField("mrt_user_indicator", StringType)
      )
    )

    val subcriberEntityList = List(
      Row("9598874741", "2019-01-16"),
      Row("9598874742", "2019-01-16"),
      Row("9598874743", "2019-01-16"),
      Row("9598874744", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("date", StringType)
      )
    )

    val cdrMoAttributeEntity = List(
      Row("169598874741", "2", "2", "3", "2019-01-16"),
      Row("169598874742", "4", "4", "4", "2019-01-16"),
      Row("169598874743", "5", "5", "5", "2019-01-16")
    )
    val cdrMoAttributeEntitySchema = StructType(
      List(
        StructField("aDirectionNumber", StringType),
        StructField("aFirstCell", StringType),
        StructField("aFirstSac", StringType),
        StructField("aFirstLac", StringType),
        StructField("date", StringType)
      )
    )

    val sdrMoAttributeEntity = List(
      Row("169598874741", "1", "1", "1", "2019-01-16"),
      Row("169598874741", "6", "6", "6", "2019-01-16"),
      Row("169598874744", "7", "7", "7", "2019-01-16")
    )
    val sdrMoAttributeEntitySchema = StructType(
      List(
        StructField("callingNumber", StringType),
        StructField("cell", StringType),
        StructField("sac", StringType),
        StructField("lac", StringType),
        StructField("date", StringType)
      )
    )
    val inBuildingReferenceAttributeEntity = List(
      Row("1", "mrt", "ab building", "1", "2019-01-16"),
      Row("6", "mrt", "ab building", "6", "2019-01-16"),
      Row("7", "mrt", "ab building", "7", "2019-01-16"),
      Row("2", "mrt", "ab building", "2", "2019-01-16"),
      Row("4", "mrt", "ab building", "5", "2019-01-16"),
      Row("5", "mrt", "ab building", "5", "2019-01-16")
    )
    val inBuildingReferenceAttributeEntitySchema = StructType(
      List(
        StructField("locationAreaCode", StringType),
        StructField("buildingTypeName", StringType),
        StructField("buildingGroupName", StringType),
        StructField("cellId", StringType),
        StructField("date", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val cdrMoEntityDF = spark.createDataFrame(
      spark.sparkContext.parallelize(cdrMoAttributeEntity),
      cdrMoAttributeEntitySchema
    )

    val sdrMoEntityDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sdrMoAttributeEntity),
      sdrMoAttributeEntitySchema
    )

    val inbuildingReferenceEntityDF = spark.createDataFrame(
      spark.sparkContext.parallelize(inBuildingReferenceAttributeEntity),
      inBuildingReferenceAttributeEntitySchema
    )

    val sourceDF =
      Array(
        ("GlobeSubscriber", subsDF),
        ("cdr_mo", cdrMoEntityDF),
        ("sdr_mo", sdrMoEntityDF),
        ("in_building_solution_reference", inbuildingReferenceEntityDF)
      ).toMap
    val sourceColumns = Array(
      SourceColumn("cdr_mo", "aDirectionNumber"),
      SourceColumn("cdr_mo", "aFirstCell"),
      SourceColumn("sdr_mo", "callingNumber"),
      SourceColumn("sdr_mo", "cell"),
      SourceColumn("in_building_solution_reference", "locationAreaCode"),
      SourceColumn("in_building_solution_reference", "buildingTypeName"),
      SourceColumn("in_building_solution_reference", "buildingGroupName"),
      SourceColumn("cdr_mo", "aFirstSac"),
      SourceColumn("sdr_mo", "sac"),
      SourceColumn("in_building_solution_reference", "cellId"),
      SourceColumn("sdr_mo", "lac"),
      SourceColumn("cdr_mo", "aFirstLac")
    )
    val joinColumns =
      Option(Array(JoinColumn("aDirectionNumber", "msisdn_value")))
    val targetColumn = "mrt_user_indicator"
    val df = GetMrtUserIndicator
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("msisdn_value").collect().toList
    val expectList =
      List(Row("9598874741", "1"), Row("9598874742", "0"), Row("9598874743", "1"), Row("9598874744", "1"))
    assertResult(expectList)(calulatedDF)
  }
}
