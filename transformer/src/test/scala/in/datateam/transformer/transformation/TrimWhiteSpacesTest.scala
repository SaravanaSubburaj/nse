package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.SourceColumn

class TrimWhiteSpacesTest extends FlatSpec with SparkTest {

  " Test Trim" should "value will give first name without white space" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

    val sampleList = List(
      Row(" Allen", "John "),
      Row("Allen     ", " John"),
      Row("        Allen ", " John "),
      Row(" All en ", "  John "),
      Row("Allen", "John"),
      Row("Allen ", "John")
    )
    val schema = StructType(
      List(
        StructField("firstName", StringType),
        StructField("lastName", StringType)
      )
    )
    val sourceColumns = Array(SourceColumn("sample", "firstName"))
    val sampleDF = spark
      .createDataFrame(spark.sparkContext.parallelize(sampleList), schema)
      .alias("sample")
    val calculatedDF = TrimWhiteSpaces
      .process(sampleDF, sourceColumns, "firstName")(spark, extraParameters)
    val collectCalculatedDf = calculatedDF.collect()

    val expectedDF = List(
      Row("Allen", "John "),
      Row("Allen", " John"),
      Row("Allen", " John "),
      Row("All en", "  John "),
      Row("Allen", "John"),
      Row("Allen", "John")
    )
    assertResult(expectedDF)(collectCalculatedDf)
  }
  "Test Trim" should "value will give last name without white spaces" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val sampleList = List(
      Row("Allen", "John "),
      Row("Allen", " John"),
      Row("Allen ", " John   "),
      Row("All en", "        Jo hn "),
      Row("Allen", "John"),
      Row("Allen", "John     ")
    )
    val schema = StructType(
      List(
        StructField("firstName", StringType),
        StructField("lastName", StringType)
      )
    )
    val sourceColumns = Array(SourceColumn("sample", "lastName"))
    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleList),
      schema
    )
    val calculatedDF = TrimWhiteSpaces
      .process(sampleDF, sourceColumns, "lastName")(spark, extraParameters)
    val collectCalculatedDF = calculatedDF.collect()
    val expectedDF = List(
      Row("Allen", "John"),
      Row("Allen", "John"),
      Row("Allen ", "John"),
      Row("All en", "Jo hn"),
      Row("Allen", "John"),
      Row("Allen", "John")
    )
    assertResult(expectedDF)(collectCalculatedDF)
  }
}
