package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetProjectedDataUsageQuantityTest extends FlatSpec with SparkTest {

  "GetProjectedDataUsageQuantityTest" should "Test the ProjectedDataUsageQuantity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters = Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(Row("1", ""))
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("projectedDataUsageQuantity", StringType)
        )
      )

      val subcriberEntityList = List(Row("1", "1", "2019-01-16"))
      val subcriberEntityListSchema =
        StructType(
          List(
            StructField("subsSubscriberID", StringType),
            StructField("customerId", StringType),
            StructField("date", StringType)
          )
        )

      val GlobeDailyPostpaidEntityList =
        List(Row("1", "5589987", "post", "data", "2019-01-07", "2019-01-16"))
      val GlobeDailyPostpaidEntityListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("totalDataVOlumeCount", StringType),
          StructField("paymentCategoryCode", StringType),
          StructField("usageTypeCode", StringType),
          StructField("usageTransactionDate", StringType),
          StructField("date", StringType)
        )
      )

      val FinancialAccountInvoiceStaging = List(
        Row("2019-02-28", "2019-01-01", "112", "2019-01-18", "2019-01", "1234"),
        Row("2019-02-28", "2019-01-01", "112", "2019-01-18", "2019-01", "1233")
      )
      val FinancialAccountInvoiceStagingSchema = StructType(
        List(
          StructField("cycleRunDate", StringType),
          StructField("periodCoverageStartDate", StringType),
          StructField("financialAccountId", StringType),
          StructField("periodCoverageEndDate", StringType),
          StructField("date", StringType),
          StructField("billNumber", StringType)
        )
      )

      val FinancialAccount = List(
        Row("112", "1", "O", "2019-01-01", "2019-01-16"),
        Row("112", "1", "SUS", "2019-01-01", "2019-01-15"),
        Row("113", "2", "SUS", "2019-01-02", "2019-01-16"),
        Row("114", "3", "SUS", "2019-01-02", "2019-01-16"),
        Row("115", "4", "O", "2019-01-03", "2019-01-16"),
        Row("116", "5", "O", "2019-01-04", "2019-01-16")
      )
      val FinancialAccountSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("customerId", StringType),
          StructField("financialAccountStatusCode", StringType),
          StructField("financialAccountActivationDate", StringType),
          StructField("date", StringType)
        )
      )

      val FinancialAccountSubscriberReference = List(
        Row("112", "1", null, "2019-01-16"),
        Row("112", "1", null, "2019-01-15")
      )
      val FinancialAccountSubscriberReferenceSchema = StructType(
        List(
          StructField("financialAccountId", StringType),
          StructField("subscriberId", StringType),
          StructField("expirationDate", StringType, nullable = true),
          StructField("date", StringType)
        )
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val dailyPostpaidDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(GlobeDailyPostpaidEntityList),
        GlobeDailyPostpaidEntityListSchema
      )
      val financialDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountInvoiceStaging),
        FinancialAccountInvoiceStagingSchema
      )
      val financialAccount = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccount),
        FinancialAccountSchema
      )
      val subscriberReferenceDF = spark.createDataFrame(
        spark.sparkContext.parallelize(FinancialAccountSubscriberReference),
        FinancialAccountSubscriberReferenceSchema
      )
      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("GlobeDailyPostpaidSubscriberUsageStaging", dailyPostpaidDF),
        ("financialAccount", financialAccount),
        ("FinancialAccountInvoiceStaging", financialDF),
        ("FinancialAccountSubscriberReference", subscriberReferenceDF)
      ).toMap

      val sourceColumns = Array(
        SourceColumn(
          "GlobeDailyPostpaidSubscriberUsageStaging",
          "subscriberId"
        ),
        SourceColumn(
          "GlobeDailyPostpaidSubscriberUsageStaging",
          "totalDataVOlumeCount"
        ),
        SourceColumn("FinancialAccountInvoiceStaging", "cycleRunDate"),
        SourceColumn(
          "FinancialAccountInvoiceStaging",
          "periodCoverageStartDate"
        ),
        SourceColumn("FinancialAccountInvoiceStaging", "financialAccountId"),
        SourceColumn(
          "financialAccount",
          "financialAccountId"
        ),
        SourceColumn("financialAccount", "customerId"),
        SourceColumn(
          "GlobeDailyPostpaidSubscriberUsageStaging",
          "paymentCategoryCode"
        ),
        SourceColumn(
          "GlobeDailyPostpaidSubscriberUsageStaging",
          "usageTypeCode"
        ),
        SourceColumn(
          "GlobeDailyPostpaidSubscriberUsageStaging",
          "usageTransactionDate"
        ),
        SourceColumn("FinancialAccountInvoiceStaging", "periodCoverageEndDate"),
        SourceColumn("FinancialAccountInvoiceStaging", "billNumber"),
        SourceColumn("financialAccount", "financialAccountStatusCode"),
        SourceColumn("financialAccount", "financialAccountActivationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "subscriberId"),
        SourceColumn("FinancialAccountSubscriberReference", "expirationDate"),
        SourceColumn("FinancialAccountSubscriberReference", "financialAccountId")
      )
      val joinColumns =
        Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
      val targetColumn = "projectedDataUsageQuantity"
      val df = GetProjectedDataUsageQuantity
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calculatedDF = df.collect().toList
      val expectList = List(Row("1", -15.282277488708496))
      assertResult(expectList)(calculatedDF)
  }
}
