package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetGcashUserIndicatorTest extends FlatSpec with SparkTest {
  "GetGcashUserIndicatorTest" should "direct pull from subscriber" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(
      Row("1", ""),
      Row("2", ""),
      Row("3", ""),
      Row("4", ""),
      Row("5", "")
    )
    val profileListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("gcash_user_indicator", StringType)
      )
    )

    val subcriberEntityList = List(
      Row("1", "Y", "2019-01-16"),
      Row("2", "N", "2019-01-16"),
      Row("3", "Y", "2019-01-16"),
      Row("4", "Y", "2019-01-16"),
      Row("5", "N", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("gcashUserIndicator", StringType),
        StructField("date", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val sourceDF = Array(("GlobeSubscriber", subsDF)).toMap
    val sourceColumns = Array(
      SourceColumn("GlobeSubscriber", "gcashUserIndicator"),
      SourceColumn("GlobeSubscriber", "subscriberId")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriberId")))
    val targetColumn = "gcash_user_indicator"
    val df = GetGcashUserIndicator
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("subscriberId").collect().toList
    val expectList =
      List(Row("1", 1), Row("2", 0), Row("3", 1), Row("4", 1), Row("5", 0))
    assertResult(expectList)(calulatedDF)
  }
}
