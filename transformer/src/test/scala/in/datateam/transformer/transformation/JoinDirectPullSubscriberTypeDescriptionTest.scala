package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullSubscriberTypeDescriptionTest extends FlatSpec with SparkTest {

  "JoinDirectPullTest" should "Take 2 dataframes make a Join based on the Col name match" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleNameList = List(
        Row("LT-101", "1", "2019-01-16"),
        Row("MT-909", "2", "2019-01-16"),
        Row("TY-876", "3", "2019-01-16"),
        Row("AK-470", "4", "2019-01-16")
      )

      val schemaName = StructType(
        List(
          StructField("productTypeCode", StringType),
          StructField("subsId", StringType),
          StructField("date", StringType)
        )
      )
      val sampleSubsDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleNameList),
        schemaName
      )

      val sampleEmpList = List(
        Row("LT-101", "123", "IT", "2019-01-16"),
        Row("MT-909", "245", "HR", "2019-01-16"),
        Row("TY-876", "356", "Admin", "2019-01-16"),
        Row("AK-470", "467", "Non-IT", "2019-01-16")
      )

      val schemaEmp = StructType(
        List(
          StructField("productId", StringType),
          StructField("Dept-id", StringType),
          StructField("productTypeDescription", StringType),
          StructField("date", StringType)
        )
      )
      val sampleProductDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleEmpList),
        schemaEmp
      )

      val sampleProfList = List(
        Row("Prasad", "1", "", "2019-01-16"),
        Row("Amit", "2", "", "2019-01-16"),
        Row("david", "6", "", "2019-01-16"),
        Row("Akash", "4", "", "2019-01-16"),
        Row("Sharma", "5", "", "2019-01-16")
      )

      val schemaProf = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("productTypeDescription", StringType),
          StructField("date", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        schemaProf
      )

      val sourceDFs =
        Map("subscriber" -> sampleSubsDF, "product" -> sampleProductDF)
      val sourceColumns = Array(
        SourceColumn("product", "productId"),
        SourceColumn("subscriber", "productTypeCode"),
        SourceColumn("product", "productTypeDescription")
      )
      val targetColumn: String = "productTypeDescription"

      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))
      val joinedDF = JoinDirectPullSubscriberTypeDescription
        .process(
          sourceDFs,
          targetDF,
          sourceColumns,
          targetJoinColumn,
          targetColumn
        )(spark, extraParameters)
      val actualRecordList = joinedDF.sort("subscriberId").collect()

      val expectedRecordList = Array(
        Row("Prasad", "1", "IT", "2019-01-16"),
        Row("Amit", "2", "HR", "2019-01-16"),
        Row("Akash", "4", "Non-IT", "2019-01-16"),
        Row("Sharma", "5", null, "2019-01-16"),
        Row("david", "6", null, "2019-01-16")
      )
      assertResult(expectedRecordList)(actualRecordList)

  }

}
