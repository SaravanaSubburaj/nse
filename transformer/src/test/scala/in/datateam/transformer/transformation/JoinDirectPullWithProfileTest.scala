package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullWithProfileTest extends FlatSpec with SparkTest {
  "JoinDirectPullWithProfileTest" should "join and direct pull from Profile" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

      val profileList = List(
        Row("1", "112", "", "2019-01-16"),
        Row("2", "113", "", "2019-01-16"),
        Row("3", "114", "", "2019-01-16"),
        Row("4", "115", "", "2019-01-16"),
        Row("5", "116", "", "2019-01-16")
      )
      val profileListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("main_billing_offer_id", StringType),
          StructField("billing_offer_desc", StringType),
          StructField("date", StringType)
        )
      )

      val attributeEntity = List(
        Row("112", "1", "offer30gb", "2019-01-16"),
        Row("113", "2", "offer40gb", "2019-01-16"),
        Row("114", "3", "offer50gb", "2019-01-16"),
        Row("115", "4", "offer60gb", "2019-01-16"),
        Row("116", "5", "offer70gb", "2019-01-16")
      )
      val attributeEntitySchema = StructType(
        List(
          StructField("billingOfferId", StringType),
          StructField("subscriberId", StringType),
          StructField("billingOfferDescription", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val entityDF = spark.createDataFrame(
        spark.sparkContext.parallelize(attributeEntity),
        attributeEntitySchema
      )
      val sourceDF = Array(
        ("GlobeBillingOffer", entityDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("GlobeBillingOffer", "billingOfferDescription")
      )
      val joinColumns =
        Option(Array(JoinColumn("billingOfferId", "main_billing_offer_id")))
      val targetColumn = "billing_offer_desc"
      val df = JoinDirectPullWithProfile
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)
      val calculatedDF = df.collect().toList.sortBy(_.getAs[String](0))
      val expectList = List(
        Row("1", "112", "offer30gb", "2019-01-16"),
        Row("2", "113", "offer40gb", "2019-01-16"),
        Row("3", "114", "offer50gb", "2019-01-16"),
        Row("4", "115", "offer60gb", "2019-01-16"),
        Row("5", "116", "offer70gb", "2019-01-16")
      )
      assertResult(expectList)(calculatedDF)
  }
}
