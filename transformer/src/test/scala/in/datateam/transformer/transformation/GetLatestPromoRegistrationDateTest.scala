package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetLatestPromoRegistrationDateTest extends FlatSpec with SparkTest {
  "GetLatestPromoRegistrationDateTest" should "Get latest Date of promo Registration" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters = Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(Row("1", ""), Row("2", ""), Row("3", ""))
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("allDayRoamSurfLastRegistrationDatePrepaid", StringType)
        )
      )
      val subcriberEntityList = List(
        Row("1")
      )
      val subcriberEntityListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType)
        )
      )
      val globePrepaidPromoAvailmentStaging = List(
        Row("1", "2019-01-07", "1", "1003", "1274", "30", "2019-01-16"),
        Row("3", "2019-01-07", "6", "1000", "1264", "40", "2019-01-16"),
        Row("3", "2019-01-07", "39", "1001", "2374", "50", "2019-01-16"),
        Row("4", "2019-01-07", "4", "1002", "1234", "40", "2019-01-16")
      )
      val globePrepaidPromoAvailmentStagingSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("promoRegistrationDatetime", StringType),
          StructField("promoOperationId", StringType),
          StructField("availmentChannelCode", StringType),
          StructField("serviceId", StringType),
          StructField("promoDenominationValue", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileListSchema)
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val globePrepaidPromoAvailmentStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globePrepaidPromoAvailmentStaging),
        globePrepaidPromoAvailmentStagingSchema
      )
      val sourceDF =
        Array(("Subscriber", subsDF), ("globePrepaidPromoAvailmentStaging", globePrepaidPromoAvailmentStagingDF)).toMap
      val sourceColumns = Array(
        SourceColumn("globePrepaidPromoAvailmentStaging", "promoRegistrationDatetime"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "subscriberID"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "promoOperationId"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "availmentChannelCode"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "serviceId"),
        SourceColumn("globePrepaidPromoAvailmentStaging", "promoDenominationValue")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subsSubscriberID"))
      )
      val targetColumn = "allDayRoamSurfLastRegistrationDatePrepaid"
      val df = GetLatestPromoRegistrationDate
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)
      val collectedDF = df.collect().toList
      val expectList = List(
        Row("3", "2019-01-07"),
        Row("1", "2019-01-07"),
        Row("2", null)
      )

      assertResult(expectList)(collectedDF)
  }
}
