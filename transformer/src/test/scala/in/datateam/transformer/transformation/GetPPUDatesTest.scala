package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetPPUDatesTest extends FlatSpec with SparkTest {

  "GetPPUDatesTest" should "test GetPpuDates implementation" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")

    val sampleData = List(
      Row("1", "2019-01-14", null, "2019-01-16"),
      Row("1", "2019-01-15", null, "2019-01-16"),
      Row("1", "2019-01-16", null, "2019-01-16"),
      Row("2", null, "2019-01-16", "2019-01-16"),
      Row("2", null, "2019-01-10", "2019-01-16"),
      Row("2", null, "2019-01-11", "2019-01-16"),
      Row("3", null, "2019-01-01", "2019-01-16"),
      Row("4", null, "2019-01-06", "2019-01-16"),
      Row("5", null, "2019-01-09", "2019-01-16")
    )

    val profileData = List(
      Row("1", null, null),
      Row("2", null, null),
      Row("3", null, null),
      Row("4", null, null),
      Row("5", null, null)
    )

    val sampleSchema = StructType(
      List(
        StructField("subscriberKey", StringType),
        StructField("last_data_ppu_date", StringType),
        StructField("last_core_ppu_date", StringType),
        StructField("date", StringType)
      )
    )

    val profileSchema = StructType(
      List(
        StructField("subscriber_id", StringType),
        StructField("last_core_ppu_date", StringType),
        StructField("last_data_ppu_date", StringType)
      )
    )

    val sourceColumns = Array(
      SourceColumn("mutables_usagepostpaidmutable", "subscriberKey"),
      SourceColumn("mutables_usagepostpaidmutable", "last_core_ppu_date"),
      SourceColumn("mutables_usagepostpaidmutable", "subscriberKey"),
      SourceColumn("mutables_usagepostpaidmutable", "last_data_ppu_date")
    )

    val joinCols = Option(Array(JoinColumn("subscriberKey", "subscriber_id")))

    val sampleDF = spark.createDataFrame(
      spark.sparkContext.parallelize(sampleData),
      sampleSchema
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileData),
      profileSchema
    )

    val processedDF = GetPpuDates
      .process(
        Array(("mutables_usagepostpaidmutable", sampleDF)).toMap,
        profileDF,
        sourceColumns,
        joinCols,
        "last_core_ppu_date"
      )(spark, extraParameters)
      .sort("subscriber_id")

    val expectedDF = Array(
      Row("1", null, null, null),
      Row("2", null, null, "2019-01-16"),
      Row("3", null, null, "2019-01-01"),
      Row("4", null, null, "2019-01-06"),
      Row("5", null, null, "2019-01-09")
    )

    assertResult(expectedDF)(processedDF.collect().toList)

  }

}
