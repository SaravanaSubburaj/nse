package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMaxReloadAmountTest extends FlatSpec with SparkTest {
  "GetMaxReload30DaysAmount" should "Get max of denominationValue for 30 days" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subsSubscriberID", StringType),
          StructField("reload_max_tu_tra_rolling_30_days_amount", StringType)
        )
      )

      val subcriberEntityList = List(Row("1"), Row("2"))
      val subcriberEntityListSchema =
        StructType(List(StructField("subsSubscriberID", StringType)))

      val reloadSummary = List(
        Row("1", "10", "AX", "12", "1", "2019-01-16", "2019-01-16"),
        Row("1", "10", "AX", "11", "1", "2019-01-16", "2019-01-16"),
        Row("1", "10", "AX", "15", "2", "2019-01-16", "2019-01-16"),
        Row("1", "10", "AX", "13", "2", "2019-01-16", "2019-01-16"),
        Row("2", "11", "AX", "13", "1", "2019-01-16", "2019-01-15"),
        Row("3", "22", "AX", "10", "1", "2019-01-16", "2019-01-16"),
        Row("4", "3", "AX", "9", "1", "2019-01-16", "2019-01-16"),
        Row("4", "14", "BX", "34", "1", "2019-01-16", "2019-01-16"),
        Row("5", "15", "AX", "45", "1", "2019-01-16", "2019-01-16")
      )
      val reloadSummarySchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("totalReloadAmount", StringType),
          StructField("reloadChannelTypeCode", StringType),
          StructField("reloadDenominationAmount", StringType),
          StructField("totalReloadCount", StringType),
          StructField("reloadDate", StringType),
          StructField("date", StringType)
        )
      )
      val reloadSummaryDF = spark.createDataFrame(
        spark.sparkContext.parallelize(reloadSummary),
        reloadSummarySchema
      )

      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )

      val sourceDF = Array(
        ("Subscriber", subsDF),
        ("GlobeDailySubscriberReloadSummary", reloadSummaryDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("GlobeDailySubscriberReloadSummary", "subscriberId"),
        SourceColumn("GlobeDailySubscriberReloadSummary", "totalReloadAmount"),
        SourceColumn("GlobeDailySubscriberReloadSummary", "reloadChannelTypeCode"),
        SourceColumn("GlobeDailySubscriberReloadSummary", "reloadDenominationAmount"),
        SourceColumn("GlobeDailySubscriberReloadSummary", "reloadDate")
      )
      val joinColumns = Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
      val targetColumn = "reload_max_tu_tra_rolling_30_days_amount"
      val df = GetMaxReload30DaysAmount
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subsSubscriberID").collect().toList
      val expectList = List(
        Row("1", "15"),
        Row("2", "13"),
        Row("3", "10"),
        Row("4", "9"),
        Row("5", "45")
      )
      assertResult(expectList)(calulatedDF)
  }
  "GetMaxReload90DaysAmount" should "Get max of denomvalue for 90 days" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(
      Row("1", ""),
      Row("2", ""),
      Row("3", ""),
      Row("4", ""),
      Row("5", "")
    )
    val profileListSchema = StructType(
      List(
        StructField("subsSubscriberID", StringType),
        StructField("reload_max_tu_tra_rolling_90_days_amount", StringType)
      )
    )

    val subcriberEntityList = List(Row("1"), Row("2"))
    val subcriberEntityListSchema =
      StructType(List(StructField("subsSubscriberID", StringType)))

    val reloadSummary = List(
      Row("1", "10", "AX", "12", "1", "2019-01-10", "2019-01-16"),
      Row("1", "10", "AX", "11", "1", "2019-01-10", "2019-01-16"),
      Row("1", "10", "AX", "15", "2", "2019-01-10", "2019-01-16"),
      Row("1", "10", "AX", "13", "2", "2019-01-10", "2019-01-16"),
      Row("2", "11", "AX", "13", "1", "2019-01-10", "2019-01-15"),
      Row("3", "22", "AX", "10", "1", "2019-01-10", "2019-01-16"),
      Row("4", "3", "AX", "9", "1", "2019-01-10", "2019-01-16"),
      Row("4", "14", "BX", "34", "1", "2019-01-10", "2019-01-16"),
      Row("5", "15", "AX", "45", "1", "2019-01-10", "2019-01-16")
    )
    val reloadSummarySchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("totalReloadAmount", StringType),
        StructField("reloadChannelTypeCode", StringType),
        StructField("reloadDenominationAmount", StringType),
        StructField("totalReloadCount", StringType),
        StructField("reloadDate", StringType),
        StructField("date", StringType)
      )
    )
    val reloadSummaryDF = spark.createDataFrame(
      spark.sparkContext.parallelize(reloadSummary),
      reloadSummarySchema
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )

    val sourceDF = Array(
      ("Subscriber", subsDF),
      ("GlobeDailySubscriberReloadSummary", reloadSummaryDF)
    ).toMap
    val sourceColumns = Array(
      SourceColumn("GlobeDailySubscriberReloadSummary", "subscriberId"),
      SourceColumn("GlobeDailySubscriberReloadSummary", "totalReloadAmount"),
      SourceColumn("GlobeDailySubscriberReloadSummary", "reloadChannelTypeCode"),
      SourceColumn("GlobeDailySubscriberReloadSummary", "reloadDenominationAmount"),
      SourceColumn("GlobeDailySubscriberReloadSummary", "reloadDate")
    )
    val joinColumns = Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
    val targetColumn = "reload_max_tu_tra_rolling_90_days_amount"
    val df = GetMaxReload90DaysAmount
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("subsSubscriberID").collect().toList
    val expectList = List(
      Row("1", "15"),
      Row("2", "13"),
      Row("3", "10"),
      Row("4", "9"),
      Row("5", "45")
    )
    assertResult(expectList)(calulatedDF)
  }
}
