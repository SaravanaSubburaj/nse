package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDerivePrepaidTopupSegmentationScoreTest extends FlatSpec with SparkTest {

  "JoinDerivePrepaidTopupSegmentationScoreTest" should "process for current month" in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2019-01"
        ),
        Row(
          "12388",
          "0004145778",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2019-01"
        ),
        Row(
          "4922",
          "000162810",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2019-01"
        ),
        Row(
          "6367",
          "000146482",
          "5",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2019-01"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(
        Row("Prasad", "10663", ""),
        Row("Amit", "202", ""),
        Row("David", "4922", ""),
        Row("Akash", "471", ""),
        Row("Sharma", "6367", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("prepaidTopupSegmentationScore", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "prepaidTopupSegmentationScore"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = JoinDerivePrepaidTopupSegmentationScore.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(
        Row("Akash", "471", null),
        Row("Amit", "202", null),
        Row("David", "4922", null),
        Row("Prasad", "10663", null),
        Row("Sharma", "6367", null)
      )
      assertResult(expectedDF)(actualDF)

  }

  "JoinDerivePrepaidTopupSegmentationScoreTest" should "process for month - 1 " in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-12"
        ),
        Row(
          "12388",
          "0004145778",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-12"
        ),
        Row(
          "4922",
          "000162810",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-11"
        ),
        Row(
          "202",
          "000162810",
          "4",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-12"
        ),
        Row(
          "6367",
          "000146482",
          "5",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-12"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(
        Row("Prasad", "10663", ""),
        Row("Amit", "202", ""),
        Row("David", "4922", ""),
        Row("Akash", "471", ""),
        Row("Sharma", "6367", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("prepaidTopupSegmentationScore", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "prepaidTopupSegmentationScore"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = JoinDerivePrepaidTopupSegmentationScore.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(
        Row("Akash", "471", null),
        Row("Amit", "202", "4"),
        Row("David", "4922", null),
        Row("Prasad", "10663", null),
        Row("Sharma", "6367", "5")
      )
      assertResult(expectedDF)(actualDF)

  }

  "JoinDerivePrepaidTopupSegmentationScoreTest" should "process for month - 2 " in withSparkSession {
    spark: SparkSession =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val sampleAAModelScoreData = List(
        Row(
          "10663",
          "0003909257",
          "1",
          "TM-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-02"
        ),
        Row(
          "12388",
          "0004145778",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TM_CONTENT_MUSIC",
          "2018-01"
        ),
        Row(
          "4922",
          "000162810",
          "3",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-10"
        ),
        Row(
          "202",
          "000162810",
          "4",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-11"
        ),
        Row(
          "6367",
          "000146482",
          "5",
          "GHP-PREPAID",
          "1",
          "18JUL2018:14:35:19",
          "TOPUP_SEGMENTATION",
          "2018-11"
        )
      )

      val aaModelScoreSchema = StructType(
        List(
          StructField("subsId", StringType),
          StructField("msisdn", StringType),
          StructField("score", StringType),
          StructField("brand", StringType),
          StructField("updateDy", StringType),
          StructField("updateDttp", StringType),
          StructField("model", StringType),
          StructField("date", StringType)
        )
      )

      val aaModelScoreDF =
        spark.createDataFrame(
          spark.sparkContext.parallelize(sampleAAModelScoreData),
          aaModelScoreSchema
        )

      val sampleProfList = List(
        Row("Prasad", "10663", ""),
        Row("Amit", "202", ""),
        Row("David", "4922", ""),
        Row("Akash", "471", ""),
        Row("Sharma", "6367", "")
      )

      val profileSchema = StructType(
        List(
          StructField("subsName", StringType),
          StructField("subscriberId", StringType),
          StructField("prepaidTopupSegmentationScore", StringType)
        )
      )

      val targetDF = spark.createDataFrame(
        spark.sparkContext.parallelize(sampleProfList),
        profileSchema
      )

      val sourceDFs = Map("AAModelScore" -> aaModelScoreDF)

      val sourceColumns = Array(
        SourceColumn("AAModelScore", "subsId"),
        SourceColumn("AAModelScore", "model"),
        SourceColumn("AAModelScore", "score")
      )

      val targetColumn: String = "prepaidTopupSegmentationScore"
      val targetJoinColumn = Option(Array(JoinColumn("subsId", "subscriberId")))

      val joinedDF = JoinDerivePrepaidTopupSegmentationScore.process(
        sourceDFs,
        targetDF,
        sourceColumns,
        targetJoinColumn,
        targetColumn
      )(spark, extraParameters)

      val actualDF = joinedDF
        .select(profileSchema.fieldNames.map(c => col(c)): _*)
        .sort(profileSchema.fieldNames.head)
        .collect()

      val expectedDF = Array(
        Row("Akash", "471", null),
        Row("Amit", "202", "4"),
        Row("David", "4922", null),
        Row("Prasad", "10663", null),
        Row("Sharma", "6367", "5")
      )
      assertResult(expectedDF)(actualDF)

  }
}
