package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetHandsetTypeNameTest extends FlatSpec with SparkTest {
  "GetHandsetTypeNameTest" should "Get the latest used device" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(
      Row("1", "11221", "L", ""),
      Row("2", "11222", "L", ""),
      Row("3", "11223", "L", ""),
      Row("4", "11224", "L", ""),
      Row("5", "11225", "L", "")
    )
    val profileListSchema = StructType(
      List(
        StructField("subscriber_id", StringType),
        StructField("msisdn_value", StringType),
        StructField("subscriber_status_code", StringType),
        StructField("handset_type_name", StringType)
      )
    )
    val subcriberEntityList = List(
      Row("1", "11221", "L", "2019-01-16"),
      Row("2", "11222", "L", "2019-01-16"),
      Row("3", "11223", "L", "2019-01-16"),
      Row("4", "11224", "L", "2019-01-16"),
      Row("5", "11225", "L", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("msisdnValue", StringType),
        StructField("subscriberStatusCode", StringType),
        StructField("date", StringType)
      )
    )
    val networkDaily = List(
      Row("11221", "112", "2018-12-06T16:00:00.000Z", "2019-01-16"),
      Row("11221", "112", "2018-12-06T16:00:00.000Z", "2019-01-16"),
      Row("11222", "113", "2018-12-06T16:00:00.000Z", "2019-01-16"),
      Row("11223", "114", "2018-12-06T16:00:00.000Z", "2019-01-16")
    )
    val networkDailySchema = StructType(
      List(
        StructField("msisdnValue", StringType),
        StructField("deviceID", StringType),
        StructField("topLocationDate", StringType),
        StructField("date", StringType)
      )
    )
    val networkDailyVoice = List(
      Row("11221", "112", "2018-11-06T16:00:00.000Z", "2019-01-16"),
      Row("11221", "112", "2018-11-06T16:00:00.000Z", "2019-01-16")
    )
    val networkDailyVoiceSchema = StructType(
      List(
        StructField("msisdnValue", StringType),
        StructField("deviceID", StringType),
        StructField("topLocationDate", StringType),
        StructField("date", StringType)
      )
    )
    val networkDailySms = List(
      Row("11221", "112", "2018-12-06T16:00:00.000Z", "2019-01-16"),
      Row("11221", "112", "2018-11-06T16:00:00.000Z", "2019-01-16")
    )
    val networkDailySmsSchema = StructType(
      List(
        StructField("msisdnValue", StringType),
        StructField("deviceID", StringType),
        StructField("topLocationDate", StringType),
        StructField("date", StringType)
      )
    )
    val device = List(
      Row("112", "MotoG4", "2019-01"),
      Row("113", "MotoG5", "2019-01"),
      Row("114", "MotoG6", "2019-01"),
      Row("115", "MotoG7", "2019-01"),
      Row("116", "MotoG8", "2019-01")
    )
    val deviceSchema = StructType(
      List(
        StructField("deviceId", StringType),
        StructField("deviceName", StringType),
        StructField("date", StringType)
      )
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val networkDailyDF = spark.createDataFrame(
      spark.sparkContext.parallelize(networkDaily),
      networkDailySchema
    )
    val networkDailyVoiceDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(networkDailyVoice),
        networkDailyVoiceSchema
      )
    val networkDailySmsDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(networkDailySms),
        networkDailySmsSchema
      )
    val deviceDF = spark.createDataFrame(
      spark.sparkContext.parallelize(device),
      deviceSchema
    )
    val sourceDF = Array(
      ("Subscriber", subsDF),
      ("DailyNetworkDataGITransactionTopLocation", networkDailyDF),
      ("Device", deviceDF),
      ("DailyNetworkVoiceMOTransactionTopLocation", networkDailyVoiceDF),
      ("DailyNetworkSMSMOTransactionTopLocation", networkDailySmsDF)
    ).toMap
    val sourceColumns = Array(
      SourceColumn("DailyNetworkDataGITransactionTopLocation", "msisdnValue"),
      SourceColumn("DailyNetworkDataGITransactionTopLocation", "deviceID"),
      SourceColumn(
        "DailyNetworkDataGITransactionTopLocation",
        "topLocationDate"
      ),
      SourceColumn(
        "DailyNetworkVoiceMOTransactionTopLocation",
        "msisdnValue"
      ),
      SourceColumn("DailyNetworkVoiceMOTransactionTopLocation", "deviceID"),
      SourceColumn(
        "DailyNetworkVoiceMOTransactionTopLocation",
        "topLocationDate"
      ),
      SourceColumn("DailyNetworkSMSMOTransactionTopLocation", "msisdnValue"),
      SourceColumn("DailyNetworkSMSMOTransactionTopLocation", "deviceID"),
      SourceColumn(
        "DailyNetworkSMSMOTransactionTopLocation",
        "topLocationDate"
      ),
      SourceColumn("Device", "deviceId"),
      SourceColumn("Device", "deviceName"),
      SourceColumn("Subscriber", "subscriberStatusCode"),
      SourceColumn("Subscriber", "subscriberId"),
      SourceColumn("Subscriber", "msisdnValue")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriber_id")))
    val targetColumn = "handset_type_name"
    val df = GetHandsetTypeName
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF = df.sort("subscriber_id").collect().toList
    val expectList = List(
      Row("1", "11221", "L", "MotoG4"),
      Row("2", "11222", "L", "MotoG5"),
      Row("3", "11223", "L", "MotoG6"),
      Row("4", "11224", "L", null),
      Row("5", "11225", "L", null)
    )
    assertResult(expectList)(collectedDF)
  }
}
