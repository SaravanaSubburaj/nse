package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullOnSubscriberIdTest extends FlatSpec with SparkTest {
  "JoinDirectPullWithEntityTest" should "join and direct pull from invoiceStaging" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subscriber_id", StringType),
          StructField("subscriber_status_reason_key", StringType)
        )
      )

      val attributeEntity = List(
        Row("22015874", "1", "2019-01-16-21-30"),
        Row("22015875", "2", "2019-01-16-21-30"),
        Row("22015876", "3", "2019-01-16-21-30"),
        Row("22015877", "4", "2019-01-16-21-30"),
        Row("22015878", "5", "2019-01-16-21-30")
      )
      val attributeEntitySchema = StructType(
        List(
          StructField("subscriberStatusReasonKey", StringType),
          StructField("subscriberKey", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val entityDF = spark.createDataFrame(
        spark.sparkContext.parallelize(attributeEntity),
        attributeEntitySchema
      )
      val sourceDF = Array(("subscriberDataset", entityDF)).toMap
      val sourceColumns = Array(
        SourceColumn("subscriberDataset", "subscriberStatusReasonKey"),
        SourceColumn("subscriberDataset", "subscriberKey")
      )
      val joinColumns =
        Option(Array(JoinColumn("subscriberKey", "subscriber_id")))
      val targetColumn = "subscriber_status_reason_key"
      val df = JoinDirectPullOnSubscriberId
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subscriber_id").collect().toList
      val expectList = List(
        Row("1", "22015874"),
        Row("2", "22015875"),
        Row("3", "22015876"),
        Row("4", "22015877"),
        Row("5", "22015878")
      )
      assertResult(expectList)(calulatedDF)
  }
}
