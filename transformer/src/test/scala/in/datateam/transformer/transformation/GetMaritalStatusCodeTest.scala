package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMaritalStatusCodeTest extends FlatSpec with SparkTest {
  "GetMaritalStatusCodeTest" should "join and derived from contact entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("marital_status_type_description_postpaid", StringType)
        )
      )

      val subcriberEntityList = List(
        Row("1", "112", "POST", "2019-01-16"),
        Row("2", "113", "POST", "2019-01-16"),
        Row("3", "114", "PRE", "2019-01-16"),
        Row("4", "115", "POST", "2019-01-16"),
        Row("5", "116", "POST", "2019-01-16")
      )
      val subcriberEntityListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("mainContactId", StringType),
          StructField("paymentCategoryCode", StringType),
          StructField("date", StringType)
        )
      )

      val contactEntity = List(
        Row("112", "S", "2019-01-16"),
        Row("113", "M", "2019-01-16"),
        Row("114", "S", "2019-01-16"),
        Row("115", "M", "2019-01-16"),
        Row("116", "S", "2019-01-16")
      )
      val contactEntitySchema = StructType(
        List(
          StructField("contactID", StringType),
          StructField("contactMaritalStatusCode", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val entityDF = spark.createDataFrame(
        spark.sparkContext.parallelize(contactEntity),
        contactEntitySchema
      )
      val sourceDF =
        Array(("GlobeSubscriber", subsDF), ("contact", entityDF)).toMap
      val sourceColumns = Array(
        SourceColumn("GlobeSubscriber", "subscriberId"),
        SourceColumn("GlobeSubscriber", "mainContactId"),
        SourceColumn("contact", "contactID"),
        SourceColumn("contact", "contactMaritalStatusCode"),
        SourceColumn("GlobeSubscriber", "paymentCategoryCode")
      )
      val joinColumns =
        Option(Array(JoinColumn("subscriberId", "subscriberId")))
      val targetColumn = "marital_status_type_description_postpaid"
      val df = GetMaritalStatusCode
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subscriberId").collect().toList
      val expectList = List(
        Row("1", "S"),
        Row("2", "M"),
        Row("3", null),
        Row("4", "M"),
        Row("5", "S")
      )
      assertResult(expectList)(calulatedDF)
  }
}
