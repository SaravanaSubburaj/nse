package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

/**
  * @author Saravana(saravana.subburaj@thedatateam.in)
  */

class GetAllDayRoamSurfRegistrationDateCombineTest extends FlatSpec with SparkTest {
  "GetAllDayRoamSurfRegistrationDateCombineTest" should "Test prepaid and postpaid availment_all_day_roam_surf_last_registration_90days_date combined in a single attribute" in withSparkSession {
    spark: SparkSession =>
      val sourceColumns = Array(
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "promoRegistrationDatetime"),
        SourceColumn("PrepaidPromo", "prepaidPromoDescription"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "subscriberId"),
        SourceColumn("GlobePrepaidPromoAvailmentStaging", "prepaidPromoId"),
        SourceColumn("PrepaidPromo", "prepaidPromoId"),
        SourceColumn("SubscriberAssignedBillingOffer", "subscriberId"),
        SourceColumn("SubscriberAssignedBillingOffer", "billingOfferDescription"),
        SourceColumn("SubscriberAssignedBillingOffer", "subscriberAssignedBillingOfferStartDate")
      )
      val joinColumns = Option(
        Array(JoinColumn("subscriberId", "subscriber_id"))
      )
      val date = "2019-01-16"
      val subscriberList = List(
        Row("1", date),
        Row("2", date),
        Row("3", date)
      )
      val subscriberDFSchema = StructType(
        List(
          StructField("subscriber_id", StringType, nullable = false),
          StructField("date", StringType, nullable = false)
        )
      )
      val globePrepaidPromoAvailmentStagingSchema = StructType(
        List(
          StructField("promoRegistrationDatetime", dataType = StringType),
          StructField("subscriberId", dataType = StringType),
          StructField("prepaidPromoId", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )
      val prepaidPromoSchema = StructType(
        List(
          StructField("prepaidPromoDescription", dataType = StringType),
          StructField("prepaidPromoId", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )

      val subscriberAssignedBillingOfferSchema = StructType(
        List(
          StructField("subscriberId", dataType = StringType),
          StructField("billingOfferDescription", dataType = StringType),
          StructField("subscriberAssignedBillingOfferStartDate", dataType = StringType),
          StructField("date", dataType = StringType, nullable = false)
        )
      )

      val globePrepaidPromoAvailmentStagingList = List(
        Row("2019-01-04", "1", "GS50_BASE3", date),
        Row("2019-01-09", "2", "FREE_WNP2_2GB3", date),
        Row("2019-01-05", "3", "ALLNETPROMO_USSD_30D3", date)
      )

      val prepaidPromoList = List(
        Row("ALLNETPROMO_USSD_30D", "FREE_WNP2_2GB3", date),
        Row("ALLNEroamsurfSSD_50D", "ALLNETPROMO_USSD_30D3", date),
        Row("GoSurf50_Baseplan", "GS50_BASE3", date)
      )
      val subscriberAssignedBillingOfferList = List(
        Row("1", "Roam Surf 15 Days (P4999)", "2019-01-12", date),
        Row("2", "Roam Saver", "2019-01-09", date),
        Row("3", "Add On - Easy Roam 149", "2019-01-06", date)
      )

      val profileList = List(Row("1"), Row("2"), Row("3"))
      val profileSchema = StructType(
        List(
          StructField("subscriber_id", StringType)
        )
      )

      val subscriberDF = spark.createDataFrame(spark.sparkContext.parallelize(subscriberList), subscriberDFSchema)

      val globePrepaidPromoAvailmentStagingDF = spark.createDataFrame(
        spark.sparkContext.parallelize(globePrepaidPromoAvailmentStagingList),
        globePrepaidPromoAvailmentStagingSchema
      )

      val prepaidPromoDF = spark.createDataFrame(
        spark.sparkContext.parallelize(prepaidPromoList),
        prepaidPromoSchema
      )

      val subscriberAssignedBillingOfferDF = spark.createDataFrame(
        spark.sparkContext.parallelize(subscriberAssignedBillingOfferList),
        subscriberAssignedBillingOfferSchema
      )

      val profileDF = spark.createDataFrame(spark.sparkContext.parallelize(profileList), profileSchema)

      implicit val extraParameters: Map[String, String] = Map(TransformerEnums.PARTITION_VALUE -> date)

      val sourceDFs = Map(
        "Subscriber"                        -> subscriberDF,
        "GlobePrepaidPromoAvailmentStaging" -> globePrepaidPromoAvailmentStagingDF,
        "PrepaidPromo"                      -> prepaidPromoDF,
        "SubscriberAssignedBillingOffer"    -> subscriberAssignedBillingOfferDF
      )
      val targetColumn = "availment_all_day_roam_surf_last_registration_90days_date"

      val getAllDayRoamSurfRegistrationDateCombineDF = GetAllDayRoamSurfRegistrationDateCombine
        .process(sourceDFs, profileDF, sourceColumns, joinColumns, targetColumn)(spark, extraParameters)

      val actual = getAllDayRoamSurfRegistrationDateCombineDF.collect()
      val expected = Array(
        Row("3", "2019-01-05"),
        Row("1", "2019-01-12"),
        Row("2", null)
      )
      assertResult(expected)(actual)
  }
}
