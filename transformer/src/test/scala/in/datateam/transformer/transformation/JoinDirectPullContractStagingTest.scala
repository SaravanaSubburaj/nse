package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class JoinDirectPullContractStagingTest extends FlatSpec with SparkTest {
  "JoinDirectPullContractStagingTest" should "join and direct pull from contract staging Entity" in withSparkSession {
    (spark: SparkSession) =>
      implicit val extraParameters =
        Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
      val profileList = List(
        Row("1", ""),
        Row("2", ""),
        Row("3", ""),
        Row("4", ""),
        Row("5", "")
      )
      val profileListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("lock_in_end_date", StringType)
        )
      )

      val subcriberEntityList = List(
        Row("1", "2019-01-16"),
        Row("2", "2019-01-16"),
        Row("3", "2019-01-16"),
        Row("4", "2019-01-16"),
        Row("5", "2019-01-16")
      )
      val subcriberEntityListSchema = StructType(
        List(
          StructField("subscriberId", StringType),
          StructField("date", StringType)
        )
      )

      val ContractStaging = List(
        Row("2018-01-16", "1", "2019-01-16", "2019-01-16"),
        Row("2018-01-17", "2", "2019-01-17", "2019-01-16"),
        Row("2018-01-18", "3", "2019-01-18", "2019-01-16"),
        Row("2018-01-19", "4", "2019-01-19", "2019-01-16"),
        Row("2018-01-20", "5", "2019-01-20", "2019-01-16")
      )
      val ContractStagingSchema = StructType(
        List(
          StructField("contractStartDate", StringType),
          StructField("subscriberId", StringType),
          StructField("contractEndDate", StringType),
          StructField("date", StringType)
        )
      )
      val profileDF = spark.createDataFrame(
        spark.sparkContext.parallelize(profileList),
        profileListSchema
      )
      val subsDF = spark.createDataFrame(
        spark.sparkContext
          .parallelize(subcriberEntityList),
        subcriberEntityListSchema
      )
      val entityDF = spark.createDataFrame(
        spark.sparkContext.parallelize(ContractStaging),
        ContractStagingSchema
      )
      val sourceDF = Array(
        ("GlobeSubscriber", subsDF),
        ("subscriberContractStaging", entityDF)
      ).toMap
      val sourceColumns = Array(
        SourceColumn("subscriberContractStaging", "subscriberId"),
        SourceColumn("subscriberContractStaging", "contractEndDate"),
        SourceColumn("subscriberContractStaging", "contractStartDate")
      )
      val joinColumns =
        Option(Array(JoinColumn("subscriberId", "subscriberId")))
      val targetColumn = "lock_in_end_date"
      val df = JoinDirectPullContractStaging
        .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
          spark,
          extraParameters
        )
      val calulatedDF = df.sort("subscriberId").collect().toList
      val expectList = List(
        Row("1", "2019-01-16"),
        Row("2", "2019-01-17"),
        Row("3", "2019-01-18"),
        Row("4", "2019-01-19"),
        Row("5", "2019-01-20")
      )
      assertResult(expectList)(calulatedDF)
  }
}
