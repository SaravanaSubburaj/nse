package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetMostFreqAvailAllDayRoamSurfPostpaidTest extends FlatSpec with SparkTest {
  "join test" should "Test join operation" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val subscriberEntitiyInput = List(
      Row(
        "1",
        "sunil",
        "saini",
        "12/01/2018",
        "ghp",
        "515023409285162",
        "9175882420",
        "GSM",
        "2019-01-16"
      ),
      Row(
        "2",
        "prabhu",
        "gs",
        "12/02/2018",
        "ghp",
        "515023409285163",
        "9175882421",
        "GSM",
        "2019-01-16"
      ),
      Row(
        "3",
        "Ram",
        "Kri",
        "12/01/2018",
        "ghp",
        "515023409285162",
        "9175882420",
        "GSM",
        "2019-01-16"
      ),
      Row(
        "4",
        "Ramk",
        "Kri",
        "12/02/2018",
        "ghp",
        "515023409285163",
        "9175882421",
        "GSM",
        "2019-01-16"
      )
    )

    val schema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("firstName", StringType),
        StructField("lastName", StringType),
        StructField("activationDate", StringType),
        StructField("brandType", StringType),
        StructField("imsi", StringType),
        StructField("msisdn", StringType),
        StructField("subscriptionType", StringType),
        StructField("date", StringType)
      )
    )

    val subsAssignBillOffer = List(
      Row("1", "roam surf 149", "2018-12-01", "2019-01-16"),
      Row("2", "roam surf 149", "2018-03-13", "2019-01-16"),
      Row("1", "roam surf 149", "2018-12-01", "2019-01-16"),
      Row("2", "roam surf 249", "2018-03-13", "2019-01-16"),
      Row("1", "roam surf 249", "2018-12-01", "2019-01-16"),
      Row("2", "roam surf 349", "2018-03-13", "2019-01-16")
    )

    val subsAssignBillOfferSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("billingOfferDescription", StringType),
        StructField("subscriberAssignedBIllingOfferStartDate", StringType),
        StructField("date", StringType)
      )
    )

    val date = "2019-01-16"
    val targetList = List(Row("1", null, date), Row("2", null, date))

    val schema2 = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField(
          "mostFrequentAvailedAllayRoamSurfVariantNamePostpaid",
          StringType
        ),
        StructField("date", StringType)
      )
    )

    val subsDF = spark.createDataFrame(
      spark.sparkContext.parallelize(subscriberEntitiyInput),
      schema
    )
    val subsAssignBillOfferDF =
      spark.createDataFrame(
        spark.sparkContext.parallelize(subsAssignBillOffer),
        subsAssignBillOfferSchema
      )
    val targetDF = spark.createDataFrame(
      spark.sparkContext.parallelize(targetList),
      schema2
    )
    val sourceDF = Array(
      ("Subscriber", subsDF),
      ("subscriberAssignedBillingOffer", subsAssignBillOfferDF)
    ).toMap
    val sourceColumns = Array(
      SourceColumn("subscriberAssignedBillingOffer", "subscriberId"),
      SourceColumn(
        "subscriberAssignedBillingOffer",
        "billingOfferDescription"
      ),
      SourceColumn(
        "subscriberAssignedBillingOffer",
        "subscriberAssignedBIllingOfferStartDate"
      )
    )

    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriberId")))
    val targetColumn = "mostFrequentAvailedAllayRoamSurfVariantNamePostpaid"
    val df =
      GetMostFreqAvailAllDayRoamSurfPostpaid.process(
        sourceDF,
        targetDF,
        sourceColumns,
        joinColumns,
        targetColumn
      )(spark, extraParameters)
    val collectedDF =
      df.select(schema2.fieldNames.map(c => col(c)): _*).collect().toList
    val expectList =
      List(Row("1", "roam surf 149", date), Row("2", null, date))
    assertResult(expectList)(collectedDF)
  }
}
