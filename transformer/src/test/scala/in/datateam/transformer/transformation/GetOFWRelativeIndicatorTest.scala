package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetOFWRelativeIndicatorTest extends FlatSpec with SparkTest {
  "GetOFWRelativeIndicatorTest" should "should indicate OFW relative" in withSparkSession { (spark: SparkSession) =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList =
      List(Row("1", ""), Row("2", ""), Row("3", ""), Row("4", ""))
    val profileListSchema = StructType(
      List(
        StructField("subscriber_id", StringType),
        StructField("ofw_relative_indicator", StringType)
      )
    )

    val subcriberEntityList = List(
      Row("1", "2019-01-16"),
      Row("2", "2019-01-16"),
      Row("3", "2019-01-16"),
      Row("4", "2019-01-16")
    )
    val subcriberEntityListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("date", StringType)
      )
    )

    val billOfferEntity = List(
      Row("169598874741", "ofw", "2019-01-16", "2019-01-16"),
      Row("169598874742", "ofwlike", "2019-01-16", "2019-01-16"),
      Row("169598874743", "likeofw", "2019-01-16", "2019-01-16")
    )
    val billOfferEntitySchema = StructType(
      List(
        StructField("billingOfferId", StringType),
        StructField("billingOfferDescription", StringType),
        StructField("billingOfferSaleExpirationDate", StringType),
        StructField("date", StringType)
      )
    )

    val assignedBillOfferAttributeEntity = List(
      Row("169598874741", "1", "2019-01-15", "2019-01-16"),
      Row("169598874742", "2", "2019-01-15", "2019-01-16"),
      Row("169598874743", "3", "2019-01-15", "2019-01-16")
    )
    val assignedBillOfferAttributeEntitySchema = StructType(
      List(
        StructField("billingOfferId", StringType),
        StructField("subscriberId", StringType),
        StructField("subscriberAssignedBillingOfferStartDate", StringType),
        StructField("date", StringType)
      )
    )

    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subcriberEntityList),
      subcriberEntityListSchema
    )
    val billOfferEntityDF = spark.createDataFrame(
      spark.sparkContext.parallelize(billOfferEntity),
      billOfferEntitySchema
    )

    val assignedBillOfferAttributeEntityDF = spark.createDataFrame(
      spark.sparkContext.parallelize(assignedBillOfferAttributeEntity),
      assignedBillOfferAttributeEntitySchema
    )

    val sourceDF =
      Array(
        ("GlobeSubscriber", subsDF),
        ("billing_offer", billOfferEntityDF),
        ("subscriber_assigned_billing_offer", assignedBillOfferAttributeEntityDF)
      ).toMap
    val sourceColumns = Array(
      SourceColumn("billing_offer", "billingOfferId"),
      SourceColumn("billing_offer", "billingOfferDescription"),
      SourceColumn("billing_offer", "billingOfferSaleExpirationDate"),
      SourceColumn("subscriber_assigned_billing_offer", "billingOfferId"),
      SourceColumn("subscriber_assigned_billing_offer", "subscriberId"),
      SourceColumn("subscriber_assigned_billing_offer", "subscriberAssignedBillingOfferStartDate")
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subscriber_id")))
    val targetColumn = "ofw_relative_indicator"
    val df = GetOFWRelativeIndicator
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val calulatedDF = df.sort("subscriber_id").collect().toList
    val expectList =
      List(Row("1", "1"), Row("2", "1"), Row("3", "1"), Row("4", null))
    assertResult(expectList)(calulatedDF)
  }
}
