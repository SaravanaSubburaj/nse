package in.datateam.transformer.transformation

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.transformer.enums.TransformerEnums
import in.datateam.utils.configparser.JoinColumn
import in.datateam.utils.configparser.SourceColumn

class GetAllDayRoamSurfRegistrationDatePostpaidTest extends FlatSpec with SparkTest {
  "GetAllDayRoamSurfRegistrationDatePostpaidTest" should "get roam " +
  "surf Reistration Date for PospatidTest" in withSparkSession { spark: SparkSession =>
    implicit val extraParameters =
      Map(TransformerEnums.PARTITION_VALUE -> "2019-01-16")
    val profileList = List(Row("1", ""))
    val profileListSchema = StructType(
      List(
        StructField("subsSubscriberID", StringType),
        StructField("allDayRoamSurfLastRegistrationDatePostpaid", StringType)
      )
    )
    val subscriberEntityList = List(Row("1"))
    val subscriberEntityListSchema =
      StructType(List(StructField("subsSubscriberID", StringType)))

    val AssignedBillingOfferList =
      List(Row("1", "roam surf", "2019-01-07", "2019-01-16"))
    val AssignedBillingOfferListSchema = StructType(
      List(
        StructField("subscriberId", StringType),
        StructField("billingOfferDescription", StringType),
        StructField("subscriberAssignedBIllingOfferStartDate", StringType),
        StructField("date", StringType)
      )
    )
    val profileDF = spark.createDataFrame(
      spark.sparkContext.parallelize(profileList),
      profileListSchema
    )
    val subsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(subscriberEntityList),
      subscriberEntityListSchema
    )
    val assignedBillingOfferDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(AssignedBillingOfferList),
      AssignedBillingOfferListSchema
    )

    val sourceDF = Array(
      ("Subscriber", subsDF),
      ("subscriberAssignedBillingOffer", assignedBillingOfferDF)
    ).toMap

    val sourceColumns = Array(
      SourceColumn("subscriberAssignedBillingOffer", "subscriberId"),
      SourceColumn(
        "subscriberAssignedBillingOffer",
        "billingOfferDescription"
      ),
      SourceColumn(
        "subscriberAssignedBillingOffer",
        "subscriberAssignedBIllingOfferStartDate"
      )
    )
    val joinColumns =
      Option(Array(JoinColumn("subscriberId", "subsSubscriberID")))
    val targetColumn = "allDayRoamSurfLastRegistrationDatePostpaid"
    val df = GetAllDayRoamSurfRegistrationDatePostpaid
      .process(sourceDF, profileDF, sourceColumns, joinColumns, targetColumn)(
        spark,
        extraParameters
      )
    val collectedDF = df
      .select(profileListSchema.fieldNames.map(c => col(c)): _*)
      .collect()
      .toList
    val expectList = List(Row("1", "2019-01-07"))
    assertResult(expectList)(collectedDF)
  }
}
