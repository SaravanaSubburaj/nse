package in.datateam.harness

import java.util.TimeZone

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger
import org.scalatest.FlatSpec

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
trait SparkTest {

  import SparkTest.spark

  def withSparkSession(testCode: SparkSession => Unit): Unit = {
    try {
      testCode(spark)
    }
  }
}

object SparkTest extends FlatSpec {

  val logger: Logger = Logger.getLogger(getClass.getName)
  HadoopTest.setUp()
  lazy val conf: SparkConf =
    new SparkConf()
      .setAppName("Cadence User Profile")
      .setMaster("local[*]")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.sql.parquet.compression.codec", "gzip")
      .set("spark.files.overwrite", "true")
      .set("spark.sql.warehouse.dir", "target/tmp/spark-warehouse")
      .set("spark.worker.cleanup.enabled", "true")
      .set("spark.sql.session.timeZone", "UTC")
  spark.sparkContext.setCheckpointDir("target/tmp/spark-checkpoint")
  TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

  logger.info("Starting Spark Session.")
  implicit lazy val spark = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()

}
