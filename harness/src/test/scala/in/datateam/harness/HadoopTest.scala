package in.datateam.harness

import java.io.File

import org.apache.hadoop.fs.Path

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object HadoopTest {

  def setUp(): Unit = {
    val isWindows: Boolean = System.getProperty("os.name").contains("Windows")
    val windowBinariesLocation: String =
      "harness/src/test/resources/hadoop-2.7.2_winbinariesx64"

    if (isWindows) {
      val hadoopHome = new File(windowBinariesLocation).getAbsolutePath
      System.getProperties.put("hadoop.home.dir", hadoopHome)
      System.getProperties
        .put("java.library.path", hadoopHome + Path.SEPARATOR + "bin")
      System.getProperties.put("hadoop.tmp.dir", "target/tmp/hadoop")
      System.getProperties.put("java.io.tmpdir", "target/tmp/local")

      new File(windowBinariesLocation + "/bin").listFiles
        .filter(_.getName.endsWith(".dll"))
        .foreach(file => System.load(file.getAbsolutePath))
    }
    System.getProperties.put("dfs.permissions.enabled", "false")
  }
}
