    function plotGenderGraph(graphDiv,female) {

        var trace = [{
        values: [female,(100-female)],
          labels: ['Female', 'Male'],
          type: 'pie'
        }];

        var layout = {
          height: 350,
          width: 350,
          showlegend:false
        };

        Plotly.newPlot(graphDiv, trace, layout,{displayModeBar: false});

    };




var base_url;
$.getJSON('./dist/js/AppConfig.json',function(data){
    base_url = data.baseUrl;
    ext_ip = base_url.substring(0 , base_url.length-3)
    console.log(base_url);
	getAudience();
});

    function getAudience(search) {
        var query = "";

        if(search == undefined){
            // query = {
            //     "query": {
            //         "wildcard" : {
            //             "user" : "*sunil*"
            //         }
            //     }
            // };
            query = {
                "query": {
                    "bool": {
                        "must": [
                             {"match": {"user": "sunil"}}
                            //  {"match": {"status": "enabled"}}
                        ]
                    }
                }
            }
        } else {
            var my_audience = $('.my-audience-details-scroll .owl-carousel');
            my_audience.owlCarousel('destroy');

            query = {
                "query": {
                    "wildcard" : {
                        "name" : "*"+ search +"*"
                    }
                }
            };
        }

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/audiences/_search',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(query),
            success: function (data) {
                
                if(data != undefined && data.hits.hits.length > 0) {
                    pageAudience.setAudience(data.hits.hits);
                } else {
                    data = "<div style='display: block;text-align: -webkit-center;vertical-align: middle;align-items: center;padding: 250px;font-size: x-large;'>No Search Results Found</div>";
                    $(".my-audience-details-scroll .owl-carousel").html(data);
                }
                          },
            error: function (error) {
                console.log(error);
            }
        });
    }

var pageAudience = {
    myAudiencesCount : 0,
    productSuggestionCount : 0,
    currentMyAudiencePage : 1,
    currentProductPage : 1,
    productsKeyword : [],
    bubbleChartTree : [],
    breadCrumbs : [],
    filters : [],
    user : "sunil",

    getClusterCriteriaKeywords : function (type) {
        var cluster,
            data = pageAudience.bubbleChartTree;

        if(data.length < 1){
            cluster = "none";
        } else if(type == "1"){
            cluster = data.join("-").replace(/\s/g, '').toLowerCase();
        } else {
            cluster = data.join("_").replace(/\s/g, '').toLowerCase();
        }

        return cluster;
    },
    getSearchCriteriaKeywords : function () {
        return pageAudience.productsKeyword.join("_");
    },
    getFilterCriteriaKeywords : function () {
        var filter;

        if(pageAudience.filters.length < 1){
            filter = "none";
        } else {
            filter = pageAudience.filters.join("_");
        }

        return filter;
    },

    sortByName : function () {
        var query = "";

        query = {
            "query": {
                "wildcard" :
                    { "user" : "sunil" }
                },
            "sort": { "name": { "order": "asc" }}
        }   

        var my_audience = $('.my-audience-details-scroll .owl-carousel');
        my_audience.owlCarousel('destroy');

        $.ajax({
        contentType: 'application/json',
        url: base_url + 'test/audiences/_search',
        processData: true,
        dataType: 'json',
        type: "POST",
        data: JSON.stringify(query),
        success: function (data) {
            if(data != undefined && data.hits.hits.length > 0) {
                pageAudience.setAudience(data.hits.hits);
            } else {
                data = "<div style='display: block;text-align: -webkit-center;vertical-align: middle;align-items: center;padding: 250px;font-size: x-large;'>No Search Results Found</div>";
                $(".my-audience-details-scroll .owl-carousel").html(data);
            }
                   },
        error: function (error) {
            console.log(error);
        },
        timeout: 60000
        });
    },  

    setAudience : function (data) {
        var audienceStr = "";

        for(var i = 0; i < data.length; i++){

            var audienceNumber = data.length;
            $('#countAudience').html(audienceNumber);
            // console.log(countAudience);
            var search_keywords = "\""+ data[i]._source["search_criteria"] +"\"";
            var filterOptions = "\""+ data[i]._source["filters"] +"\"";
            var levelOptions = "\""+ data[i]._source["cluster_criteria"] +"\"";

            if(i % 2 === 0){
                audienceStr += "<div class='col-md-3'><div class='my-audience-details-card'>" +
                "<div class='front'>" +
                "<span class='my-audience-details-card-flip pointer'>" +
                "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                "</span>" +
                "<p class='mb-3'>"+ data[i]._source.name +"</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Audience Size:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source.size +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Created:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source.created +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Active Campaigns:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["active campaigns"] +"</span>" +
                "</p>" +
                "<p class='m-0 text-right w-100 my-audience-btn'>" +
                "<span class='mr-2 pointer' onclick='pageAudience.editAudience("+ search_keywords +","+levelOptions+","+filterOptions+")'>" +
                "<img src='../dist/images/Edit%20Audience.svg'>" +
                "</span>" +
                "<span class='pointer' onclick='pageAudience.deleteAudience()'>" +
                "<img src='../dist/images/Delete%20Audience.svg'>" +
                "</span>" +
                "</p>" +
                "</div>" +
                "<div class='back'>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Search Keywords:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["search_criteria"].split("_").join(",") +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Filters:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["filters"].split("_").join(",") +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>level:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["cluster_criteria"] +"</span>" +
                "</p>" +
                "<span class='my-audience-details-card-flip pointer'>" +
                "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                "</span>" +
                "</div>" +
                "</div>";

            }
            else {
                audienceStr +=
                    "<div class='my-audience-details-card mt-2'>" +
                    "<div class='front'>" +
                    "<span class='my-audience-details-card-flip pointer'>" +
                    "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                    "</span>" +
                    "<p class='mb-3'>"+ data[i]._source.name +"</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Audience Size:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.size +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Created:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.created +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Active Campaigns:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["active campaigns"] +"</span>" +
                    "</p>" +
                    "<p class='m-0 text-right w-100 my-audience-btn'>" +
                    "<span class='mr-2 pointer' onclick='pageAudience.editAudience("+ search_keywords +")'>" +
                    "<img src='../dist/images/Edit%20Audience.svg'>" +
                    "</span>" +
                    "<span class='pointer' onclick='pageAudience.deleteAudience()'>" +
                    "<img src='../dist/images/Delete%20Audience.svg'>" +
                    "</span>" +
                    "</p>" +
                    "</div>" +
                    "<div class='back'>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Search Keywords:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["search_criteria"].split("_").join(",") +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Filters:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["filters"].split("_").join(",") +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>level:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["cluster_criteria"] +"</span>" +
                    "</p>" +
                    "<span class='my-audience-details-card-flip pointer'>" +
                    "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                    "</span>" +
                    "</div>" +
                    "</div></div>";
            }
        }

        $(".my-audience-details-scroll .owl-carousel").html(audienceStr);


        pageAudience.myAudiencesCount = Math.ceil($(".my-audience-details-scroll .owl-carousel .col-md-3").length / 4);
        $(".my-audiences").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);

        var my_audience = $('.my-audience-details-scroll .owl-carousel');
        my_audience.owlCarousel({
            mouseDrag : false,
            touchDrag : false,
            pullDrag : false,
            freeDrag : false,
            items : 4,
            slideBy : 4
        });

        $('.my-audience-details-arrows .custom_left_arrow').click(function() {
            my_audience.trigger('prev.owl.carousel');
            if(pageAudience.currentMyAudiencePage > 1) {
                pageAudience.currentMyAudiencePage;
                $(".my-audiences").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);
            }
        });

        $('.my-audience-details-arrows .custom_right_arrow').click(function() {
            my_audience.trigger('next.owl.carousel');
            if(pageAudience.myAudiencesCount > pageAudience.currentMyAudiencePage) {
                pageAudience.currentMyAudiencePage++;
                $(".my-audiences").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);
            }
        });

        $('.my-audience-details-card-flip').click(function (e) {
            $(this).closest(".my-audience-details-card").toggleClass('flipped');
        });

        var height = $(".my-audience-details-card").map(function () {return $(this).height();}).get(),

            maxHeight = Math.max.apply(null, height);

        $(".my-audience-details-card .front").css("min-height",maxHeight);
    },
    searchAudience : function () {
        var search_key = $("#search_audience_btn").val();
        getAudience(search_key);

        return false;
    },

    editAudience : function (keywords,clusters,filters) {
        pageAudience.productsKeyword = [];
        for(var i = 0; i < keywords.split("_").length; i++){
            pageAudience.productsKeyword.push(keywords.split("_")[i]);
        }

        pageAudience.onTagAdded();
        $("#product_search").importTags(pageAudience.productsKeyword.join(","));

        $('html, body').animate({
            'scrollTop' : ($(".product-search").position().top + 75)
        });
    },

    deleteAudience : function (search) {
        console.log("deleted");
            
        query = {
            "doc": 
                {"status": "disabled"}
            }

            console.log();

            // query = {
            //     "query": {
            //         "bool": {
            //             "must": [
            //                  {"match": {"user": "sunil"}},
            //                  {"match": {"status": "enabled"}}
            //             ]
            //         }
            //     }
            // }
        
        
        $.ajax({
        contentType: 'application/json',
        url: base_url + 'test/audiences/AV9OPNjMPOCrgvcL1hUs',
        processData: true,
        dataType: 'json',
        type: "POST",
        data: JSON.stringify(query),
        success: function (data) {
            console.log(data); 
            if(data != undefined && data.hits.hits.length > 0) {
                pageAudience.setAudience(data.hits.hits);
                       
                
            } else {
                data = "<div style='display: block;text-align: -webkit-center;vertical-align: middle;align-items: center;padding: 250px;font-size: x-large;'>No Search Results Found</div>";
                $(".my-audience-details-scroll .owl-carousel").html(data);
            }
        },
        error: function (error) {
            console.log(error);
        },
        timeout: 60000
        });

        

        var my_audience = $('.my-audience-details-scroll .owl-carousel');
        my_audience.owlCarousel('destroy');
    },

    getKeywordSuggestions : function () {
        var search_query = pageAudience.productsKeyword.join("_");

        if(search_query == ""){
            pageAudience.setKeywordSuggestions("");
            return;
        }

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/suggestion/_search?q=_id:' + search_query,
            processData : false,
            dataType: 'json',
            method: "GET",
            success: function(data) {
                if(data != undefined && data.hits.hits.length > 0) {
                    pageAudience.setKeywordSuggestions(data.hits.hits[0]._source.suggestions);
                } else {
                    data = "";
                    pageAudience.setKeywordSuggestions(data);
                }
            },
            error: function(error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setKeywordSuggestions : function (data) {
        var str = "",
            product_keyword_suggestion = $(".product-overview-rds-slider .owl-carousel");

        for(var i in data) {
            if(i == 0 && i%5 == 0){
                str += "<div>"
            }
            str += "<div class='row'>" +
                        "<div class='col-9'>"+ data[i].value +"</div>" +
                        "<div class='col-3'>" +
                            "<label class='label--checkbox'>" +
                                "<input type='checkbox' class='checkbox' data-value='"+ data[i].value +"'>" +
                            "</label>" +
                        "</div>" +
                    "</div>";

            if(i > 0 && i%5 == 0 || i == data.length){
                str += "</div>";
            }
        }

        product_keyword_suggestion.html(str);

        pageAudience.productSuggestionCount = product_keyword_suggestion.children().length;

        $(".product-overview-rds-footer").find(".slider-count").html(pageAudience.currentProductPage + "/" + pageAudience.productSuggestionCount);

        product_keyword_suggestion.owlCarousel({
            mouseDrag : false,
            touchDrag : false,
            pullDrag : false,
            freeDrag : false,
            items : 1,
            slideBy : 1
        });
        $(".product-overview-rds-arrows .custom_left_arrow").click(function () {
            product_keyword_suggestion.trigger('prev.owl.carousel');
            if(pageAudience.currentProductPage > 1) {
                pageAudience.currentProductPage--;
                $(".product-overview-rds-footer").find(".slider-count").html(pageAudience.currentProductPage + "/" + pageAudience.productSuggestionCount);
            }
        });
        $('.product-overview-rds-arrows .custom_right_arrow').click(function() {
            product_keyword_suggestion.trigger('next.owl.carousel');
            if(pageAudience.productSuggestionCount > pageAudience.currentProductPage) {
                pageAudience.currentProductPage++;
                $(".product-overview-rds-footer").find(".slider-count").html(pageAudience.currentProductPage + "/" + pageAudience.productSuggestionCount);
            }
        });

        if(data != "") {
            $(".product-overview-rds-footer").removeClass("hidden-xs-up");
        } else {
            $(".product-overview-rds-footer").addClass("hidden-xs-up");
        }
    },
    suggestionsIncludeKeywords : function () {
          $(".product-overview-rds-slider .label--checkbox .checkbox").each(function () {
                if($(this).prop("checked") == true){
                    pageAudience.productsKeyword.push($(this).attr("data-value"))
                }
          });

        pageAudience.filters = [];
        pageAudience.onTagAdded();
        $("#product_search").importTags(pageAudience.productsKeyword.join(','));
    },

    getBubbleCharts : function (type) {
        pageAudience.bubbleChartChild = [];
        var query = {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"search_criteria": pageAudience.getSearchCriteriaKeywords()}},
                            {"match": {"cluster_criteria": pageAudience.getClusterCriteriaKeywords(type)}},
                            {"match": {"filters": pageAudience.getFilterCriteriaKeywords()}}
                        ]
                    }
                }
        };

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/customer_segments/_search',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(query),
            success: function (data) {
                pageAudience.bubbleChartTree = [];
                pageAudience.breadCrumbs = [];
                if(data != undefined && data.hits.hits.length > 0){
                    data = data.hits.hits[0]._source.bubble_charts[1].sublevels;
                }
                else {
                    data = "";
                    $(".product-overview-breadcrumbs p").html("&nbsp;");
                }
                pageAudience.setBubbleChart(data);
            },
            error: function (error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setBubbleChart : function (root) {
        $("#chart").html("");
        if(root == ""){
            return;
        }
        var vulnerability = "",
            parentText = "";
        var margin = {top: 0, right: 0, bottom: 0, left: 0},
            width = $(".product-overview-graph").width(),
            height = $(".product-overview-graph").height() - margin.top - margin.bottom,
            formatNumber = d3.format(",d"),
            transitioning,
            grandparentClicked = false,
            data = "",
            flag = false;

        var x = d3.scale.linear()
            .domain([0, width])
            .range([0, width]);

        var y = d3.scale.linear()
            .domain([0, height])
            .range([0, height]);

        var treemap = d3.layout.treemap()
            .children(function(d, depth) { return depth ? null : d._children; })
            .sort(function(a, b) { return a.value - b.value; })
            .ratio(height / width * 0.5 * (1 + Math.sqrt(5)))
            .round(false);

        var svg = d3.select("#chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.bottom + margin.top)
            .style("margin-left", -margin.left + "px")
            .style("margin.right", -margin.right + "px")
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .style("shape-rendering", "crispEdges");

        var grandparent = svg.append("g")
            .attr("class", "grandparent"),
            grandparent1 = $("#breadcrumbs_hierarchy");

        /*grandparent.append("rect")
            .attr("y", -margin.top)
            .attr("width", width)
            .attr("height", 50);

        grandparent.append("text")
            .attr("x", 15)
            .attr("y", -35)
            .attr("dy", "1em")
            .attr("style", "font-size:14px");*/

        function treemapCreate() {
            initialize(root);
            accumulate(root);
            layout(root);
            display(root);

            function initialize(root) {
                root.x = root.y = 0;
                root.dx = width;
                root.dy = height;
                root.depth = 0;
            }

            // Aggregate the values for internal nodes. This is normally done by the
            // treemap layout, but not here because of our custom implementation.
            // We also take a snapshot of the original children (_children) to avoid
            // the children being overwritten when when layout is computed.
            function accumulate(d) {
                return (d._children = d.children)
                    ? d.value = d.children.reduce(function(p, v) { return p + accumulate(v); }, 0)
                    : d.value;
            }

            // Compute the treemap layout recursively such that each group of siblings
            // uses the same size (1×1) rather than the dimensions of the parent cell.
            // This optimizes the layout for the current zoom state. Note that a wrapper
            // object is created for the parent node for each group of siblings so that
            // the parent’s dimensions are not discarded as we recurse. Since each group
            // of sibling was laid out in 1×1, we must rescale to fit using absolute
            // coordinates. This lets us use a viewport to zoom.
            function layout(d) {
                if (d._children) {
                    treemap.nodes({_children: d._children});
                    d._children.forEach(function(c) {
                        c.x = d.x + c.x * d.dx;
                        c.y = d.y + c.y * d.dy;
                        c.dx *= d.dx;
                        c.dy *= d.dy;
                        c.parent = d;
                        layout(c);
                    });
                }
            }

            function display(d) {

                pageAudience.bubbleChartChild = d._children;
                grandparent1.unbind().on("click",function () {
                    if(!flag) {
                        grandparentClicked = true;
                        transition(d.parent);
                    }

                });

                grandparent
                    .datum(d.parent)
                    .on("click",function () {
                        grandparentClicked = true;
                        transition(d.parent);
                    })
                    .select("text")
                    .text(name(d));

                parentText = d.parent;

                var g1 = svg.insert("g", ".grandparent")
                    .datum(d)
                    .attr("class", "depth");

                var g = g1.selectAll("g")
                    .data(d._children)
                    .enter().append("g");

                g.filter(function(d) { return d._children; })
                    .classed("children", true)
                    .on("click", transition);

                g.selectAll(".child")
                    .data(function(d) { return d._children || [d]; })
                    .enter().append("rect")
                    .attr("class", "child")
                    .call(rect);

                /**
                 * ADD CLICK EVENT FOR Non Parent Nodes
                 */
                g.filter(function(d) { return !d._children; })
                    .classed("leafNode", true)
                    .on("click", displayResult);

                g.append("rect")
                    .attr("class", "parent")
                    .call(rect)
                    .append("title")
                    .text(function(d) { return formatNumber(d.value); });

                g.append("text")
                    .attr("dy", ".75em")
                    .text(function(d) { return d.name; })
                    .call(text);

                displayResult(d);
///
                /**
                 * ADD FUNCTION FOR Non Parent Node Click Event
                 */
                function displayResult(d){

                    if(!grandparentClicked) {
                        pageAudience.bubbleChartHierarchy(d);
                    } else {
                        pageAudience.removeBubbleChartHierarchy();
                        grandparentClicked = false;
                    }

                    var newDataSet = vulnerability[d.name];
                    if(!newDataSet){
                        newDataSet = [];
                    }
                }

                function transition(d) {
                    if (transitioning || !d) return;
                    transitioning = true;

                    var g2 = display(d),
                        t1 = g1.transition().duration(750),
                        t2 = g2.transition().duration(750);

                    // Update the domain only after entering new elements.
                    x.domain([d.x, d.x + d.dx]);
                    y.domain([d.y, d.y + d.dy]);

                    // Enable anti-aliasing during the transition.
                    svg.style("shape-rendering", null);

                    // Draw child nodes on top of parent nodes.
                    svg.selectAll(".depth").sort(function(a, b) { return a.depth - b.depth; });

                    // Fade-in entering text.
                    g2.selectAll("text").style("fill-opacity", 0);

                    // Transition to the new view.
                    t1.selectAll("text").call(text).style("fill-opacity", 0);
                    t2.selectAll("text").call(text).style("fill-opacity", 1);
                    t1.selectAll("rect").call(rect);
                    t2.selectAll("rect").call(rect);

                    // Remove the old node when the transition is finished.
                    t1.remove().each("end", function() {
                        svg.style("shape-rendering", "crispEdges");
                        transitioning = false;
                    });
                }

                return g;
            }

            function text(text) {
                text.attr("x", function(d) { return x(d.x) + 15; })
                    .attr("y", function(d) { return y(d.y) + 15; });
            }

            function rect(rect) {
                rect.attr("x", function(d) { return x(d.x); })
                    .attr("y", function(d) { return y(d.y); })
                    .attr("width", function(d) { return x(d.x + d.dx) - x(d.x); })
                    .attr("height", function(d) { return y(d.y + d.dy) - y(d.y); });
            }

            function name(d) {
                return d.parent ? name(d.parent) + ' / ' + d.name : d.name;
            }
        }

        treemapCreate();
    },
    bubbleChartHierarchy : function (data) {
        var name = data.name,
            parent = data.parent;

        if(data._children){
            if(data.parent) {
                pageAudience.bubbleChartTree.push(name);
            }
            pageAudience.breadCrumbs.push(name);
            function runLoop(data) {
                $.each(data, function(key, val) { recursiveFunction(key, val) });
                function recursiveFunction(key, val) {
                    if(key['parent'] != undefined){
                        var value = key['parent'];
                        if (value instanceof Object) {
                            $.each(value, function(key, val) {
                                recursiveFunction(key, val)
                            });
                        }
                    }
                }
            }
            runLoop(data);

            $(".product-overview-breadcrumbs p").html(pageAudience.breadCrumbs.join("<span> > </span>"));
            pageAudience.getCustomerProfile();
        }
    },
    removeBubbleChartHierarchy : function () {
        pageAudience.bubbleChartTree.pop();
        pageAudience.breadCrumbs.pop();

        $(".product-overview-breadcrumbs p").html(pageAudience.breadCrumbs.join("<span> > </span>"));
        pageAudience.getCustomerProfile();
    },

    getCustomerProfile : function () {
        var query = {
            "query": {
                "bool": {
                    "must": [
                        {"match": {"search_criteria": pageAudience.getSearchCriteriaKeywords()}},
                        {"match": {"cluster_criteria": pageAudience.getClusterCriteriaKeywords()}}
                    ]
                }
            }
        };

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/customer_profile_detail/_search',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(query),
            success: function (data) {
                if(data != undefined && data.hits.hits.length > 0){
                    pageAudience.setProductOverview(data.hits.hits[0]._source.score);  
                    console.log("setting product overview");                  
                    pageAudience.setCustomerProfile(data.hits.hits[0]._source.customer_profile_details);
                    
                    console.log("graph done");
                }
                else {
                    pageAudience.setProductOverview("");
                    pageAudience.setCustomerProfile("");
                }
            },
            error: function (error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setProductOverview : function (data) {
        var str = "";
        for(var i in data){
            if(data[i].type == "No. of Customers"){
                pageAudience.size = data[i].value;
            } else if (data[i].type == "Females") {                                
                var females = parseFloat(data[i].value);
                plotGenderGraph("audience-overview",females);

            }
            str += "<p class='mb-0'><span class='product-overview-details-title'>"+ data[i].type +"</span><br><span class='product-overview-details-list'>"+ data[i].value +"</span></p>";
        }
        $(".product-overview-details").html(str);

        console.log("setting audience overview");
                    

    },
    setCustomerProfile : function (data) {
        var table_header = "",
            table_header_str = "",
            table_body_str = "";

        for(var i in data) {
            if(i == 0){
                table_header = Object.keys(data[i]);
            }

            table_body_str += "<tr>";

            var count = 0;

            for(var j in data[i]){
                if(count == 0){
                    table_body_str += "<td><a href='"+ ext_ip +"profile/customer_detail.html?customerId="+ data[i][j] +"'>"+ data[i][j] +"</a></td>";
                    count ++;
                } else {
                    table_body_str += "<td>"+ data[i][j] +"</td>";
                }
            }

            table_body_str += "</tr>";
        }

        for(var k in table_header){
            table_header_str += "<th>" + table_header[k] + "</th>";
        }

        $(".target-audience-table thead").html("<tr>" + table_header_str + "</tr>");
        $(".target-audience-table tbody").html(table_body_str);
    },

    onTagAdded: function() {
        pageAudience.getKeywordSuggestions();
        pageAudience.getBubbleCharts();
        pageAudience.getCustomerProfile();
        pageAudience.getFilterCriteria();
    },

    getFilterCriteria : function () {
        var query = {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"search_criteria": pageAudience.getSearchCriteriaKeywords()}},
                            {"match": {"filters": pageAudience.getFilterCriteriaKeywords()}},
                            {"match": {"cluster_criteria": pageAudience.getClusterCriteriaKeywords()}}
                        ]
                    }
                }
            };

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/data_filters/_search',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(query),
            success: function (data) {
                if(data != undefined && data.hits.hits.length > 0){
                    pageAudience.filterCriteria = data.hits.hits[0]._source.criteria;
                    pageAudience.setFilterCriteria(data.hits.hits[0]._source.criteria);
                    pageAudience.setFilterCriteriaModal();
                } else {
                    pageAudience.filterCriteria = [];
                    $(".filter-criteria").html("");
                    pageAudience.setFilterCriteriaModal();
                }
            },
            error: function (error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setFilterCriteria : function (data) {
        $(".filter-criteria").html("");

        pageAudience.filterCriteriaSelected = [];

        var filterStr = "";
        for (var i = 0; i < data.length; i++){
            if(i < 3){
                if(data[i].type == "continuous"){
                    pageAudience.filterCriteriaSelected.push(data[i].name + "-" + data[i].range.min + "-" + data[i].range.max);
                    filterStr = "<div class='col-md-4'>" +
                        "<div id='slider"+ data[i].rank +"' class='rslider' data-name='"+ data[i].name +"' data-range-min='"+ (data[i].range.min) +"' data-range-max='"+ (data[i].range.max) +"'></div>" +
                        "<p class='text-center text-capitalize'>"+ data[i].name +"</p>" +
                        "</div>";

                    $(".filter-criteria").append(filterStr);

                    $("#slider" + data[i].rank).roundSlider({
                        handleShape: "dot",
                        editableTooltip : false,
                        radius: 70,
                        width: 4,
                        min: data[i].range.min,
                        max: data[i].range.max,
                        sliderType: "range",
                        showTooltip: false,
                        value: (data[i].range.min) + "," + (data[i].range.max),
                        create : function (create) {
                            pageAudience.updateFilterTooltip($("#slider" + data[i].rank),create.value,1);
                        },
                        stop: function (stop) {
                            pageAudience.updateFilterTooltip(stop.handle.element[0],stop.value,2);
                            pageAudience.changeFilters();
                        },
                        drag : function (drag) {
                            pageAudience.updateFilterTooltip(drag.handle.element[0],drag.value,2);
                        },
                        change : function (change) {
                            pageAudience.updateFilterTooltip(change.handle.element[0],change.value,2);
                        },
                        handleSize: "+10"
                    });
                } else {
                    var count = data[i].values.split(","),
                        checkbox = "";

                    for(var j = 0; j < count.length; j++){
                        checkbox += "<div class='custom-checkbox' style='top: 23%;'>" +
                            "<div style='width: 100%;font-size: 13px;text-transform: capitalize'>"+ count[j] +"</div>" +
                            "<label class='label--checkbox'>" +
                            "<input type='checkbox' class='checkbox filter-checkbox' data-name='"+ count[j] +"' data-value='"+ data[i].rank +"' checked onclick='pageAudience.updateCheckboxBorder(this)'>" +
                            "</label>" +
                            "</div>";
                    }

                    filterStr = "<div class='col-md-4'>" +
                        "<div class='filter-criteria-checkbox'>" +
                        "<div class='custom-tooltip'>" +
                        checkbox +
                        "</div>" +
                        "</div>" +
                        "<p class='text-center text-capitalize'>"+ data[i].name +"</p>" +
                        "</div>";
                    
                    $(".filter-criteria").append(filterStr);

                    pageAudience.filterCriteriaSelected.push(data[i].name + "-" + count.join("-"));
                }
            }
        }

        pageAudience.changeFilters(1);
    },
    changeFilters : function (type) {
        pageAudience.filters = [];

        $(".rslider").each(function(){
            pageAudience.filters.push($(this).attr("data-name") + "-" + $(this).attr("data-range-min") + "-" + $(this).attr("data-range-max"));
        });

        var filter = [],
            filterName = "";

        $(".filter-criteria-checkbox").each(function () {
            filterName = $(this).siblings("p").html();
                $(this).find(".filter-checkbox").each(function () {
                    if($(this).prop("checked") == true){
                    filter.push($(this).attr("data-name"));
                }
            });

            if(filter.length > 0) {
                pageAudience.filters.push(filterName + "-" + filter.join("-"));
            }
        });

        if(type != 1) {
            pageAudience.getBubbleCharts();
        }
    },

    updateFilterTooltip : function (elem,values,type) {
        var min = values.split(",")[0],
            max = values.split(",")[1];

        $(elem).closest(".rslider").attr("data-range-min",min);
        $(elem).closest(".rslider").attr("data-range-max",max);

        if( type == 1) {
            $(elem).find(".rs-container").append("<div class='custom-tooltip'>from<div style='width: 75px;background: #F6F6F6;font-size: 12px;margin: 2px 0;padding: 2px 0;'>" + min + "</div>to<div style='width: 75px;background: #F6F6F6;font-size: 12px;margin: 2px 0;padding: 2px 0;'>" + max + "</div></div>");
        } else {
            $(elem).closest(".rs-container").find(".custom-tooltip").html("from<div style='width: 75px;background: #F6F6F6;font-size: 12px;margin: 2px 0;padding: 2px 0;'>" + min + "</div>to<div style='width: 75px;background: #F6F6F6;font-size: 12px;margin: 2px 0;padding: 2px 0;'>" + max + "</div>");
        }
    },
    updateCheckboxBorder : function (elem) {
        var total = 0,
            checked = 0,
            degree,
            cssStyle = "";

        $(elem).closest(".filter-criteria-checkbox").find(".checkbox").each(function () {
            if($(this).prop("checked") == true){
                checked++;
            }
            total++;
        });
        /* 10% = 126deg = 90 + ( 360 * .1 ) */

        degree = parseInt(checked) / parseInt(total);

        switch (degree){
            case 0:
                cssStyle = "none";
                break;

            case 0.5:
                cssStyle = "linear-gradient(0deg, #56B9E4 50%, #ededed 50%)";
                break;

            case 1:
                cssStyle = "linear-gradient(-214deg, #64C4C9 0%, #56B9E4 100%)";
                break;

            default :
                cssStyle = "linear-gradient("+ (90 + (360 * degree)) +"deg, transparent 50%, #64C4C9 50%),linear-gradient(90deg, #56B9E4 50%, #ededed 50%)"
        }

        $(elem).closest(".filter-criteria-checkbox").css({"background-image":cssStyle});
        pageAudience.changeFilters();
    },

    setFilterCriteriaModal : function () {
        var data = pageAudience.filterCriteria,
            str = "",
            checked = "checked";

        for(var i = 0; i < data.length; i++){
            if(i > 2){
                checked = "";
            }
            str +=  "<div class='col-md-4'>" +
                        "<div class='row align-items-center'>" +
                            "<div class='col-7 p-0 text-capitalize'>"+ data[i].name +"</div>" +
                            "<div class='col-5 p-0'>" +
                                "<label class='label--checkbox'>" +
                                    "<input type='checkbox' class='checkbox' data-value='"+ i +"' "+ checked +" onchange='pageAudience.limitFilterCriteria(this)'>" +
                                "</label>" +
                            "</div>" +
                        "</div>" +
                    "</div>";
        }


        $("#filter_criteria").find(".modal-body .row").html(str);
    },
    limitFilterCriteria : function (elem) {
        var limit = 3,
            checkedCount = 0;

        $("#filter_criteria .label--checkbox .checkbox").each(function () {
            if($(this).prop("checked") == true){
                checkedCount++;
            }
        });

        if(checkedCount > limit){
            $(elem).prop("checked",false);
        }
    },
    applyFilterCriteria : function () {
        var data = [];
        $("#filter_criteria .label--checkbox .checkbox").each(function () {
            if($(this).prop("checked") == true){
                data.push(pageAudience.filterCriteria[$(this).attr("data-value")]);
            }
        });

        pageAudience.setFilterCriteria(data);
        pageAudience.getBubbleCharts();
        $("#filter_criteria").modal("hide");
    },

    generateUniqueId : function () {
        var min = 10000,
            max = 999999;

        return (Math.random() * ((max ? max : min) - (max ? min : 0) + 1) + (max ? min : 0)) | 0;
    },

    openSendCampaignModal : function () {
        $("#send_campaign").modal("show");
    },
    
    openSaveAudienceModal : function () {
        $("#audience_name").val("");
        $("#save_audience").modal("show");
    },
    validateAudienceSelection : function () {
        var name = $("#audience_name").val();

        if(name.trim().length < 1){
            $("#audience_name").closest(".input-group").siblings(".error").html("<p>Please add a name</p>");

            setTimeout(function () {
                $("#audience_name").closest(".input-group").siblings(".error").html("<p></p>");
            },2000);
            return false;
        }

        var m_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var d = new Date(),
            curr_date = d.getDate(),
            curr_month = d.getMonth(),
            curr_year = d.getFullYear(),
            date = "";

        switch (curr_date){
            case "01":
            case "21":
            case "31":
                date = curr_date + "st";
                break;

            case "02":
            case "22":
                date = curr_date + "nd";
                break;

            case "03":
            case "23" :
                date = curr_date + "rd";
                break;

            default :
                date = curr_date + "th";
        }

        date = date + " " + m_names[curr_month] + " " + curr_year;
        var data = {
            "user": pageAudience.user,
            "name": name,
            "size": pageAudience.size,
            "created": date,
            "search_criteria": pageAudience.getSearchCriteriaKeywords(),
            "filters": pageAudience.getFilterCriteriaKeywords(),
            "cluster_criteria": pageAudience.getClusterCriteriaKeywords(),
            "session" : localStorage.getItem("session"),
            "active campaigns" : "0",
            "status": "enabled"
        };

        return pageAudience.saveAudienceSelection(data);
    },
    saveAudienceSelection : function (data) {
        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/audiences/',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(data),
            beforeSend : function () {
                $("#save_audience_submit").html("<img src='../dist/images/Loading.svg'> Saving Audience").prop("disabled",true);
            },
            success: function (data) {
                console.log(data);
                setTimeout(function () {
                    pageAudience.getSavedAudiences();
                    $("#save_audience").modal("hide");
                    $("#save_audience_submit").html("Save Audience").prop("disabled",false);
                },1000);
            },
            error: function (error) {
                console.log(error);
            },
            timeout: 60000
        });

        return false;
    },

    getSavedAudiences : function () {
        var query = {
            "query": {
                "bool": {
                    "must": [
                        {"match": {"session": localStorage.getItem("session")}}
                    ]
                }
            }
        };

        var my_audience = $('.saved-audience-grid .owl-carousel');
        my_audience.owlCarousel('destroy');

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/audiences/_search',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(query),
            success: function (data) {
                if(data != undefined && data.hits.hits.length > 0) {
                    pageAudience.setSavedAudiences(data.hits.hits);
                    $(".product-audience").removeClass("hidden-xs-up");
                } else {
                    data = "<div style='display: block;text-align: -webkit-center;vertical-align: middle;align-items: center;padding: 250px;font-size: x-large;'>No Search Results Found</div>";
                    $(".product-audience .owl-carousel").html(data);
                }
            },
            error: function (error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setSavedAudiences : function (data) {
        var savedAudience = "";

        for(var i = 0; i < data.length; i++){
            if(i % 2 === 0){
                savedAudience += "<div class='col-md-4'>" +
                    "<div class='saved-audience-details-card'>" +
                    "<div class='front'>" +
                    "<div class='saved-audience-checkbox'>" +
                    "<label class='label--checkbox'>" +
                    "<input type='checkbox' class='checkbox' data-value='"+ data[i]._source.value +"'>" +
                    "</label>" +
                    "</div>" +
                    "<p class='mb-3 mr-3'>"+ data[i]._source.name +"</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Audience Size:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.size +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Created:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.created +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Active Campaigns:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["active campaigns"] +"</span>" +
                    "</p>" +
                    "</div>" +
                    "</div>";

            }
            else {
                savedAudience +=
                    "<div class='saved-audience-details-card mt-2'>" +
                    "<div class='front'>" +
                    "<div class='saved-audience-checkbox'>" +
                    "<label class='label--checkbox'>" +
                    "<input type='checkbox' class='checkbox' data-value='"+ data[i]._source.value +"'>" +
                    "</label>" +
                    "</div>" +
                    "<p class='mb-3 mr-3'>"+ data[i]._source.name +"</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Audience ID:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.id +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Audience Size:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.size +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Created:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.created +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Active Campaigns:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["active campaigns"] +"</span>" +
                    "</p>" +
                    "</div>" +
                    "</div></div>";
            }
        }

        $(".saved-audience-grid .owl-carousel").html(savedAudience);

        pageAudience.myAudiencesCount = Math.ceil($(".saved-audience-grid .owl-carousel .col-md-4").length / 3);
        $(".product-audience").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);

        var my_audience = $('.saved-audience-grid .owl-carousel');
        my_audience.owlCarousel({
            mouseDrag : false,
            touchDrag : false,
            pullDrag : false,
            freeDrag : false,
            items : 3,
            slideBy : 3
        });

        $('.saved-audience-details-arrows .custom_left_arrow').click(function() {
            my_audience.trigger('prev.owl.carousel');
            if(pageAudience.currentMyAudiencePage > 1) {
                pageAudience.currentMyAudiencePage--;
                $(".product-audience").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);
            }
        });

        $('.saved-audience-details-arrows .custom_right_arrow').click(function() {
            my_audience.trigger('next.owl.carousel');
            if(pageAudience.myAudiencesCount > pageAudience.currentMyAudiencePage) {
                pageAudience.currentMyAudiencePage++;
                $(".product-audience").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);
            }
        });

        var height = $(".saved-audience-details-card").map(function () {return $(this).height();}).get(),

            maxHeight = Math.max.apply(null, height);

        $(".saved-audience-details-card .front").css("min-height",maxHeight);

        setTimeout(function () {
            $('html, body').animate({
                'scrollTop' : ($(".product-audience").position().top + 90)
            });
        },300);
    },

    openMergeGridModal : function () {
        var data = pageAudience.bubbleChartChild,
            str = "";

        for(var i = 0; i < data.length; i++){
            str +=  "<div class='col-md-6'>" +
                "<div class='row align-items-center'>" +
                "<div class='col-7 p-0 text-capitalize'>"+ data[i].name + " <small class='text-muted'>(" +  data[i].value +")</small></div>" +
                "<div class='col-5 p-0'>" +
                "<label class='label--checkbox'>" +
                "<input type='checkbox' class='checkbox merge-grid-checkbox' data-name='"+ data[i].name +"'>" +
                "</label>" +
                "</div>" +
                "</div>" +
                "</div>";
        }

        $("#merge_grid .modal-body .row").html(str);
        $("#merge_grid").modal("show");
    },
    applyMergeGrid : function () {
        $(".merge-grid-checkbox").each(function () {
            if($(this).prop("checked") == true){
                pageAudience.bubbleChartTree.push($(this).attr("data-name"));
            }
        });

        pageAudience.filters = [];

        $("#merge_grid").modal("hide");
        pageAudience.getBubbleCharts(1);
    },

    removeDiv : function (elem) {
        $(elem).closest(".rds-suggestions").addClass('animated bounceOutRight');

        setTimeout(function () {
            $(elem).closest(".rds-suggestions").remove();
        },500);
    }
};

$(document).ready(function () {
    localStorage.setItem("session",pageAudience.generateUniqueId());
   // getAudience();
});

$(window).on("load",function () {
    $(".rds_include_search").on("click",function () {
        pageAudience.suggestionsIncludeKeywords();
    });

    $(".change-criteria-btn").on("click",function () {
        $("#filter_criteria").modal("show");
    });

    $(".save-filter-criteria").on("click",function () {
        pageAudience.applyFilterCriteria();
    });

    $("#send_campaign_btn").on("click",function () {
        pageAudience.openSendCampaignModal();
    });

    $("#save_audience_btn").on("click",function () {
        pageAudience.openSaveAudienceModal();
    });

    $(".save-merge-grid").on("click",function () {
        pageAudience.applyMergeGrid();
    });

    $("#filter-criteria-select").multiselect();

    $("#product_search").tagsInput({
        'height':'48px',
        'width':'100%',
        'interactive':true,
        'defaultText':' ',
        'delimiter': [','],
        'removeWithBackspace' : true,
        'unique' : true,
        'onAddTag': function (add) {
            pageAudience.filters = [];
            pageAudience.productsKeyword.push(add);
            pageAudience.onTagAdded();
        },
        'onRemoveTag': function (remove) {
            var i = pageAudience.productsKeyword.indexOf(remove);
            if(i != -1) {
                pageAudience.productsKeyword.splice(i, 1);
            }
            pageAudience.onTagAdded();
        },
        'onChange' : function (changed) {

        },
        'placeholderColor' : '#666666'
    });

    $(".my-audience-rds .rds-suggestions-body").owlCarousel({
        mouseDrag : false,
        touchDrag : false,
        pullDrag : false,
        freeDrag : false,
        items : 1,
        slideBy : 1
    });

    $(".saved-audience-rds .rds-suggestions-body").owlCarousel({
        mouseDrag : false,
        touchDrag : false,
        pullDrag : false,
        freeDrag : false,
        items : 1,
        slideBy : 1
    });
});