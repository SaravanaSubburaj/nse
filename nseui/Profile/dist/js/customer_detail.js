var base_url;
var customerEntity;
   var searchParam = "";
$.getJSON('./dist/js/AppConfig.json',function(data){
    base_url = data.baseUrl;
    console.log(base_url);
        var queryString = new Array();
        $(function () {

            if (queryString.length == 0) {
                if (window.location.search.split('?').length > 1) {
                    var params = window.location.search.split('?')[1].split('&');
                    for (var i = 0; i < params.length; i++) {
                        var key = params[i].split('=')[0];
                        var value = decodeURIComponent(params[i].split('=')[1]);
                        queryString[key] = value;
                    }
                }
            }
            if (queryString["customerId"] != null) {
                // console.log(queryString['search_key']);
                searchParam = queryString['customerId'];

                getCustomerProfile();
            }
        });

});
$.getJSON('./dist/js/WebConfig.json',function(data){
    customerEntity = data;
    console.log("here");
    console.log(customerEntity['language']);
});

var pageAudience = {
    myAudiencesCount : 0,
    productSuggestionCount : 0,
    currentMyAudiencePage : 1,
    currentProductPage : 1,
    productsKeyword : [],
    currentUser : "",
    

    getAudience : function (search) {
        var query = "";

        if(search == undefined){
            query = {
                "query": {
                    "wildcard" : {
                        "user" : "*sunil*"
                    }
                }
            };
        } else {
            var my_audience = $('.my-audience-details-scroll .owl-carousel');
            my_audience.owlCarousel('destroy');

            query = {
                "query": {
                    "wildcard" : {
                        "name" : "*"+ search +"*"
                    }
                }
            };
        }

        $.ajax({
            contentType: 'application/json',
            url: base_url + '/test/audiences/_search',
            processData: true,
            dataType: 'json',
            type: "POST",
            data: JSON.stringify(query),
            success: function (data) {
                console.log(data);
                if(data != undefined && data.hits.hits.length > 0) {
                  //  pageAudience.setAudience(data.hits.hits);
                } else {
                    data = "<div style='display: block;text-align: -webkit-center;vertical-align: middle;align-items: center;padding: 250px;font-size: x-large;'>No Search Results Found</div>";
                    $(".my-audience-details-scroll .owl-carousel").html(data);
                }
            },
            error: function (error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setAudience : function (data) {
        var audienceStr = "";
        for(var i = 0; i < data.length; i++){

            var search_keywords = "\""+ data[i]._source["search_keywords"] +"\"";

            if(i % 2 === 0){
                audienceStr += "<div class='col-md-3'><div class='my-audience-details-card'>" +
                "<div class='front'>" +
                "<span class='my-audience-details-card-flip pointer'>" +
                "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                "</span>" +
                "<p class='mb-3'>"+ data[i]._source.name +"</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Audience Size:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source.size +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Created:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source.created +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Active Campaigns:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["active campaigns"] +"</span>" +
                "</p>" +
                "<p class='m-0 text-right w-100'>" +
                "<span class='mr-2 pointer' onclick='pageAudience.editAudience("+ search_keywords +")'>" +
                "<img src='../dist/images/Edit%20Audience.svg'>" +
                "</span>" +
                "<span class='pointer'>" +
                "<img src='../dist/images/Delete%20Audience.svg'>" +
                "</span>" +
                "</p>" +
                "</div>" +
                "<div class='back'>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Search Keywords:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["search_keywords"] +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>Filters:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["filters"] +"</span>" +
                "</p>" +
                "<p class='mb-2'>" +
                "<span class='card-label'>level:</span>" +
                "<br>" +
                "<span class='card-value'>"+ data[i]._source["level"] +"</span>" +
                "</p>" +
                "<span class='my-audience-details-card-flip pointer'>" +
                "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                "</span>" +
                "</div>" +
                "</div>";

            }
            else {
                audienceStr +=
                    "<div class='my-audience-details-card mt-2'>" +
                    "<div class='front'>" +
                    "<span class='my-audience-details-card-flip pointer'>" +
                    "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                    "</span>" +
                    "<p class='mb-3'>"+ data[i]._source.name +"</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Audience ID:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.id +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Audience Size:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.size +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Created:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source.created +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Active Campaigns:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["active campaigns"] +"</span>" +
                    "</p>" +
                    "<p class='m-0 text-right w-100'>" +
                    "<span class='mr-2 pointer' onclick='pageAudience.editAudience("+ search_keywords +")'>" +
                    "<img src='../dist/images/Edit%20Audience.svg'>" +
                    "</span>" +
                    "<span class='pointer'>" +
                    "<img src='../dist/images/Delete%20Audience.svg'>" +
                    "</span>" +
                    "</p>" +
                    "</div>" +
                    "<div class='back'>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Search Keywords:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["search_keywords"] +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>Filters:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["filters"] +"</span>" +
                    "</p>" +
                    "<p class='mb-2'>" +
                    "<span class='card-label'>level:</span>" +
                    "<br>" +
                    "<span class='card-value'>"+ data[i]._source["level"] +"</span>" +
                    "</p>" +
                    "<span class='my-audience-details-card-flip pointer'>" +
                    "<img src='../dist/images/Flip%20Campaign%20Folder%202.svg'>" +
                    "</span>" +
                    "</div>" +
                    "</div></div>";
            }
        }

        $(".my-audience-details-scroll .owl-carousel").html(audienceStr);


        pageAudience.myAudiencesCount = Math.ceil($(".my-audience-details-scroll .owl-carousel .col-md-3").length / 4);
        $(".my-audiences").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);

        var my_audience = $('.my-audience-details-scroll .owl-carousel');
        my_audience.owlCarousel({
            mouseDrag : false,
            touchDrag : false,
            pullDrag : false,
            freeDrag : false,
            items : 4,
            slideBy : 4
        });

        $('.my-audience-details-arrows .custom_left_arrow').click(function() {
            my_audience.trigger('prev.owl.carousel');
            if(pageAudience.currentMyAudiencePage > 1) {
                pageAudience.currentMyAudiencePage--;
                $(".my-audiences").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);
            }
        });

        $('.my-audience-details-arrows .custom_right_arrow').click(function() {
            my_audience.trigger('next.owl.carousel');
            if(pageAudience.myAudiencesCount > pageAudience.currentMyAudiencePage) {
                pageAudience.currentMyAudiencePage++;
                $(".my-audiences").find(".slider-count").html(pageAudience.currentMyAudiencePage + "/" + pageAudience.myAudiencesCount);
            }
        });

        $('.my-audience-details-card-flip').click(function (e) {
            $(this).closest(".my-audience-details-card").toggleClass('flipped');
        });
    },
    searchAudience : function () {
        var search_key = $("#search_audience_btn").val();
        pageAudience.getAudience(search_key);

        return false;
    },
    editAudience : function (keywords) {
        pageAudience.productsKeyword = [];
        for(var i = 0; i < keywords.split(",").length; i++){
            pageAudience.productsKeyword.push(keywords.split(",")[i]);
        }

        pageAudience.onTagAdded();
        $("#product_search").importTags(keywords);
    },

    getKeywordSuggestions : function () {
        var search_query = pageAudience.productsKeyword.join("_");

        $.ajax({
            contentType: 'application/json',
            url: base_url + 'test/suggestion/_search?q=_id:' + search_query,
            processData : false,
            dataType: 'json',
            method: "GET",
            success: function(data) {
                console.log(data);
                if(data != undefined && data.hits.hits.length > 0) {
                    pageAudience.setKeywordSuggestions(data.hits.hits[0]._source.suggestions);
                } else {
                    data = "";
                    pageAudience.setKeywordSuggestions(data);
                }
            },
            error: function(error) {
                console.log(error);
            },
            timeout: 60000
        });
    },
    setKeywordSuggestions : function (data) {
        var str = "",
            product_keyword_suggestion = $(".product-overview-rds-slider .owl-carousel");

        for(var i in data) {
            if(i == 0 && i%5 == 0){
                str += "<div>"
            }
            str += "<div class='row'>" +
                        "<div class='col-9'>"+ data[i].value +"</div>" +
                        "<div class='col-3'>" +
                            "<label class='label--checkbox'>" +
                                "<input type='checkbox' class='checkbox' data-value='"+ data[i].value +"'>" +
                            "</label>" +
                        "</div>" +
                    "</div>";

            if(i > 0 && i%5 == 0 || i == data.length){
                str += "</div>";
            }
        }

        product_keyword_suggestion.html(str);

        pageAudience.productSuggestionCount = product_keyword_suggestion.children().length;

        $(".product-overview-rds-footer").find(".slider-count").html(pageAudience.currentProductPage + "/" + pageAudience.productSuggestionCount);

        product_keyword_suggestion.owlCarousel({
            mouseDrag : false,
            touchDrag : false,
            pullDrag : false,
            freeDrag : false,
            items : 1,
            slideBy : 1
        });
        $(".product-overview-rds-arrows .custom_left_arrow").click(function () {
            product_keyword_suggestion.trigger('prev.owl.carousel');
            if(pageAudience.currentProductPage > 1) {
                pageAudience.currentProductPage--;
                $(".product-overview-rds-footer").find(".slider-count").html(pageAudience.currentProductPage + "/" + pageAudience.productSuggestionCount);
            }
        });
        $('.product-overview-rds-arrows .custom_right_arrow').click(function() {
            product_keyword_suggestion.trigger('next.owl.carousel');
            if(pageAudience.productSuggestionCount > pageAudience.currentProductPage) {
                pageAudience.currentProductPage++;
                $(".product-overview-rds-footer").find(".slider-count").html(pageAudience.currentProductPage + "/" + pageAudience.productSuggestionCount);
            }
        });

        if(data != "") {
            $(".product-overview-rds-footer").removeClass("hidden-xs-up");
        } else {
            $(".product-overview-rds-footer").addClass("hidden-xs-up");
        }
    },
    suggestionsIncludeKeywords : function () {
          $(".label--checkbox .checkbox").each(function () {
                if($(this).prop("checked") == true){
                    pageAudience.productsKeyword.push($(this).attr("data-value"))
                }
          });

        pageAudience.onTagAdded();
        $("#product_search").importTags(pageAudience.productsKeyword.join(','));
    }
};