
var express = require('express')
//var cors=require('cors')

var app = express()
//app.use(cors)
//app.options('*', cors());

//const dse = require('dse-driver');
//const dseGraph = require('dse-graph');


//const client = new dse.Client({
//  contactPoints: ['localhost'],
//  graphOptions:  { name: 'Barclays' }
//});


// Obtain a traversal source, used to create traversals
//const g = dseGraph.traversalSource(client);


// Sample one just gives count of event transactions

app.get('/', function (req, res) {
	console.log("entered the function");
	res.end( "Hello");
});


app.get('/getDistinctPaths', function (req, res) {
	console.log("entered the function");
	data = {
			"[Login, Check Exchange Rate, Add Beneficiary, Check Charges, Rates Confirmation, Fund Transfer Submit]":35,
			"[Login, Check Exchange Rate, Add Beneficiary, Check Charges, Rates Confirmation, Check Charges, Fund Transfer Submit]":5,
			"[Login, Check Exchange Rate, Add Beneficiary]":15,
			"[Login, Check Exchange Rate, Add Beneficiary,Fund Transfer In Branch]":10
			}
	res.json(data)
});




app.get('/getNextJourneySteps', function (req, res) {
	console.log("entered the Get distinct paths for given start and one end event ");
	console.log("Start event :"+req.query.numDepth);
	console.log("Start event :"+req.query.numPath);
	console.log(req.query.eventName);	
	console.log(req.query.eventName.replace('"',''));	
	data={}
	if(req.query.eventName == 'Login' & req.query.numDepth == '1'){
			data = {
			"[Login, Check Exchange Rate]":35
			}
			console.log("setting data value");
	} else if (req.query.eventName == 'Login' & req.query.numDepth == '2'){
			data = {"[Login, Check Exchange Rate, Add Beneficiary]":35};
	}

	res.json(data)
});


app.get('/getPrevJourneySteps', function (req, res) {
	console.log("entered the Get distinct paths for given start and one end event ");
	console.log("Start event :"+req.query.numDepth);
	console.log("Start event :"+req.query.numPath);
	console.log(req.query.eventName);	
	console.log(req.query.eventName.replace('"',''));	
	data={}
	if(req.query.eventName == 'Fund Transfer In Branch' & req.query.numDepth == '1'){
			data = {
				//console.log("here");
			"[Add Beneficiary,Fund Transfer In Branch]":10
			}
			console.log("setting data value");
	} else if (req.query.eventName == 'Fund Transfer In Branch' & req.query.numDepth == '2'){
		console.log("here2");
			data = {"[Check Exchange Rate, Add Beneficiary,Fund Transfer In Branch]":10};
	}

	res.json(data)
});

app.get('/getDistinctPathsGivenStartAndEndEventNames', function (req, res) {
	console.log("entered the Get distinct paths for given start and one end event ");
	console.log("Start event :"+req.query.getStartEventName);
	console.log("Start event :"+req.query.getEndEventName);

	data={}
	if (req.query.getStartEventName.trim() == 'Login' & req.query.getEndEventName.trim() == 'Fund Transfer Submit'){
		
			data = {
			"[Login, Check Exchange Rate, Add Beneficiary, Check Charges, Rates Confirmation, Fund Transfer Submit]":35,
			"[Login, Check Exchange Rate, Add Beneficiary, Check Charges, Rates Confirmation, Check Charges, Fund Transfer Submit]":5,
			"[Login, Check Exchange Rate, Add Beneficiary]":15			
			}
	}



	res.json(data)
});


app.get('/getCrossChannelPaths', function (req, res) {
	console.log("entered the Get distinct paths for given start and one end event ");
	console.log("Start event :"+req.query.failureStepName);
	console.log("Start event :"+req.query.successStepName);


	data={}
	if(req.query.failureStepName == 'Login' & req.query.successStepName== 'Fund Transfer In Branch'){
			data = {
				//console.log("here");
			"[Login, Add Beneficiary,Fund Transfer In Branch]":10
			}
			console.log("setting data value");
	} 



	res.json(data)
});




app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})


