package in.datateam.cadup

import org.apache.spark.SparkConf
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger
import scopt.OParser

import in.datateam.bifrost.Bifrost
import in.datateam.bifrost.processor.CarryForwardAndLastModifiedProcessor
import in.datateam.bifrost.processor.DeltaProcessor
import in.datateam.bifrost.processor.PersistProcessor
import in.datateam.bifrost.utils.CarryForwardDFAndLastModifiedDFCaseClass
import in.datateam.bifrost.utils.ValidAndInvalidDFCaseClass
import in.datateam.fury.AuditAndLineageManager
import in.datateam.fury.AuditColumnsCaseClass
import in.datateam.fury.LineageColumnsCaseClass
import in.datateam.transformer.TransformV1
import in.datateam.utils.InvalidArgumentException
import in.datateam.utils.RerunCleanUpException
import in.datateam.utils.TargetExistException
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils
import in.datateam.utils.helper.FileSystemUtils.getFileSystem
import in.datateam.utils.helper.Misc

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object BatchJob {
  val logger: Logger = Logger.getLogger(getClass.getName)

  val usage =
    """
    Usage: [[entity] or [profile]] [config-file-path] [partition-value] [rerun]
  """

  def main(args: Array[String]): Unit = {

    case class argsConfig(
        eventType: String = "",
        configPath: String = "",
        batchDate: String = "",
        rerunFlag: String = "",
        deltaDate: String = "")

    val parserBuilder = OParser.builder[argsConfig]

    val argsParser = {
      import parserBuilder._
      OParser.sequence(
        programName("process_cadup"),
        head("process_cadup"),
        opt[String]("eventType")
          .required()
          .text("job type, can be 'entity' OR 'profile' (without quotes)")
          .validate(
            x => if (CommonEnums.JOB_TYPES.contains(x)) success else failure("invalid job type")
          )
          .action((argVal, confObj) => confObj.copy(eventType = argVal)),
        opt[String]("configPath")
          .required()
          .text("path to config for job (on local filesystem)")
          .action((argVal, confObj) => confObj.copy(configPath = argVal)),
        opt[String]("batchDate")
          .required()
          .text("value of partition for which to run batch job")
          .action((argVal, confObj) => confObj.copy(batchDate = argVal)),
        opt[String]("rerunFlag")
          .optional()
          .text("flag specifying if this is a rerun job, must have the value 'rerun'")
          .action((argVal, confObj) => confObj.copy(rerunFlag = argVal)),
        opt[String]("deltaDate")
          .optional()
          .text("if running a profile job, this value specifies against existing profile to calculate delta")
          .action((argVal, confObj) => confObj.copy(deltaDate = argVal)),
        checkConfig(
          c =>
            if (!c.deltaDate.isEmpty && c.eventType != CommonEnums.PROFILE) {
              failure("deltaDate is only supported for profile jobs")
            } else success
        )
      )
    }

    OParser.parse(argsParser, args, argsConfig()) match {
      case Some(argsConfig) => {
        val eventType: String = argsConfig.eventType.trim()
        val trimmedConfigPath: String = argsConfig.configPath.trim()
        val partitionValue: String = argsConfig.batchDate.trim()
        val rerunFlag: Boolean = argsConfig.rerunFlag.trim() == "rerun"
        val deltaDate: String = argsConfig.deltaDate.trim()

        implicit val spark = getSparkSession

        val stagingConfigPath =
          s"${System.getenv("SPARK_YARN_STAGING_DIR")}/${trimmedConfigPath}"
        logger.info(s"Config path ${stagingConfigPath}")

        val config: Config = ConfigParser.getConfig(eventType, stagingConfigPath)

        val passedDeltaDate = deltaDate match {
          case "" => {
            if (eventType == CommonEnums.PROFILE) {
              Misc.getNthDate(partitionValue, -1)
            } else {
              partitionValue
            }
          }
          case _ => deltaDate
        }

        process(config, partitionValue, passedDeltaDate, rerunFlag)
        FileSystemUtils.cleanUp()
      }

      case _ => {
        throw InvalidArgumentException("invalid/missing arguments")
      }
    }
  }

  def addConfig()(implicit spark: SparkSession): Unit = {
    spark.conf.set("spark.sql.parquet.compression.codec", "gzip")
    spark.conf
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    spark.conf.set(
      "spark.kryo.classesToRegister",
      "org.apache.hadoop.io.LongWritable,org.apache.hadoop.io.Text"
    )
    spark.conf.set(
      "spark.io.compression.codec",
      "org.apache.spark.io.SnappyCompressionCodec"
    )
    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)
    spark.conf.set("spark.sql.hive.convertMetastoreParquet", "false")
    val checkpointDir = spark.sparkContext.getCheckpointDir
    checkpointDir match {
      case Some(_) => Unit
      case None =>
        spark.sparkContext.setCheckpointDir(FileSystemUtils.getCheckpointDir())
    }
  }

  def extraConfigValidation(config: Config)(implicit spark: SparkSession): Unit = {
    PersistProcessor.validateConfig(config)
  }

  def process(
      config: Config,
      partitionValue: String,
      deltaDate: String,
      rerunFlag: Boolean = false
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    addConfig()
    extraConfigValidation(config)
    if (rerunFlag) rerunCleanUp(config, partitionValue)
    verifyTargetPathEmpty(config, partitionValue)
    val lineageSchema = Encoders.product[LineageColumnsCaseClass].schema
    val auditsSchema = Encoders.product[AuditColumnsCaseClass].schema
    val lineage =
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], lineageSchema)
    val audits =
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], auditsSchema)
    implicit val auditAndLineageManager =
      new AuditAndLineageManager(lineage, audits)
    auditAndLineageManager.initialize()
    val (
      validAndInvalidDF: ValidAndInvalidDFCaseClass,
      sourceDFs: Map[String, DataFrame]
    ) = Bifrost
      .process(config, partitionValue)

    val transformedDF: DataFrame = TransformV1
      .process(config, validAndInvalidDF.validDF, sourceDFs, partitionValue)

    val carryForwardDFAndLastModifiedDF = if (config.configType == CommonEnums.PROFILE) {
      CarryForwardAndLastModifiedProcessor.process(config, transformedDF, partitionValue)
    } else {
      val emptyDF = spark.emptyDataFrame
      CarryForwardDFAndLastModifiedDFCaseClass(transformedDF, emptyDF)
    }
    val carryForwardedDF = carryForwardDFAndLastModifiedDF.carryForwardedDF

    val typeCastedDF: DataFrame = Bifrost
      .applyTargetSchema(config, carryForwardedDF)

    logger.info("Storing valid data.")
    storeFinalData(config, typeCastedDF, CommonEnums.VALID_RECORDS)

    if (config
          .getStorageDetails(CommonEnums.DELTA_RECORDS)
          .filter(targetDetails => targetDetails.persistFlag == "true")
          .length > 0) {
      logger.info("Storing delta data.")
      val primaryKey = config.targetSchema(0).columnName
      val deltaDF =
        DeltaProcessor.process(config, typeCastedDF, partitionValue, primaryKey, deltaDate)
      val typeCastedDeltaDF: DataFrame =
        Bifrost.applyTargetSchema(config, deltaDF, isDelta = true)
      storeFinalData(config, typeCastedDeltaDF, CommonEnums.DELTA_RECORDS)
    }

    if (config.configType == CommonEnums.ENTITY) {
      logger.info("Storing invalid data.")
      storeFinalData(
        config,
        validAndInvalidDF.InvalidDF,
        CommonEnums.INVALID_RECORDS
      )
    }

    if (config
          .getStorageDetails(CommonEnums.AUDIT_RECORDS)
          .filter(targetDetails => targetDetails.persistFlag == "true")
          .length > 0) {
      logger.info("Storing audit data.")
      val auditDF = auditAndLineageManager.getAuditDF(config)
      storeFinalData(config, auditDF, CommonEnums.AUDIT_RECORDS)
    }

    if (config
          .getStorageDetails(CommonEnums.LINEAGE_RECORDS)
          .filter(targetDetails => targetDetails.persistFlag == "true")
          .length > 0) {
      logger.info("Storing lineage data.")
      val lineageDF = auditAndLineageManager.getLineageDF(config)
      storeFinalData(config, lineageDF, CommonEnums.LINEAGE_RECORDS)
    }

    if (config
          .getStorageDetails(CommonEnums.LAST_MODIFIED_RECORDS)
          .filter(targetDetails => targetDetails.persistFlag == "true")
          .length > 0) {
      logger.info("Storing last modified metadata.")
      val lastModifiedDF = carryForwardDFAndLastModifiedDF.lastModifiedDF
      storeFinalData(config, lastModifiedDF, CommonEnums.LAST_MODIFIED_RECORDS)
    }

    typeCastedDF
  }

  def storeFinalData(config: Config, dataFrame: DataFrame, recordType: String)(implicit spark: SparkSession): Unit = {
    config
      .getStorageDetails(recordType)
      .foreach { targetDetail =>
        if (targetDetail.persistFlag == "true") {
          logger.info(
            s"Storing ${recordType} data using storage handler ${targetDetail.storageHandler}"
          )
          PersistProcessor.process(config, dataFrame, targetDetail)
        } else {
          logger.info(
            s"Skip storing ${recordType} data using storage handler ${targetDetail.storageHandler}"
          )
        }
      }
  }

  def verifyTargetPathEmpty(config: Config, partitionValue: String)(implicit spark: SparkSession): Unit = {
    val mapPath = getCleanUpPaths(config, partitionValue)
    mapPath.keys.foreach { recordsType =>
      val targetPathList = mapPath(recordsType)
      targetPathList.foreach { targetPathStr =>
        val fs: FileSystem = getFileSystem(targetPathStr)
        val targetPath = new Path(targetPathStr)
        if (fs.exists(targetPath)) {
          if (fs.listStatus(targetPath).length > 0) {
            throw TargetExistException(
              s"${recordsType} already exist. Try with rerun flag."
            )
          }
        }
      }
    }
  }

  def getCleanUpPaths(
      config: Config,
      partitionValue: String
    )(
      implicit spark: SparkSession
    ): Map[String, Array[String]] = {
    val datePartition =
      s"${CommonEnums.DATE_PARTITION_COLUMN}=${partitionValue}"
    val entityName = config.configType match {
      case CommonEnums.ENTITY =>
        s"entity=${config.sourceDetails.head.entityName}"
      case CommonEnums.PROFILE => s"entity=${CommonEnums.PROFILE_MASTER}"
    }

    val validRecordsPartition =
      config.configType match {
        case CommonEnums.ENTITY =>
          val partition = config.sourceDetails.head.partitionDetails match {
            case Some(partitionDetails) =>
              Misc.getFormattedPartitionDate(
                partitionDetails.partitionFormat
                  .mkString(CommonEnums.PARTITION_DELIMITER.toString),
                partitionDetails.partitionType,
                partitionValue
              )
            case None => partitionValue
          }
          s"date=${partition}"
        case CommonEnums.PROFILE => s"date=${partitionValue}"
      }

    val validRecordsPathList = config
      .getStorageDetails(CommonEnums.VALID_RECORDS)
      .filter(targetDetail => targetDetail.storageHandler == "PersistHadoopFS")
      .map(
        targetDetail =>
          targetDetail.fileSystem.get match {
            case CommonEnums.HDFS | CommonEnums.S3 =>
              s"${targetDetail.pathUrl.get}/${validRecordsPartition}"
          }
      )

    val deltaRecordsPathList = config
      .getStorageDetails(CommonEnums.DELTA_RECORDS)
      .filter(targetDetail => targetDetail.storageHandler == "PersistHadoopFS")
      .map(
        targetDetail =>
          targetDetail.fileSystem.get match {
            case CommonEnums.HDFS | CommonEnums.S3 =>
              s"${targetDetail.pathUrl.get}/${validRecordsPartition}"
          }
      )

    val inValidRecordsPathList = if (config.configType == CommonEnums.ENTITY) {
      val paths = config
        .getStorageDetails(CommonEnums.INVALID_RECORDS)
        .filter(
          targetDetail => targetDetail.storageHandler == "PersistHadoopFS"
        )
        .map(
          targetDetail =>
            targetDetail.fileSystem.get match {
              case CommonEnums.HDFS | CommonEnums.S3 =>
                s"${targetDetail.pathUrl.get}/${datePartition}/${entityName}"
            }
        )
      paths
    } else {
      Array("")
    }

    val auditRecordsPathList = config
      .getStorageDetails(CommonEnums.AUDIT_RECORDS)
      .filter(targetDetail => targetDetail.storageHandler == "PersistHadoopFS")
      .map(
        targetDetail =>
          targetDetail.fileSystem.get match {
            case CommonEnums.HDFS | CommonEnums.S3 =>
              s"${targetDetail.pathUrl.get}/${datePartition}/${entityName}"
          }
      )

    val lineageRecordsPathList = config
      .getStorageDetails(CommonEnums.LINEAGE_RECORDS)
      .filter(targetDetail => targetDetail.storageHandler == "PersistHadoopFS")
      .map(
        targetDetail =>
          targetDetail.fileSystem.get match {
            case CommonEnums.HDFS | CommonEnums.S3 =>
              s"${targetDetail.pathUrl.get}/${datePartition}/${entityName}"
          }
      )

    val mapPath: Map[String, Array[String]] = List(
      CommonEnums.VALID_RECORDS   -> validRecordsPathList,
      CommonEnums.AUDIT_RECORDS   -> auditRecordsPathList,
      CommonEnums.LINEAGE_RECORDS -> lineageRecordsPathList
    ).toMap
    val finalMap: Map[String, Array[String]] = config.configType match {
      case CommonEnums.ENTITY => mapPath + (CommonEnums.INVALID_RECORDS -> inValidRecordsPathList)
      case CommonEnums.PROFILE => mapPath + (CommonEnums.DELTA_RECORDS  -> deltaRecordsPathList)
    }
    finalMap
  }

  def rerunCleanUp(config: Config, partitionValue: String)(implicit spark: SparkSession): Unit = {
    val mapPath = getCleanUpPaths(config, partitionValue)

    mapPath.keys.foreach { recordsType =>
      val cleanUpPathList = mapPath(recordsType)
      cleanUpPathList.foreach { cleanUpPathStr =>
        val fs: FileSystem = getFileSystem(cleanUpPathStr)
        val cleanUpPath = new Path(cleanUpPathStr)
        if (fs.exists(cleanUpPath)) {
          if (!fs.delete(cleanUpPath, true)) {
            logger.error(
              s"Rerun: deleting ${recordsType} ${cleanUpPathStr} failed"
            )
            throw RerunCleanUpException(
              s"deleting ${recordsType} ${cleanUpPathStr} failed"
            )
          }
          logger.info(
            s"Rerun: deleting ${recordsType} ${cleanUpPathStr} succeed"
          )
        }
      }
    }
  }

  def getSparkSession: SparkSession = {
    val conf: SparkConf = new SparkConf()
      .setAppName("Cadence UUP")

    logger.info("Starting Spark Session.")
    implicit val spark = SparkSession
      .builder()
      .config(conf)
      .enableHiveSupport()
      .getOrCreate()
    spark
  }
}
