#!/usr/bin/env bash

source {{params.aws_credential_path}}
MUTABLE_VAL={{params.mutable_val}}
DATA_FREQ={{params.data_frequency}}
PARTITION_DATE={{ execution_date.strftime("%Y-%m-%d") }}
SOURCE_PATH={{params.source_path}}/${SOURCE_PARTITION}*
TARGET_PATH={{params.target_path}}/${TARGET_PARTITION}
ENTITY_NAME={{params.entity_name}}
echo "Data Frequency:" ${DATA_FREQ}
echo "Is Mutable:" ${MUTABLE_VAL}
echo "Source Partition:" ${SOURCE_PARTITION}
echo "Target Partition:" ${TARGET_PARTITION}
echo "Partition run:" ${PARTITION}
echo "Entity marker root directory:" ${ENTITY_MARKER_ROOT_DIR_S3}


kinit -kt {{params.kerberos_keytab_value}} {{params.kerberos_principal_value}}

copy_entity_marker(){
    entity_name=$1
    partition_value=$2
    marker_root_dir_s3=$3
    status=$4
    echo "Partition Value: ${partition_value}"
    echo "Entity Name: ${entity_name}"
    ENTITY_MARKER_FILE_DIR="${marker_root_dir_s3}${entity_name}/"
    echo "Marker file directory: ${entity_name}"
    marker_file_name=''
    marker_file_name_negation=''
    if [[ "$status" = "success" ]];
    then
        marker_file_name=${partition_value}.success
        marker_file_name_negation=${partition_value}.failure
    elif [[ "$status" = "failure" ]];
    then
        marker_file_name=${partition_value}.failure
        marker_file_name_negation=${partition_value}.success
    else
        echo "Invalid status type : $SOURCE_TYPE. Excepted one of success/failure."
        exit 1
    fi
    echo "creating marker file: $marker_file_name"
    touch ${marker_file_name}
    hadoop fs -mkdir -p ${ENTITY_MARKER_FILE_DIR}
    hadoop fs -rm ${ENTITY_MARKER_FILE_DIR}${marker_file_name_negation}
    hadoop fs -put -f ${marker_file_name} ${ENTITY_MARKER_FILE_DIR}
    echo "Marker file: $marker_file_name created."
    rm ${marker_file_name}
}

hadoop fs -ls ${SOURCE_PATH}

if [[ $? -eq 0 ]];
then
    echo "Source exist."
else
    echo "Source doesn't exist."
    copy_entity_marker ${ENTITY_NAME} ${PARTITION} ${ENTITY_MARKER_ROOT_DIR_S3} "failure"
    exit 1
fi

hadoop fs -ls ${TARGET_PATH}

if [[ $? -eq 0 ]];
then
    echo "Processed entity exist in entity store. Deleting it."
    hadoop fs -rm -r ${TARGET_PATH}*
    if [[ $? -eq 0 ]];
    then
        echo "Deletion completed."
    else
        echo "Deletion failed."
        copy_entity_marker ${ENTITY_NAME} ${PARTITION} ${ENTITY_MARKER_ROOT_DIR_S3} "failure"
        exit 1
    fi
else
    echo "Processed entity doesn't exist in entity store."
fi

exit 0