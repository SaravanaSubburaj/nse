#!/usr/bin/env bash

source {{params.aws_credential_path}}
echo "Partition Value:" ${PARTITION_VALUE}
marker_file_name=${PARTITION_VALUE}.marker
echo "creating marker file: $marker_file_name"

kinit -kt {{params.kerberos_keytab_value}} {{params.kerberos_principal_value}}

touch ${marker_file_name}
hadoop fs -mkdir -p {{params.marker_root_dir_s3}}
hadoop fs -put -f ${marker_file_name} {{params.marker_root_dir_s3}}
rm ${marker_file_name}
