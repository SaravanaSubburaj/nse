#!/usr/bin/env bash

source {{params.aws_credential_path}}
CONFIG_TYPE={{params.config_type}}
CONFIG_FILE={{params.config_file_path}}
PARTITION_DATE={{execution_date.strftime("%Y-%m-%d")}}

CADUP_HOME_DIR={{params.cadup_home}}
CADUP_YARN_QUEUE={{params.yarn_queue_name}}
CADUP_DATA_FREQ={{params.data_frequency}}
CADUP_KEYTAB={{params.kerberos_keytab_value}}
CADUP_PRINCIPAL={{params.kerberos_principal_value}}
CADUP_CONF_DIR="$CADUP_HOME_DIR/configs"
CADUP_LIB_DIR="$CADUP_HOME_DIR/lib"
CADUP_SPARK_CONF_DIR="$CADUP_HOME_DIR/configs/spark_configs"
SOURCE_TYPE={{params.source_type}}
ENTITY_NAME={{params.entity_name}}
echo "Batch:" ${BATCH}
echo "Partition run:" ${PARTITION}
echo "Entity marker root directory:" ${ENTITY_MARKER_ROOT_DIR_S3}

if [[ "$CONFIG_TYPE" = "" ]] || [[ "$CONFIG_FILE" = "" ]] || [[ "$BATCH" = "" ]] || [[ "$SOURCE_TYPE" = "" ]];
then
    exit 1
fi

if [[ "$SOURCE_TYPE" = "small" ]];
then
    SPARK_CONF="small_spark.conf"
elif [[ "$SOURCE_TYPE" = "medium" ]];
then
    SPARK_CONF="medium_spark.conf"
elif [[ "$SOURCE_TYPE" = "large" ]];
then
    SPARK_CONF="large_spark.conf"
else
    echo "Invalid source type : $SOURCE_TYPE. Excepted one of small/medium/large."
    exit 1
fi


copy_entity_marker(){
    entity_name=$1
    partition_value=$2
    marker_root_dir_s3=$3
    status=$4
    echo "Partition Value: ${partition_value}"
    echo "Entity Name: ${entity_name}"
    ENTITY_MARKER_FILE_DIR="${marker_root_dir_s3}${entity_name}/"
    echo "Marker file directory: ${entity_name}"
    marker_file_name=''
    marker_file_name_negation=''
    if [[ "$status" = "success" ]];
    then
        marker_file_name=${partition_value}.success
        marker_file_name_negation=${partition_value}.failure
    elif [[ "$status" = "failure" ]];
    then
        marker_file_name=${partition_value}.failure
        marker_file_name_negation=${partition_value}.success
    else
        echo "Invalid status type : $SOURCE_TYPE. Excepted one of success/failure."
        exit 1
    fi
    echo "creating marker file: $marker_file_name"
    touch ${marker_file_name}
    hadoop fs -mkdir -p ${ENTITY_MARKER_FILE_DIR}
    hadoop fs -rm ${ENTITY_MARKER_FILE_DIR}${marker_file_name_negation}
    hadoop fs -put -f ${marker_file_name} ${ENTITY_MARKER_FILE_DIR}
    echo "Marker file: $marker_file_name created."
    rm ${marker_file_name}
}

CONFIG_FILE_NAME=`basename ${CONFIG_FILE}`

spark2-submit \
        --class in.datateam.cadup.BatchJob \
        --master yarn \
        --deploy-mode cluster \
        --queue "$CADUP_YARN_QUEUE" \
        --properties-file "$CADUP_SPARK_CONF_DIR/$SPARK_CONF" \
        --keytab "$CADUP_KEYTAB" \
        --principal "$CADUP_PRINCIPAL" \
        --jars $(echo ${CADUP_LIB_DIR}/*.jar | tr ' ' ',')  \
        --files "$CONFIG_FILE" \
        ${CADUP_LIB_DIR}/cadup_2.11-1.0.jar \
        "$CONFIG_TYPE" "$CONFIG_FILE_NAME" "$BATCH"

if [[ $? -eq 0 ]]; then
    echo "Entity processing completing."
else
    echo "Entity processing failed."
    copy_entity_marker ${ENTITY_NAME} ${PARTITION} ${ENTITY_MARKER_ROOT_DIR_S3} "failure"
    exit 1
fi

exit 0