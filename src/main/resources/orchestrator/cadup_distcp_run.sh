#!/usr/bin/env bash

source {{params.aws_credential_path}}

data_freq={{params.data_frequency}}
echo "Partition Value:" ${PARTITION_VALUE}
ENTITY_NAME={{params.entity_name}}
echo "Partition run:" ${PARTITION}
echo "Entity marker root directory:" ${ENTITY_MARKER_ROOT_DIR_S3}

kinit -kt {{params.kerberos_keytab_value}} {{params.kerberos_principal_value}}

hadoop fs -ls {{params.entity_root_path_s3}}/${PARTITION_VALUE}

if [[ $? -eq 0 ]];
    then
        echo "The partition already exists in s3. Deleting it."
        hadoop fs -rm -r {{params.entity_root_path_s3}}/${PARTITION_VALUE}
    else
        echo "The partition doesnt not exist in s3. We can move the data"
fi

copy_entity_marker(){
    entity_name=$1
    partition_value=$2
    marker_root_dir_s3=$3
    status=$4
    echo "Partition Value: ${partition_value}"
    echo "Entity Name: ${entity_name}"
    ENTITY_MARKER_FILE_DIR="${marker_root_dir_s3}${entity_name}/"
    echo "Marker file directory: ${entity_name}"
    marker_file_name=''
    marker_file_name_negation=''
    if [[ "$status" = "success" ]];
    then
        marker_file_name=${partition_value}.success
        marker_file_name_negation=${partition_value}.failure
    elif [[ "$status" = "failure" ]];
    then
        marker_file_name=${partition_value}.failure
        marker_file_name_negation=${partition_value}.success
    else
        echo "Invalid status type : $SOURCE_TYPE. Excepted one of success/failure."
        exit 1
    fi
    echo "creating marker file: $marker_file_name"
    touch ${marker_file_name}
    hadoop fs -mkdir -p ${ENTITY_MARKER_FILE_DIR}
    hadoop fs -rm ${ENTITY_MARKER_FILE_DIR}${marker_file_name_negation}
    hadoop fs -put -f ${marker_file_name} ${ENTITY_MARKER_FILE_DIR}
    echo "Marker file: $marker_file_name created."
    rm ${marker_file_name}
}

hadoop distcp ${AWS} -Dmapred.job.queue.name={{params.yarn_queue_name}} {{params.entity_root_path_hdfs}}/${PARTITION_VALUE} {{params.entity_root_path_s3}}/${PARTITION_VALUE}

if [[ $? -eq 0 ]];
    then
        echo "Distcp completed successfully."
        copy_entity_marker ${ENTITY_NAME} ${PARTITION} ${ENTITY_MARKER_ROOT_DIR_S3} "success"
else
    echo "Distcp completed successfully."
    copy_entity_marker ${ENTITY_NAME} ${PARTITION} ${ENTITY_MARKER_ROOT_DIR_S3} "failure"
    exit 1
fi

 exit 0
