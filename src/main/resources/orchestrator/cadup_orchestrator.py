import json
from datetime import datetime

from airflow import DAG
from airflow.executors import GetDefaultExecutor
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import ShortCircuitOperator
from airflow.operators.subdag_operator import SubDagOperator

# TODO: Populate correct path
CONFIG_PATH = ""
CHECK_RUN_DATE_TASK_PREFIX = "check_run_date"
CHECK_PARTITION_TASK_PREFIX = "check_partition"
ENTITY_RUN_TASK_PREFIX = "entity_run"
DISTCP_RUN_TASK_PREFIX = "distcp_run"
DISTCP_MARKER_TASK = "distcp_marker_run"
DAILY_ENTITY_DAG_NAME = "daily_entity"
MONTHLY_ENTITY_DAG_NAME = "monthly_entity"
MUTABLE_ENTITY_DAG_NAME = "mutable_entity"
MUTABLE_ENTITY = "mutable"
DAILY_ENTITY = "daily"
MONTHLY_ENTITY = "monthly"


def load_config():
    _config_path = "{}/cadup_config.json".format(CONFIG_PATH)
    _config_contents = ' '.join(open(_config_path).read().split('\n'))
    _config = json.loads(_config_contents)
    return _config


def get_formatted_partition_value_entity(_day, _date_format, _delta_type='days'):
    _formatted_partition_value_entity_run_mutable = '(execution_date - macros.dateutil.relativedelta.relativedelta({2}={0})).strftime("{1}")'.format(_day, _date_format,
                                                                                                                                                     _delta_type)
    return _formatted_partition_value_entity_run_mutable


config = load_config()
dag_latency = int(config.get("dagLatency", "0"))
raw_partition_value = get_formatted_partition_value_entity(dag_latency, "%Y-%m-%d")
marker_root_dir_s3 = config['markerRootDirS3']
entity_marker_root_dir_s3_format = "{}/entity_run/date=%Y-%m-%d/".format(marker_root_dir_s3)
entity_marker_root_dir_s3 = get_formatted_partition_value_entity(dag_latency, entity_marker_root_dir_s3_format)
number_of_concurrent_task = config['concurrentTask']
dag_name = config['dagName']
mutable_days = int(config['mutableDays'])
cadup_home = config['cadupHome']
aws_credential_path = "{}/cadup_aws_credentials".format(CONFIG_PATH)

yarn_queue_entity = config['yarnQueueEntity']
yarn_queue_distcp = config['yarnQueueDistcp']
kerberos_keytab_value = config['kerberosKeytabPath']
kerberos_principal_value = config['kerberosPrincipal']

profile_marker_root_dir_s3 = "{}/profile_run/".format(marker_root_dir_s3)
java_to_python_date_format = {"yyyy": "%Y", "MM": "%m", "dd": "%d", "M": "%-m", "d": "%-d"}

entity_run = []

args = {
    'owner': 'EDO_UUP',
    'start_date': datetime(2019, 5, 21),
    'retries': 0,
    'concurrency': 12
}


def get_partition_format(_partition_format):
    return map(
        lambda x: '-'.join(map(
            lambda y: java_to_python_date_format[y],
            x.split("-"))),
        _partition_format
    )


def get_multi_partition_format(_partition_format):
    _multi_partition_format = []
    if len(_partition_format) == 2:
        _multi_partition_format = ["%Y", "%-m"]
    elif len(_partition_format) == 3:
        _multi_partition_format = ["%Y", "%-m", "%-d"]
    else:
        _multi_partition_format = ["%Y-%m-%d"]
    return _multi_partition_format


def get_formatted_partition_value(_java_partition_format):
    _python_partition_format = get_partition_format(_java_partition_format)
    _multi_partition_format = get_multi_partition_format(_python_partition_format)
    return _python_partition_format, _multi_partition_format


def get_partition_details(_entity_details):
    default_values = {'partitionType': "daily", 'partitionColumn': [], 'partitionFormat': ['yyyy-MM-dd']}
    _data_frequency = _entity_details.get('partitionDetails', default_values)['partitionType']
    _partition_columns = _entity_details.get('partitionDetails', default_values)['partitionColumn']
    _partition_format = _entity_details.get('partitionDetails', default_values)['partitionFormat']
    return _data_frequency, _partition_columns, _partition_format


def create_check_partition_data(_entity_name, _source_path, _source_partition_format,
                                _data_frequency, _target_path, _target_partition_format,
                                _mutable_flag, _kerberos_keytab_value, _kerberos_principal_value, _partition, _marker_root_dir_s3, _aws_credential_path, _dag):
    src_partition = "{{ " + _source_partition_format + " }}"
    dst_partition = "{{ " + _target_partition_format + " }}"
    partition = "{{ " + _partition + " }}"
    _entity_marker_root_dir_s3 = "{{ " + _marker_root_dir_s3 + " }}"
    entity_name = _entity_name if _mutable_flag == 'false' else '_'.join(_entity_name.split('_')[:-2])
    _check_partition_data = BashOperator(
        task_id='{0}_{1}'.format(CHECK_PARTITION_TASK_PREFIX, _entity_name),
        bash_command="cadup_check_partition.sh",
        params={
            'source_path': _source_path,
            'data_frequency': _data_frequency,
            'target_path': _target_path,
            'mutable_val': _mutable_flag,
            'kerberos_keytab_value': _kerberos_keytab_value,
            'kerberos_principal_value': _kerberos_principal_value,
            'entity_name': entity_name,
            'aws_credential_path': _aws_credential_path,
        },
        dag=_dag,
        env={'SOURCE_PARTITION': src_partition, 'TARGET_PARTITION': dst_partition, 'PARTITION': partition, 'ENTITY_MARKER_ROOT_DIR_S3': _entity_marker_root_dir_s3}
    )
    return _check_partition_data


def create_entity_run_partition(_entity_name, _formatted_partition_value, _config_path,
                                _data_frequency, _cadup_home, _yarn_queue_entity,
                                _kerberos_keytab_value, _kerberos_principal_value, _mutable_flag, _source_type,
                                _partition, _marker_root_dir_s3, _aws_credential_path, _dag):
    batch = "{{ " + _formatted_partition_value + " }}"
    partition = "{{ " + _partition + " }}"
    _entity_marker_root_dir_s3 = "{{ " + _marker_root_dir_s3 + " }}"
    entity_name = _entity_name if _mutable_flag == 'false' else '_'.join(_entity_name.split('_')[:-2])
    _entity_run_partition = BashOperator(
        task_id='{0}_{1}'.format(ENTITY_RUN_TASK_PREFIX, _entity_name),
        bash_command="cadup_entities_run.sh",
        params={
            'config_type': 'entity',
            'config_file_path': _config_path,
            'data_frequency': _data_frequency,
            'cadup_home': _cadup_home,
            'yarn_queue_name': _yarn_queue_entity,
            'kerberos_keytab_value': _kerberos_keytab_value,
            'kerberos_principal_value': _kerberos_principal_value,
            'entity_name': entity_name,
            'aws_credential_path': _aws_credential_path,
            'source_type': _source_type
        },
        dag=_dag,
        env={'BATCH': batch, 'PARTITION': partition, 'ENTITY_MARKER_ROOT_DIR_S3': _entity_marker_root_dir_s3}
    )
    return _entity_run_partition


def create_task_distcp_run(_entity_name, _entity_root_path_hdfs, _entity_root_path_s3,
                           _distcp_partition_value_mutable, _aws_credential_path, _yarn_queue_distcp,
                           _data_frequency, _kerberos_keytab_value, _kerberos_principal_value,
                           _mutable_flag, _partition, _marker_root_dir_s3, _dag):
    partition_val = "{{ " + _distcp_partition_value_mutable + " }}"
    partition = "{{ " + _partition + " }}"
    _entity_marker_root_dir_s3 = "{{ " + _marker_root_dir_s3 + " }}"
    entity_name = _entity_name if _mutable_flag == 'false' else '_'.join(_entity_name.split('_')[:-2])
    _task_distcp_run = BashOperator(
        task_id='{0}_{1}'.format(DISTCP_RUN_TASK_PREFIX, _entity_name),
        bash_command="cadup_distcp_run.sh",
        params={
            'entity_root_path_hdfs': _entity_root_path_hdfs,
            'entity_root_path_s3': _entity_root_path_s3,
            'aws_credential_path': _aws_credential_path,
            'yarn_queue_name': _yarn_queue_distcp,
            'data_frequency': _data_frequency,
            'kerberos_keytab_value': _kerberos_keytab_value,
            'kerberos_principal_value': _kerberos_principal_value,
            'entity_name': entity_name
        },
        dag=_dag,
        env={'PARTITION_VALUE': partition_val, 'PARTITION': partition, 'ENTITY_MARKER_ROOT_DIR_S3': _entity_marker_root_dir_s3}
    )
    return _task_distcp_run


def create_task_distcp_run_marker(_marker_root_dir_s3, _partition_value, _aws_credential_path,
                                  _kerberos_keytab_value, _kerberos_principal_value, _dag):
    partition_val = "{{ " + _partition_value + " }}"
    _task_distcp_run_marker = BashOperator(
        task_id='{0}'.format(DISTCP_MARKER_TASK),
        bash_command="cadup_distcp_run_marker.sh",
        params={
            'marker_root_dir_s3': _marker_root_dir_s3,
            'aws_credential_path': _aws_credential_path,
            'kerberos_keytab_value': _kerberos_keytab_value,
            'kerberos_principal_value': _kerberos_principal_value
        },
        trigger_rule="all_done",
        dag=_dag,
        env={'PARTITION_VALUE': partition_val}
    )
    return _task_distcp_run_marker


def sub_dag(parent_dag_name, child_dag_name, start_date, schedule_interval):
    _sdag = DAG(
        '%s.%s' % (parent_dag_name, child_dag_name),
        schedule_interval=schedule_interval,
        start_date=start_date,
        default_args=args
    )
    return _sdag


def get_check_run_date(_entity_details, _dag):
    _run_date = _entity_details.get('runDate', '0')
    _entity_name = _entity_details['entityName']
    _run_date_operator = ShortCircuitOperator(
        task_id='{0}_{1}'.format(CHECK_RUN_DATE_TASK_PREFIX, _entity_name),
        provide_context=True,
        python_callable=check_run_date,
        params={"run_date": _run_date},
        dag=_dag)
    return _run_date_operator


def get_sub_dag(_entity_type, _data_frequency, _daily_entity_dag, _monthly_entity_dag, _mutable_entity_dag):
    if _entity_type == MUTABLE_ENTITY:
        return _mutable_entity_dag
    elif _data_frequency == DAILY_ENTITY:
        return _daily_entity_dag
    elif _data_frequency == MONTHLY_ENTITY:
        return _monthly_entity_dag


def process_entity(_entity_detail, dag_type, _dag):
    _entity_dag = sub_dag(dag_name, dag_type, _dag.start_date, _dag.schedule_interval)
    for _entity_detail in _entity_detail:
        entity_name = _entity_detail['entityName']
        entity_type = _entity_detail['entityType']
        config_path = _entity_detail['configPath']
        entity_latency = int(_entity_detail.get("entityLatency", dag_latency))
        executor_definition = _entity_detail['executorDefinition']
        entity_config_contents = ' '.join(open(config_path).read().split('\n'))
        entity_config = json.loads(entity_config_contents)
        source_path = entity_config['entityDetails']['pathUrl']

        data_frequency, partition_columns, src_partition_format = get_partition_details(entity_config['entityDetails'])
        target_partition_format = {'daily': ["yyyy-MM-dd"], 'monthly': ["yyyy-MM"]}
        entity_root_path_hdfs = "{}/{}".format(config['entityStoreHDFS'], entity_name)
        entity_root_path_s3 = "{}/{}".format(config['entityStoreS3'], entity_name)
        src_python_partition_format, src_multi_partition_format = get_formatted_partition_value(src_partition_format)
        src_formatted_partition_value_entity_run = "^".join(src_multi_partition_format)
        src_formatted_partition_value_distcp_run = "-".join(src_python_partition_format)
        target_formatted_partition_value_distcp_format = get_partition_format(target_partition_format[data_frequency])
        target_formatted_partition_value_distcp_run = "-".join(target_formatted_partition_value_distcp_format)

        p_value = get_formatted_partition_value_entity(entity_latency, src_formatted_partition_value_entity_run)

        target_path = filter(lambda x: x["recordType"] == "valid-records", entity_config['storageDetails'])[0]['pathUrl']
        target_path = target_path[:-1] if target_path[-1] == '/' else target_path
        zipped_partition = map(lambda x: x[0] + "=" + x[1], zip(partition_columns, src_formatted_partition_value_entity_run.split("^")))
        sub_path = '/'.join(zipped_partition)
        _target_partition_path = 'date=' + target_formatted_partition_value_distcp_run
        source_partition = get_formatted_partition_value_entity(entity_latency, sub_path)
        target_partition = get_formatted_partition_value_entity(entity_latency, _target_partition_path)
        partition = get_formatted_partition_value_entity(entity_latency, target_formatted_partition_value_distcp_run)

        if entity_type == MUTABLE_ENTITY:
            mutable_partition_format = "^".join(src_multi_partition_format)
            for day in range(mutable_days):
                day_with_latency = day + entity_latency
                updated_entity_name = "{0}_mutable_d-{1}".format(entity_name, str(day))
                _target_partition_path = 'date=' + target_formatted_partition_value_distcp_run
                zipped_partition = map(lambda x: x[0] + "=" + x[1], zip(partition_columns, mutable_partition_format.split("^")))
                p_value = get_formatted_partition_value_entity(day_with_latency, mutable_partition_format)
                sub_path = '/'.join(zipped_partition)
                source_partition = get_formatted_partition_value_entity(day_with_latency, sub_path)
                target_partition = get_formatted_partition_value_entity(day_with_latency, _target_partition_path)
                partition = get_formatted_partition_value_entity(day_with_latency, target_formatted_partition_value_distcp_run)

                _check_partition_data = create_check_partition_data(
                    updated_entity_name,
                    source_path,
                    source_partition,
                    data_frequency,
                    target_path,
                    target_partition,
                    'true',
                    kerberos_keytab_value,
                    kerberos_principal_value,
                    partition,
                    entity_marker_root_dir_s3,
                    aws_credential_path,
                    _entity_dag
                )

                _entity_run_partition = create_entity_run_partition(
                    updated_entity_name,
                    p_value,
                    config_path,
                    data_frequency,
                    cadup_home,
                    yarn_queue_entity,
                    kerberos_keytab_value,
                    kerberos_principal_value,
                    'true',
                    executor_definition,
                    partition,
                    entity_marker_root_dir_s3,
                    aws_credential_path,
                    _entity_dag
                )

                _task_distcp_run = create_task_distcp_run(
                    updated_entity_name,
                    entity_root_path_hdfs,
                    entity_root_path_s3,
                    target_partition,
                    aws_credential_path,
                    yarn_queue_distcp,
                    data_frequency,
                    kerberos_keytab_value,
                    kerberos_principal_value,
                    'true',
                    partition,
                    entity_marker_root_dir_s3,
                    _entity_dag
                )
                _check_partition_data >> _entity_run_partition >> _task_distcp_run
        elif data_frequency == MONTHLY_ENTITY:
            delta_type = 'months'
            run_date_operator = get_check_run_date(_entity_detail, _entity_dag)
            if len(src_python_partition_format) == (1 or 3):
                sub_path = '/'.join(zipped_partition)[:-2] + '01'
                source_partition = get_formatted_partition_value_entity(entity_latency, sub_path, delta_type)
                src_formatted_partition_value_entity_run = "^".join(src_multi_partition_format)[:-2] + '01'
                p_value = get_formatted_partition_value_entity(entity_latency, src_formatted_partition_value_entity_run, delta_type)
                target_partition = get_formatted_partition_value_entity(entity_latency, target_formatted_partition_value_distcp_run, delta_type)
            elif len(src_python_partition_format) == 2:
                source_partition = get_formatted_partition_value_entity(entity_latency, sub_path, delta_type)
                target_partition = get_formatted_partition_value_entity(entity_latency, target_formatted_partition_value_distcp_run, delta_type)
                p_value = get_formatted_partition_value_entity(entity_latency, src_formatted_partition_value_entity_run, delta_type)
            partition = get_formatted_partition_value_entity(entity_latency, target_formatted_partition_value_distcp_run, delta_type)
            _check_partition_data = create_check_partition_data(
                entity_name,
                source_path,
                source_partition,
                data_frequency,
                target_path,
                target_partition,
                'false',
                kerberos_keytab_value,
                kerberos_principal_value,
                partition,
                entity_marker_root_dir_s3,
                aws_credential_path,
                _entity_dag
            )

            _entity_run_partition = create_entity_run_partition(
                entity_name,
                p_value,
                config_path,
                data_frequency,
                cadup_home,
                yarn_queue_entity,
                kerberos_keytab_value,
                kerberos_principal_value,
                'false',
                executor_definition,
                partition,
                entity_marker_root_dir_s3,
                aws_credential_path,
                _entity_dag
            )

            _task_distcp_run = create_task_distcp_run(
                entity_name,
                entity_root_path_hdfs,
                entity_root_path_s3,
                target_partition,
                aws_credential_path,
                yarn_queue_distcp,
                data_frequency,
                kerberos_keytab_value,
                kerberos_principal_value,
                'false',
                partition,
                entity_marker_root_dir_s3,
                _entity_dag
            )
            run_date_operator >> _check_partition_data >> _entity_run_partition >> _task_distcp_run
        else:
            _check_partition_data = create_check_partition_data(
                entity_name,
                source_path,
                source_partition,
                data_frequency,
                target_path,
                target_partition,
                'false',
                kerberos_keytab_value,
                kerberos_principal_value,
                partition,
                entity_marker_root_dir_s3,
                aws_credential_path,
                _entity_dag
            )

            _entity_run_partition = create_entity_run_partition(
                entity_name,
                p_value,
                config_path,
                data_frequency,
                cadup_home,
                yarn_queue_entity,
                kerberos_keytab_value,
                kerberos_principal_value,
                'false',
                executor_definition,
                partition,
                entity_marker_root_dir_s3,
                aws_credential_path,
                _entity_dag
            )

            _task_distcp_run = create_task_distcp_run(
                entity_name,
                entity_root_path_hdfs,
                entity_root_path_s3,
                target_partition,
                aws_credential_path,
                yarn_queue_distcp,
                data_frequency,
                kerberos_keytab_value,
                kerberos_principal_value,
                'false',
                partition,
                entity_marker_root_dir_s3,
                _entity_dag
            )
            _check_partition_data >> _entity_run_partition >> _task_distcp_run
    _entity_sub_dag = SubDagOperator(subdag=_entity_dag, executor=GetDefaultExecutor(), task_id=dag_type, dag=_dag)
    return _entity_sub_dag


def check_run_date(**kwargs):
    _run_date = kwargs['params']['run_date']
    exe_date = kwargs['execution_date'].strftime("%d")
    print("Run Date: {0}".format(_run_date))
    print("Execution Date: {0}".format(exe_date))
    if _run_date == '0' or _run_date == exe_date:
        return True
    else:
        return False


def get_all_entity_details(_entity_details):
    _daily_entity_details = []
    _monthly_entity_details = []
    _mutable_entity_details = []
    for _entity_detail in _entity_details:
        _entity_type = _entity_detail['entityType']
        _entity_name = _entity_detail['entityName']
        _config_path = _entity_detail['configPath']
        entity_config_contents = ' '.join(open(_config_path).read().split('\n'))
        entity_config_detail = json.loads(entity_config_contents)
        _data_frequency, _partition_columns, _src_partition_format = get_partition_details(entity_config_detail['entityDetails'])
        if _entity_type == MUTABLE_ENTITY:
            _mutable_entity_details.append(_entity_detail)
        elif _data_frequency == DAILY_ENTITY:
            _daily_entity_details.append(_entity_detail)
        elif _data_frequency == MONTHLY_ENTITY:
            _monthly_entity_details.append(_entity_detail)

    return _daily_entity_details, _monthly_entity_details, _mutable_entity_details


dag = DAG(dag_name,
          description='UUP Entity Calculation',
          schedule_interval="59 00 * * *",
          default_args=args, catchup=False)

entity_details = config['entityDetails']

daily_entity_details, monthly_entity_details, mutable_entity_details = get_all_entity_details(entity_details)
daily_entity_sub_dag = process_entity(daily_entity_details, DAILY_ENTITY_DAG_NAME, dag)
monthly_entity_sub_dag = process_entity(monthly_entity_details, MONTHLY_ENTITY_DAG_NAME, dag)
mutable_entity_sub_dag = process_entity(mutable_entity_details, MUTABLE_ENTITY_DAG_NAME, dag)

partition_value = get_formatted_partition_value_entity(dag_latency, "%Y-%m-%d")

task_distcp_run_marker = create_task_distcp_run_marker(
    profile_marker_root_dir_s3,
    partition_value,
    aws_credential_path,
    kerberos_keytab_value,
    kerberos_principal_value,
    dag
)
init_operator = BashOperator(
    task_id='start_uup',
    bash_command="echo started execution for {{execution_date}}",
    dag=dag
)

init_operator >> daily_entity_sub_dag >> task_distcp_run_marker
init_operator >> monthly_entity_sub_dag >> task_distcp_run_marker
init_operator >> mutable_entity_sub_dag >> task_distcp_run_marker
