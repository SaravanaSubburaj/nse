from __future__ import print_function

import json
import logging
import os

import boto3
from botocore.exceptions import ClientError


def load_log_config():
    # Basic config. Replace with your own logging config if required
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger


logger = load_log_config()
sqs = boto3.resource('sqs')
sns = boto3.client('sns')


def read_config():
    config_path = os.environ['LAMBDA_TASK_ROOT'] + "/config.json"
    logger.info("Looking for config.json at " + config_path)
    config_contents = ' '.join(open(config_path).read().split('\n'))
    config = json.loads(config_contents)
    return config


def get_unique_messages(messages):
    unique_messages = list(set(messages))
    json_messages = [json.loads(msg) for msg in unique_messages]
    return json_messages


def read_messages(sqs_url):
    queue = sqs.Queue(sqs_url)
    messages = []
    while True:
        messages_to_delete = []
        for message in queue.receive_messages(WaitTimeSeconds=20, MaxNumberOfMessages=10):
            body = message
            msg = body.body
            messages.append(msg)
            messages_to_delete.append({
                'Id': message.message_id,
                'ReceiptHandle': message.receipt_handle
            })
        if len(messages_to_delete) == 0:
            break
        else:
            delete_response = queue.delete_messages(Entries=messages_to_delete)
    unique_messages = get_unique_messages(messages)
    return unique_messages


def write_messages(sqs_url, messages):
    queue = sqs.Queue(sqs_url)
    for message in messages:
        for retry in range(10):
            try:
                msg_body = json.dumps(dict(message))
                response = queue.send_message(
                    QueueUrl=sqs_url,
                    MessageBody=msg_body
                )
                break
            except ClientError as ce:
                logger.warning("Queue send message failed.")
                logger.warning(ce)


def create_uup_core_message(sqs_url, messages, partition_value):
    entity_message_header = ["Entity Name", "Run Date", "Partition Date", "Status"]
    entity_message_list = []
    extract_message_header = ["Run Type", "Partition Date", "Status"]
    extract_message_list = []
    other_messages = []
    for message in messages:
        run_type = message['run_type']
        if message['run_date'] != partition_value:
            other_messages.append(message)
            continue
        if run_type in ['entity_run']:
            entity_name = message['entity_name']
            run_date = message['run_date']
            partition_date = message['partition_date']
            status = message['status']
            entity_message_list.append([entity_name, run_date, partition_date, status])
        elif run_type in ['profile', 'redshift', 'dynamo']:
            run_date = message['run_date']
            status = message['status']
            extract_message_list.append([run_type, run_date, status])
        else:
            other_messages.append(message)

    write_messages(sqs_url, other_messages)
    formatted_entity_message = create_dict(entity_message_list, entity_message_header)
    formatted_extract_message = create_dict(extract_message_list, extract_message_header)
    core_message = {"Core Run Status": formatted_extract_message, "Entity Status": formatted_entity_message}
    formatted_core_message = json.dumps(core_message, indent=2)
    return formatted_core_message


def create_dpa_extract_message(sqs_url, messages, partition_value):
    extract_message_header = ["Run Type", "Partition Date", "Status"]
    extract_message_list = []
    other_messages = []
    for message in messages:
        run_type = message['run_type']
        if message['run_date'] != partition_value:
            other_messages.append(message)
            continue
        if run_type in ['daily_extract', 'monthly_extract']:
            run_date = message['run_date']
            status = message['status']
            extract_message_list.append([run_type, run_date, status])
        else:
            other_messages.append(message)

    write_messages(sqs_url, other_messages)
    dpa_extract_message = create_dict(extract_message_list, extract_message_header)
    extract_message = {"DPA Extract Status": dpa_extract_message}
    formatted_extract_message = json.dumps(extract_message, indent=2)
    return formatted_extract_message


def create_json(table, header):
    table_dict = list(map(lambda row: dict(zip(header, row)), table))
    return json.dumps(table_dict)


def create_dict(table, header):
    table_dict = list(map(lambda row: dict(zip(header, row)), table))
    return table_dict


def format_table(table, header):
    width = [len(i) for i in header]
    table.sort(key=lambda x: x[0])
    for row in table:
        row_size = list(map(lambda x: len(x), row))
        for i in range(len(row_size)):
            width[i] = max(width[i], row_size[i])
    width = list(map(lambda x: x + 2, width))
    formatted_strings = [
        ' ' + ' '.join(map(lambda x: "_" * x[0], zip(width, header))) + ' ',
        '|' + '|'.join(map(lambda x: x[1].center(x[0]), zip(width, header))) + '|',
        '|' + '|'.join(map(lambda x: "_" * x[0], zip(width, header))) + '|'
    ]

    for row in table:
        formatted_row = '|' + '|'.join(map(lambda x: x[1].center(x[0]), zip(width, row))) + '|'
        formatted_strings.append(formatted_row)
        formatted_strings.append('|' + '|'.join(map(lambda x: "_" * x[0], zip(width, header))) + '|')
    formatted_message = '\n'.join(formatted_strings)
    return formatted_message


def publish_message(sns_topic_arn, message):
    logger.info("Publishing message:")
    logger.info(message)
    sns.publish(
        TopicArn=sns_topic_arn,
        Message=message
    )


def lambda_handler(event, context):
    config = read_config()
    queue_url = config['sqsUrl']
    partition_value = event['partitionValue']
    messages = read_messages(queue_url)
    if event['event_type'] == "UUP_CORE":
        uup_core_message = create_uup_core_message(queue_url, messages, partition_value)
        publish_message(config['uupCoreSNSARN'], uup_core_message)
    elif event['event_type'] == "DPA_EXTRACT":
        dpa_extract_message = create_dpa_extract_message(queue_url, messages, partition_value)
        publish_message(config['dpaExtractSNSARN'], dpa_extract_message)
    return event
