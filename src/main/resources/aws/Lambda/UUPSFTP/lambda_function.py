import json
import logging
import os

from helper import read_s3_file
from ssh_client import SSHClient


def load_log_config():
    # Basic config. Replace with your own logging config if required
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger


logger = load_log_config()


def start_ssh_client(config):
    ssh_host = config.get('sshHostname')
    ssh_username = config.get('sshUsername')
    ssh_password = config.get('sshPassword')
    ssh_private_key = config.get('sshPrivateKey')
    assert ssh_password or ssh_private_key, "Missing sshPassword or SshPrivateKey"
    ssh_port = int(config.get('sshPort', 22))
    ssh_working_dir = config.get('sshWorkingDir')
    ssh_client = SSHClient(ssh_host, ssh_port, ssh_username, ssh_password, ssh_private_key, ssh_working_dir)
    return ssh_client


def get_config(extract_config_path):
    extract_config = json.loads(read_s3_file(extract_config_path))
    s3_path = extract_config['storageDetails'][0]['pathUrl']
    file_name = filter(lambda x: x['key'] == 'csv.filename', extract_config['storageDetails'][0]['dataFormatOption'])[0]['value']
    return s3_path, file_name


def get_commands(event):
    partition_value = "date={0}/".format(event['partitionValue'])
    s3_destination_root_path, file_name = get_config(event['extractConfig'])
    unload_working_dir = os.path.join(event['unloadWorkingDir'], event['unloadRunId'])
    extract_name = os.path.join(unload_working_dir, file_name)
    file_name_list = file_name.split('.')
    file_name_list.insert(1, ''.join(event['partitionValue'].split("-")))
    sftp_file_name = '.'.join(file_name_list)
    s3_destination = os.path.join(s3_destination_root_path, partition_value)
    private_key_name = event['destinationPrivateKey'].split('/')[-1]
    args_map = {
        "host": event['destinationHost'],
        "username": event['destinationUserName'],
        "sftp_directory": os.path.join(event['destinationDirectory'], sftp_file_name),
        "private_key_path": event['destinationPrivateKey'],
        "private_key_name": private_key_name,
        "file_name": extract_name,
        "s3_destination": s3_destination
    }

    commands = [
        "set -e",
        "aws s3 cp {private_key_path} {{working_dir}}/",
        "chmod 400 {{working_dir}}/{private_key_name}",
        "set +e",
        "scp -i {{working_dir}}/{private_key_name} {file_name} {username}@{host}:{sftp_directory}",
        "if [[ \$? -ne 0 ]];",
        "then",
        "   rm -f {{working_dir}}/{private_key_name}",
        "   rm -f {file_name}",
        "   exit 1",
        "fi",
        "set -e",
        "rm -f {{working_dir}}/{private_key_name}",
        "rm -f {file_name}",
        "echo 'done'"
    ]
    command = "\n".join(commands).format(**args_map)
    return command


def perform_sftp(event, ssh_client):
    env = {}
    command = get_commands(event)
    working_dir = ssh_client.execute_shell_script(command, env)
    return working_dir


def check_process_status(ssh_client, working_dir):
    status = ssh_client.check_status(working_dir)
    return status


def lambda_handler(event, context):
    logger.info('UUPSFTP: Started.')
    ssh_client = start_ssh_client(event)
    event_type = event['eventType']
    if event_type == "SFTP":
        logger.info("UUPSFTP: sftp in progress.")
        working_dir_name = perform_sftp(event, ssh_client)
        event['runId'] = working_dir_name
    elif event_type == "STATUS":
        logger.info("UUPSFTP: Checking status.")
        working_dir = event['runId']
        process_status = check_process_status(ssh_client, working_dir)
        event['runStatus'] = process_status
    else:
        logger.error("UUPSFTP: Invalid event type. Expected SFTP/STATUS but got {0}".format(event_type))
    ssh_client.close()
    return event
