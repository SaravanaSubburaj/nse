import base64
import logging

import boto3

logger = logging.getLogger(__name__)


def decrypt_password(encrypted_password):
    kms = boto3.client('kms')
    binary_data = base64.b64decode(encrypted_password)
    meta = kms.decrypt(CiphertextBlob=binary_data)
    plaintext = meta[u'Plaintext']
    return plaintext.decode()


def decrypt_password_file(encrypted_password_file):
    encrypted_password = read_s3_file(encrypted_password_file)
    return decrypt_password(encrypted_password)


def get_bucket_and_key(s3_file):
    bucket_name = s3_file.split("/")[2]
    key_name = '/'.join(s3_file.split("/")[3:])
    return bucket_name, key_name


def read_s3_file(s3_file):
    logger.info("Reading file: {0}".format(s3_file))
    bucket, key = get_bucket_and_key(s3_file)
    key_obj = boto3.resource('s3').Object(bucket, key)
    content = key_obj.get()['Body'].read().decode('utf-8')
    return content
