from __future__ import print_function

import boto3
import datetime
import json
import logging
import logging.config
import os
import uuid


def load_log_config():
    # Basic config. Replace with your own logging config if required
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger


logger = load_log_config()
s3 = boto3.client('s3')
glue = boto3.client('glue')


def start_step_function(client, config):
    step_function_input = json.dumps({k: config[k] for k in config if k != "stateMachineArn"})
    step_name = str(uuid.uuid1())
    logger.info("Starting step function with name {}".format(step_name))
    response = client.start_execution(
        stateMachineArn=config["stateMachineArn"],
        name=step_name,
        input=step_function_input
    )
    return response


def load_config():
    config_path = os.environ['LAMBDA_TASK_ROOT'] + "/config.json"
    logger.info("Looking for config.json at " + config_path)
    config_contents = ' '.join(open(config_path).read().split('\n'))
    config = json.loads(config_contents)
    return config


def lambda_handler(event, context):
    client = boto3.client('stepfunctions')
    config = load_config()
    for record in event['Records']:
        # Marker file format: yyyy-MM-dd.marker
        raw_partition_date = record['s3']['object']['key'].replace("%3D", "=")
        logger.info("Created key: {}".format(record['s3']['object']['key']))
        raw_partition_value = raw_partition_date.split('/')[-2].split("=")[-1]
        try:
            partition_date = datetime.datetime.strptime(raw_partition_value, "%Y-%m-%d")
            if partition_date.strftime("%d") == config["monthlyExtractDate"].strip():
                config.update({"dailyExtractFlag": "true", "monthlyExtractFlag": "true"})
            else:
                config.update({"dailyExtractFlag": "true", "monthlyExtractFlag": "false"})
            partition_value = raw_partition_value
            logger.info("Started run for {}".format(partition_value))
            config.update({"partitionValue": partition_value})
            start_step_function(client, config)
            logger.info("Started State Machine.")
        except ValueError:
            logger.error("Unexpected '{}' marker file name. Expected YYYY-mm-dd.marker".format(raw_partition_value))
    return event
