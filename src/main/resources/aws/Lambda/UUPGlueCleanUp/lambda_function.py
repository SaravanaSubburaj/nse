from __future__ import print_function

import boto3
import json
import logging
import logging.config


def load_log_config():
    # Basic config. Replace with your own logging config if required
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger


logger = load_log_config()
glue = boto3.client('glue')


def delete_glue_job(config):
    job_name = config['glueJobName']
    response = glue.delete_job(
        JobName=job_name
    )
    logger.info("Deleted glue job {}".format(response['Name']))


def restore_wcu(config):
    if "dynamodb" in config['glueJobName'].lower():
        dynamodb = boto3.client('dynamodb')
        dynamodb_table_name = config['dynamodb_table_name']
        dynamodb_wcu = config['dynamodb_wcu']

        response = dynamodb.update_global_table_settings(
            GlobalTableName=dynamodb_table_name,
            GlobalTableProvisionedWriteCapacityUnits=dynamodb_wcu
        )
        logger.info("Restore Dynamo DB write capacity unit.")
    else:
        logger.info("Restore Dynamo DB write capacity unit not required.")


def lambda_handler(event, context):
    logger.info('*** Glue clean up lambda function starting ***')
    logger.info("Event data : {}".format(json.dumps(event)))
    config = event
    restore_wcu(config)
    return event
