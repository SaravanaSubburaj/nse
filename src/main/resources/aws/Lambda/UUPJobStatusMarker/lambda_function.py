from __future__ import print_function

import boto3
import json
import logging
import logging.config
import os


def load_log_config():
    # Basic config. Replace with your own logging config if required
    root = logging.getLogger()
    root.setLevel(logging.INFO)
    return root


logger = load_log_config()
s3 = boto3.client('s3')


def load_config():
    config_path = os.environ['LAMBDA_TASK_ROOT'] + "/config.json"
    logger.info("Looking for config.json at " + config_path)
    config_contents = ' '.join(open(config_path).read().split('\n'))
    config = json.loads(config_contents)
    return config


def get_marker_file_name(job_execution_type, run_status):
    _run_status_negation = "failure" if run_status == "success" else "success"
    _marker_file_name = "{0}.{1}".format(job_execution_type, run_status)
    _marker_file_name_negation = "{0}.{1}".format(job_execution_type, _run_status_negation)
    return _marker_file_name, _marker_file_name_negation


def get_bucket_and_key(s3_file):
    bucket_name = s3_file.split("/")[2]
    key_name = '/'.join(s3_file.split("/")[3:])
    return bucket_name, key_name


def create_marker(config):
    job_execution_type = config['jobExecutionType']
    partition_value = config['partitionValue']
    run_status = config['runStatus']
    marker_root_dir = config['markerRootPath']
    marker_file_name, marker_file_name_negation = get_marker_file_name(job_execution_type, run_status)
    bucket_name, key_name = get_bucket_and_key(marker_root_dir)
    key_name_with_marker = "{0}/date={1}/{2}".format(key_name, partition_value, marker_file_name)
    key_name_with_negation_marker = "{0}/date={1}/{2}".format(key_name, partition_value, marker_file_name_negation)
    s3.delete_object(Bucket=bucket_name, Key=key_name_with_negation_marker)
    s3.put_object(Bucket=bucket_name, Key=key_name_with_marker)


def lambda_handler(event, context):
    logger.info('UUPJobStatusMarker: Started.*')
    config = load_config()
    config.update(event)
    create_marker(config)
    return event
