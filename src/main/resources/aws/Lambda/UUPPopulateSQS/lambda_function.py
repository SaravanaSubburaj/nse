import json
import logging
import os
import re
from urllib.parse import unquote

import boto3


def load_log_config():
    # Basic config. Replace with your own logging config if required
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger


logger = load_log_config()
sqs = boto3.client('sqs')
lambda_client = boto3.client('lambda')


def read_config():
    config_path = os.environ['LAMBDA_TASK_ROOT'] + "/config.json"
    logger.info("Looking for config.json at " + config_path)
    config_contents = ' '.join(open(config_path).read().split('\n'))
    config = json.loads(config_contents)
    return config


def create_message(s3_key):
    try:
        logger.info("Creating message for key: {}".format(s3_key))
        raw_values = s3_key.split('/')
        run_type = raw_values[1]
        logger.info("run_type: " + run_type)
        final_message = None
        if run_type == 'entity_run':
            run_date = raw_values[2].split('=')[1]
            entity_name = raw_values[3]
            partition_date, status = raw_values[4].split('.')
            final_message = {"run_type": run_type, "entity_name": entity_name, "run_date": run_date, "partition_date": partition_date, "status": status}
        elif run_type == 'extract_run':
            run_date = raw_values[2].split('=')[1]
            run_type, status = raw_values[3].split('.')
            final_message = {"run_type": run_type, "run_date": run_date, "status": status}
        return json.dumps(final_message)
    except Exception as e:
        logger.error("Incorrect marker file {0}".format(s3_key))
        logger.error("Exception occurred ".format(e))


def create_payload(s3_key):
    payload = {
        "Records": [
            {
                "s3": {
                    "object": {
                        "key": s3_key
                    }
                }
            }
        ]
    }
    return json.dumps(payload)


def lambda_callback_handler(s3_key, lambda_callbacks):
    for lambda_callback in lambda_callbacks:
        pattern = lambda_callback['eventRegex']
        lambda_names = lambda_callback['lambdaNames']
        result = re.match(pattern, s3_key)
        if result is None:
            continue
        for lambda_name in lambda_names:
            payload = create_payload(s3_key)
            logger.info("Calling lambda {0} for s3 event {1}".format(lambda_name, s3_key))
            lambda_client.invoke(
                FunctionName=lambda_name,
                InvocationType='Event',
                Payload=payload
            )


def lambda_handler(event, context):
    logger.info(event)
    config = read_config()
    sqs_queue_url = config["sqsUrl"]
    lambda_callbacks = config["lambdaCallbacks"]
    for record in event['Records']:
        s3_key = unquote(record['s3']['object']['key'])
        logger.info("S3 key: {}".format(s3_key))
        msg_body = create_message(s3_key)
        logger.info("Message content: {0}".format(msg_body))
        if msg_body == 'null':
            logger.info("Empty Message. Skip populating to sqs.".format(msg_body))
        else:
            msg = sqs.send_message(QueueUrl=sqs_queue_url, MessageBody=msg_body)
        lambda_callback_handler(s3_key, lambda_callbacks)
    return event
