from __future__ import print_function

import boto3
import json
import logging
import logging.config
import os
import sys


def load_log_config():
    # Basic config. Replace with your own logging config if required
    _logger = logging.getLogger()
    _logger.setLevel(logging.INFO)
    return _logger


logger = load_log_config()
glue = boto3.client('glue')


def load_config():
    config_path = os.environ['LAMBDA_TASK_ROOT'] + "/config.json"
    logger.info("Looking for config.json at " + config_path)
    config_contents = ' '.join(open(config_path).read().split('\n'))
    config = json.loads(config_contents)
    return config


def get_spark_extra_config(config):
    spark_config_file_path = config['extraSparkConfigFile']
    bucket_name = spark_config_file_path.split("/")[2]
    key_name = '/'.join(spark_config_file_path.split("/")[3:])
    s3 = boto3.resource('s3')
    s3_object = s3.Object(bucket_name, key_name)
    body = s3_object.get()['Body'].read().decode('utf-8')
    config_list = filter(lambda c: len(c) == 2, list(map(lambda conf: conf.split("="), body.split('\n'))))
    config_str = ' --conf '.join(map(lambda conf: '='.join(map(lambda k: k.strip(), conf)), config_list))
    return config_str


def start_glue_jobs(config):
    logger.info('Glue runner started')

    glue_job_capacity = config['glueJobCapacity']
    glue_job_name = config['GlueJobName']
    config_file_path = config['configFile']
    partition_value = config['partitionValue']
    entity_type = config['entityType']
    rerun_flag = config['rerunFlag']

    args = {
        '--partition_value': partition_value,
        '--config_path': config_file_path,
        '--config_type': entity_type,
        '--rerun': rerun_flag
    }

    spark_config_str = get_spark_extra_config(config)
    logger.info("Extra spark configs: {}".format(spark_config_str))
    args.update({'--conf': spark_config_str})

    logger.info("Extra Arguments {}".format(args))
    try:
        response = glue.start_job_run(
            JobName=glue_job_name,
            Arguments=args,
            AllocatedCapacity=glue_job_capacity
        )

        glue_job_run_id = response['JobRunId']
        logger.info(str(glue_job_run_id))
        return glue_job_name, glue_job_run_id

    except Exception as e:
        logger.error('Failed to start Glue job named "{}"..'.format(glue_job_name))
        logger.error('Reason: {}'.format(e))
        sys.exit(1)


def lambda_handler(event, context):
    logger.info('*** Glue Runner lambda function starting ***')
    logger.info("Event data : {}".format(json.dumps(event)))
    config = load_config()
    config.update(event)
    glue_job_name, glue_job_run_id = start_glue_jobs(config)
    event.update({"glueJobName": glue_job_name, "glueJobRunId": glue_job_run_id})
    return event
