from __future__ import print_function

import json
import logging
import logging.config

import boto3


def load_log_config():
    # Basic config. Replace with your own logging config if required
    _logger = logging.getLogger()
    _logger.setLevel(logging.INFO)
    return _logger


logger = load_log_config()
dynamodb = boto3.client('dynamodb')


def increase_wcu(config):
    logger.info("Boosting Dynamo DB write capacity unit.")
    dynamodb_table_name = config['dynamoDBTableName']
    dynamodb_updated_wcu = int(config['dynamoDBBoostWCU'])
    dynamodb_updated_rcu = int(config['dynamoDBBoostRCU'])
    dynamodb_index_name = config['dynamoDBIndexName']
    provisioned_throughput = {
        "ReadCapacityUnits": dynamodb_updated_rcu,
        "WriteCapacityUnits": dynamodb_updated_wcu
    }
    global_secondary_index_updates = [{
        "Update": {
            'IndexName': dynamodb_index_name,
            'ProvisionedThroughput': provisioned_throughput,
        }
    }
    ]
    response = dynamodb.update_table(
        TableName=dynamodb_table_name,
        ProvisionedThroughput=provisioned_throughput,
        GlobalSecondaryIndexUpdates=global_secondary_index_updates
    )
    return response


def decrease_wcu(config):
    logger.info("Restoring Dynamo DB write capacity unit.")
    dynamodb_table_name = config['dynamoDBTableName']
    dynamodb_updated_wcu = int(config['dynamoDBWCU'])
    dynamodb_updated_rcu = int(config['dynamoDBRCU'])
    dynamodb_index_name = config['dynamoDBIndexName']
    provisioned_throughput = {
        "ReadCapacityUnits": dynamodb_updated_rcu,
        "WriteCapacityUnits": dynamodb_updated_wcu
    }
    global_secondary_index_updates = [{
        "Update": {
            'IndexName': dynamodb_index_name,
            'ProvisionedThroughput': provisioned_throughput,
        }
    }
    ]
    response = dynamodb.update_table(
        TableName=dynamodb_table_name,
        ProvisionedThroughput=provisioned_throughput,
        GlobalSecondaryIndexUpdates=global_secondary_index_updates
    )
    return response


def lambda_handler(event, context):
    logger.info('Housekeeping job started')
    logger.info("Event data : {}".format(json.dumps(event)))

    config = event
    if config['actionType'] == "BOOST":
        increase_wcu(config)
    elif config['actionType'] == "RESTORE":
        decrease_wcu(config)
    else:
        logger.error("Invalid actionType. Expected BOOST/RESTORE but got {0}".format(config['actionType']))
    return event
