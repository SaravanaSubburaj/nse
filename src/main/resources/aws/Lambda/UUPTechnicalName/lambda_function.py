import json
import logging
import logging.config
import os

import boto3
from botocore.exceptions import ClientError


def load_log_config():
    # Basic config. Replace with your own logging config if required
    _logger = logging.getLogger()
    _logger.setLevel(logging.INFO)
    return _logger


logger = load_log_config()
dynamodb = boto3.client("dynamodb")
table_name = os.environ['TECHNICAL_NAME_STORE']
business_name_key = os.environ['BUSINESS_NAME_KEY']
technical_name_key = os.environ['TECHNICAL_NAME_KEY']
RETRY_COUNT = int(os.environ.get('NUMBER_OF_RETRY', 5))


def get_technical_name(business_name):
    try:
        for retry in range(RETRY_COUNT):
            response = dynamodb.get_item(
                TableName=table_name,
                Key={business_name_key: {"S": business_name}}
            )
            technical_name = response['Item'][technical_name_key]["S"]
            return technical_name
    except ClientError as ce:
        logger.warning("DynamoDB request failed.")
        logger.warning(ce)


def get_bucket_and_key(s3_file):
    bucket_name = s3_file.split("/")[2]
    key_name = '/'.join(s3_file.split("/")[3:])
    return bucket_name, key_name


def read_s3_file(s3_file):
    logger.info("Reading file: {0}".format(s3_file))
    bucket, key = get_bucket_and_key(s3_file)
    key_obj = boto3.resource('s3').Object(bucket, key)
    content = key_obj.get()['Body'].read().decode('utf-8')
    return content


def populate(technical_store_json_path):
    data = read_s3_file(technical_store_json_path)
    json_parsed = json.loads(data)
    for put_request in json_parsed['technical_name_store']:
        for retry in range(5):
            try:
                item = put_request['PutRequest']['Item']
                dynamodb.put_item(
                    TableName=table_name,
                    Item=item
                )
                break
            except ClientError as ce:
                logger.warning("DynamoDB request failed.")
                logger.warning(ce)
                continue


def lambda_handler(event, context):
    if "technical_store_json_path" in event.keys():
        populate(event['technical_store_json_path'])
        return event
    business_names = event['business_names']
    business_technical_name_dict = {}
    for business_name in business_names:
        technical_name = get_technical_name(business_name)
        business_technical_name_dict[business_name] = technical_name
    return business_technical_name_dict
