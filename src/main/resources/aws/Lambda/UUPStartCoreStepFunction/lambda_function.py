from __future__ import print_function

import datetime
import json
import logging
import logging.config
import os
import uuid

import boto3
import sys
from botocore.errorfactory import ClientError


def load_log_config():
    # Basic config. Replace with your own logging config if required
    _logger = logging.getLogger()
    _logger.setLevel(logging.INFO)
    return _logger


logger = load_log_config()
s3 = boto3.client('s3')
glue = boto3.client('glue')
lambda_client = boto3.client('lambda')


def start_step_function(client, config):
    step_function_input = json.dumps({k: config[k] for k in config if k != "stateMachineArn"})
    step_name = str(uuid.uuid1())
    logger.info("Starting step function with name {}".format(step_name))
    response = client.start_execution(
        stateMachineArn=config["stateMachineArn"],
        name=step_name,
        input=step_function_input
    )
    return response


def load_config():
    config_path = os.environ['LAMBDA_TASK_ROOT'] + "/config.json"
    logger.info("Looking for config.json at " + config_path)
    config_contents = ' '.join(open(config_path).read().split('\n'))
    config = json.loads(config_contents)
    return config


def get_bucket_and_key(s3_file):
    bucket_name = s3_file.split("/")[2]
    key_name = '/'.join(s3_file.split("/")[3:])
    return bucket_name, key_name


def check_entity_status(entity_marker_file_path):
    bucket_name, key_name = get_bucket_and_key(entity_marker_file_path)
    logger.info("Checking entity status for {0}".format(entity_marker_file_path))
    try:
        content = s3.head_object(Bucket=bucket_name, Key=key_name)
        if content.get('ResponseMetadata', None) is not None:
            logger.info("File exists {0}".format(entity_marker_file_path))
        else:
            logger.info("File doesn't exists {0}".format(entity_marker_file_path))
            return False
    except ClientError:
        logger.info("File doesn't exists {0}".format(entity_marker_file_path))
        return False
    return True


def check_mandatory_entities(config, mandatory_entities, marker_root_path, partition_value):
    for entity in mandatory_entities:
        entity_marker_file_path = "{0}/entity_run/date={1}/{2}/{1}.success".format(marker_root_path, partition_value, entity)
        if not check_entity_status(entity_marker_file_path):
            job_execution_type = "profile",
            partition_value = config['partitionValue']
            run_status = "failure"
            msg = {'jobExecutionType': job_execution_type, 'partitionValue': partition_value, 'runStatus': run_status}
            response = lambda_client.invoke(
                FunctionName=config["marker_status_lambda"],
                InvocationType='RequestResponse',
                Payload=json.dumps(msg)
            )
            logger.info("Mandatory entity '{0}' does not exist.\nExiting.".format(entity))
            sys.exit(1)


def lambda_handler(event, context):
    client = boto3.client('stepfunctions')
    config = load_config()
    for record in event['Records']:
        # Marker file format: yyyy-MM-dd.marker
        marker_file_name = record['s3']['object']['key'].split('/')[-1]
        raw_partition_value = marker_file_name.split('.')[0]
        try:
            datetime.datetime.strptime(raw_partition_value, "%Y-%m-%d")
            partition_value = raw_partition_value
            logger.info("Started run for {}".format(partition_value))
            mandatory_entities = config['mandatoryEntities']
            marker_root_path = config['markerRootPath']
            check_mandatory_entities(config, mandatory_entities, marker_root_path, partition_value)
            config.update({"partitionValue": partition_value})
            start_step_function(client, config)
            logger.info("Started State Machine.")
        except ValueError:
            logger.error("Unexpected '{}' marker file name. Expected YYYY-mm-dd.marker".format(raw_partition_value))
    return event
