from __future__ import print_function

import boto3
import logging
import logging.config


def load_log_config():
    # Basic config. Replace with your own logging config if required
    root = logging.getLogger()
    root.setLevel(logging.INFO)
    return root


def check_glue_jobs(glue_job_name, glue_job_run_id):
    glue_status_response = glue.get_job_run(
        JobName=glue_job_name,
        RunId=glue_job_run_id,
        PredecessorsIncluded=False
    )
    job_run_state = glue_status_response['JobRun']['JobRunState']
    logger.debug('Job with Run Id {} is currently in state "{}"'.format(glue_job_run_id, job_run_state))

    if job_run_state in ['SUCCEEDED']:
        logger.info('Job with Run Id {} SUCCEEDED.'.format(glue_job_run_id))
        task_output = "success"
        return task_output

    elif job_run_state in ['FAILED', 'STOPPED']:
        logger.info('Job with Run Id {} FAILED.'.format(glue_job_run_id))
        task_output = "failed"
        return task_output

    elif job_run_state in ['STARTING', 'RUNNING', 'STOPPING']:
        task_output = "running"
        return task_output


logger = load_log_config()
glue = boto3.client('glue')


def lambda_handler(event, context):
    logger.info('*** Cheking glue job status ***')
    glue_job_run_id = event['glueJobRunId']
    glue_job_name = event['glueJobName']
    task_output = check_glue_jobs(glue_job_name, glue_job_run_id)

    if task_output == "success":
        event.update({"glueJobStatus": "success"})
        logger.info("Glue job Id: {}  succeeded".format(glue_job_run_id))
    elif task_output == "failed":
        event.update({"glueJobStatus": "failed"})
        logger.info("Glue job Id: {}  failed".format(glue_job_run_id))
    elif task_output == "running":
        event.update({"glueJobStatus": "running"})
    else:
        logger.error("Unknown status {}".format(task_output))
        event.update({"glueJobStatus": task_output})
    return event
