import json
import logging

from helper import read_s3_file

logger = logging.getLogger(__name__)


class Redshift:
    def __init__(self, table_name, partition_date, role_arn):
        self.table_name = table_name
        self.partition_date = partition_date
        self.role_arn = role_arn
        self.temp_table_name = "{}_temp".format(self.table_name)

    def populate_partition(self, source_path):
        source_path = source_path if source_path[-1] != "/" else source_path[:-1]
        partition_path = "{0}/date={1}/".format(source_path, self.partition_date)

        arg_dict = {"table_name": self.table_name, "temp_table_name": self.temp_table_name}
        drop_create_command = r"DROP TABLE IF EXISTS {temp_table_name};CREATE TABLE {temp_table_name} (LIKE {table_name});ALTER TABLE {temp_table_name} DROP COLUMN date;".format(
            **arg_dict)
        arg_dict = {"temp_table_name": self.temp_table_name, "partition_path": partition_path, "role_arn": self.role_arn}
        copy_command = r"COPY {temp_table_name} FROM '{partition_path}' IAM_ROLE '{role_arn}' GZIP CSV;".format(**arg_dict)
        arg_dict = {"table_name": self.table_name, "temp_table_name": self.temp_table_name, "partition_date": self.partition_date}
        alter_command = "ALTER TABLE {temp_table_name} ADD COLUMN date VARCHAR(65535) DEFAULT '{partition_date}';ALTER TABLE {table_name} APPEND FROM {temp_table_name} FILLTARGET;\\echo done".format(
            **arg_dict)
        command = drop_create_command + copy_command + alter_command
        return command

    @staticmethod
    def get_columns(column_schema):
        column_name = column_schema['columnName']
        data_type = column_schema['dataType']
        if column_name == "msisdn_value":
            return "63||{0}".format(column_name)
        if data_type.lower() == "date":
            return "TRUNC({0})".format(column_name)
        if data_type.lower() == "boolean":
            return "cast({0} as integer)".format(column_name)
        return column_name

    def get_unload_command(self, extract_config_path):
        extract_config = json.loads(read_s3_file(extract_config_path))
        columns = list(map(lambda column_schema: self.get_columns(column_schema), extract_config['targetSchema']))
        extract_path = extract_config['storageDetails'][0]['pathUrl']
        extract_path = extract_path if extract_path[-1] != "/" else extract_path[:-1]
        partitioned_extract_path = "{0}/date={1}/".format(extract_path, self.partition_date)

        dpa_filter = "lower(trim(brand_type_code)) in (''tm'', ''ghp'', ''ghp-prepaid'') AND " + \
                     "lower(trim(subscriber_status_code)) not in (''c'', ''l'', ''t'')"
        query_arg_dict = {
            "columns": ", ".join(columns),
            "table_name": self.table_name,
            "partition_date": self.partition_date,
            "dpa_filter": dpa_filter
        }

        dpa_query = "SELECT {columns} FROM {table_name} WHERE date=''{partition_date}'' AND {dpa_filter}".format(**query_arg_dict)
        query_arg_dict = {
            "dpa_query": dpa_query,
            "extract_path": partitioned_extract_path,
            "role_arn": self.role_arn
        }
        query = "UNLOAD ('{dpa_query}') to '{extract_path}' IAM_ROLE '{role_arn}' CSV ALLOWOVERWRITE PARALLEL ON".format(**query_arg_dict)
        data_format_options = extract_config['storageDetails'][0]['dataFormatOption']
        file_name = filter(lambda x: x['key'] == 'csv.filename', data_format_options)[0]['value']
        return query, file_name, partitioned_extract_path
