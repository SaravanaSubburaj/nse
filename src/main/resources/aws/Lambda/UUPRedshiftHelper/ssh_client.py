import io
import logging
import uuid

import boto3
import paramiko
import sys
from helper import get_bucket_and_key

logger = logging.getLogger(__name__)

RETRY = 5


class SSHClient:
    def __init__(self, host, port, username, password, private_key, working_dir):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.private_key = private_key
        self.ssh_client = None
        self.channel = None
        self.shell = None
        self.base_working_dir = working_dir
        self.working_dir = "{0}/{1}".format(working_dir, str(uuid.uuid1()))
        self.sftp_client = None
        self.initialize_ssh_client()

    def initialize_ssh_client(self):
        logger.info("UUPSSH: Initializing ssh session")
        if self.private_key:
            key_obj = self.get_private_key(self.private_key)
        else:
            key_obj = None
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh_client.connect(
            hostname=self.host,
            port=self.port,
            username=self.username,
            password=self.password,
            pkey=key_obj
        )
        transport = self.ssh_client.get_transport()
        self.channel = transport.open_session()
        self.sftp_client = self.ssh_client.open_sftp()
        logger.info("UUPSSH: ssh session initiated.")
        logger.info("UUPSSH: Creating working directory to {0}".format(self.working_dir))
        command = "mkdir -p {0}".format(self.working_dir)
        output = self.execute_command(command)
        logger.info("UUPSSH: Creating working directory output: {0}".format(output))
        logger.info("UUPSSH: Changing working directory to {0}".format(self.working_dir))
        command = "cd {0}".format(self.working_dir)
        output = self.execute_command(command)
        logger.info("UUPSSH: Changing working directory output: {0}".format(output))

    def close(self):
        self.ssh_client.close()

    def get_private_key(self, s3_file):
        bucket, key = get_bucket_and_key(s3_file)
        key_obj = boto3.resource('s3').Object(bucket, key)
        key_str = key_obj.get()['Body'].read().decode('utf-8')
        key = paramiko.RSAKey.from_private_key(io.StringIO(key_str))
        logger.info("UUPSSH: Retrieved private key from S3")
        return key

    def execute_command(self, command, env={}):
        # logger.info("Command: {0}".format(command))
        exit_status = None
        for i in range(RETRY):
            stdin, stdout, stderr = self.ssh_client.exec_command(command)
            exit_status = int(stdout.channel.recv_exit_status())
            logger.info("UUPSSH: exit status: {}".format(exit_status))
            if exit_status != 0:
                logger.info("UUPSSH: error {}".format(stderr.readlines()))
                continue
            else:
                break
        logger.info("UUPSSH: exit status: {}".format(exit_status))
        if exit_status != 0:
            sys.exit(1)
        stdout_lines = stdout.readlines()
        return stdout_lines

    def execute_channel_command(self, command, env={}):
        self.channel.exec_command(command)
        exit_status = self.channel.exit_status_ready()
        logger.info("UUPSSH: exit status: {}".format(exit_status))
        # from time import sleep
        # while not exit_status:
        #     sleep(1)
        #     exit_status = self.channel.exit_status_ready()
        #     logger.info("UUPSSH: exit status: {}".format(exit_status))

    def execute_command_query(self, command, query, env={}):
        logger.info("UUPSSH: Creating sql file({1}/script.sql) with content: {0}".format(query, self.working_dir))
        for q in query.split(";"):
            create_command = 'echo "{0};" >> {1}/script.sql'.format(q, self.working_dir)
            output = self.execute_command(create_command, env)
        logger.info("UUPSSH: Created sql file. Output: {0}".format(output))
        processed_command = command.format(**{"working_dir": self.working_dir})
        logger.info("UUPSSH: Started executing command with nohup {0}".format(processed_command))
        nohup_command = "export PGPASSWORD={2};nohup {0} > {1}/run.log 2>&1 & echo $! > {1}/run.pid; sleep 1".format(
            processed_command,
            self.working_dir,
            env['PGPASSWORD']
        )

        self.execute_channel_command(nohup_command, env)
        return self.working_dir.split("/")[-1]

    def execute_shell_script(self, content, query="", env={}):
        logger.info("UUPSSH: Creating sql file({1}/script.sql) with content: {0}".format(query, self.working_dir))
        for q in query.split(";"):
            create_command = 'echo "{0};" >> {1}/script.sql'.format(q, self.working_dir)
            output = self.execute_command(create_command, env)
        logger.info("UUPSSH: Created sql file. Output: {0}".format(output))

        logger.info("UUPSSH: Creating shell script ({1}/run.sh) with content: {0}".format(content, self.working_dir))

        for q in content.split("\n"):
            processed_q = q.format(**{"working_dir": self.working_dir})
            create_command = 'echo "{0}" >> {1}/run.sh'.format(processed_q, self.working_dir)
            output = self.execute_command(create_command, env)
        logger.info("UUPSSH: Created shell script. Output: {0}".format(output))
        nohup_command = "export PGPASSWORD={1};nohup sh {0}/run.sh > {0}/run.log 2>&1 & echo $! > {0}/run.pid; sleep 1".format(
            self.working_dir,
            env['PGPASSWORD']
        )
        self.execute_channel_command(nohup_command, env)
        return self.working_dir.split("/")[-1]

    def read_remote_file(self, file_path):
        lines = None
        logger.info("Reading remote file: {0}".format(file_path))
        with self.sftp_client.open(file_path) as f:
            lines = f.readlines()
        return lines

    def check_run_log(self, run_detail):
        working_dir = "{0}/{1}".format(self.base_working_dir, run_detail)
        log_file = "{0}/run.log".format(working_dir)
        logs = self.read_remote_file(log_file)
        if logs[-1].strip()[:4] == "done":
            return True
        else:
            return False

    def check_status_pid(self, run_detail):
        working_dir = "{0}/{1}".format(self.base_working_dir, run_detail)
        pid_file = "{0}/run.pid".format(working_dir)
        pid = self.read_remote_file(pid_file)[0].strip()
        for i in range(5):
            try:
                command = '[ -d "/proc/{0}" ]'.format(pid)
                output = self.execute_command(command)
                if len(output) > 0:
                    return True
                else:
                    return False
            except:
                continue
        return True

    def check_status(self, run_detail):
        pid_status = self.check_status_pid(run_detail)
        log_status = self.check_run_log(run_detail)
        if log_status:
            return "success"
        if pid_status:
            return "failed"
        else:
            return "running"
