import logging

from helper import decrypt_password_file
from redshift import Redshift
from ssh_client import SSHClient


def load_log_config():
    # Basic config. Replace with your own logging config if required
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger


logger = load_log_config()


def start_ssh_client(config):
    ssh_host = config.get('sshHostname')
    ssh_username = config.get('sshUsername')
    ssh_password = config.get('sshPassword')
    ssh_private_key = config.get('sshPrivateKey')
    assert ssh_password or ssh_private_key, "Missing sshPassword or SshPrivateKey"
    ssh_port = int(config.get('sshPort', 22))
    ssh_working_dir = config.get('sshWorkingDir')
    ssh_client = SSHClient(ssh_host, ssh_port, ssh_username, ssh_password, ssh_private_key, ssh_working_dir)
    return ssh_client


def get_commands(event, redshift_helper, command_type):
    file_name = None
    if command_type == "LOAD":
        source_path = event['sourcePath']
        query = redshift_helper.populate_partition(source_path)
    elif command_type == "UNLOAD":
        extracts_config = event['extractConfig']
        query, file_name, s3_destination = redshift_helper.get_unload_command(extracts_config)
    args_map = {
        "host": event['redshiftHost'],
        "username": event['redshiftUsername'],
        "db_name": event['redshiftDbName'],
        "port": event['redshiftPort'],
        "query": query
    }
    if command_type == "LOAD":
        command = 'psql -h {host} -U {username} -d {db_name} -p {port} -a -w -v ON_ERROR_STOP=1 -f {{working_dir}}/script.sql'.format(
            **args_map)
    else:
        psql_query = 'psql -h {host} -U {username} -d {db_name} -p {port} -a -w -v ON_ERROR_STOP=1 -f {{working_dir}}/script.sql'.format(
            **args_map)
        args_map = {
            "psql_query": psql_query,
            "s3_destination": s3_destination,
            "file_name": file_name,
        }
        commands = [
            "set -e",
            "{psql_query}",
            "aws s3 cp {s3_destination} {{working_dir}}/extracts/ --recursive",
            "cat {{working_dir}}/extracts/* > {{working_dir}}/{file_name}",
            "rm -f {{working_dir}}/extracts/*",
            "echo 'done'"
        ]
        command = "\n".join(commands).format(**args_map)
    return command, query


def load_redshift(event, ssh_client, redshift_helper):
    password = decrypt_password_file(event['redshiftPasswordFile'])
    env = {"PGPASSWORD": password}
    logger.info("Started running copy command.")
    command, query = get_commands(event, redshift_helper, "LOAD")
    working_dir = ssh_client.execute_command_query(command, query, env)
    return working_dir


def unload_redshift(event, ssh_client, redshift_helper):
    password = decrypt_password_file(event['redshiftPasswordFile'])
    env = {"PGPASSWORD": password}
    logger.info("UUPRedshiftHelper: Generating extracts from redshift.")
    command, query = get_commands(event, redshift_helper, "UNLOAD")
    working_dir = ssh_client.execute_shell_script(command, query, env)
    return working_dir


def check_process_status(ssh_client, working_dir):
    status = ssh_client.check_status(working_dir)
    return status


def lambda_handler(event, context):
    logger.info('UUPRedshiftHelper: Started.')
    ssh_client = start_ssh_client(event)
    event_type = event['eventType']
    if event_type == "LOAD":
        redshift_helper = Redshift(event['tableName'], event['partitionValue'], event['roleArn'])
        logger.info("UUPRedshiftHelper: Load in progress.")
        working_dir_name = load_redshift(event, ssh_client, redshift_helper)
        event['runId'] = working_dir_name
    elif event_type == "UNLOAD":
        redshift_helper = Redshift(event['tableName'], event['partitionValue'], event['roleArn'])
        logger.info("UUPRedshiftHelper: Unload in progress.")
        working_dir_name = unload_redshift(event, ssh_client, redshift_helper)
        event['runId'] = working_dir_name
    elif event_type == "STATUS":
        logger.info("UUPRedshiftHelper: Checking status.")
        working_dir = event['runId']
        process_status = check_process_status(ssh_client, working_dir)
        event['runStatus'] = process_status
    else:
        logger.error("UUPRedshiftHelper: Invalid event type. Expected LOAD/UNLOAD but got {0}".format(event_type))
    ssh_client.close()
    return event
