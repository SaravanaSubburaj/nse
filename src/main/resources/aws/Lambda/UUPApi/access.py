import json

import boto3

dynamodb = boto3.client("dynamodb")


def check_attribute_access_dynamodb(username, attributes, table_name):
    username_key = "username"
    authorized_attributes = "authorized_attributes"
    response = dynamodb.get_item(
        TableName=table_name,
        Key={username_key: {"S": username}}
    )
    if len(response['Item']) == 0:
        raise Exception("User: {0} not found to check attribute level access".format(username))
    authorized_attributes = list(map(lambda attribute: attribute['S'], response['Item'][authorized_attributes]['L']))
    for attribute in attributes:
        if attribute not in authorized_attributes:
            raise Exception("Unauthorized access of attribute: {0} by user: {1}".format(attribute, username))


def check_attribute_access_json(username, attributes):
    with open('user_access_store.json') as access_file:
        user_details = json.loads(' '.join(access_file.readlines()))
        for user_detail in user_details['users']:
            if user_detail['username'] == username:
                for attribute in attributes:
                    if attribute not in user_detail['authorized_attributes']:
                        raise Exception("Unauthorized access of attribute: {0} by user: {1}".format(attribute, username))
            else:
                raise Exception("User: {0} not found to check attribute level access".format(username))
