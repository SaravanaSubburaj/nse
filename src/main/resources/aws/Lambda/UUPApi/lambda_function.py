import json
import logging
import logging.config
import os
import traceback

import boto3
from access import *
from api import *
from misc import *


def load_log_config():
    # Basic config. Replace with your own logging config if required
    _logger = logging.getLogger()
    _logger.setLevel(logging.INFO)
    return _logger


logger = load_log_config()
config_keys = ["table", "msisdn_api_parameter", "subscriber_id_api_parameter", "msisdn_index_name", "msisdn_column_name", "subscriber_id_column_name", "mapping_lambda_name"]
config = {k: os.environ.get(k) for k in config_keys}
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(config['table'])
lambda_client = boto3.client('lambda')
RETRY_COUNT = int(os.environ.get('NUMBER_OF_RETRY', 5))
user_access_table_name = os.environ.get('user_access_table_name', None)


def verify_mapping(mapped_cols):
    for k in mapped_cols:
        if type(mapped_cols[k]) is str:
            pass
        else:
            raise Exception('Mapping not found')


def column_name_translation(name_mapping_lambda, columns):
    msg = {'business_names': columns}
    response = lambda_client.invoke(
        FunctionName=name_mapping_lambda,
        InvocationType='RequestResponse',
        Payload=json.dumps(msg)
    )
    mapped_cols = json.loads(response['Payload'].read())
    verify_mapping(mapped_cols)
    return mapped_cols


def map_response_column_name(b2t_mapping, response):
    t2b_mapping = {b2t_mapping[k]: k for k in b2t_mapping}
    mapped_response = {t2b_mapping[k]: response[k] for k in response}
    return mapped_response


def lambda_handler(event, context):
    try:
        query_parameters = {}
        subscriber_id_parameter = config['subscriber_id_api_parameter']
        msisdn_api_parameter = config['msisdn_api_parameter']
        columns_list = event['headers']['requested_columns'].split(',')
        if msisdn_api_parameter in event['headers']:
            query_parameters = {msisdn_api_parameter: event['headers'][msisdn_api_parameter]}
        elif subscriber_id_parameter in event['headers']:
            query_parameters = {subscriber_id_parameter: event['headers'][subscriber_id_parameter]}
        else:
            json_response = create_error_response("Bad Request", "Expected subscriberId/msisdn.")
            return custom_response(400, json_response)
        name_mapping_lambda = config['mapping_lambda_name']
        username = event['requestContext']['authorizer']['claims']['username']
        if user_access_table_name is None:
            check_attribute_access_json(username, columns_list)
        else:
            check_attribute_access_dynamodb(username, columns_list, user_access_table_name)
        b2t_mapping = column_name_translation(name_mapping_lambda, columns_list)
        mapped_column_list = list(map(lambda x: b2t_mapping[x], b2t_mapping))
        if msisdn_api_parameter in query_parameters:
            msisdn_value = query_parameters[msisdn_api_parameter]
            logger.info("Request: msisdn: {0} columns: {1}".format(msisdn_value, event['headers']['requested_columns']))
            mapped_subscriber_id = get_msisdn(config, table, msisdn_value)
            raw_response = get_subscriber_id(config, table, mapped_subscriber_id, mapped_column_list)
        elif subscriber_id_parameter in query_parameters:
            subscriber_id = query_parameters[subscriber_id_parameter]
            logger.info("Request: subscriber_id: {0} columns: {1}".format(subscriber_id, event['headers']['requested_columns']))
            raw_response = get_subscriber_id(config, table, subscriber_id, mapped_column_list)
        else:
            json_response = create_error_response("Bad Request", "Expected subscriberId/msisdn.")
            return custom_response(400, json_response)
        mapped_response = map_response_column_name(b2t_mapping, raw_response)
        return custom_response(200, mapped_response)
    except Exception as e:
        json_response = create_error_response("Internal Server Error", str(e))
        traceback.print_exc()
        return custom_response(500, json_response)
