from boto3.dynamodb.conditions import Key
from misc import *


def get_msisdn(config, table, msisdn_value):
    result = table.query(
        IndexName=config["msisdn_index_name"],
        KeyConditionExpression=Key(config["msisdn_column_name"]).eq(msisdn_value),
        ProjectionExpression=config["subscriber_id_column_name"])
    if len(result["Items"]) == 0:
        raise Exception("msisdn: {0} not found".format(msisdn_value))
    return result["Items"][0][config["subscriber_id_column_name"]]


def get_subscriber_id(config, table, subscriber_id, columns_list):
    result = table.query(
        KeyConditionExpression=Key(config["subscriber_id_column_name"]).eq(subscriber_id),
        ProjectionExpression=", ".join(columns_list))
    if len(result["Items"]) == 0:
        raise Exception("subscriber_id: {0} not found".format(subscriber_id))
    raw_result = result["Items"][0]
    populated_empty_attributes = [(column, raw_result[column]) if column in list(raw_result.keys()) else (column, "") for column in columns_list]
    return dict(populated_empty_attributes)
