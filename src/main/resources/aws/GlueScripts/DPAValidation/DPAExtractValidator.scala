import org.apache.spark.SparkContext
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.catalyst.parser.CatalystSqlParser
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import org.apache.log4j.Logger
import com.amazonaws.services.glue.DynamicFrame
import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.util.GlueArgParser
import com.amazonaws.services.glue.util.Job
import com.amazonaws.services.glue.util.JsonOptions
import com.amazonaws.services.glue.types._
import in.datateam.utils.helper.FileSystemUtils
import spray.json._

object DPAExtractValidator {
  case class UnitSchema(columnName: String, dataType: String)

  case class Config(
      primaryKey: String,
      attributes: List[String],
      schema: List[UnitSchema],
      newUUPPath: String,
      OldUUPPath: String,
      targetInvalidRecordsPath: String,
      targetRowCountStatsPath: String,
      targetAttributeStatsPath: String)

  case class ResultDFs(rowCountStatsDF: DataFrame, attributeStatsDF: DataFrame, invalidRecordsDF: DataFrame)

  def main(args: Array[String]): Unit = {
    val logger: Logger = Logger.getLogger(getClass.getName)
    try {
      val argMap: Map[String, String] = GlueArgParser.getResolvedOptions(
        args,
        Array("config_path", "old_uup_path", "new_uup_path", "partition")
      )
      val configJson = argMap.get("config_path").get.trim
      val oldUUPPath = argMap.get("old_uup_path").get.trim
      val newUUPPath = argMap.get("new_uup_path").get.trim
      val partitionValue = argMap.get("partition").get.trim
      logger.info(s"Config path: ${configJson}.")
      logger.info(s"Partition value: ${partitionValue}.")
      logger.info(s"Old UUP Path: ${oldUUPPath}.")
      logger.info(s"New UUP Path: ${newUUPPath}.")
      implicit val sc: SparkContext = new SparkContext()
      implicit val glueContext: GlueContext = new GlueContext(sc)
      implicit val spark: SparkSession = glueContext.getSparkSession
      process(configJson, partitionValue, newUUPPath, oldUUPPath)
    } catch {
      case c: Exception => throw c
    }
  }

  def getConfig(configPath: String)(implicit spark: SparkSession): Config = {
    val inputJson: String = FileSystemUtils.readFile(configPath)
    object DpaConfigJsonProtocol extends DefaultJsonProtocol {
      implicit val unitSchemaFormat = jsonFormat2(UnitSchema)
      implicit val configFormat = jsonFormat8(Config)
    }
    import DpaConfigJsonProtocol._
    import spray.json._
    val config = inputJson.parseJson.convertTo[Config]
    config
  }

  def readDataFrame(
      schema: StructType,
      sourcePath: String,
      headerFlag: Boolean
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val inputDF: DataFrame = headerFlag match {
      case true =>
        val allOptions =
          List(("header" -> "true"), ("inferSchema" -> "false")).toMap
        spark.read
          .format("csv")
          .options(allOptions)
          .schema(schema)
          .load(sourcePath)
      case false =>
        val allOptions = List(("inferSchema" -> "false")).toMap
        spark.read
          .format("csv")
          .options(allOptions)
          .schema(schema)
          .load(sourcePath)
    }
    inputDF
  }

  def writeDataFrame(
      dataFrame: DataFrame,
      dataFormat: String,
      targetPath: String
    )(
      implicit spark: SparkSession
    ): Unit = {
    dataFrame.write
      .format(dataFormat)
      .mode("overwrite")
      .option("header", "true")
      .save(targetPath)
  }

  def getSchema(config: Config)(implicit spark: SparkSession): StructType = {
    val structFields = config.schema.map(
      schemaUnit =>
        StructField(
          schemaUnit.columnName,
          CatalystSqlParser.parseDataType(schemaUnit.dataType)
        )
    )
    val schema = StructType(structFields)
    schema
  }

  def validate(expected: Long, actual: Long): String = {
    if (expected == actual) {
      "PASSED"
    } else {
      "FAILED"
    }
  }

  def getRowCountStatsDF(
      config: Config,
      sourceTdtDF: DataFrame,
      sourceUupDF: DataFrame,
      joinedDF: DataFrame
    )(
      implicit spark: SparkSession
    ): DataFrame = {
    val rowCountActual = sourceTdtDF.count()
    val rowCountExpected = sourceUupDF.count()
    val rowCountStatus = validate(rowCountExpected, rowCountActual)

    val distinctRowCountActual =
      sourceTdtDF.select(col(config.primaryKey)).distinct().count()
    val distinctRowCountExpected =
      sourceUupDF.select(col(config.primaryKey)).distinct().count()
    val distinctRowCountStatus =
      validate(distinctRowCountExpected, distinctRowCountActual)

    val expectedPrimaryKey = s"expected_${config.primaryKey}"
    val actualPrimaryKey = s"actual_${config.primaryKey}"
    val extraRecordsOldUUP = joinedDF
      .select(actualPrimaryKey, expectedPrimaryKey)
      .where(col(actualPrimaryKey).isNull)
      .count()
    val extraRecordsOldUUPStatus = validate(extraRecordsOldUUP, 0)
    val extraRecordsNewUUP = joinedDF
      .select(actualPrimaryKey, expectedPrimaryKey)
      .where(col(expectedPrimaryKey).isNull)
      .count()
    val extraRecordsNewUUPStatus = validate(extraRecordsNewUUP, 0)
    val rowCountList = List(
      Row("Row Count", rowCountExpected, rowCountActual, rowCountStatus),
      Row(
        "Distinct Row Count",
        distinctRowCountExpected,
        distinctRowCountActual,
        distinctRowCountStatus
      ),
      Row(
        "Extra Records in Old UPP",
        0L,
        extraRecordsOldUUP,
        extraRecordsOldUUPStatus
      ),
      Row(
        "Extra Records in New UPP",
        0L,
        extraRecordsNewUUP,
        extraRecordsNewUUPStatus
      )
    )
    val rowCountStatsDF = spark.createDataFrame(
      spark.sparkContext
        .parallelize(rowCountList),
      StructType(
        List(
          StructField("validation_type", StringType),
          StructField("expected", LongType),
          StructField("actual", LongType),
          StructField("status", StringType)
        )
      )
    )
    rowCountStatsDF
  }

  def validate(config: Config, oldUUPDF: DataFrame, newUUPDF: DataFrame)(implicit spark: SparkSession): ResultDFs = {
    val primaryKey = config.primaryKey
    val attributes = Array(primaryKey) ++ config.attributes
    val attributeList = config.attributes

    val sourceTdtDF = newUUPDF
    val sourceUupDF = oldUUPDF

    val tdtCols = sourceTdtDF.columns.toList
    val addedCols =
      sourceUupDF.columns.filterNot(columnName => tdtCols.contains(columnName))
    val sourceTdtAddedDF = addedCols.foldLeft(sourceTdtDF)(
      (sourceTdtDF, addCol) => sourceTdtDF.withColumn(addCol, lit(""))
    )

    val sourceTdtDpaDF =
      sourceTdtAddedDF.select(
        attributes.map(c => col(c).as(s"actual_${c}")): _*
      )
    val sourceUupDpaDF =
      sourceUupDF.select(attributes.map(c => col(c).as(s"expected_${c}")): _*)

    val totalColumns = sourceTdtDpaDF.columns.length
    val expectedPrimaryKey = s"expected_${primaryKey}"
    val actualPrimaryKey = s"actual_${primaryKey}"
    val primaryKeyIndexLeft = sourceTdtDpaDF.columns.indexOf(actualPrimaryKey)
    val primaryKeyIndexRight = totalColumns + sourceTdtDpaDF.columns.indexOf(
        expectedPrimaryKey
      )
    val currentDFIndex = (0 until totalColumns).toArray
    val nextDFIndex = (totalColumns until (2 * totalColumns)).toArray
    val wholeDFIndex = (0 until 2 * totalColumns).toArray

    val rawJoinedDF = sourceTdtDpaDF.join(
      sourceUupDpaDF,
      sourceTdtDpaDF(actualPrimaryKey) === sourceUupDpaDF(expectedPrimaryKey),
      "full_outer"
    )

    val rawJoinedRenamedDF = rawJoinedDF

    val extraRecordsDF = rawJoinedRenamedDF.filter(
      col(actualPrimaryKey).isNull || col(expectedPrimaryKey).isNull
    )
    val rawJoinedNoNullDF = rawJoinedRenamedDF.filter(
      col(actualPrimaryKey).isNotNull && col(expectedPrimaryKey).isNotNull
    )
    val FLAG = "FLAG"
    val combinedDFSchema = StructType(
      rawJoinedDF.schema.fields ++ Array(StructField(FLAG, StringType))
    )
    implicit val combinedDFEncoder = RowEncoder(combinedDFSchema)
    val joinedDF = rawJoinedNoNullDF.flatMap { row =>
      val isEqualFlag = currentDFIndex
        .map(
          index => row.getString(index) == row.getString(totalColumns + index)
        )
        .reduce(_ && _)
      if (isEqualFlag) {
        val rowContent: Array[Any] = wholeDFIndex
            .map(index => row.getString(index)) ++ Array("VALID")
        val selectedRow = Row(rowContent: _*)
        List(selectedRow)
      } else {
        val rowContent: Array[Any] = wholeDFIndex
            .map(index => row.getString(index)) ++ Array("INVALID")
        val selectedRow = Row(rowContent: _*)
        List(selectedRow)
      }
    }

    val dpaDFSchema = StructType(
      rawJoinedDF.schema.fields ++ Array(StructField(FLAG, StringType))
    )
    implicit val dpaDFEncoder = RowEncoder(dpaDFSchema)

    val noNullDFCount = rawJoinedRenamedDF.count()
    val attributeCalcList = attributeList.map { attributeName =>
      val q = rawJoinedNoNullDF.withColumn(
        s"result_${attributeName}",
        when(
          col(s"actual_${attributeName}") <=> col(s"expected_${attributeName}"),
          1
        ).otherwise(0)
      )
      val valid = q.filter(col(s"result_${attributeName}") === 1).count()
      Row(attributeName, valid, noNullDFCount - valid)
    }
    val attrCalcDFSchema = StructType(
      List(
        StructField("attribute_name", StringType),
        StructField("records_passed", LongType),
        StructField("records_failed", LongType)
      )
    )

    val attrCalcDF = spark.createDataFrame(
      spark.sparkContext.parallelize(attributeCalcList),
      attrCalcDFSchema
    )

    val attributeStatsDF = attrCalcDF.withColumn(
      "status",
      when(col("records_failed") === 0, "Passed")
        .otherwise("Failed")
    )

    //attributeStatsDF.show()

    val invalidDF =
      joinedDF.where(col(FLAG) === "INVALID").drop(col(FLAG))
    val targetSchema =
      attributes.flatMap(x => List(col(s"expected_${x}"), col(s"actual_${x}")))
    val invalidRecordsDF = invalidDF
      .union(extraRecordsDF)
      .select(targetSchema: _*)

    val rowCountStatsDF =
      getRowCountStatsDF(config, sourceTdtDF, sourceUupDF, rawJoinedRenamedDF)

    val resultDFs =
      ResultDFs(rowCountStatsDF, attributeStatsDF, invalidRecordsDF)
    resultDFs
  }

  def process(
      configPath: String,
      runDate: String,
      newUUPPath: String = "",
      oldUUPPath: String = ""
    )(
      implicit spark: SparkSession
    ): Unit = {
    val config = getConfig(configPath)
    val schema = getSchema(config)
    val finalOldUUPPath =
      if (oldUUPPath != "") {
        oldUUPPath
      } else {
        config.OldUUPPath
      }
    val finalNewUUPPath =
      if (newUUPPath != "") {
        newUUPPath
      } else {
        config.newUUPPath
      }
    val sourceTdtDF = readDataFrame(schema, finalNewUUPPath, true)
    val sourceUupDF = readDataFrame(schema, finalOldUUPPath, false)
    val resultDFs = validate(config, sourceUupDF, sourceTdtDF)
    writeDataFrame(
      resultDFs.invalidRecordsDF,
      "parquet",
      s"${config.targetInvalidRecordsPath}/${runDate}"
    )
    writeDataFrame(
      resultDFs.rowCountStatsDF.coalesce(1),
      "csv",
      s"${config.targetRowCountStatsPath}/${runDate}"
    )
    writeDataFrame(
      resultDFs.attributeStatsDF.coalesce(1),
      "csv",
      s"${config.targetAttributeStatsPath}/${runDate}"
    )
  }

}
