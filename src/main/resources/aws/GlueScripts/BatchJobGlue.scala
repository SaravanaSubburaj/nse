import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger
import com.amazonaws.services.glue.DynamicFrame
import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.util.GlueArgParser
import com.amazonaws.services.glue.util.Job
import com.amazonaws.services.glue.util.JsonOptions
import com.amazonaws.services.glue.types._

import in.datateam.cadup.BatchJob
import in.datateam.utils.InvalidArgumentException
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils
import in.datateam.utils.helper.Misc

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object BatchJobGlue {
  val logger: Logger = Logger.getLogger(getClass.getName)

  val usage =
    """
    Usage: [--config_type entity/profile --config_path config-file-path --partition_value partition-value --rerun flag]
  """

  def main(args: Array[String]): Unit = {

    try {
    val argNamesArray = args.contains("--delta_date") match
    {
      case true => Array("config_path", "config_type", "partition_value", "rerun", "delta_date")
      case _ => Array("config_path", "config_type", "partition_value", "rerun")
    }
      val argMap: Map[String, String] = GlueArgParser.getResolvedOptions(
        args,
        argNamesArray
      )
      val configJson = argMap.get("config_path").get.trim
      val eventType: String =
        argMap.get("config_type").get.trim match {
          case "entity" => CommonEnums.ENTITY
          case "profile" => CommonEnums.PROFILE
          case _ =>
            throw InvalidArgumentException(
              s"Expected either entity or " +
                s"profile but got ${argMap.get("config_type").get}."
            )
        }
      val partitionValue: String = argMap.get("partition_value").get.trim
      val rerunFlag: Boolean = argMap.get("rerun").get.trim == "true"
      val deltaDate: String = if(args.contains("--delta_date")) {
        argMap.get("delta_date").get.trim
      } else {
        Misc.getNthDate(partitionValue, -1)
      }
      logger.info(s"Executing for Event type: ${eventType}.")
      logger.info(s"Config path: ${configJson}.")
      logger.info(s"Partition value: ${partitionValue}.")
      logger.info(s"Delta date: ${deltaDate}.")
      implicit val sc: SparkContext = new SparkContext()
      implicit val glueContext: GlueContext = new GlueContext(sc)
      implicit val spark: SparkSession = glueContext.getSparkSession
      val config: Config = ConfigParser.getConfig(eventType, configJson)
      BatchJob.process(config, partitionValue, rerunFlag, deltaDate)
      FileSystemUtils.cleanUp()
    } catch {
      case c: Exception => throw c
    }
  }
}
