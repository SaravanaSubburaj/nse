#!/bin/bash

CONFIG_TYPE=$1
CONFIG_FILE=$2
BATCH=$3
SOURCE_TYPE=$4

numArgs="$#"


case $numArgs in
 5) if [ $5 = "rerun" ]
    then
      RERUN=$5
    elif [ $5 != "" ]
      then
        DELTA_DATE=$5
    fi
    ;;
 6) if [ $5 = "rerun" ]
    then
      RERUN=$5
      DELTA_DATE=$6
    elif [ $6 = "rerun" ]
    then
      RERUN=$6
      DELTA_DATE=$5
    else
      RERUN=""
      DELTA_DATE=""
    fi
    ;;
esac


#source /etc/environment
CADUP_HOME_DIR=/mnt/disk1/edgeapp/uup/cadup-1.0/
CADUP_CONF_DIR="$CADUP_HOME_DIR/configs"
CADUP_SPARK_CONF_DIR="$CADUP_HOME_DIR/configs/spark_configs"
CADUP_LIB_DIR="$CADUP_HOME_DIR/lib"
SPARK_CONF=""


if [[ "$CONFIG_TYPE" = "" ]] || [[ "$CONFIG_FILE" = "" ]] || [[ "$BATCH" = "" ]] || [[ "$SOURCE_TYPE" = "" ]];
then
    exit 1
fi

if [[ "$SOURCE_TYPE" = "small" ]];
then
    SPARK_CONF="small_spark.conf"
elif [[ "$SOURCE_TYPE" = "medium" ]];
then
    SPARK_CONF="medium_spark.conf"
elif [[ "$SOURCE_TYPE" = "large" ]];
then
    SPARK_CONF="large_spark.conf"
else
    echo "Invalid source type : $SOURCE_TYPE. Excepted one of small/medium/large."
    exit 1
fi



CONFIG_FILE_NAME=`basename $CONFIG_FILE`


spark2-submit \
        --class in.datateam.cadup.BatchJob \
        --master yarn \
        --deploy-mode cluster \
        --queue tier1_edo_np_uup_tdt_grp \
        --properties-file "$CADUP_SPARK_CONF_DIR/$SPARK_CONF" \
       --jars $(echo ${CADUP_LIB_DIR}/*.jar | tr ' ' ',')  \
        --files "$CONFIG_FILE" \
        ${CADUP_LIB_DIR}/cadup_2.11-1.0.jar \
        "--eventType" "$CONFIG_TYPE" "--configPath" "$CONFIG_FILE_NAME" "--batchDate" "$BATCH" "--rerunFlag" "$RERUN" "--deltaDate" "$DELTA_DATE"
