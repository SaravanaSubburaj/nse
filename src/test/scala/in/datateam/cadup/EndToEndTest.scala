package in.datateam.cadup

import scala.reflect.io.Path

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.when
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructType

import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.utils.configparser.Config
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums
import in.datateam.utils.helper.FileSystemUtils

/* *
 * Created by Niraj(niraj.kumar.das@thedatateam.in).
 * */
class EndToEndTest extends FlatSpec with SparkTest {
  val configDir = "src/test/resources/configs"
  val expectedEntitiesDir = "src/test/resources/data/expectedEntities/"
  val entityConfigPath: String => String = s"${configDir}/entity_configs/" + _
  val profileConfigPath: String => String = s"${configDir}/profile_configs/" + _
  val expectedEntitiesPath: String => String = s"${expectedEntitiesDir}/" + _

  def cleanUpDirs(config: Config, partitionValue: String)(implicit spark: SparkSession): Unit = {
    val cleaUpPathMap = BatchJob.getCleanUpPaths(config, partitionValue)
    val cleanUpDirs = cleaUpPathMap.map { case (_: String, paths: Array[String]) => paths.toList }.flatten
      .map(path => Path(path))
    cleanUpDirs.map(cleanUpDir => cleanUpDir.deleteRecursively())
  }

  def removeNulls(df: DataFrame): DataFrame = {
    val filteredDF = df.schema.fields.foldLeft(df) { (df, schemaArg) =>
      {
        if (schemaArg.dataType === StringType) {
          df.withColumn(
            schemaArg.name,
            when(col(schemaArg.name) === lit(""), null)
              .otherwise(col(schemaArg.name))
          )
        } else {
          df.withColumn(
            schemaArg.name,
            df(schemaArg.name).cast(schemaArg.dataType)
          )
        }
      }
    }
    filteredDF
  }

  def readExpected(
      path: String,
      inputSchema: StructType,
      partition: Option[String] = None
    )(
      implicit spark: SparkSession
    ): DataFrame = {

    val expectedEntity = spark.read
      .format("csv")
      .option("header", "true")
      .option("timestampFormat", "yyyy-MM-dd hh mm ss")
      .schema(inputSchema)
      .load(expectedEntitiesPath(path))

    val schema = inputSchema.fields.map(f => col(f.name).cast(f.dataType))
    val typeCastedDF = expectedEntity.select(schema: _*)
    partition match {
      case Some(partitionValue) =>
        typeCastedDF.where(s"date='$partitionValue'")
      case None => typeCastedDF
    }
  }

  "EndToEndTest" should "entity calculation globe_subscriber" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val subscriberEntityConfigJson = entityConfigPath("globe_subscriber_entity_config.json")
    val subscriberEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, subscriberEntityConfigJson)
    cleanUpDirs(subscriberEntityConfig, partition)(spark)
    val actualSubscriberEntity = BatchJob.process(subscriberEntityConfig, partition, partition)(spark)

    val expectedSubscriberEntityList = readExpected("GlobeSubscriber", actualSubscriberEntity.schema).collect().toList
    val actualSubscriberEntityList = actualSubscriberEntity.collect()
    assertResult(expectedSubscriberEntityList)(actualSubscriberEntityList)
    FileSystemUtils.cleanUp()(spark)

  }

  "EndToEndTest" should "entity calculation globe_product_type" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val productEntityConfigJson = entityConfigPath("globe_product_type_entity_config.json")
    val productEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, productEntityConfigJson)
    cleanUpDirs(productEntityConfig, partition)(spark)
    val actualProductEntity = BatchJob.process(productEntityConfig, partition, partition)(spark)
    val expectedProductEntity = readExpected("GlobeProduct", actualProductEntity.schema)
    assertResult(expectedProductEntity.collect().toList)(actualProductEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation billing_offer" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val billingOfferEntityConfigJson = entityConfigPath("billing_offer_entity_config.json")
    val billingOfferEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, billingOfferEntityConfigJson)
    cleanUpDirs(billingOfferEntityConfig, partition)(spark)
    val actualBillingOfferEntity = BatchJob
      .process(billingOfferEntityConfig, partition, partition)(spark)
    val expectedBillingOfferEntity = readExpected("BillingOffers", actualBillingOfferEntity.schema)
    assertResult(expectedBillingOfferEntity.collect().toList)(actualBillingOfferEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation daily_postpaid_subscriber_availment_staging" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val dailyPostSubsAvailStageJson =
        entityConfigPath("daily_postpaid_subscriber_availment_staging_entity_config.json")
      val dailyPostSubsAvailConfig =
        ConfigParser.getConfig(CommonEnums.ENTITY, dailyPostSubsAvailStageJson)
      cleanUpDirs(dailyPostSubsAvailConfig, partition)(spark)
      val actualdailyPostSubsAvailStage = BatchJob
        .process(dailyPostSubsAvailConfig, partition, partition)(spark)
      val expecteddailyPostSubsAvailStage =
        readExpected("DailyPostpaidSubscriberAvailmentStaging", actualdailyPostSubsAvailStage.schema)
      assertResult(expecteddailyPostSubsAvailStage.collect().toList)(actualdailyPostSubsAvailStage.collect().toList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation daily_prepaid_subscriber_usage_staging" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      //Test to process prepaid_subscriber_usage_staging entity
      val prepaidUsageEntityConfigJson = entityConfigPath(
        "daily_prepaid_subscriber_usage_staging_entity_config.json"
      )
      val prepaidUsageEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, prepaidUsageEntityConfigJson)
      cleanUpDirs(prepaidUsageEntityConfig, partition)(spark)
      val actualPrepaidUsageEntity = BatchJob
        .process(prepaidUsageEntityConfig, partition, partition)(spark)
      val expectedPrepaidUsageEntity = readExpected("PrepaidUsage", actualPrepaidUsageEntity.schema)
      assertResult(expectedPrepaidUsageEntity.collect().toList)(actualPrepaidUsageEntity.collect().toList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation daily_postpaid_subscriber_usage" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      //Test to process postpaid_subscriber_usage_staging entity
      val postpaidUsageEntityConfigJson = entityConfigPath("daily_postpaid_subscriber_usage_entity_config.json")
      val postpaidUsageEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, postpaidUsageEntityConfigJson)
      cleanUpDirs(postpaidUsageEntityConfig, partition)(spark)
      val actualPostpaidUsageEntity = BatchJob
        .process(postpaidUsageEntityConfig, partition, partition)(spark)
      val expectedPostpaidUsageEntity = readExpected("PostpaidUsage", actualPostpaidUsageEntity.schema)
      assertResult(expectedPostpaidUsageEntity.collect().toList)(actualPostpaidUsageEntity.collect().toList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation reward_entity" in withSparkSession { implicit spark: SparkSession =>
    val multiLevelPartition = "2019^01^16"
    val rewardEntityConfigJson = entityConfigPath("reward_entity_config.json")
    val rewardEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, rewardEntityConfigJson)
    cleanUpDirs(rewardEntityConfig, multiLevelPartition)(spark)
    val actualRewardsEntity = BatchJob
      .process(rewardEntityConfig, multiLevelPartition, multiLevelPartition)(spark)

    val expectedRewardsEntity = readExpected("MillipedeRewardTransactions", actualRewardsEntity.schema)
    assertResult(expectedRewardsEntity.collect().toList)(actualRewardsEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation subscriber_assigned_billing_offer" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val subsAssignBillOffer = entityConfigPath("subscriber_assigned_billing_offer_entity_config.json")
      val subsAssignBillOfferConfig = ConfigParser.getConfig(CommonEnums.ENTITY, subsAssignBillOffer)
      cleanUpDirs(subsAssignBillOfferConfig, partition)(spark)
      val actualSubAssignedBillingOfferEntity = BatchJob.process(subsAssignBillOfferConfig, partition, partition)(spark)

      val expectedSubAssignedBillingOfferEntity =
        readExpected(
          "SubscriberAssignedBillingOffer",
          actualSubAssignedBillingOfferEntity.schema
        )
      val expectedSubAssignedBillingOfferEntityList = expectedSubAssignedBillingOfferEntity.collect().toList
      val actualSubAssignedBillingOfferEntityList =
        removeNulls(actualSubAssignedBillingOfferEntity)
          .collect()
          .toList
      assertResult(expectedSubAssignedBillingOfferEntityList)(actualSubAssignedBillingOfferEntityList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation monthly_postpaid_subscriber_revenue" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val monthlyPostpaidEntityConfigJson = entityConfigPath("monthly_postpaid_subscriber_revenue_entity_config.json")
      val monthlyPostpaidEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, monthlyPostpaidEntityConfigJson)
      cleanUpDirs(monthlyPostpaidEntityConfig, partition)(spark)
      val actualMonthlyPostpaidSummaryEntity =
        BatchJob.process(monthlyPostpaidEntityConfig, partition, partition)(spark)

      val expectedMonthlyPostpaidSummaryEntity =
        readExpected(
          "MonthlyPostpaidSubscriberRevenueSummary",
          actualMonthlyPostpaidSummaryEntity.schema
        )
      val expectedMonthlyPostpaidSummaryEntityList = expectedMonthlyPostpaidSummaryEntity.collect().toList
      assertResult(expectedMonthlyPostpaidSummaryEntityList)(actualMonthlyPostpaidSummaryEntity.collect().toList)
      FileSystemUtils.cleanUp()(spark)

  }

  "EndToEndTest" should "entity calculation monthly_prepaid_subscriber_revenue_summary" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val monthlyPrepaidEntityConfigJson =
        entityConfigPath("monthly_prepaid_subscriber_revenue_summary_entity_config.json")
      val monthlyPrepaidEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, monthlyPrepaidEntityConfigJson)
      cleanUpDirs(monthlyPrepaidEntityConfig, partition)(spark)

      val actualMonthlyPrepaidSummaryEntity = BatchJob.process(monthlyPrepaidEntityConfig, partition, partition)(spark)

      val expectedMonthlyPrepaidSummaryEntity =
        readExpected(
          "MonthlyPrepaidSubscriberRevenueSummary",
          actualMonthlyPrepaidSummaryEntity.schema
        )
      val expectedMonthlyPrepaidSummaryEntityList = expectedMonthlyPrepaidSummaryEntity.collect().toList
      assertResult(expectedMonthlyPrepaidSummaryEntityList)(actualMonthlyPrepaidSummaryEntity.collect().toList)

      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation dpa_segment" in withSparkSession { implicit spark: SparkSession =>
    val noSepPartition = "20190116"
    val dpaEntityConfigJson = entityConfigPath("dpa_segment_entity_config.json")
    val dpaEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, dpaEntityConfigJson)
    cleanUpDirs(dpaEntityConfig, noSepPartition)(spark)
    val actualDPAEntity = BatchJob.process(dpaEntityConfig, noSepPartition, noSepPartition)(spark)

    val expectedDPAEntityList = readExpected("DpaSegment", actualDPAEntity.schema).collect().toList
    assertResult(expectedDPAEntityList)(actualDPAEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)

  }

  "EndToEndTest" should "entity calculation globe_daily_subscriber_reload_summary" in withSparkSession {
    implicit spark: SparkSession =>
      val PartitionsList = List("2019-01-07")
      val dailySubscriberReloadConfigJson = entityConfigPath("globe_daily_subscriber_reload_summary_entity_config.json")
      val dailySubscriberReloadConfig = ConfigParser.getConfig(CommonEnums.ENTITY, dailySubscriberReloadConfigJson)
      val actualSRSList = PartitionsList.map(partitionVal => {
        cleanUpDirs(dailySubscriberReloadConfig, partitionVal)(spark)
        removeNulls(BatchJob.process(dailySubscriberReloadConfig, partitionVal, partitionVal)(spark))
      })

      val expectedSRSList = PartitionsList.map(partitionVal => {
        readExpected(
          "GlobeDailySubscriberReloadSummary",
          actualSRSList(0).schema
        ).where(s"date='$partitionVal'")
          .collect()
          .toList
      })

      for (i <- 0 until actualSRSList.size) {
        assertResult(expectedSRSList(i))(actualSRSList(i).collect().toList)
      }

      FileSystemUtils.cleanUp()(spark)

  }

  "EndToEndTest" should "entity calculation globe_prepaid_promo_availment_staging" in withSparkSession {
    implicit spark: SparkSession =>
      val noSepPartition = "20190116"
      val prepaidPromoAvailConfigJson = entityConfigPath("globe_prepaid_promo_availment_staging_entity_config.json")
      val prepaidPromoAvailConfig = ConfigParser.getConfig(CommonEnums.ENTITY, prepaidPromoAvailConfigJson)
      cleanUpDirs(prepaidPromoAvailConfig, noSepPartition)(spark)
      val actualPrepaidPromoAvailmentEntity =
        BatchJob.process(prepaidPromoAvailConfig, noSepPartition, noSepPartition)(spark)

      val expectedPrepaidPromoAvailmentEntityList =
        readExpected(
          "GlobePrepaidPromoAvailmentStaging",
          actualPrepaidPromoAvailmentEntity.schema
        ).collect().toList
      val actualPrepaidPromoAvailmentEntityList = removeNulls(actualPrepaidPromoAvailmentEntity).collect().toList
      assertResult(expectedPrepaidPromoAvailmentEntityList)(actualPrepaidPromoAvailmentEntityList)

      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation prepaid_promo" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val prepaidPromoEntityConfigJson = entityConfigPath("prepaid_promo_entity_config.json")
    val prepaidPromoEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, prepaidPromoEntityConfigJson)
    cleanUpDirs(prepaidPromoEntityConfig, partition)(spark)
    val actualPrepaidPromoEntity = BatchJob.process(prepaidPromoEntityConfig, partition, partition)(spark)

    val expectedPrepaidPromoEntityList =
      readExpected("PrepaidPromo", actualPrepaidPromoEntity.schema)
        .collect()
        .toList
    assertResult(expectedPrepaidPromoEntityList)(actualPrepaidPromoEntity.collect().toList)

    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation globe_daily_postpaid_subscriber_usage_staging" in withSparkSession {
    implicit spark: SparkSession =>
      val noSepPartition = "20190116"
      val globeDailyPostpaidSubscriberUsageStagingConfigJson = entityConfigPath(
        "globe_daily_postpaid_subscriber_usage_staging_entity_config.json"
      )
      val globeDailyPostpaidSubscriberUsageStagingConfig =
        ConfigParser.getConfig(CommonEnums.ENTITY, globeDailyPostpaidSubscriberUsageStagingConfigJson)
      cleanUpDirs(globeDailyPostpaidSubscriberUsageStagingConfig, noSepPartition)(spark)
      val actualPostpaidSubUsageEntity =
        BatchJob.process(
          globeDailyPostpaidSubscriberUsageStagingConfig,
          noSepPartition,
          noSepPartition
        )(spark)

      val expectedPostpaidUsageEntityList =
        readExpected(
          "GlobeDailyPostpaidSubscriberUsageStaging",
          actualPostpaidSubUsageEntity.schema
        ).collect().toList
      assertResult(expectedPostpaidUsageEntityList)(removeNulls(actualPostpaidSubUsageEntity).collect().toList)

      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation financial_account_subscriber_reference" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val financialAccountSubscriberEntityConfigJson = entityConfigPath(
        "financial_account_subscriber_reference_entity_config.json"
      )
      val financialAccountSubscriberEntityConfig = ConfigParser
        .getConfig(
          CommonEnums.ENTITY,
          financialAccountSubscriberEntityConfigJson
        )
      cleanUpDirs(financialAccountSubscriberEntityConfig, partition)(spark)
      val actualFinancialAccountEntity =
        BatchJob.process(financialAccountSubscriberEntityConfig, partition, partition)(spark)
      val expectedFinancialAccountEntityList =
        readExpected(
          "FinancialAccountSubscriberReference",
          actualFinancialAccountEntity.schema
        ).collect().toList
      assertResult(expectedFinancialAccountEntityList)(
        actualFinancialAccountEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)

  }

  "EndToEndTest" should "entity calculation financial_account_invoice_staging" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val financialInvoiceStagingEntityConfigJson = entityConfigPath(
        "financial_account_invoice_staging_entity_config.json"
      )
      val financialInvoiceStagingEntityConfig = ConfigParser
        .getConfig(CommonEnums.ENTITY, financialInvoiceStagingEntityConfigJson)
      cleanUpDirs(financialInvoiceStagingEntityConfig, partition)(spark)
      val actualFinancialInvoiceStagingEntity =
        BatchJob.process(financialInvoiceStagingEntityConfig, partition, partition)(spark)

      val expectedFinancialInvoiceStagingEntityList =
        readExpected(
          "FinancialAccountInvoiceStaging",
          actualFinancialInvoiceStagingEntity.schema
        ).collect().toList
      assertResult(expectedFinancialInvoiceStagingEntityList)(
        actualFinancialInvoiceStagingEntity.collect.toList
      )

      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation globe_billing_offers" in withSparkSession { implicit spark: SparkSession =>
    val noSepPartition = "20190116"
    val billingEntityConfigJson =
      entityConfigPath("globe_billing_offers_entity_config.json")
    val billingEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, billingEntityConfigJson)
    cleanUpDirs(billingEntityConfig, noSepPartition)(spark)
    val actualBillingOffersEntity =
      BatchJob.process(billingEntityConfig, noSepPartition, noSepPartition)(spark)

    val expectedBillingOffersEntityList =
      readExpected("GlobeBillingOffer", actualBillingOffersEntity.schema)
        .collect()
        .toList

    assertResult(expectedBillingOffersEntityList)(
      removeNulls(actualBillingOffersEntity).collect().toList
    )

    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation globe_daily_prepaid_subscriber_usage" in withSparkSession {
    implicit spark: SparkSession =>
      val noSepPartition = "20190116"
      val prepaidSubsUsageConfigJson =
        entityConfigPath(
          "globe_daily_prepaid_subscriber_usage_entity_config.json"
        )
      val prepaidSubsUsageConfig =
        ConfigParser.getConfig(CommonEnums.ENTITY, prepaidSubsUsageConfigJson)
      cleanUpDirs(prepaidSubsUsageConfig, noSepPartition)(spark)
      val actualPrepaidSubsUsageEntity =
        BatchJob.process(prepaidSubsUsageConfig, noSepPartition, noSepPartition)(spark)

      val expectedPrepaidSubsUsageEntityList =
        readExpected(
          "GlobeDailyPrepaidSubscriberUsage",
          actualPrepaidSubsUsageEntity.schema
        ).collect().toList
      assertResult(expectedPrepaidSubsUsageEntityList)(
        removeNulls(actualPrepaidSubsUsageEntity).collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation device" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val deviceConfigJson =
      entityConfigPath("device_entity_config.json")
    val deviceConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, deviceConfigJson)
    cleanUpDirs(deviceConfig, partition)(spark)
    val actualDeviceEntity =
      removeNulls(BatchJob.process(deviceConfig, partition, partition)(spark))
    val expectedDeviceEntity =
      readExpected("Device", actualDeviceEntity.schema)
    assertResult(expectedDeviceEntity.collect().toList)(
      actualDeviceEntity.collect().toList
    )
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation billing_arrangement" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val billingArrangementConfigJson =
      entityConfigPath("billing_arrangement_entity_config.json")
    val billingArrangementConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, billingArrangementConfigJson)
    cleanUpDirs(billingArrangementConfig, partition)(spark)
    val actualBillingArrangementEntity = removeNulls(
      BatchJob.process(billingArrangementConfig, partition, partition)(spark)
    )
    val expectedBillingArrangementEntity = readExpected(
      "BillingArrangement",
      actualBillingArrangementEntity.schema
    )
    assertResult(expectedBillingArrangementEntity.collect().toList)(
      actualBillingArrangementEntity.collect().toList
    )

    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation aa_model_score" in withSparkSession { implicit spark: SparkSession =>
    val aaPartition = "2018^12"

    val aaModelScoreEntityConfigJson =
      entityConfigPath("aa_model_score_entity_config.json")
    val aaModelScoreEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, aaModelScoreEntityConfigJson)
    cleanUpDirs(aaModelScoreEntityConfig, aaPartition)(spark)
    val actualAAModelScoreEntity =
      BatchJob
        .process(aaModelScoreEntityConfig, aaPartition, aaPartition)(spark)
    val expectedAAModelScoreEntity =
      readExpected("AAModelScore", actualAAModelScoreEntity.schema)(spark)
    assertResult(expectedAAModelScoreEntity.sort("model").collect().toList)(
      actualAAModelScoreEntity.sort("model").collect().toList
    )
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation daily_network_data_gi_transaction_top_location" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val transactionTopLocationDataGIConfigJson =
        entityConfigPath(
          "daily_network_data_gi_transaction_top_location_entity_config.json"
        )
      val transactionTopLocationDataGIConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          transactionTopLocationDataGIConfigJson
        )
      cleanUpDirs(transactionTopLocationDataGIConfig, partition)(spark)
      val actualTransactionTopLocationDataGIEntity =
        removeNulls(
          BatchJob.process(transactionTopLocationDataGIConfig, partition, partition)(spark)
        )
      val expectedTransactionTopLocationDataGIEntity = readExpected(
        "DailyNetworkDataGITransactionTopLocation",
        actualTransactionTopLocationDataGIEntity.schema
      ).collect().toList
      assertResult(expectedTransactionTopLocationDataGIEntity)(
        actualTransactionTopLocationDataGIEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation daily_network_data_gn_transaction_top_location" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val transactionTopLocationDataGNConfigJson =
        entityConfigPath(
          "daily_network_data_gn_transaction_top_location_entity_config.json"
        )
      val transactionTopLocationDataGNConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          transactionTopLocationDataGNConfigJson
        )
      cleanUpDirs(transactionTopLocationDataGNConfig, partition)(spark)
      val actualTransactionTopLocationDataGNEntity =
        removeNulls(
          BatchJob.process(transactionTopLocationDataGNConfig, partition, partition)(spark)
        )
      val expectedTransactionTopLocationDataGNEntity = readExpected(
        "DailyNetworkDataGNTransactionTopLocation",
        actualTransactionTopLocationDataGNEntity.schema
      ).collect().toList
      assertResult(expectedTransactionTopLocationDataGNEntity)(
        actualTransactionTopLocationDataGNEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)

  }

  "EndToEndTest" should "entity calculation daily_network_sms_mo_transaction_top_location" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val transactionTopLocationSMSMOConfigJson =
        entityConfigPath(
          "daily_network_sms_mo_transaction_top_location_entity_config.json"
        )
      val transactionTopLocationSMSMOConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          transactionTopLocationSMSMOConfigJson
        )
      cleanUpDirs(transactionTopLocationSMSMOConfig, partition)(spark)
      val actualTransactionTopLocationSMSMOEntity =
        removeNulls(
          BatchJob.process(transactionTopLocationSMSMOConfig, partition, partition)(spark)
        )
      val expectedTransactionTopLocationSMSMOEntity = readExpected(
        "DailyNetworkSMSMOTransactionTopLocation",
        actualTransactionTopLocationSMSMOEntity.schema
      ).collect().toList
      assertResult(expectedTransactionTopLocationSMSMOEntity)(
        actualTransactionTopLocationSMSMOEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation daily_network_voice_mo_transaction_top_location" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val transactionTopLocationVoiceMOConfigJson =
        entityConfigPath(
          "daily_network_voice_mo_transaction_top_location_entity_config.json"
        )
      val transactionTopLocationVoiceMOConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          transactionTopLocationVoiceMOConfigJson
        )
      cleanUpDirs(transactionTopLocationVoiceMOConfig, partition)(spark)
      val actualTransactionTopLocationVoiceMOEntity =
        removeNulls(
          BatchJob
            .process(transactionTopLocationVoiceMOConfig, partition, partition)(spark)
        )
      val expectedTransactionTopLocationVoiceMOEntity = readExpected(
        "DailyNetworkVoiceMOTransactionTopLocation",
        actualTransactionTopLocationVoiceMOEntity.schema
      ).collect().toList
      assertResult(expectedTransactionTopLocationVoiceMOEntity)(
        actualTransactionTopLocationVoiceMOEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation customer" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val customerConfigJson = entityConfigPath("customer_entity_config.json")
    val customerConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, customerConfigJson)
    cleanUpDirs(customerConfig, partition)(spark)
    val customerEntity =
      removeNulls(BatchJob.process(customerConfig, partition, partition)(spark))
    val expectedCustomerEntity =
      readExpected("customer", customerEntity.schema).collect().toList
    assertResult(expectedCustomerEntity)(customerEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation charge_staging" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val chargeStagingConfigJson =
      entityConfigPath("charge_staging_entity_config.json")
    val chargeStagingConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, chargeStagingConfigJson)
    cleanUpDirs(chargeStagingConfig, partition)(spark)
    val chargeStagingEntity =
      removeNulls(BatchJob.process(chargeStagingConfig, partition, partition)(spark))
    val expectedChargeStagingEntity = readExpected(
      "charge_staging",
      chargeStagingEntity.schema
    ).collect().toList
    assertResult(expectedChargeStagingEntity)(
      chargeStagingEntity.collect().toList
    )
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation financial_account" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val financialAccountStagingConfigJson =
      entityConfigPath("financial_account_entity_config.json")
    val financialAccountStagingConfig = ConfigParser.getConfig(
      CommonEnums.ENTITY,
      financialAccountStagingConfigJson
    )
    cleanUpDirs(financialAccountStagingConfig, partition)(spark)
    val financialAccountStagingEntity = removeNulls(
      BatchJob.process(financialAccountStagingConfig, partition, partition)(spark)
    )
    val expectedFinancialAccountStagingEntity =
      readExpected("financial_account", financialAccountStagingEntity.schema)
        .collect()
        .toList
    assertResult(expectedFinancialAccountStagingEntity)(
      financialAccountStagingEntity.collect().toList
    )
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation subscriber_contract_staging" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val subscriberContractStagingConfigJson =
        entityConfigPath("subscriber_contract_staging_entity_config.json")
      val subscriberContractStagingConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          subscriberContractStagingConfigJson
        )
      cleanUpDirs(subscriberContractStagingConfig, partition)(spark)
      val subscriberContractStagingEntity =
        removeNulls(
          BatchJob.process(subscriberContractStagingConfig, partition, partition)(spark)
        )
      val expectedSubscriberContractStagingEntity =
        readExpected(
          "subscriber_contract_staging",
          subscriberContractStagingEntity.schema
        ).collect().toList
      assertResult(expectedSubscriberContractStagingEntity)(
        subscriberContractStagingEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation customer_facing_unit_type" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val customerFacingUnitTypeCodeConfigJson =
        entityConfigPath("customer_facing_unit_type_entity_config.json")
      val customerFacingUnitTypeCodeConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          customerFacingUnitTypeCodeConfigJson
        )
      cleanUpDirs(customerFacingUnitTypeCodeConfig, partition)(spark)
      val customerFacingUnitTypeCodeEntity =
        removeNulls(
          BatchJob.process(customerFacingUnitTypeCodeConfig, partition, partition)(spark)
        )
      val expectedcustomerFacingUnitTypeCodeEntity =
        readExpected(
          "customerFacingUnitType",
          customerFacingUnitTypeCodeEntity.schema
        ).collect().toList
      assertResult(expectedcustomerFacingUnitTypeCodeEntity)(
        customerFacingUnitTypeCodeEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation customer_facing_unit_sub_type" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"

      val customerFacingUnitSubTypeCodeConfigJson =
        entityConfigPath("customer_facing_unit_sub_type_entity_config.json")
      val customerFacingUnitSubTypeCodeConfig =
        ConfigParser.getConfig(
          CommonEnums.ENTITY,
          customerFacingUnitSubTypeCodeConfigJson
        )
      cleanUpDirs(customerFacingUnitSubTypeCodeConfig, partition)(spark)
      val customerFacingUnitSubTypeCodeEntity =
        removeNulls(
          BatchJob
            .process(customerFacingUnitSubTypeCodeConfig, partition, partition)(spark)
        )

      val expectedcustomerFacingUnitSubTypeCodeEntity =
        readExpected(
          "customerFacingUnitSubType",
          customerFacingUnitSubTypeCodeEntity.schema
        ).collect().toList
      assertResult(expectedcustomerFacingUnitSubTypeCodeEntity)(
        customerFacingUnitSubTypeCodeEntity.collect().toList
      )
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation subscriber_dataset" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val subscriberDatasetConfigJson =
      entityConfigPath("subscriber_dataset_entity_config.json")
    val subscriberDatasetConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, subscriberDatasetConfigJson)
    cleanUpDirs(subscriberDatasetConfig, partition)(spark)
    val subscriberDatasetEntity = removeNulls(
      BatchJob.process(subscriberDatasetConfig, partition, partition)(spark)
    )
    val expectedSubscriberDatasetEntity =
      readExpected(
        "subscriberDataset",
        subscriberDatasetEntity.schema
      ).collect().toList
    assertResult(expectedSubscriberDatasetEntity)(subscriberDatasetEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation payment_staging" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val paymentStagingConfigJson =
      entityConfigPath("payment_staging_entity_config.json")
    val paymentStagingConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, paymentStagingConfigJson)
    cleanUpDirs(paymentStagingConfig, partition)(spark)
    val paymentStagingEntity =
      removeNulls(BatchJob.process(paymentStagingConfig, partition, partition)(spark))
    FileSystemUtils.cleanUp()(spark)
    val expectedPaymentStaging = readExpected("paymentStaging", paymentStagingEntity.schema)
    assertResult(expectedPaymentStaging.collect().toList)(paymentStagingEntity.collect().toList)

  }

  "EndToEndTest" should "entity calculation contact" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val contactConfigJson = entityConfigPath("contact_entity_config.json")
    val contactConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, contactConfigJson)
    cleanUpDirs(contactConfig, partition)(spark)
    val contactEntity =
      removeNulls(BatchJob.process(contactConfig, partition, partition)(spark))
    val expectedContactEntity = readExpected("contact", contactEntity.schema)
    assertResult(expectedContactEntity.collect().toList)(contactEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation radcom_gi" in withSparkSession { implicit spark: SparkSession =>
    val multiLevelPartition = "2019^01^13"
    val radcomEntityConfigJson =
      entityConfigPath("radcom_gi_entity_config.json")
    val radcomEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, radcomEntityConfigJson)
    cleanUpDirs(radcomEntityConfig, multiLevelPartition)(spark)
    val radcomEntity = removeNulls(
      BatchJob.process(radcomEntityConfig, multiLevelPartition, multiLevelPartition, true)(spark)
    ).sort("msisdn", "totalBytesSendByMs_sum")
    val expectedRadcomEntity =
      readExpected("radcom_gi", radcomEntity.schema)
        .sort("msisdn", "totalBytesSendByMs_sum")
        .collect
        .toList
    assertResult(expectedRadcomEntity)(radcomEntity.collect.toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation mutables_usagepostpaidmutable" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val adhConfigJson =
        entityConfigPath("mutables_usagepostpaidmutable_entity_config.json")
      val adhConfig = ConfigParser.getConfig(CommonEnums.ENTITY, adhConfigJson)
      cleanUpDirs(adhConfig, partition)(spark)
      val adhEntity =
        removeNulls(
          BatchJob
            .process(adhConfig, partition, partition)(spark)
        ).sort("subscriberKey")
      val expectedAdhEntity =
        readExpected("mutables_usagepostpaidmutable", adhEntity.schema)
          .sort("subscriberKey")
          .collect
          .toList
      assertResult(expectedAdhEntity)(adhEntity.collect.toList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation mutables_usageprepaidevent" in withSparkSession {
    implicit spark: SparkSession =>
      val partition = "2019-01-16"
      val adhPrepaidConfigJson =
        entityConfigPath("mutables_usageprepaidevent_entity_config.json")
      val adhPrepaidConfig =
        ConfigParser.getConfig(CommonEnums.ENTITY, adhPrepaidConfigJson)
      cleanUpDirs(adhPrepaidConfig, partition)(spark)
      val adhPrepaidEntity =
        removeNulls(
          BatchJob
            .process(adhPrepaidConfig, partition, partition)(spark)
        ).sort("subscriberKey")
      val expectedADHPrepaidEntity = readExpected("mutables_usageprepaidevent", adhPrepaidEntity.schema)
      assertResult(expectedADHPrepaidEntity.sort("subscriberKey").collect().toList)(
        adhPrepaidEntity.sort("subscriberKey").collect().toList
      )
      FileSystemUtils.cleanUp()(spark)

  }
  "EndToEndTest" should "entity for unique" in withSparkSession { spark: SparkSession =>
    val partition = "2019-01-16"
    val uniqueEntityConfigJson =
      entityConfigPath("unique_new_entity_config.json")
    val uniqueEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, uniqueEntityConfigJson)
    cleanUpDirs(uniqueEntityConfig, partition)(spark)
    val actualuniqueEntity =
      BatchJob.process(uniqueEntityConfig, partition, partition)(spark)
    val expecteduniqueEntity =
      readExpected("unique_new", actualuniqueEntity.schema)(spark)
    assertResult(expecteduniqueEntity.collect().toList)(actualuniqueEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity for suspects" in withSparkSession { spark: SparkSession =>
    val partition = "2019-01-16"
    val suspectsEntityConfigJson =
      entityConfigPath("suspects_new_entity_config.json")
    val suspectsEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, suspectsEntityConfigJson)
    cleanUpDirs(suspectsEntityConfig, partition)(spark)
    val actualsuspectsEntity =
      BatchJob.process(suspectsEntityConfig, partition, partition)(spark)
    val expectedsuspectsEntity =
      readExpected("suspects_new", actualsuspectsEntity.schema)(spark)
    assertResult(expectedsuspectsEntity.collect().toList)(actualsuspectsEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity for confident" in withSparkSession { spark: SparkSession =>
    val partition = "2019-01-16"
    val confidentsEntityConfigJson =
      entityConfigPath("confidents_new_entity_config.json")
    val confidentsEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, confidentsEntityConfigJson)
    cleanUpDirs(confidentsEntityConfig, partition)(spark)
    val actualconfidentsEntity =
      BatchJob.process(confidentsEntityConfig, partition, partition)(spark)
    val expectedconfidentsEntity =
      readExpected("confidents_new", actualconfidentsEntity.schema)(spark)
    assertResult(expectedconfidentsEntity.collect().toList)(actualconfidentsEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity for cdr and sdr Multilevel partition for cdr_mo entity" in withSparkSession {
    spark: SparkSession =>
      val multiLevelPartition = "2019^1^16"
      val cdrEntityConfigJson = entityConfigPath("cdr_mo_entity_config.json")
      val cdrEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, cdrEntityConfigJson)
      cleanUpDirs(cdrEntityConfig, multiLevelPartition)(spark)
      val cdrActualEntity = BatchJob.process(cdrEntityConfig, multiLevelPartition, multiLevelPartition)(spark)
      val expectedcdrEntity =
        readExpected("cdr_mo", cdrActualEntity.schema)(spark)
      assertResult(expectedcdrEntity.collect().toList)(cdrActualEntity.collect().toList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity for cdr and sdr Multilevel partition for sdr_mo entity" in withSparkSession {
    spark: SparkSession =>
      val multiLevelPartition = "2019^1^16"
      val sdrEntityConfigJson = entityConfigPath("sdr_mo_entity_config.json")
      val sdrEntityConfig = ConfigParser.getConfig(CommonEnums.ENTITY, sdrEntityConfigJson)
      cleanUpDirs(sdrEntityConfig, multiLevelPartition)(spark)
      val sdrActualEntity = BatchJob.process(sdrEntityConfig, multiLevelPartition, multiLevelPartition)(spark)
      val expectedsdrEntity =
        readExpected("sdr_mo", sdrActualEntity.schema)(spark)
      assertResult(expectedsdrEntity.collect().toList)(sdrActualEntity.collect().toList)
      FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity for in_building_referenc" in withSparkSession { spark: SparkSession =>
    val partition = "2019-01-16"
    val inBuildingReferenceEntityConfigJson =
      entityConfigPath("in_building_solution_reference_entity_config.json")
    val inBuildingReferenceEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, inBuildingReferenceEntityConfigJson)
    cleanUpDirs(inBuildingReferenceEntityConfig, partition)(spark)
    val actualconfidentsEntity =
      BatchJob.process(inBuildingReferenceEntityConfig, partition, partition)(spark)
    val expectedconfidentsEntity =
      readExpected("in_building_solution_reference", actualconfidentsEntity.schema)(spark)
    assertResult(expectedconfidentsEntity.collect().toList)(actualconfidentsEntity.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity calculation industry_type" in withSparkSession { implicit spark: SparkSession =>
    val partition = "2019-01-16"
    val industryTypeJson =
      entityConfigPath(
        "industry_type_entity_config.json"
      )
    val industryTypeConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, industryTypeJson)
    cleanUpDirs(industryTypeConfig, partition)(spark)
    val actualIndustryType = BatchJob
      .process(industryTypeConfig, partition, partition)(spark)
      .sort("industryTypeCode")
    val expectedIndustryType = readExpected(
      "IndustryType",
      actualIndustryType.schema,
      Some(partition)
    )
    assertResult(expectedIndustryType.collect().toList)(actualIndustryType.collect().toList)
    FileSystemUtils.cleanUp()(spark)
  }

  "EndToEndTest" should "entity for amp_mds_journey_prepaid" in withSparkSession { spark: SparkSession =>
    val partition = "2018-12-16"
    val ampMdsJourneyPrepaidEntityConfigJson =
      entityConfigPath("amp_mds_journey_prepaid_entity_config.json")
    val ampMdsJourneyPrepaidEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, ampMdsJourneyPrepaidEntityConfigJson)
    cleanUpDirs(ampMdsJourneyPrepaidEntityConfig, partition)(spark)
    val actualAmpMdsJourneyPrepaidEntity =
      BatchJob.process(ampMdsJourneyPrepaidEntityConfig, partition, partition)(spark)
    val expectedAmpMdsJourneyPrepaidEntity =
      readExpected("amp_mds_journey_prepaid", actualAmpMdsJourneyPrepaidEntity.schema)(spark)
    assertResult(expectedAmpMdsJourneyPrepaidEntity.collect().toList)(actualAmpMdsJourneyPrepaidEntity.collect().toList)
  }

  "EndToEndTest" should "entity for amp_mds_journey_postpaid" in withSparkSession { spark: SparkSession =>
    val partition = "2018-12-16"
    val ampMdsJourneyPostpaidEntityConfigJson =
      entityConfigPath("amp_mds_journey_postpaid_entity_config.json")
    val ampMdsJourneyPostpaidEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, ampMdsJourneyPostpaidEntityConfigJson)
    cleanUpDirs(ampMdsJourneyPostpaidEntityConfig, partition)(spark)
    val actualAmpMdsJourneyPostpaidEntity =
      BatchJob.process(ampMdsJourneyPostpaidEntityConfig, partition, partition)(spark)
    val expectedAmpMdsJourneyPostpaidEntity =
      readExpected("amp_mds_journey_postpaid", actualAmpMdsJourneyPostpaidEntity.schema)(spark)
    assertResult(expectedAmpMdsJourneyPostpaidEntity.collect().toList)(
      actualAmpMdsJourneyPostpaidEntity.collect().toList
    )
  }

  "EndToEndTest" should "entity for amp_multisim_prepaid" in withSparkSession { spark: SparkSession =>
    val partition = "2018-12-16"
    val ampMultisimPrepaidEntityConfigJson =
      entityConfigPath("amp_multisim_prepaid_entity_config.json")
    val ampMultisimPrepaidEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, ampMultisimPrepaidEntityConfigJson)
    cleanUpDirs(ampMultisimPrepaidEntityConfig, partition)(spark)
    val ampMultisimPrepaidEntity =
      BatchJob.process(ampMultisimPrepaidEntityConfig, partition, partition)(spark)
    val expectedAmpMultisimPrepaidEntity =
      readExpected("amp_multisim_prepaid", ampMultisimPrepaidEntity.schema)(spark)
    assertResult(expectedAmpMultisimPrepaidEntity.collect().toList)(ampMultisimPrepaidEntity.collect().toList)
  }

  "EndToEndTest" should "entity for amp_affluence_lifestage_postpaid" in withSparkSession { spark: SparkSession =>
    val partition = "2018-12-16"
    val ampAffluenceLifestagePostpaidEntityConfigJson =
      entityConfigPath("amp_affluence_lifestage_postpaid_entity_config.json")
    val ampAffluenceLifestagePostpaidEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, ampAffluenceLifestagePostpaidEntityConfigJson)
    cleanUpDirs(ampAffluenceLifestagePostpaidEntityConfig, partition)(spark)
    val ampAffluenceLifestagePostpaidEntity =
      BatchJob.process(ampAffluenceLifestagePostpaidEntityConfig, partition, partition)(spark)
    val expectedAmpAffluenceLifestagePostpaidEntity =
      readExpected("amp_affluence_lifestage_postpaid", ampAffluenceLifestagePostpaidEntity.schema)(spark)
    assertResult(expectedAmpAffluenceLifestagePostpaidEntity.collect().toList)(
      ampAffluenceLifestagePostpaidEntity.collect().toList
    )
  }

  "EndToEndTest" should "entity for amp_affluence_lifestage_prepaid" in withSparkSession { spark: SparkSession =>
    val partition = "2018-12-16"
    val ampAffluenceLifestagePrepaidEntityConfigJson =
      entityConfigPath("amp_affluence_lifestage_prepaid_entity_config.json")
    val ampAffluenceLifestagePrepaidEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, ampAffluenceLifestagePrepaidEntityConfigJson)
    cleanUpDirs(ampAffluenceLifestagePrepaidEntityConfig, partition)(spark)
    val ampAffluenceLifestagePrepaidEntity =
      BatchJob.process(ampAffluenceLifestagePrepaidEntityConfig, partition, partition)(spark)
    val expectedAmpAffluenceLifestagePrepaidEntity =
      readExpected("amp_affluence_lifestage_prepaid", ampAffluenceLifestagePrepaidEntity.schema)(spark)
    assertResult(expectedAmpAffluenceLifestagePrepaidEntity.collect().toList)(
      ampAffluenceLifestagePrepaidEntity.collect().toList
    )
  }

  "EndToEndTest" should "complete profile calculation" in withSparkSession { spark: SparkSession =>
    val partition = "2019-01-16"
    val profileConfigJson = profileConfigPath("profile_config.json")
    val profileConfig =
      ConfigParser.getConfig(CommonEnums.PROFILE, profileConfigJson)
    cleanUpDirs(profileConfig, partition)(spark)
    FileSystemUtils.cleanUp()(spark)
    val actualProfile =
      removeNulls(BatchJob.process(profileConfig, partition, partition, true)(spark))
    val expectedProfile =
      readExpected("GlobeProfile", actualProfile.schema)(spark)
        .sort("subscriber_id")
        .collect()
        .toList
    assertResult(expectedProfile)(
      actualProfile.sort("subscriber_id").collect().toList
    )

  }

  "EndToEndTest" should "complete profile calculation with late subscriber" in withSparkSession { spark: SparkSession =>
    val partition = "2019-01-17"
    val profileConfigJson = profileConfigPath("profile_config.json")
    val profileConfig =
      ConfigParser.getConfig(CommonEnums.PROFILE, profileConfigJson)
    cleanUpDirs(profileConfig, partition)(spark)
    FileSystemUtils.cleanUp()(spark)
    val actualProfile =
      removeNulls(BatchJob.process(profileConfig, partition, partition, true)(spark))
    val expectedProfile =
      readExpected("GlobeProfile", actualProfile.schema)(spark)
        .sort("subscriber_id")
        .collect()
        .toList
    assertResult(expectedProfile)(
      actualProfile.sort("subscriber_id").collect().toList
    )
  }
}
