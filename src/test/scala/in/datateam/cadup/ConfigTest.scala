package in.datateam.cadup

import java.io.File

import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger
import org.scalatest.FlatSpec

import in.datateam.bifrost.processor.PersistProcessor
import in.datateam.harness.SparkTest
import in.datateam.utils.InvalidConfigException
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums

/* *
 * Created by Niraj(niraj.kumar.das@thedatateam.in).
 * */
class ConfigTest extends FlatSpec with SparkTest {

  val logger: Logger = Logger.getLogger(getClass.getName)

  "ConfigTest" should "validate dev resource configs" in withSparkSession { implicit spark: SparkSession =>
    val configDir = "src/main/resources/configs/dev_configs"
    val entityConfigPath: String = s"${configDir}/entity_configs/"
    val extractConfigPaths: Array[String] = Array(
      s"${configDir}/extract_configs/daily_extracts",
      s"${configDir}/extract_configs/monthly_extracts"
    )
    val profileConfigPath: String = s"${configDir}/profile_configs/"
    runTest(entityConfigPath, profileConfigPath, extractConfigPaths)
  }

  "ConfigTest" should "validate prod resource configs" in withSparkSession { implicit spark: SparkSession =>
    val configDir = "src/main/resources/configs/prod_configs"
    val entityConfigPath: String = s"${configDir}/entity_configs/"
    val extractConfigPaths: Array[String] = Array(
      s"${configDir}/extract_configs/daily_extracts",
      s"${configDir}/extract_configs/monthly_extracts"
    )
    val profileConfigPath: String = s"${configDir}/profile_configs/"
    runTest(entityConfigPath, profileConfigPath, extractConfigPaths)
  }

  "ConfigTest" should "validate uat resource configs" in withSparkSession { implicit spark: SparkSession =>
    val configDir = "src/main/resources/configs/uat_configs"
    val entityConfigPath: String = s"${configDir}/entity_configs/"
    val extractConfigPaths: Array[String] = Array(
      s"${configDir}/extract_configs/daily_extracts",
      s"${configDir}/extract_configs/monthly_extracts"
    )
    val profileConfigPath: String = s"${configDir}/profile_configs/"
    runTest(entityConfigPath, profileConfigPath, extractConfigPaths)
  }

  "ConfigTest" should "validate test resource configs" in withSparkSession { implicit spark: SparkSession =>
    val configDir = "src/test/resources/configs"
    val entityConfigPath: String = s"${configDir}/entity_configs/"
    val profileConfigPath: String = s"${configDir}/profile_configs/"
    runTest(entityConfigPath, profileConfigPath)
  }

  private[this] def runTest(
      entityConfigPath: String,
      profileConfigPath: String,
      extractConfigPaths: Array[String] = Array()
    )(
      implicit spark: SparkSession
    ): Unit = {
    val entityConfigRootDir = new File(entityConfigPath)
    val configSchemaMap = entityConfigRootDir.listFiles.map { entityConfigFile =>
      logger.info(s"Validating entity config: ${entityConfigFile.getName}")
      val config = ConfigParser.getConfig(
        CommonEnums.ENTITY,
        entityConfigFile.getAbsolutePath
      )
      PersistProcessor.validateConfig(config)(spark)
      (config.sourceDetails.head.entityName, config)
    }.toMap
    val profileConfigRootDir = new File(profileConfigPath)
    val profileColumns = profileConfigRootDir.listFiles.map { profileConfigFile =>
      logger.info(
        s"Validating profile config: ${profileConfigFile.getName}"
      )
      val config = ConfigParser.getConfig(
        CommonEnums.PROFILE,
        profileConfigFile.getAbsolutePath
      )
      PersistProcessor.validateConfig(config)(spark)
      config.sourceDetails.foreach { sourceDetail =>
        if (sourceDetail.joinColumn.isDefined) {
          val entityConfig = configSchemaMap.get(sourceDetail.entityName)
          if (entityConfig.isEmpty) {
            throw InvalidConfigException(
              s"${sourceDetail.entityName} Entity not found in ${entityConfigPath}."
            )
          }
          entityConfig.get.checkColumnExistTargetSchema(
            sourceDetail.joinColumn.get
          )
        }
      }
      config.columnMapping.foreach { columnMappingUnit =>
        configSchemaMap(columnMappingUnit.sourceEntity)
          .checkColumnExistTargetSchema(columnMappingUnit.sourceColumn)
      }
      config.transformationsDetails.foreach { transformationUnit =>
        transformationUnit.sourceColumns.foreach { sourceColumn =>
          if (sourceColumn.entityName != CommonEnums.PROFILE_MASTER) {
            configSchemaMap(sourceColumn.entityName)
              .checkColumnExistTargetSchema(sourceColumn.columnName)
          } else {
            config.checkColumnExistAsAttributeArgument(
              sourceColumn.entityName
            )
          }
        }
      }
      config.getAllColumns
    }.head

    extractConfigPaths.foreach { extractConfigPath =>
      val extractConfigRootDir = new File(extractConfigPath)
      extractConfigRootDir.listFiles.foreach { extractConfigFile =>
        if (extractConfigFile.isFile && extractConfigFile.getName.contains("dpa_redshift_extract")) {
          logger.info(s"Validating extract config: ${extractConfigFile.getName}")
          val config = ConfigParser.getConfig(
            CommonEnums.ENTITY,
            extractConfigFile.getAbsolutePath
          )
          PersistProcessor.validateConfig(config)(spark)
          val valid = config.getAllColumns.map { columnName =>
            if (!profileColumns.contains(columnName)) {
              logger.info(s"Column ${columnName} not found in profile config")
              false
            } else {
              true
            }
          }.reduce(_ && _)
          assertResult(true)(valid)
        }
      }
    }
  }
}
