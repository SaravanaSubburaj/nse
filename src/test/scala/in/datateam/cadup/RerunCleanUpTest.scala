package in.datateam.cadup

import org.apache.spark.sql.SparkSession

import org.apache.log4j.Logger
import org.scalatest.FlatSpec

import in.datateam.harness.SparkTest
import in.datateam.utils.configparser.ConfigParser
import in.datateam.utils.enums.CommonEnums

/* *
 * Created by Niraj(niraj.kumar.das@thedatateam.in).
 * */
class RerunCleanUpTest extends FlatSpec with SparkTest {

  val logger: Logger = Logger.getLogger(getClass.getName)

  "RerunCleanUpTest" should "return clean up paths for rerun" in withSparkSession { spark: SparkSession =>
    val configDir = "src/test/resources/configs"
    val entityConfigPath: String => String = s"${configDir}/entity_configs/" + _
    val profileConfigPath: String => String = s"${configDir}/profile_configs/" + _

    val partitionValue = "2018-12-07"
    val subscriberEntityConfigJson =
      entityConfigPath("globe_subscriber_entity_config.json")
    val subscriberEntityConfig =
      ConfigParser.getConfig(CommonEnums.ENTITY, subscriberEntityConfigJson)
    val actualCleanUpPathsEntity =
      BatchJob.getCleanUpPaths(subscriberEntityConfig, partitionValue)(spark)
    val expectedCleanUpPathsEntity: Map[String, Array[String]] = List(
      CommonEnums.VALID_RECORDS ->
      Array("target/EntityStore/GlobeSubscriber/date=2018-12-07"),
      CommonEnums.INVALID_RECORDS ->
      Array("target/InvalidRecords/date=2018-12-07/entity=GlobeSubscriber"),
      CommonEnums.AUDIT_RECORDS ->
      Array("target/Audit/date=2018-12-07/entity=GlobeSubscriber")
    ).toMap

    expectedCleanUpPathsEntity.keys.foreach { recordsType =>
      val left = expectedCleanUpPathsEntity(recordsType)
      val right = actualCleanUpPathsEntity(recordsType)
      assertResult(left)(right)
    }

    val profileConfigJson = profileConfigPath("profile_config.json")
    val profileConfig =
      ConfigParser.getConfig(CommonEnums.PROFILE, profileConfigJson)
    val actualCleanUpPathsProfile =
      BatchJob.getCleanUpPaths(profileConfig, partitionValue)(spark)
    val expectedCleanUpPathsProfile = List(
      CommonEnums.VALID_RECORDS ->
      Array("target/GlobeProfile/date=2018-12-07"),
      CommonEnums.AUDIT_RECORDS ->
      Array("target/Audit/date=2018-12-07/entity=profile_master")
    ).toMap

    expectedCleanUpPathsProfile.keys.foreach { recordsType =>
      val left = expectedCleanUpPathsProfile(recordsType)
      val right = actualCleanUpPathsProfile(recordsType)
      assertResult(left)(right)
    }

  }
}
