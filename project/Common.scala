import Dependencies.awsDependencies
import Dependencies.hadoopDependencies
import Dependencies.overrideDependencies
import Dependencies.scalaTestDependencies
import Dependencies.scoptDependency
import Dependencies.sparkDependencies
import net.virtualvoid.sbt.graph.DependencyGraphPlugin.autoImport.filterScalaLibrary
import org.scalafmt.sbt.ScalafmtPlugin.autoImport._
import org.scalastyle.sbt.ScalastylePlugin.autoImport._
import sbt.Keys._
import sbt.Keys.test
import sbt._
import sbtassembly.AssemblyKeys.assembly
import sbtassembly.AssemblyPlugin.autoImport.Assembly
import sbtassembly.AssemblyPlugin.autoImport.MergeStrategy
import sbtassembly.AssemblyPlugin.autoImport.ShadeRule
import sbtassembly.AssemblyPlugin.autoImport.assemblyMergeStrategy
import sbtassembly.AssemblyPlugin.autoImport.assemblyShadeRules
import sbtassembly.PathList
import xerial.sbt.pack.PackPlugin.autoImport.packExcludeJars
import xerial.sbt.pack.PackPlugin.autoImport.packExcludeLibJars
import xerial.sbt.pack.PackPlugin.autoImport.packGenerateMakefile
import xerial.sbt.pack.PackPlugin.autoImport.packGenerateWindowsBatFile
import xerial.sbt.pack.PackPlugin.autoImport.packMain
import xerial.sbt.pack.PackPlugin.autoImport.packResourceDir

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object Common extends AutoPlugin {

  lazy val compileToTest = "compile->test"
  lazy val testToTest = "test->test"
  lazy val compileToCompileAndTestToTest = "compile->compile;test->test"

  lazy val testScalastyle: TaskKey[Unit] = taskKey[Unit]("testScalastyle")
  lazy val formatAll = taskKey[Unit](
    "Format all the source code which includes src, test, and build files"
  )
  lazy val checkFormat = taskKey[Unit](
    "Check all the source code which includes src, test, and build files"
  )

  val settings: Seq[Def.Setting[_]] = Seq(
    scalaVersion := "2.11.8",
    javacOptions ++= Seq("-source", "1.8", "-target", "1.8"),
    scalacOptions ++= Seq("-deprecation", "-unchecked"),
    parallelExecution in test := false,
    testScalastyle            := scalastyle.in(Test).toTask("").value,
    (compile in Compile)      := ((compile in Compile) dependsOn (formatAll, testScalastyle)).value,
    //    (test in Test)            := ((test in Test) dependsOn (formatAll, testScalastyle)).value,
    libraryDependencies ++= hadoopDependencies ++ sparkDependencies ++ scalaTestDependencies ++ awsDependencies ++ scoptDependency,
    dependencyOverrides ++= overrideDependencies,
    resolvers ++= Seq(
        Resolver.defaultLocal,
        Resolver.mavenLocal,
        Resolver.mavenCentral,
        "Artima Maven Repository" at "http://repo.artima.com/releases",
        "Sbt plugins" at "https://dl.bintray.com/sbt/sbt-plugin-releases"
      ),
    testScalastyle := {
      (scalastyle in Compile).toTask("").value
    },
    formatAll := {
      (scalafmtIncremental in Compile).value
      (scalafmt in Compile).value
      (scalafmt in Test).value
      (scalafmtSbt in Compile).value
    },
    checkFormat := {
      (scalafmtCheck in Compile).value
      (scalafmtCheck in Test).value
      (scalafmtSbtCheck in Compile).value
    }
  )

  val assemblySettings: Seq[Def.Setting[_]] = Seq(
    test in assembly   := {},
    filterScalaLibrary := true,
    assemblyShadeRules in assembly := Seq(
        ShadeRule.rename("com.fasterxml.**" -> "my.shaded.@0").inAll,
        ShadeRule.rename("javax.ws.**"      -> "my.shaded.@0").inAll
      ),
    assemblyMergeStrategy in assembly := {
      case x if Assembly.isConfigFile(x) =>
        MergeStrategy.concat
      case PathList(ps @ _*) if Assembly.isReadme(ps.last) || Assembly.isLicenseFile(ps.last) =>
        MergeStrategy.rename
      case PathList("META-INF", xs @ _*) =>
        (xs map {
          _.toLowerCase
        }) match {
          case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) =>
            MergeStrategy.discard
          case ps @ (x :: xs) if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") =>
            MergeStrategy.discard
          case "plexus" :: xs =>
            MergeStrategy.discard
          case "services" :: xs =>
            MergeStrategy.filterDistinctLines
          case ("spring.schemas" :: Nil) | ("spring.handlers" :: Nil) =>
            MergeStrategy.filterDistinctLines
          case _ => MergeStrategy.first
        }
      case _ => MergeStrategy.first
    }
  )

  val resourceDirectory = "src/main/resources"
  val configDirectory = "src/main/resources/configs"

  val packSettings: Seq[Def.Setting[_]] = Seq(
    packMain                   := Map(),
    packGenerateWindowsBatFile := false,
    packGenerateMakefile       := false,
    packExcludeLibJars += ("harness"),
//    FIXME:Update exclusion logic.
    packExcludeJars                                                                  := Seq("scala*"),
    packResourceDir += (baseDirectory.value / s"$resourceDirectory/process_cadup.sh" -> "bin/process_cadup.sh"),
    packResourceDir += (baseDirectory.value / s"$configDirectory/spark_configs"      -> "configs/spark_configs"),
    packResourceDir += (baseDirectory.value / s"$configDirectory/dev_configs"        -> "configs/dev_configs"),
    packResourceDir += (baseDirectory.value / s"$configDirectory/prod_configs"       -> "configs/prod_configs"),
    packResourceDir += (baseDirectory.value / s"$configDirectory/uat_configs"        -> "configs/uat_configs"),
    packResourceDir += (baseDirectory.value / s"$resourceDirectory/orchestrator"     -> "orchestrator")
  )
}
