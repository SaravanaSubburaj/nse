import sbt.ModuleID
import sbt._

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
object Dependencies {

  lazy val scalaTestVersion = "3.0.5"
  lazy val sparkVersion = "2.2.1"
  lazy val hadoopVersion = "2.7.1"
  lazy val sprayJsonVersion = "1.3.2"
  lazy val amazonEmrVersion = "4.7.0"
  lazy val sparkRedshiftVersion = "2.0.1"
  lazy val amazonSDKVersion = "1.11.160"

  lazy val sparkGroupID: String = "org.apache.spark"
  lazy val servletGroupID: String = "javax.servlet"
  lazy val hadoopGroupID: String = "org.apache.hadoop"
  lazy val amazonEmrGroupID: String = "com.amazon.emr"
  lazy val databricksGroupID: String = "com.databricks"
  lazy val amazonSDKGroupID: String = "com.amazonaws"
  lazy val scoptGroupID: String = "com.github.scopt"

  lazy val scalaTestDependencies: Seq[ModuleID] = Seq(
    "org.scalactic" %% "scalactic" % scalaTestVersion % Test,
    "org.scalatest" %% "scalatest" % scalaTestVersion % Test
  )

  lazy val sparkDependencies: Seq[ModuleID] = Seq(
    sparkGroupID %% "spark-core" % sparkVersion % Provided,
    sparkGroupID %% "spark-sql"  % sparkVersion % Provided
  )

  lazy val hadoopDependencies: Seq[ModuleID] = Seq(
    hadoopGroupID % "hadoop-common" % hadoopVersion % Provided,
    hadoopGroupID % "hadoop-hdfs"   % hadoopVersion % Provided,
    hadoopGroupID % "hadoop-aws"    % hadoopVersion % Provided
  )

  lazy val awsDependencies: Seq[ModuleID] = Seq(
    amazonEmrGroupID  % "emr-dynamodb-hadoop" % amazonEmrVersion     % Provided,
    databricksGroupID %% "spark-redshift"     % sparkRedshiftVersion % Provided
  )

  lazy val jsonDependencies: Seq[ModuleID] = Seq(
    "io.spray" %% "spray-json" % sprayJsonVersion
  )

  lazy val scoptDependency: Seq[ModuleID] = Seq(scoptGroupID %% "scopt" % "4.0.0-RC2")

  lazy val overrideDependencies: Seq[ModuleID] = Seq(
    "com.fasterxml.jackson.core"       % "jackson-core"              % "2.8.7",
    "com.fasterxml.jackson.core"       % "jackson-databind"          % "2.8.7",
    "com.fasterxml.jackson.module"     % "jackson-module-scala_2.11" % "2.8.7",
    "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml"   % "2.8.7",
    amazonSDKGroupID                   % "aws-java-sdk"              % amazonSDKVersion
  )
}
