package in.datateam.fury

import scala.collection.mutable.ListBuffer

import org.apache.spark.sql.Column
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.concat_ws
import org.apache.spark.sql.functions.lit

import org.apache.log4j.Logger

import in.datateam.utils.DuplicateRecordsException
import in.datateam.utils.configparser.Config
import in.datateam.utils.enums.AuditEnums
import in.datateam.utils.enums.CommonEnums

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
class AuditAndLineageManager(
    var lineage: DataFrame,
    var audits: DataFrame,
    var preAudits: ListBuffer[PreAuditColumnsCaseClass] = new ListBuffer[PreAuditColumnsCaseClass]) {
  //  TODO: try removing vars
  val logger: Logger = Logger.getLogger(getClass.getName)
  val inlineAuditFlag: Boolean = false

  def initialize()(implicit spark: SparkSession): Unit = {
    val lineageSchema = Encoders.product[LineageColumnsCaseClass].schema
    val auditsSchema = Encoders.product[AuditColumnsCaseClass].schema
    lineage = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], lineageSchema)
    audits = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], auditsSchema)
  }

  val getEntityName = (config: Config) =>
    config.configType match {
      case CommonEnums.ENTITY => config.sourceDetails.head.entityName
      case CommonEnums.PROFILE => CommonEnums.PROFILE_MASTER
    }

  def process(
      preDF: DataFrame,
      postDF: DataFrame,
      processName: String,
      columnName: String,
      config: Config,
      partitionValue: String,
      lineageColumns: String*
    )(
      implicit spark: SparkSession
    ): Unit = {
    val entityName: String = getEntityName(config)
    val lineageCols = lineageColumns.map(columnName => col(columnName))
    implicit val postDFEncoder = RowEncoder(postDF.schema)
    val jobId = spark.sparkContext.applicationId

    logger.info(s"Calculating audit inline: ${inlineAuditFlag}")

    populateAuditDF(
      preDF,
      postDF,
      lineageCols,
      partitionValue,
      jobId,
      entityName,
      processName,
      columnName
    )
    populateLineageDF(
      preDF,
      postDF,
      lineageCols,
      partitionValue,
      jobId,
      entityName,
      processName,
      columnName
    )

  }

  def populateAuditDF(
      preDF: DataFrame,
      postDF: DataFrame,
      lineageCols: Seq[Column],
      partitionValue: String,
      jobId: String,
      entityName: String,
      processName: String,
      columnName: String
    )(
      implicit spark: SparkSession
    ): Unit = {
    if (!inlineAuditFlag) {
      val preAuditColumnsCaseClass = PreAuditColumnsCaseClass(
        partitionValue,
        jobId,
        entityName,
        entityName,
        processName,
        columnName,
        preDF.select(lineageCols: _*),
        postDF.select(lineageCols: _*)
      )
      preAudits += preAuditColumnsCaseClass
    } else {
      import spark.implicits._
      val recordsOut = postDF.select(lineageCols: _*).count()
      val recordsIn = preDF.select(lineageCols: _*).count()
      logger.info(
        s"Column Name: ${columnName} Function Name: ${processName} Records in: ${recordsIn} Records out: ${recordsOut}"
      )
      if (recordsIn < recordsOut && processName != AuditEnums.COLUMN_MAPPING_FUNCTION_NAME) {
        val message =
          s"Duplicates detected for Column Name: ${columnName} Function Name: ${processName} Records in: ${recordsIn} Records out: ${recordsOut}"
        logger.warn(message)
        if (AuditEnums.EXIT_ON_DUPLICATE_DETECTION) {
          throw DuplicateRecordsException(message)
        }
      }
      val partAuditDF = Seq(
        AuditColumnsCaseClass(
          partitionValue,
          jobId,
          entityName,
          entityName,
          processName,
          columnName,
          recordsIn,
          recordsOut
        )
      ).toDF
      audits = audits.union(partAuditDF)
    }
  }

  def populateLineageDF(
      preDF: DataFrame,
      postDF: DataFrame,
      lineageCols: Seq[Column],
      partitionValue: String,
      jobId: String,
      entityName: String,
      processName: String,
      columnName: String
    )(
      implicit spark: SparkSession
    ): Unit = {
    val records = postDF
      .select(
        concat_ws(CommonEnums.DATA_DELIMITER, lineageCols: _*).as(AuditEnums.RECORDS)
      )
      .withColumn(AuditEnums.DATE_PARTITION, lit(partitionValue))
      .withColumn(AuditEnums.JOB_ID, lit(jobId))
      .withColumn(AuditEnums.ENTITY_NAME, lit(entityName))
      .withColumn(AuditEnums.ENTITY, lit(entityName))
      .withColumn(AuditEnums.PROCESS_NAME, lit(processName))
      .withColumn(AuditEnums.COLUMN_NAME, lit(columnName))
    val lineageRecords = records.select(
      AuditEnums.DATE_PARTITION,
      AuditEnums.JOB_ID,
      AuditEnums.ENTITY_NAME,
      AuditEnums.ENTITY,
      AuditEnums.PROCESS_NAME,
      AuditEnums.COLUMN_NAME,
      AuditEnums.RECORDS
    )
    lineage = lineage.union(lineageRecords)
  }

  def calculateAuditDF(config: Config, calculateAuditFlag: Boolean)(implicit spark: SparkSession): Unit = {
    if (!calculateAuditFlag) {
      import spark.implicits._
      audits = preAudits.map { row =>
        val recordsOut = row.recordsOutDF.count()
        val recordsIn = row.recordsInDF.count()
        logger.info(
          s"Column Name: ${row.columnName} Function Name: ${row.processName} Records in: ${recordsIn} Records out: ${recordsOut}"
        )
        AuditColumnsCaseClass(
          row.date,
          row.jobId,
          row.entityName,
          row.entity,
          row.processName,
          row.columnName,
          recordsIn,
          recordsOut
        )
      }.toDF
    }
  }

  def getAuditDF(config: Config)(implicit spark: SparkSession): DataFrame = {
    calculateAuditDF(config, inlineAuditFlag)
    audits.coalesce(1)
  }

  def getLineageDF(config: Config)(implicit spark: SparkSession): DataFrame = {
    lineage
  }
}
