package in.datateam.fury

import org.apache.spark.sql.DataFrame

/**
  * Created by Niraj(niraj.kumar.das@thedatateam.in).
  */
case class AuditColumnsCaseClass(
    date: String,
    jobId: String,
    entityName: String,
    entity: String,
    processName: String,
    columnName: String,
    recordsIn: Long,
    recordsOut: Long)

case class PreAuditColumnsCaseClass(
    date: String,
    jobId: String,
    entityName: String,
    entity: String,
    processName: String,
    columnName: String,
    recordsInDF: DataFrame,
    recordsOutDF: DataFrame)

case class LineageColumnsCaseClass(
    date: String,
    jobId: String,
    entityName: String,
    entity: String,
    processName: String,
    columnName: String,
    records: String)
